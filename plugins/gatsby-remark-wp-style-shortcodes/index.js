const visit = require('unist-util-visit');

function findClosingShortcodeIndex(shortcodeName, shortcodeIndex, parent) {
  const childrenAfterStartingShortcode = parent.children.slice(
    shortcodeIndex + 1
  );
  const relativeClosingIndex = childrenAfterStartingShortcode.findIndex(
    (node) => node.identifier === `/${shortcodeName}`
  );
  const absoluteClosingIndex =
    relativeClosingIndex === -1
      ? -1
      : relativeClosingIndex + shortcodeIndex + 1;
  return absoluteClosingIndex;
}
function findShortcodeContent(shortcodeIndex, closingShortcodeIndex, parent) {
  if (closingShortcodeIndex === -1) {
    return [];
  }

  const contentNodes = parent.children.slice(
    shortcodeIndex + 1,
    closingShortcodeIndex
  );
  return contentNodes;
}

module.exports = ({ markdownAST }, pluginOptions) => {
  if (!pluginOptions.replacers) {
    return;
  }

  visit(markdownAST, 'linkReference', (node, index, parent) => {
    const shortcodeSegments = node.identifier.split(' ');
    const shortcodeName = shortcodeSegments[0];
    const replacer =
      pluginOptions.replacers[shortcodeName] || pluginOptions.replacers['*'];
    if (!replacer) {
      throw new Error(
        `No replacer function for the shortcode "${shortcodeName}". Make sure you have defined one in options.replacers with the key "${shortcodeName}" or a catch-all replacer with the key "*" in the plugin's options in your gatsby-config.js.`
      );
    }

    const shortcodeAttributes = shortcodeSegments
      .slice(1)
      .reduce((accumulator, attribute) => {
        const [attributeName, attributeValue] = attribute.split('=');
        accumulator[attributeName] = attributeValue || true;
        return accumulator;
      }, {});

    const closingShortcodeIndex = findClosingShortcodeIndex(
      shortcodeName,
      index,
      parent
    );
    const hasClosingShortcode = closingShortcodeIndex !== -1;
    const shortcodeContentNodes = findShortcodeContent(
      index,
      closingShortcodeIndex,
      parent
    );

    const replacement = replacer(shortcodeAttributes, shortcodeContentNodes);

    const newNode = {
      type: 'html',
      children: undefined,
      value: replacement,
    };
    if (hasClosingShortcode) {
      const afterShortcodeIndex = closingShortcodeIndex + 1;
      parent.children = [
        ...parent.children.slice(0, index),
        newNode,
        ...parent.children.slice(afterShortcodeIndex),
      ];
    } else {
      parent.children[index] = newNode;
    }
  });
  return markdownAST;
};

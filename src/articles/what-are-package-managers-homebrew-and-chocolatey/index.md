---
title: 'What are package managers (Homebrew and Chocolatey)?'
date: '2018-11-16'
tags:
  - 'dev-tools'
  - 'macos'
  - 'software'
  - 'tools'
coverImage: 'images/packages.jpg'
description: 'Package managers make it easy to install, update, and manage software. Learn why theyʼre useful for developers and how to use them.'
---

**Package managers** make it easy to install, update, and manage software. Think about the Mac and iOS app stores, the Windows app store, or the various Android app stores. The differences are that package managers are primarily for command line apps (although most of the modern ones will also handle GUI apps) or for software libraries (think Node's NPM and Python's pip), and the apps are generally free and open-source (although closed-source apps also show up in package managers).

In this article, I'll be focused on the benefits and usage of the stack-agnostic package managers [Homebrew](https://brew.sh/) and [Chocolatey](https://chocolatey.org/). From here on out, when I refer to "package managers," I'm referring to those two.

![Packages stacked up](images/packages.jpg)

## Why should web developers care about package managers?

As a web developer, you'll find yourself installing lots of different command line apps. Each one will have a unique set of installation instructions and directions for upgrading. Each one of these slows down your workflow as you're having to read documentation and go through some arcane installation process.

## Example: Installing the AWS CLI

### Recommended Instructions

Here's a great example. What if you wanted to host a project on AWS? You might need to install the AWS CLI so you can automate deployments of your app.

In order to do that, you'll have to first install Python if you don't already have it on your system. You'll need Python 2.6.5+ or 3.3+ with pip. Then, you'll run this command to install the AWS CLI with pip:

```bash
pip install awscli --upgrade --user
```

Once that's done, you'll need to add the installation location to your PATH so that you can run it from the command line.

### Homebrew/Chocolatey Installation

On macOS, you can use [Homebrew](https://brew.sh/) to install the AWS CLI using this:

```bash
brew install awscli
```

On Windows, you can use [Chocolatey](https://chocolatey.org/) to install the AWS CLI using this:

```bash
choco install awscli
```

That's all you need to do. The newly installed program is automatically added to the PATH.

## The Advantages of a Package Manager

This is a slightly strange comparison because the recommended installation instructions for the AWS CLI also use a package manager: pip is the Python package manager.  (That's because the CLI is written in Python.) The nice thing about using one of the stack-agnostic package managers is that **they smooth out the unique installation processes**.

You can see that, even across operating systems, the installation commands are **simple and similar**. I just swap in the name for the appropriate package manager, and I'm done. Maybe for a different project, I decide I want to try to host on Heroku instead of AWS. They recommend I download an installer and run it. I could do that, or I could run a simple command with Homebrew or Chocolatey. A package manager **normalizes the processes for setting up new tools**.

The same holds true for upgrading software. Each tool has its own set of instructions for upgrading. Upgrading is part of the **management** the package manager provides. You can upgrade each package one-by-one or you can update all installed packages with a single command saving you tons of time over updating them piecemeal.

## Getting Started with Package Managers

If you're on a Mac, you'll install Homebrew by pasting this into your terminal:

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Windows users will install Chocolatey instead. It's not quite as straightforward but still beats installing each tool you need individually. If you're at the command prompt, you'll use this gnarly command:

```
@"%SystemRoot%System32WindowsPowerShellv1.0powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%chocolateybin"
```

From PowerShell, you'll use this instead:

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

For more information about dealing with problems you might run into when installing Chocolatey, see [their installation documentation](https://chocolatey.org/install).

_You want to become a web developer, but you keep hitting barriers. I want to help. 👇_

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step]

## Using Package Managers

(I'll show subsequent commands in Homebrew, but the commands are identical in Chocolatey. You'll just start the command with `choco` instead of `brew`.)

Now, you're ready to start installing packages. When you learn about a new command line tool you need to install, check first to see if it's in your package manager's repo. You can do this by using the `search` command.

```bash
brew search aws
```

The package manager will spit out a list of matches. Browse for the right one and then **install it** using the exact name in the results.

```bash
brew install awscli
```

If you get multiple results that look like any of them could be what you want, **there's no harm in doing a quick web search** to find out how to install the tool you're looking for. If you _don't_ find a match, this might also reveal a new repository you can add to your package manager that would allow you to install the app.

Later, when you're ready to update one of your packages, you'll use the upgrade command.

```bash
brew upgrade awscli
```

## You're Ready to Install Almost Any Tool!

Package managers have many more tools you can explore, but these are the basics to get you up and installing just about anything with minimal effort. Your package manager will make keeping tools up to date and testing out new ones **much easier and much less of a drag** on your development workflow.

---
title: 'Get a Web Development Job Without Experience?'
date: '2020-06-06'
tags:
  - 'career'
  - 'frameworks'
  - 'languages'
  - 'technologies'
coverImage: 'images/kid-coder.jpg'
description: 'Jobs want experience. You need a job to get experience. How can you get the chicken without the egg (or vice versa)? I did it, and Iʼll teach you how.'
---

https://youtu.be/O-p_XIpeERo

## Video Notes

This is one of the questions I'm asked frequently. How can I get a web development job without experience. Let me go ahead and spoil the video for you. Your options are:

1. 🤝 Know someone who can give you a job.
2. 🎓 Graduate from a prestigious engineering program.

Chances are, neither of these are possible for you. That means your dreams of being a web developmer are over. Unless…

Unless you could go out and get experience without having the job. Fortunately, you absolutely can. In this video, I'll go over a few ways you can start getting experience as a web developer BEFORE you have the job.

<!-- If you're looking for a fun way to start getting some experience, **try this free project starter kit**. It will give you all you need to kickstart Timeless, the social network which allows only a single post per year. Share your email to get that starter kit along with other resources to help you in your career transition to pro web developer! 👇

[thrive_leads id='3587'] -->

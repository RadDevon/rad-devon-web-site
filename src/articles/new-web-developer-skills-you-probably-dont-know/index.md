---
title: "New Web Developer Skills You Probably Don't Have"
date: '2020-06-12'
tags:
  - 'career'
  - 'freelancing'
  - 'git'
  - 'version-control'
coverImage: 'images/coding-zombie.jpg'
description: 'You know about a lot of the skills youʼre going to need to be a web developer: HTML, CSS, declaring variables, writing functions, object-oriented programming, and more. In this video, Iʼm focused on the skills you might not know you need.'
---

https://youtu.be/Tl1r1eUOWG8

## Video Notes

You know about a lot of the skills you're going to need to be a web developer. You'll want to know how to write HTML and CSS, declare variables, write functions, use object-oriented programming, and so on. In this video, I'm focusing on **skills you may not have** – skills bootcamps and university programs don't often teach and that you wouldn't be likely to seek out on your own if you're self-taught.

What did I miss? Please let me know in the comments!

<!-- The best way to put your skills to a practical test and get started in web development (unless you have a way to conjure up 3 years of experience out of thin air) is to try freelancing. I have a short email course that will help you start your freelance web development business! 👇

[thrive_leads id='2262'] -->

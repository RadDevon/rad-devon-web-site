---
title: 'Do I need a degree to become a web developer?'
date: '2021-08-24'
tags:
  - 'career'
coverImage: 'images/woman-holding-diploma-thumb.jpg'
---

You've been going the self-taught path for a while now or maybe you've done a coding bootcamp. You're starting to get concerned, though, that the competition may be too fierce. Knowing your stuff might not be enough to get that web development job you're dreaming of.

Is it worth it to get a Computer Science degree to increase your chances?

The short answer: **probably not**. Here's why.

![Woman in cap and gown smiling and holding a red and gold diploma](images/woman-holding-diploma-thumb-1024x681.jpg)

## It's expensive

If a CS degree were free, this might be a no-brainer… but it's very much *not* free.

[According to US News and World Report](https://www.usnews.com/education/best-colleges/paying-for-college/articles/paying-for-college-infographic), the average annual tuition for an American in-state public school is $10k. That means you're looking at **$40k** for a full BS in computer science.

If this were the difference between a minimum wage job and a guaranteed developer role, $40k might be worth it. Problem is, there are many other ways to get into a web development career. Also, the degree is not a guarantee of anything. That makes your tuition look like **a big fat $40,000 gamble**.

You could pay less at a community college, but your degree carries less weight too. If it's just the education you want, there are more efficient ways to get it. The value of college is more in the networking and the credibility conveyed by that diploma.

That's not the only cost though.

## It takes lots of time

You're going to pay not only in money but in your time. This is commonly referred to as an opportunity cost. It's the cost you pay to do something in terms of not being able to do anything else. For four years while you're doing school, you're devoting most of your time to getting a degree instead of

- working on personal projects
- taking on freelance work
- finding an internship
- applying for jobs

Will this put you in a better position than you were before you spent those four years of your life? Who knows? You won't **until you've already spent the four years and the money**. And, as far as I know, universities don't offer any kind of money-back guarantee.

## Depending on your path, it may not matter

I took a non-traditional approach to getting into web development. I actually have a computer science degree. It's a two-year degree from a small community college, but it's still a degree. It's never done much for me though. Probably because no one except for you reading this even knows I have it.

[Because I started my career by freelancing](/articles/inverted-career-path/) and all my full-time gigs have evolved from freelance gigs, I've never been asked for my credentials. I've never filled out an application, and I've never done an interview. (OK, that's not true. I _have_ done those things, but my full-time gigs did not come out of that process. I stopped doing them because it hasn't been effective for me.)

<!-- Hey, if you're interested in the self-directed path to a web development career, you'd probably enjoy my free ebook with **5 projects to get your career rolling**. 👇

[thrive_leads id='4451'] -->

## What to do instead

Instead of trying to prove yourself by getting someone else (like a university) to sign off on your abilities, build your credibility by doing real work. Once you can build a site or a web app yourself and deploy it, nothing is stopping you from finding companies who need web sites or software and selling your services to them. You don't have to get a job. You can just solve a problem for them. When you're done, you can move on to other companies who have other problems.

This is how I've made my way through my career and commanded great salaries and benefits with credentials that are both underwhelming and totally unknown to any of my employers. Think about why a degree is helpful: it provides some measure of proof you can do the work. But you know what provides even more proof you can do the work? **Actually doing the work.**

Besides, bootstrapping your career by freelancing is more empowering and less risky. It's the only way to build your credibility **while making money at the same time**. That's a big difference from paying someone else $40k to tell prospective employers you're legit. So, save your money… and start earning it instead.

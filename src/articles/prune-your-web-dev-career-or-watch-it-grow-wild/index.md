---
title: 'Prune Your Web Dev Career (or Watch It Grow Wild!)'
date: '2020-07-24'
tags:
  - 'career'
  - 'tips'
coverImage: 'images/bonsai-career-thumb.jpg'
---

All too frequently, people end up changing careers into web development only to realize their new career sucks and they just want to get out of it! 😫 This usually happens if you say yes to everything. Your career is defined not by what your goals are but by who gets to you first. 🏃‍♂️

Watch this. It's important!

https://youtu.be/_g_pEwRzMko

I'll share an analogy to bonsai trees (along with some absolutely choice puns) to help explain why saying yes to everything is a terrible idea and how pruning your career by saying no will allow you to build it into the career you ultimately want.

I'll also tell you a story about my own experience turning down a job that paid 50% more than my previous job 💸 … which cleared the way for me to take a job months later that paid 2x 💰💰 what that one would have paid.

In the process, I share a short clip of a video by [Bonsai Boise](https://www.youtube.com/channel/UCq_hcjdI7a_HJ_LxBri4prw).

<!-- Thanks for watching. While we're here, are you driving your career, or are you waiting for someone to invite you into the party? Stop waiting for someone to take a chance on you, and start freelancing instead! My Freelancing Crash Course will help you get started for free! 👇

[thrive_leads id='2262'] -->

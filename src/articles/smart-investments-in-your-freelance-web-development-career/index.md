---
title: 'Smart Investments in Your Freelance Web Development Career'
date: '2019-08-16'
tags:
  - 'career'
  - 'freelancing'
coverImage: 'images/computer-with-chart.jpg'
description: 'Success as a web developer isnʼt measured by how little you spend on your business. Sometimes, by spending a little, you can earn more or even just make your life a little easier. Here are some sound investments that are worth it.'
---

Before I was a web developer, I didn't make a lot of money. I was living paycheck to paycheck for many years. When you live this way, **you learn to save money wherever you can**. It's smart to invest money, but you just don't have the money to invest.

Web development should completely change the day-to-day economics of your life. Problem is, **you're still conditioned to scrimp everywhere you can**. If you can do it yourself, even if the results are worse and it wastes a bunch of time, you'll do it. If you can work with poor tools instead of replacing them, you will.

Let's look at a few cases where you'll be much better off if you loosen your grip on your cash and make some smart investments in your freelance web development career, prioritized by importance.

![Laptop displaying a chart](images/computer-with-chart.jpg)

## Contract Review

You need a contract when working with clients. If you're like me, you'll find an existing contract online that's close to what you want and make some customizations to fit the way you want to do business. I started with [the Contract Killer open-source contract](https://stuffandnonsense.co.uk/projects/contract-killer/) and modified it to fit my development-centric freelancing practice.

You're not a lawyer, though. You could easily create a contract that's unenforceable. To avoid this, you'll want to invest some money to have an actual attorney review your contract to make sure it's legally sound.

You'll only need to do this once or maybe a 2-3 times if you continue to make modifications to your contract as you go (which you'll probably want to do). If you're doing freelancing right, you'll be making enough to make this a trivial cost of doing business — one that's well worth the money for the peace of mind you get in return.

**Cost: ~$150-400, one-time**

<!-- If you want to start freelancing, making these smart investments alone won't get you there. The Freelancing Crash Course will teach you how to start your business **in one week**. 👇

[thrive_leads id='2262'] -->

## Tax Prep

I came into freelancing accustomed to doing my own tax prep, even though I had no idea what I was doing and it took me a good bit of time to do. When I started freelancing, taxes got more complicated since I now owned a business. Also, since I could pick up clients to fill my time, it no longer made sense to spend my time doing something I didn't enjoy and wasn't good at.

Instead, I could pay someone who is great at tax prep (i.e. an accountant) and spend my time doing client work. The accountant will get me a better outcome on my taxes, and I get to do what I Iove instead. It's a no-brainer.

I still do my own bookkeeping because my business credit card and bank account are linked to my accounting software. The time it takes me is very minimal and it's painless. This tees up my tax prep for the accountant, who I'll gladly pay every year to take this off my plate.

**Cost: ~$300-600/year**

## Ergonomics

Taking care of your body while you work should be a high priority. With my setup, that comes down to 4 pieces of equipment.

### Desk

I work on [an Xdesk Terra standing desk](https://www.xdesk.com/terra), and I stand for my entire workday. [According to the Mayo Clinic](https://www.mayoclinic.org/healthy-lifestyle/adult-health/expert-answers/sitting/faq-20058005), "…those who sat for more than eight hours a day with no physical activity had a risk of dying similar to the risks of dying posed by obesity and smoking." That's scary stuff, and it's well worth standing through my work day and buying a good standing desk.

The Xdesk is a bit pricey (about $1,400), but you can get alternatives like the [Uplift V2](https://www.upliftdesk.com/uplift-v2-standing-desk-v2-or-v2-commercial/) for around the $650 mark or even cheaper if you want a very small desk.

**Cost: $1,500 (or $650 for a budget option)**

### Keyboard

I did a comprehensive search a few months back for the ideal ergonomic keyboard… and I couldn't find it. I did get pretty close though.

I landed on an [ErgoDox EZ](https://ergodox-ez.com/) split keyboard. A split keyboard lets you separate the two halves of the keyboard so that your arms can rest at a more natural angle, instead of being angled in toward a standard keyboard.

![Illustration of an ortholinear key layout](images/ortholinear-layout.png)

It has an ortholinear key layout which puts the keys in columns rather than the typical staggered key layout. The staggered layout exists because very old typewriters had an arm attached to each key. Each key arm needed a path back to the hammer that presses the letter onto the paper.

![Typewriter key arms](images/typewriter-keys.jpg)

The staggered layout still makes some sense on a standard keyboard since your arms (and, by extension, fingers) are angled coming into the keyboard anyway, but, with a split keyboard, you'd have to artificially angle your hands to properly reach the keys.

I chose the ErgoDox EZ because it had most of the ergonomic features I wanted, and it's very programmable. Most of a developers work happens on the keyboard, so programmability gives us some important flexibility.

**Cost: $270 - $325**

### Mouse

A few months back, I picked up [a Logitech MX Vertical mouse](https://amzn.to/33s2Bsr). It's a mouse that allows your hand to rest in a more natural position while mousing. That reduces your risk of RSI (repetitive stress injury).

The one thing I've found that I didn't expect while using this is that using it causes my wrist to rest uncomfortably against my desk. I've had to put something soft down on the desk to pad my wrist. With that pad in place, the mouse has been a great purchase to make work more comfortable.

**Cost: ~$85**

### Standing Mat

If you're standing all day, you need some padding to ease the load on your feed. I just got this [Ergodriven Topo](https://amzn.to/31wRSuW) mat. It provides padding, but, more important, it begs you to change standing position and gives you lots of different ways to stand.

Before this, I worked for years standing on a much cheaper standing mat that worked fine. After years, it had started to show some wear with a few missing chunks, but it still did the job.

**Cost: ~$100**

## Software

For almost every kind of software you might want, there's an open-source alternative that gets you most of the way there. Don't be afraid to splurge on the tool that gets you all the way there. This is your work. You want to do the best job possible, and you should be able to pay for the right tools.

The best example I can think of is image editing. [GIMP](https://www.gimp.org/) has been around forever, but it just doesn't do what the commercial alternatives do. I use [Affinity Photo](https://affinity.serif.com/en-gb/photo/), and it does everything I need to do and then some. Maybe [Photoshop](https://www.adobe.com/products/photoshop.html) is your jam. In either case, the commercial options are worth the money.

I buy a lot of software that makes my life easier. [Transit](https://www.panic.com/transmit/) for FTP, [Paw](https://paw.cloud/) for testing APIs, [aText](http://www.trankynam.com/atext/) for text expansion, and [Alfred](https://www.alfredapp.com/) for a full-featured app launcher just to name a few. How would you feel about a carpenter coming to your house and driving in nails with a wrench because they didn't want to buy a hammer? You'd expect them to have the right tools and you should too.

**Cost: Varies**

## Education

If you don't keep learning in web development, you'll fall behind quickly. Buy the books and courses you need to stay on top of things. That said, you don't need to know _everything_. That's not realistic, and you'd end up knowing a little about a lot of technologies which is not super useful.

Be careful, especially when you're new, of getting stuck in tutorial purgatory. It's a real threat. Let your work drive your education by learning what you need to know just-in-time.

**Cost: Varies**

## Leverage Your Cash

Have you ever heard anyone wish they could have back the cash they invested in Apple stock in the 80s or Google stock in the mid 2000s? Of course not! That's because **the investments paid off**.

If you make these smart investments, you'll be laughing all the way to the bank as other freelance developers who hung onto every penny **struggle to make their businesses work**. Make those smart investments, and let the dividends start rolling in!

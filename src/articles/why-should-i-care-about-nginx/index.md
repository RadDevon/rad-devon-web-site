---
title: 'Why should I care about NGINX?'
date: '2019-01-04'
tags:
  - 'devops'
  - 'http'
  - 'nginx'
coverImage: 'images/toys-r-us-game-slips.jpg'
description: 'NGINX is one of the most popular web servers today. Learn about how NGINX works by hearing how I used to buy video games at Toys "R" Us in the 80s and 90s.'
---

One of my best memories of childhood was going to Toys "R" Us to buy a video game. Even though the process was pretty strange, I still think of it fondly because it was part of the anticipation of playing that brand new game.

The process went like this. First, you'd go to the video game aisle where you'd find walls filled with plastic cards containing the cover art of each game. Flipping the card up, you could look at the back of the game box. Once you found the game you wanted, you'd pull a slip of paper from a pocket hanging under the game's card showing the game's price and a UPC.

![Toys "R" Us game slips](images/toys-r-us-game-slips.jpg)

You'd take the slip through the checkout and pay for your game. Then, you'd go beyond the checkout and take your game slip to a window at the customer pickup room. There, an employee would take your slip, grab the game from the shelves, and hand it to you through the window.

![](images/toys-r-us-customer-pickup.jpg)

## What is NGINX?

What does all this have to do with [NGINX](https://nginx.org/)? NGINX is a web server. It's a piece of software that runs on a computer, **listening to HTTP requests and fulfilling them**.

It serves a similar role to that Toys "R" Us employee who worked the pickup room. You start by making a request. At Toys "R" Us, that's your slip specifying the game you bought. On the web, that's an [HTTP](/articles/why-should-i-care-about-http/) request.

Just like the customer pickup employee, the server knows where to go to fulfill that request. It gets the response and passes it back through the "window" to the user who made the request.

## Why Do I Need to Understand NGINX?

NGINX is not the only web server, but it's an extremely popular one and it's on the rise. It's open source which means you can use it for free for your own sites. It's also open source meaning you can extend it or fix bugs yourself. Even though it's in 2nd place overall (behind Apache), **it's [number 1](https://w3techs.com/technologies/cross/web_server/ranking) amongst the most trafficked sites**.

You can do a lot as a web developer _without_ understanding the web server, **but you'll eventually hit a ceiling**. If you need to redirect from one URL to another, that will be impossible. If you want /blog on your server to be a Python app and /chat to be a Node app, you can't. If you want to add headers to your request, you'll have to configure the web server to do that.

## Example NGINX Configs

One really common use case NGINX can help with is driving two (or more) sites with different domains from a single server. Here's what the NGINX configuration to do that might look like:

```nginx
server {
    listen 80;
    server_name www.raddevon.com;
    root /var/www/raddevon;
}
server {
    listen 80;
    server_name www.radworks.io;
    root /var/www/radworks;
}
```

This configuration creates two virtual servers, both listening on port 80. (That's the default HTTP port.) If an HTTP request comes in for a resource at raddevon.com, NGINX will look for that resource at that server's root path (`/var/www/raddevon` in this case). If a request comes in for a resource at radworks.io, NGINX looks for those resources at `/var/www/radworks`.

These are both static sites, but you could very easily have a Node web server running and proxy one or the other to it. Proxying allows NGINX to be a middleman, passing requests off to another web server running on the same machine. [Express](https://expressjs.com/), for example, sets up a server that listens for requests on port 3000 by default. If I ran Rad Devon as an Express app, I could take any requests for raddevon.com and proxy those to port 3000 on the same machine. Here's what that might look like:

```nginx
server {
    listen 80;
    server_name www.raddevon.com;
    location / {
        proxy_pass http://127.0.0.1:3000/;
    }
}
```

I've taken out the root directive and replaced it instead with a location block that defines what NGINX should do when someone hits raddevon.com at a certain address (in this case, the root url indicated by the `/`). I've used the `proxy_pass` directive to specify that it should send the request on to the server at http://127.0.0.1:3000. 127.0.0.1 is the loopback address which means it refers to this network interface ("this" meaning the one that made the request). 3000 is again the default port Express uses for its web server.

I could run two different Express applications on the same machine by changing the port of one to something other than 3000. 3001 would work just fine. Then, I can proxy from two different domains using `server` blocks in an NGINX config, or I could proxy to them from different paths on the same domain by using multiple `location` blocks inside a single `server` block.

## How to Configure NGINX

In the same way your HTML is simple text in a file with a `.html` extension, NGINX configuration files are simple text files but without the `.txt` extensions. The base configuration file is named `nginx.conf`. Its location on the computer once NGINX is installed can vary depending on the operating system you're running, but `/etc/nginx/nginx.conf` is a common location for the file. If that doesn't work, a search for "nginx configuration" should get you there.

Open that file in the editor of your choice, and start configuring your server. [The NGINX docs](https://docs.nginx.com/nginx/) can be daunting, but they are still an excellent resource if you want to go deeper with your configuration.

## Quick Wins from Understanding NGINX

While it's not strictly necessary to understand how a web server like NGINX works to build for the web, it does give you **some useful superpowers** like those I've mentioned above: the ability to create redirects, the ability to proxy to different applications depending on the route, the ability to manipulate request headers going to those servers, and the ability to run multiple domains from a single server.

In short, understanding NGINX and knowing how to configure it allows you full control over the way your web server behaves. **Break through that ceiling** by learning NGINX!

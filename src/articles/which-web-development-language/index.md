---
title: 'Which language should I learn to become a web developer?'
date: '2017-12-26'
tags:
  - 'languages'
coverImage: 'images/jack-johnson-vs-john-jackson.jpg'
description: 'Take this short quiz to figure out which languages you should learn when you get started in web development.'
---

import LanguageQuiz from '../../components/language-quiz.js';

![Debate between two politicians named "Jack Johnson" and "John Jackson"](images/jack-johnson-vs-john-jackson.jpg)

Take this short quiz to figure out **which languages you should learn** when you get started in web development.

<LanguageQuiz />

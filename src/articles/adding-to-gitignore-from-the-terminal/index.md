---
title: 'Quickly Adding to .gitignore from the Terminal'
date: '2013-08-05'
tags:
  - 'bash'
  - 'git'
  - 'gitignore'
  - 'shell'
  - 'terminal'
  - 'version-control'
coverImage: 'images/bash-prompt.jpg'
---

I learned a trick a while back to create my .gitignore file (and add to it) quickly from the terminal. Here's a common ignore pattern for Python development to illustrate.

`echo "*.pyc" >> .gitignore`

Run this command from the root of your project. I like to do this right after I git init. This will create the .gitignore file if it doesn't exist and add "\*.pyc" to it. If the file does exist, it will append the string to it. Substitue "\*.pyc" for whatever you like. I also tend to keep my Sublime project files out of version control. (Not sure if this is a good or poor practice, but I do it anyway.) Here's the command that would set that up for me:

`echo "*.sublime-*" >> .gitignore`

That will take care of your Sublime project and workspace files.

Now, let's imagine you've totally flubbed your .gitignore file. You're ignoring too much, and you want to start from scratch and only ignore pyc files.

`echo "*.pyc" > .gitignore`

Notice that, in this command, I use only a single angle bracket here. Where two angle brackets append, a single creates the file anew with only the value specified.

Also, you may find it difficult at first to remember the direction of the angle brackets. I always remember that they point toward the destination (the file).

[thrive_leads id='1366']

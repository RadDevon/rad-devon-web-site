---
title: 'Keep Your Resolution to Become a Web Developer'
date: '2018-01-08'
tags:
  - 'career'
  - 'goals'
  - 'willpower'
description: 'Wanting to be a web developer is easy. Becoming one is hard. Hereʼs how you can make your New Yearʼs resolution stick.'
coverImage: 'images/fireworks.jpg'
---

_Making_ a resolution to leave your crummy job behind and become a web developer is easy. _Keeping_ the resolution and seeing it through… not so much. Follow these tips to keep yourself on track for **a new career and a better life** in 2018!

## Set a Big Goal

![Astronaut](https://raddevon.com/wp-content/uploads/2018/01/astronaut.svg)

Figure out **what change you want in your life** from web development. It goes way beyond making more money. Does your boss micromanage you? Maybe you want **more autonomy**. Feel like a cog in the machine? Maybe you want to be able to **flex your creativity** in your career. Hate sending that rent check every month? Maybe you want to be able to afford **your own home**.

Figure out your goal whatever it may be, and find ways to remind yourself of it. **This is your best tool to help you push through the hard parts.** I put together [a free Big Goals email course](/stick-with-web-development/) to help you figure out your Big Goal and put it to work for you.

## Decide Which Language to Start With

![Piecing together the web](https://raddevon.com/wp-content/uploads/2018/01/language.svg)

Don’t sweat this too much. Over the course of the next couple of years, you’ll probably become familiar with several languages beyond your first one. If you can come to grips with the fundamentals, it becomes easy to learn enough of a new language to get by. You _do_ need to know what to learn first, though, so you can actually get started. Take [my very short quiz](/articles/which-web-development-language/) for suggestions on where to start your journey.

## Set a Practice Time and Stick To It

![Calendar](https://raddevon.com/wp-content/uploads/2018/01/calendar.svg)

You need consistency to be successful in learning web development. Many of the skills are not intuitive and require repetition to make them stick. This means you need to be practicing almost every day. Dedicate a time to your practice and **schedule around it**. Don't just let your practice fill the gaps in your life (unless you do that _in addition_ to a scheduled time). When things get hard, you'll find other ways to fill those same gaps.

Having a schedule is important because you don't have to find time. It's OK to reschedule your regular time if you absolutely must, but, if you're having to find and schedule your time each day or each week, that's adding friction which will make it easier for you to quit.

## Reach Out When You're About to Quit

![Email](https://raddevon.com/wp-content/uploads/2018/01/email.svg)

My mission this year is **to help people transition into careers as web developers**. I did it myself, and it completely changed my life. I want more people to transform their lives, achieve their goals, and be happier.

Here are some barriers you might come up against on your journey to becoming a developer:

- Frustration trying to understand an esoteric concept like Javascript closures, callbacks, or even something more basic.
- Difficulty using an API or a library
- Lack of time to practice
- Lack of money for software or learning materials
- Self-doubt or [imposter syndrome](https://en.wikipedia.org/wiki/Impostor_syndrome)

If you find yourself facing these or any other, please drop me a note. I'll reply back and try to help you get over the hump. I won't have all the answers, but I'll do my best to get you back on track.

[gravityform id="2" title="false" description="false" ajax="true"]

Whatever you do, don't drop the ball. 2018 is your year to start an awesome new life as a web developer. Do whatever it takes to cross that finish line!

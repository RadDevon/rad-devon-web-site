---
title: 'WordPress Image Resizing Without php-gd'
date: '2013-04-19'
tags:
  - 'image-resizing'
  - 'images'
  - 'php'
  - 'wordpress'
coverImage: 'images/magnifying-glass.jpg'
---

I ran into a problem recently on the very WordPress installation that runs this blog. After migration from my old hosting provider ([Dreamhost](http://www.dreamhost.com)) to my new VPS ([DigitalOcean](http://www.digitalocean.com/)), WordPress was no longer resizing images.

I learned that this resizing is typically done by a PHP extension called php-gd. I tried to install the plugin using `yum install php-gd`, but the process ended complaining about a dependency which wasn't being met. It appeared there was no way around this.

After posting [the problem](http://wordpress.stackexchange.com/questions/96201/additional-image-sizes-are-not-being-generated) to [StackExchange's](http://www.stackexchange.com/) [WordPress site](http://wordpress.stackexchange.com/), I was alerted to the fact that WordPress [recently started using](https://make.wordpress.org/core/2012/12/06/wp_image_editor-is-incoming/) [ImageMagick](http://www.imagemagick.org/script/index.php) for image resizing.

[thrive_leads id='1366']

Being a newcomer to Linux server administration, I still wasn't sure what to do with this knowledge. I installed ImageMagick with `yum install ImageMagick*`.

Even with ImageMagick installed I still had no image resizing natively through WordPress (and I'm still not quite sure why this was the case). Luckily, I stumbled into a solution: the [ImageMagick Engine plugin](http://wordpress.org/extend/plugins/imagemagick-engine/) for WordPress. The plugin quickly found my ImageMagick install on the server and resizing was working once again.

This plugin has an option to regenerate your thumbnails for existing uploads. This feature is provided because ImageMagick is apparently worlds better at scaling images than php-gd. It worked really nicely for me because I had no additional sizes for the images at all. I used this regenerate functionality to generate them for the first time.

As a bonus, check out the [Simple Image Sizes plugin](http://wordpress.org/extend/plugins/simple-image-sizes/). It's a great way to add custom sizes for your images that will be generated each time you upload.

---
title: 'How to Get Fewer Freelance Clients'
date: '2018-09-16'
tags:
  - 'business'
  - 'clients'
  - 'freelancing'
coverImage: 'images/soup-nazi-meme.jpg'
description: 'Some web devs trap themselves in a prison of their own making by trying to take on every client that comes through the door. Hereʼs how to shift that mindset and what to do instead.'
---

![Seinfeld's soup Nazi](images/soup-nazi-meme.jpg)

**Your first job as a freelance web developer is to stop people from hiring you.** That's right. It's _not_ to convert every lead into a client. Your freelance services aren't like toilet paper or some other commodity. You don't just want to sell them to as many people as possible. You want to sell your services to only **the right people**, but how do you know if the lead in front of you is one of the right people?

## Why Kill Deals?

Disqualifying clients is good for you _and_ for your clients. To understand why, let's imagine what **an ideal project** looks like. Here are the 10 pieces of an ideal project:

1. A prospect comes to you with **a pressing business need**.
2. They have a **clear idea of the result** they want.
3. Timing is important for the project, and **the right time is now**.
4. Solving this problem will either **make or save them lots of money**.
5. **You understand** the problem.
6. **You are capable** of solving the problem.
7. **The client will pay** what you ask.
8. You **build** the solution.
9. The client **pays**.
10. The solution **delivers the desired result**.

As a freelance web developer, you're in a choice position. **There's far more work to be done than you could ever do.** That means, you don't need to capture everything. You need to capture only these ideal projects. Here's how you can control for this _before_ the project begins.

[thrive_leads id='1378']

## What makes an Ideal client?

Since you only want to take on ideal clients, you'll need **some criteria** to help you determine which jobs to take. You'll want to customize these to your own freelance practice, but here are a few to get you started.

### Is the problem real?

_"I need a new web site."_ Not a problem. _"I need my web site to work on mobile."_ Not a problem. _"60% of the visitors to my online store use a mobile devices, but my site isn't mobile responsive. I need a new responsive web site so I can convert those visitors at a higher rate."_ Bingo.

Client conversations often start out with something like the first two examples. We can sometimes steer them to something more like the last example. This is critical to building an ideal project. Without **a real problem** (the first piece of an ideal project), we don't know if this project is **timely** (the third piece), and we can't [figure out **a real solution**](/articles/the-most-valuable-skill-for-web-developers/) (the second piece). Without understanding the desired result, we can't know if the solution we build **actually works** (the last piece).

Businesses spend money either to make _more_ money or to spend _less_ money over the long-term. They have obstacles preventing them from making more or spending less, and the budget for a solution is intended to overcome those obstacles. If you're a trusted problem solver, they may come to you with the problems first. If you're seen as a web developer, they're more likely to come to you with a solution they've worked out in their head, asking you to simply build it.

Even if they _don't_ tell you what their ultimate goal is (i.e. the pressing business need), **they're still going to evaluate success in the project against whatever metric they have in their head**. You need to get that problem out into the open so that you can

- gauge if the proposed solution will solve it
- determine if the solution is feasible within the budget
- steer the client in the right direction if either of the first two is a problem
- focus your time and energy on making sure that problem is solved

### Is the problem Valuable?

If the problem isn't valuable (the fourth piece of an ideal project), you're no longer an asset; **you're a liability**. You're someone the business feels it _has_ to hire for one reason or another.

There is no "home run" because the benefits to the business's bottom line are minimal. That's going to affect **how much they're willing to pay you** (the seventh piece) and **how much they respect you**. That's going to affect how they perceive the outcome. It's going to affect **whether they decide to come to you for more work** in the future.

Besides that, if you're pricing your projects based on the value, **a project with _no_ clear value won't fit into that paradigm**. If the project means $0 to the company, what can you charge them for it? Even if it's worth $500/year to them, what can _you_ charge that would allow you to earn a decent rate? It's just not enough room to work with.

_If you're new to value-based pricing, I highly recommend a free ebook called [Breaking the Time Barrier](https://www.freshbooks.com/blog/breakingthetimebarrier). It's a great introduction for people who are used to billing for time._

### Can I help with this (and will I enjoy it)?

If you don't have the particular skill you need to help the prospective client, it's **OK to pass on the work**. This will actually earn you **more trust** with the client, particularly if you can act as a resource and **help them find someone to solve the problem**. If you _do_ refer the work to someone else, when _that_ person comes upon work that's in _your_ wheelhouse, they'll want to return the favor.

This doesn't mean you shouldn't **take on jobs that will require you to bring on other people** with different skillsets. That's fine. If someone wants to build a marketing site, and you plan to code it yourself while hiring a designer and a copywriter, go for it!

This also doesn't mean you shouldn't **stretch yourself** when you take on new work. Maybe you find a [Vue](https://vuejs.org/) gig. You're not familiar with Vue, but it's otherwise similar to other work you've done. Do it! You'll learn Vue as you go.

I'd simply caution you not to take every single job just because it landed in your lap or because it has a big budget attached to it. If you personally have no skills to offer toward a solution, it's not the right job. Even if you're overseeing the work, you're less likely to oversee a success than if you're working on a project where you yourself can deliver a great solution (the sixth piece of an ideal project).

Optional criteria: make sure the work is something you _want_ to do with some*one* you want to do it for. The happier _you_ are, the greater the chance for success and the greater the chance the client will tell their friends and come back.

### Will this client pay?

Nothing is more demoralizing than doing work you don't get paid for. Companies are making money on the back of your work. You should too. So, how can you tell if your client is actually going to pay (the ninth piece of an ideal project)?

The best proxy for whether a client will pay is to **have them pay something up-front**. You can ask for the entire payment up-front, or you can get a partial payment with the remainder coming due later. Make sure the pre-payment is more than a token amount. This will give you some operating capital while you complete the job, but **it also shows the client is serious about the work**.

The next best way to tell they're going to pay is to **have them sign a contract**. Let them know you mean business and spell out the expectations on both sides, compensation being one of them.

If you find a prospect who balks on the contract or on pre-paying, leave them behind. This is a red-flag that they won't pay later either.

## The Cost of Bad Projects

You may think you can still take all the work, enjoy the ideal projects, and just power through the bad ones. That may be true, but you're ignoring the **opportunity cost**. Opportunity costs are the opportunities you miss by taking less-than-ideal ones. It's literally **a cost in terms of _opportunity_** instead of a cost in terms of _money_, which is how we usually think of costs.

What happens when you're loaded up with bad projects and an ideal one comes along? **You're going to lose the chance to take on the ideal project** because your resources are tied up in the bad one. That's not a place you want to be, and gigs aren't so scarce that you should ever _have_ to be there.

You'll need to overcome a psychological barrier to get comfortable with this. Think of it this way: if someone offered to fill your availability with **nothing but ideal projects**, how much would you be willing to pay for that? Got something in your head? OK, great.

Now, imagine you're going to pay _yourself_ that same amount to get you nothing but ideal projects. When you turn down bad projects waiting for the ideal ones, that's probably going to leave some small gaps in your workload that represent the time you were disqualifying clients. That time that's no longer billable, **that is the pay you're paying to _yourself_** to get only the ideal jobs.

You'll soon find yourself reaping the benefits of **happier clients**, **greater job satisfaction**, and **more profit**. Learning to turn away work is a difficult transition to make, but it's one that pays off in every aspect of your web development business.

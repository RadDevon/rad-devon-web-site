---
title: 'Freelancing on the Side'
date: '2018-11-30'
tags:
  - 'career'
  - 'freelancing'
coverImage: 'images/counting-money.jpg'
description: 'Freelancing is a mode of work that comes with a lot of freedom, but you may find some surprises if youʼve not worked this way in the past. This article will make sure youʼre prepared and donʼt run into any nasty surprises.'
---

![Counting money](images/counting-money.jpg)

Most people don't have the luxury of being able to leave their jobs while they transition into web development. The people who are working the worst jobs and need a change the most have the least flexibility to make it happen.

No worries. Freelancing to the rescue! Instead of having to lose all your income while you learn web development and transition into a new career, you can take a graduated approach. Get up to speed after work and on weekends. Then, **start taking on some freelance gigs**.

Here are some things to watch out for as you're getting started freelancing on the side.

## Don't Skip the Admin 🗃

You'll be tempted since you're just doing freelance development on the side to **skip out on all the boring administrative tasks**. You won't want to register your business, talk to an accountant, and make your clients sign contracts before working with you.

**This is a mistake.** If you don't start doing these important things now, you'll be less likely to do them ever. This will cause you a problem at some point. You'll be $5k into a project when you realize the client doesn't intend to pay… and you didn't get them to sign a contract.

Do things right from the start to avoid nasty situations like this. **Practice the way you play**.

## CYA 🍑

Make sure you're **not mixing your personal life with your freelancing side gig**. It's going to be tempting to use your personal bank accounts and to run a sole proprietorship with nothing protecting your personal assets.

Just like the previous advice, you might be able to run for a long time like this with no repercussions. Once you _do_ have a problem, though, the repercussions are going to hit hard. A client could sue you and take your personal assets. If you have a family, that means problems in your side gig could reach into your family life and affect the ones you love. **It's just not worth saving a little time and a few bucks.**

## Understand It's Not All Code 🤔

When I first started, I thought I would build a web site telling what services I offered and **wait for clients to start rolling in**. What _actually_ happened didn't resemble that in the slightest. Instead, I had to go out and get in front of people all the time in order to find work.

Today, about half my time is not billable. It's spent doing marketing, finding clients, bookkeeping, and a million other things. This is important to understand, especially if you're only going to be freelancing part-time. Take that time you _think_ is going to be devoted entirely to writing code and **cut it in half**. _That's_ how much time you're going to be able to bill for.

<!-- Need more help with freelancing? Click below to get notified when my freelancing course goes live!

[thrive_2step id='1842'] Learn Freelancing [/thrive_2step] -->

## Save for Taxes 💸

Your employer pays their portion of your taxes on the income you make at your full-time job, and your portion typically gets withheld from your check. **You don't even have to think about it** until the end of the year when you get whatever amount you overpaid back in the form of a tax refund.

When you're self-employed — even for _part_ of your time — you have to pay the employer portion of the taxes as well as the employee portion. **Make sure you save for this as you're making the money** instead of trying to pull it all together at tax time.

To find out how much to save, you should **talk to an accountant**. You may need to pay quarterly estimated tax payments, and your accountant can advise you on this too. It may cost you a couple hundred dollars, but the accountant's advice is worth it.

Last year, I was a full-time employee while also running a freelance business on the side. I saved 20% of my freelance earnings for taxes. That turned out not to be enough. **I had to come up with a few thousand dollars** to pay the difference. This year, I'm saving 35% instead.

## Flip the Switch 💡

It takes a lot of commitment to make freelancing work. As you try it out on the side, you'll see the potential, but you'll also see how much of that potential you can't tap into without **more time**. Once you understand how to get work and how to keep it coming in, start thinking about when it makes sense to make the transition from your job to full-time freelancing.

When you make that leap, you'll have other considerations like replacing the insurance your employer provides and maintaining some social connections your office provides. Make sure these are part of the calculus as you make the switch, but don't hold off for too long. You'll never have freelancing 100% figured out, but you'll learn enough to start your new life, **with you calling the shots**.

---
title: 'Your code is garbage'
date: '2020-07-17'
tags:
  - 'career'
  - 'tips'
coverImage: 'images/code-is-garbage.jpg'
---

Code isn't always meant to be permanent. In fact, it rarely is. Unless you want to become outraged and feel that sting all the time as your code is taken out of service, you need to **shift the way you think about code**. Your code is kind of like a box. 📦

Watch to find out how!

https://youtu.be/-L0hPhYB9GE

<!-- If you're not careful, it will be more than just your code being tossed out. It will be your career aspirations. Transitioning is hard. You need some way to bring yourself back and refill your willpower when the going gets tough. You need a Big Goal.

Take my free email course to help define your Big Goal **so that you don't just quit** when it gets "too hard." 👇

[thrive_leads id='1360'] -->

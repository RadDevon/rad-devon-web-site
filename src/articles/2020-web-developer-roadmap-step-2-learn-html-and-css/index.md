---
title: 'Web Developer Roadmap Step 2: Learn HTML and CSS'
date: '2020-01-17'
tags:
  - 'css'
  - 'html'
coverImage: 'images/thumbnail-1.jpg'
description: 'HTML and CSS are the foundational technologies you need to become a web developer. Here are my favorite resources for learning them.'
---

https://youtu.be/KR7zXB0v9Sk

## Video Notes

I won't teach you HTML and CSS in this video, but **I _will_ give you some great resources and tips** to get started on your own.

If you want to learn how to use a Big Goal to propel you forward into your new career, watch [the previous video in the roadmap](/articles/2020-web-developer-roadmap-step-1-set-your-big-goal/).

The two courses I recommend (even though they're a bit dated):

- ⭐️ [Learn to Code HTML and CSS by Shay Howe](https://learn.shayhowe.com/html-css/)
- ⭐️ [30 Days to Learn HTML & CSS by Envato](https://www.youtube.com/playlist?list=PLgGbWId6zgaWZkPFI4Sc9QXDmmOWa1v5F)

Links mentioned in the video (mostly in order of mention):

- [Flexbox Froggy](https://flexboxfroggy.com/)
- [CSS-Tricks Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [Wes Bos's CSS Grid course](https://cssgrid.io/)
- [CSS-Tricks Guide to CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
- [The Best IDE for a Beginning Web Developer](/articles/best-ide-beginning-web-developer/)
- [Google algorithm stance on HTML validation](https://twitter.com/JohnMu/status/858990753909022720)
- [MDN Web Technologies Reference](https://developer.mozilla.org/en-US/docs/Web)
- [10 Great Web Development Project Ideas](/articles/10-great-web-development-learning-project-ideas/)

Thanks for watching! Feel free to ask questions or share your thoughts with me [on Twitter](https://twitter.com/raddevon) or in the comments [on YouTube](https://youtu.be/KR7zXB0v9Sk). [Subscribe on YouTube](http://www.youtube.com/channel/UCxgZYWJq2Cxk7vIePGiTxFw) so you don't miss the rest of the roadmap!

## Other Videos In this Series

If you like this video, watch the others in the series for a good overview of the philosophy I used to break into web development with no connections or experience.

- [Step 1: Set Your Big Goal](/articles/2020-web-developer-roadmap-step-1-set-your-big-goal)
- [Step 3: Learn Javascript](/articles/2020-web-developer-roadmap-step-3-learn-javascript)
- [Step 4: Learn Terminal, Git, and Deployment](/articles/2020-web-developer-roadmap-step-4-learn-terminal-git-and-deployment)
- [Step 5: Relationships Are Make or Break](/articles/2020-web-developer-roadmap-step-5-getting-work-through-relationships)
- [Step 6: Time to Get Paid!](/articles/2020-web-developer-roadmap-step-6-time-to-get-paid)

## Transcript

Hey, this is Devon, and we are at step two on my web developer roadmap. This is how I would approach learning HTML and CSS in 2020.

In the previous video of this roadmap series, we looked at how to choose a Big Goal and how to keep it top of mind so that you're always motivated to keep going even when times get hard. In this video we're going to start getting into some technical stuff.

You're going to want to start by learning HTML and CSS. Those are the foundational technologies of the web, and here's why I say that. If you look at a really popular front end framework like React, it's used in only one quarter of 1% of websites. If you look broader, you'll find Javascript, which is used in most websites, 95% of them. But HTML is used on every single site on the web. CSS goes hand in hand with that to achieve all of the different looks we see on sites across the web.

I try to keep track of the best courses that are available so that I can point people in the right direction when they tell me they want to learn this stuff. I have two favorites. The first is _Learn to Code HTML and CSS_ by Shay Howe. This is available as a book, but it's also available for free online. It has embedded code samples of the concepts that are being discussed throughout the book, and those are embedded CodePens, so you can click on "edit" on CodePen, and you can go in and play around with those examples and try different things yourself, which I think is a really useful way to really understand how this stuff works.

The book has a really good explanation of the box model, which is going to be critical for understanding how to lay out your pages. It also has a great chapter on good coding practice, which will help you in the long run.

The book starts to show its age a little bit when it's talking about how to achieve different layouts. It uses floats which feels a little bit out of date to me. You might want to instead pull in some other resources to teach you some alternate methods for achieving different page layouts. I would look into Flexbox and CSS Grid. Those are the more modern page layout tools.

I found this really cool game called Flexbox Froggy that teaches you how to use the various Flexbox properties, and those are not – in _my_ opinion – not intuitive, so this is a great way to learn about those. It has you trying to return frogs to their lily pads by styling them using Flexbox properties.

To this day, I still, whenever I use Flexbox – and that's actually pretty frequently – I still refer to the CSS Tricks Flexbox guide, which is a fantastic reference. I think that would be a really useful tool for you once you have gone through the Flexbox Froggy tutorial and you kind of have a basic understanding of how everything works, the CSS Tricks guide will give you a good reference that you can turn back to later when you just need a quick refresher.

For learning CSS Grid, you might look at Wes Bos's CSS Grid course. It's a free video course that will take you through everything you need to know to start using CSS Grid for your page layouts.

Grid is a newer technology, so it might not be useful to all of you right out of the gate. Internet Explorer doesn't support CSS Grid, so if you have to build sites that work in Internet Explorer, you may have to wait a little while until that sort of falls off the radar.

But for other browsers, support for CSS Grid is good, and it's a really useful tool for layouts. Just like with Flexbox, you can't beat the CSS Tricks guide to CSS Grid. It's a nice reference, it shows you all the properties you need to know, and it's easy to refer back to when you just need a quick refresher.

Sometimes I like to do my learning of new web technologies through books like the Shay Howe book I mentioned earlier, but other times I find video is a little bit nicer. Books I like because you can easily skim through them and find just the parts you need to refer to. Videos I like because it's lot easier to capture all the steps. I like it maybe a little bit better when I'm really starting from scratch.

The best video course I can find is Envato's _30 Days to Learn HTML and CSS_ video course. It's available for free on YouTube. It does a really good job. I've taken you from no knowledge whatsoever to a basic understanding of these foundational technologies in a pretty short period of time.

My favorite thing about it is that it has an assignment in the middle of it - basically a project that lets you sort of lock in some of your learning. Both of these courses are a little bit dated, but fortunately, HTML and CSS haven't changed that much. What has changed a little bit is the browsers that we need to support now are not necessarily the same ones when these courses were new five, six, seven years ago. Also, the tools that we use to develop websites are not necessarily the same. The tools have matured a little bit.

I would make just a couple of tweaks to the video course. The first thing I'd do is, instead of using Sublime Text 2 which is not even the current version of Sublime Text anymore, I would probably go with something like Visual Studio Code.

In the years since this course was published, Visual Studio Code was released. It's open source, whereas Sublime Text is proprietary software that you have to buy. I think it costs about $80 or it did years and years ago when I bought it, and it doesn't have as many features as something like Visual Studio Code. I wrote a short article about why Visual Studio Code is my pick for new developers, and I will post the link to that in the description below.

There are a few videos in this course I think maybe you should consider skipping. Day 22 focuses on validating your HTML. This is something I don't really hear anyone talking about anymore. I did a little bit of research just to see if it's still a concern, and apparently Google does not rank pages based on whether or not they validate, so I kind of don't think anybody cares anymore at this point. I would use a linter to make sure you're writing decent HTML and try to use good coding practices, but I don't think it necessarily matters that you run your code through a HTML validator.

Day 25 is about CSS frameworks. Another thing I don't really hear people talking a lot about anymore. Especially the particular one he chooses. I think it's 960 Grid, which I had never heard of until I watched this video series. I think those have been supplanted in a lot of cases by the technologies I mentioned before - Flexbox and CSS Grid - so I would maybe replace that day with a quick self study on Flexbox and CSS Grid.

Day 27 is about slicing a PSD and turning that into a webpage, which is another sort of workflow that I don't think many people or many studios are doing anymore. Some people do that, and you may come across clients who want to do it that way, so it certainly doesn't hurt to learn it. It's just not as common anymore as it was when these videos were published.

If you sort of combine these two courses. You'll get a pretty good introduction to how to build basic webpages with HTML and CSS. The way I would do this is I would start with the course that appeals to you. So if you're most interested in a video course, start with that one. If you'd rather read through a book, then start with the the Shay Howe book. If you get to a point where some concept is just not clicking with you, try to switch over to the other one and find how it addresses that same concept and see if that is something that resonates better with the way you need to learn.

Once you have these courses under your belt, you're going to need a reference that you can go back to if you just forget something and you need a refresher or if you're using something that you know is part of HTML or CSS, but you've never really used it before, you need to see maybe an example of how it's used. And for that, I always turn to MDN. MDN has the best documentation on HTML, CSS, and Javascript that you can find anywhere. I refer to this constantly to get various CSS properties straight, to understand what attributes different HTML elements have and how to use them… all those sorts of things. I'm constantly referencing this and searching through it almost every day.

That should give you all the tools you need to learn HTML and CSS. Here's a challenge I want to leave you with. Reading these books, watching these videos… it's a lot of fun. You're going to learn something new. You're going to be able to create something and you're going to get this little hit of endorphins when you accomplish something that's in the book or in the video, that just makes you feel great. Your challenge is as soon as you're able, you need to start breaking outside of the walled gardens that these resources create for you.

I said I really liked that the video had an assignment, and I do. I think that's a really cool way to learn, but that assignment is, it's basically already canned up. It's already boxed up and delivered to you. It's ready to go. The real learning happens when you sort of go out into the frontier.

Imagine a project that you are excited about doing and that is not in any way solved, and then start to apply what you've learned to that project. Pepper your time in the courses with your own independent work where you apply what you've learned, where you lock in that knowledge you've gained by applying it to something that's outside the confines of what they've already shown you in the course.

When you're done with the courses, build a project that puts everything you've learned together. It needs to be a small project. You don't want it to take very long. It needs to be something you're excited about – that will help you keep going when you get stuck – and it needs to be unscripted. It shouldn't be, "I found this tutorial on how to build another to do list app, and so I did that." It should be your own project.

You might decide you want to build your portfolio, or maybe you want to start working on a passion project that is really interesting to you, or maybe you have a business idea and that's why you're learning this and that's what you want to start building.

If you have trouble getting inspired on what to work on, I'll link you to a list I made of 10 web development project ideas. You're welcome to just lift one of those, or you can use it as inspiration to sort of drive your own idea. Maybe you want to tweak it a little bit, or maybe you just take the kernel of it and build your own idea around that.

You might start dipping your toe into independent project work by just taking an existing webpage and trying to rebuild it. The first one of these I did when I really got serious about learning this stuff and doing it professionally was Google. I just took the Google homepage and rebuilt that in HTML and CSS.

If you pull up the Google homepage and look at it, it looks super simple and it is, but when you actually start trying to build that yourself, you're going to hit some walls despite how simple it looks and that's good for you. That's exactly what you need. You need to hit those walls and figure out how to get around them. Once you're a web developer, that's basically what your job is.

That's HTML and CSS. In the next step of the roadmap, we're going to be talking about how to learn Javascript. Thanks for watching the video. Subscribe so you don't miss the rest of the roadmap, and I will see you in the next one.

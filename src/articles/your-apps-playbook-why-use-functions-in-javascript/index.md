---
title: "Your App's Playbook: Why Use Functions in Javascript"
date: '2019-07-12'
tags:
  - 'accessibility'
  - 'javascript'
  - 'mentalhealth'
  - 'opensource'
coverImage: 'images/football.jpg'
description: 'Functions in programming are like plays in sports: theyʼre sets of instructions that can be repeated whenever theyʼre needed. Hereʼs why theyʼre useful and important.'
---

## Functions Are Like Plays In Football

![Quarterback dropping back to throw the football](images/football.jpg)

I rarely dip into sports analogies to explain web development concepts, but _this_ one is the real MVP. 😜

A play in football is a sequence of actions executed in basically the same way every time. One of the most famous plays is the "Hail Mary." I'm not much for football, but, as I understand the play, all the receivers run as fast as they can toward the end zone and the quarterback throws as far as he can to one of them.

The "Hail Mary" is a simple play, but plays can be much more complex. If you have a complicated play with multiple steps and decisions to be made as it progresses, it's much easier if the coach and the players have **a simple way to refer to it**. That makes it easy to call it over and over without having to list out all 15 steps every single time.

A function in Javascript lets you name several steps in your Javascript code. You can use this name to tell people something about what the steps are intended to do. It also lets you **easily execute the same steps over and over**. (Yes, Javascript even uses the same word — "call" — to talk about running the code in a function that sports use when talking about repeating a play.)

## Updating Plays

Imagine a football coach discovers one of the team's plays doesn't work very well against an upcoming opponent and wants to modify the play. They could now, instead of calling the old play, call for a time-out each time they want to run the play and explain all the new steps. That might work, but it would be **pretty cumbersome**.

Instead, they could tell all the players about the modifications to the play and update the playbook at practice _before_ the game. This would allow them to **call the same play by the same name but get the new play execution**.

Javascript functions give you a similar advantage. If you need to change the steps in your existing function, you can change them **in one place** — inside the function — and every other call of the function will use the new steps.

## Sharing Plays

The coach has the flu and will miss the next game. How can we allow the assistant coach to use our existing plays? We can either pass along the playbook, or we can rip out individual plays to share. Since they are written down and named, **they're easy to pass around**.

Javascript allows you to pass functions around in the same way. Sometimes in Javascript, you'll trigger some asynchronous code — that's code that takes a while to complete and will keep running in the background while the rest of your app keeps going. It's common to then need to **do something with the result** of your asynchronous operation.

One way to do this is by passing a function into your asynchronous function call. When the asynchronous operation is done, **it will call the function you passed to it**. It's kinda like sharing a play for someone else to try later. There are other cases where you'll use functions like this (often called "callback functions"), but this is an example you'll use often.

## But How?

This is the first of a multi-part series on Javascript functions. The next article will show you **the many ways to declare a function**.

<!-- **Sign up below** to get notified when that article goes live. 👇

[thrive_leads id='1366'] -->

---
title: 'Why Web Development is a Great Career'
date: '2018-09-23'
coverImage: 'images/call-center.jpg'
description: 'Web development is the best gig Iʼve ever had. I donʼt even have to get permission to go to the bathroom anymore. What a time to be alive! I think youʼll like it too. Hereʼs why.'
---

Most web developers go to college, get a degree, and find an entry-level job writing code. My career trajectory was much different. In my last job, my boss routinely told me that **my modest salary was too much**. Before that, I fielded about **80 phone calls per day** from angry customers. In a prior role, I called my boss to tell her I couldn't make it in due to snow. Her response was, **"Well, you'd better try."**

![People working in a call center](images/call-center.jpg)

My stories aren't the nightmarish ones some people suffer in the workplace, but they're still demoralizing. They still led to an existential crisis every Monday morning as I asked myself, "Is this all there is?" My stories are probably similar to many of yours.

Luckily, that's *not* all there is. If you're coming from experiences like these, a career in web development will seem like some kind of fairy tale fantasy. Even if you're coming from a pretty good job, you'll likely find web development careers come with some perks that are almost too good to be true.

## Autonomy

The thing that surprised me most about my adult career was how similar it was to my days in middle school and high school: someone standing over my shoulder, telling me where to be and when, giving me permission to use the bathroom, scripting my solutions to each problem I might encounter…

When I started my web development career, I went from one extreme to another. [Since I started my career with freelance work](/articles/inverted-career-path/), I went from having very little choice to making every decision on my own. Here are a just a few decisions I needed to make:

- how to structure my business
- where to source clients
- what kinds of work to pursue
- how much to charge
- how to bill clients
- when to start work
- where to work (both in the world and in my house)

I did well enough making these decisions to build a successful freelance practice. This led to a _new_ decision when a client asked me to come to work for them full-time. I was afraid this would mean the loss of all my autonomy, and I almost didn't accept it as a result.

What I found as an employee was even _more_ surprising than my initial discovery. I had to give up _some_ of my decision-making since I was no longer running the business, but, otherwise, **I still had loads of autonomy**. I was able to work from home, decide _when_ to work, and make decisions about the best ways to solve the problems I was working on. I even got to weigh in on higher-level decisions about product direction.

## Creativity

Your job as a web developer is to **understand the problem you're trying to solve and to use the tools you have to build your best solution to that problem**. You have quite a bit of leeway in _which_ tools to use for a given project and _how_ to use them.

If I have a client who needs help getting more leads, that could take many forms. If they already have a web site, I might help them implement email capture so they can continue talking with people who visit their site. These people might eventually become leads. If the client doesn't have a web site, I might build them one. If their audience doesn't generally look for information on the web, I might build an app that scrapes web sites to find phone numbers of potential leads and adds them to a CRM so my client can call them.

Any of these could be an excellent solution. Any of an infinite number of alternatives might also be great. As developers, we get to **flex our creativity** to find the best one and solve the problem.

## Opportunity

In [the most recent projections by the US Bureau of Labor Statistics](https://www.bls.gov/ooh/computer-and-information-technology/web-developers.htm#tab-6) in 2016, web development employment was expected to grow 15% by 2026. That's more than twice the average across all careers. This means that, _if_ you want permanent employment, those opportunities are available.

If you _don't_ care about full-time employment and want to do freelance work or consulting, you can create unlimited opportunities by helping businesses make more money. If you find a repeatable way to do this, you'll be able to find a limitless supply of takers.

## Pay

According to the BLS, [web developer salaries are nearly twice the average of all jobs](https://www.bls.gov/ooh/computer-and-information-technology/web-developers.htm#tab-5). They put the web developer average at $67,990. That tracks pretty well with my own experience. In my first year of freelancing, I nearly doubled my income compared to my previous job.

[StackOverflow's yearly developer survey tells a much more lucrative story](https://insights.stackoverflow.com/survey/2018#salary) for web developers in the US: across almost 14,000 respondents, front-end developers averaged **$93k**, full-stack developers averaged **$100k**, and back-end developers averaged **$102k**.

On the freelance side of the equation, **the sky is the limit**. I started my own rate at $40 an hour and, over the course of about a year, bumped it up to $75. I now try to stay away from hourly rates. Accomplished consultants billing based on value routinely command effective hourly rates of $400+… and their clients happily pay it.

<!-- [thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

## Satisfaction

According to the StackOverflow developer survey, **72%** of developers were satisfied with their careers. This isn't particularly surprising since we've already explored the reasons developers are more satisfied. What _is_ surprising is the gap between developer satisfaction and overall job satisfaction in the US. [Only 51%](https://www.wsj.com/articles/u-s-workers-report-highest-job-satisfaction-since-2005-1535544000) of US workers report they are satisfied with their jobs.

You're bound to know some web developers who hate their jobs. Also, not all web development jobs will come with each and every one of the advantages mentioned above. What we _can_ say is that web development careers carry these advantages at much higher rates than most other careers.

Job satisfaction is the culmination of all the other advantages. When you have **autonomy**, can be **creative**, have lots of **opportunity**, and are **paid well**, you tend to be satisfied with your work. This is why, if you're dissatisfied with your work, you should consider a rewarding career in web development that will help you [achieve your life goals](/articles/stay-motivated-learning-web-development/)… and **be happy while you do it**.

---
title: 'Superscript your ordinal numbers (2nd, 3rd, etc.) in WordPress post meta data'
date: '2013-07-12'
tags:
  - 'ordinals'
  - 'post-meta'
  - 'styling'
  - 'typography'
  - 'wordpress'
coverImage: 'images/winners-podium.jpg'
---

Here we have an example of WordPress post meta data that uses an [ordinal number](<http://en.wikipedia.org/wiki/Ordinal_number_(linguistics)>) for the date. In this case, November 2**nd** (that's the ordinal part), 2012.

![WordPress meta info with unstyled ordinal](images/unstyled-meta.png)

You may have noticed, though, that it's really ugly. The "nd" is much taller than the number, and it's not superscripted as we'd expect it to be. The whole thing just looks like a mess.

Typically, the suffix of the ordinal number is raised it above the baseline of the rest of the text. This is how we're used to reading these numbers, but, if you want that effect in WordPress, you'll have to add it yourself since the CMS doesn't do it for you.

[thrive_leads id='1366']

To do this, you need to wrap the suffix in `<sup>` and style that accordingly in your stylesheets. Wrapping the suffix involves some escaping to prevent WP from interpreting the tag as part of the format string. Actually, it involves a great deal of escaping as shown in this example:

```php
<div class="meta">
 <h6><strong>Posted on</strong> <?php the_time('F j\<\s\u\p\>S\<\/\s\u\p\>, Y') ?> <strong>by</strong> <?php the_author() ?> <strong><?php comments_popup_link( 'No Comments', '1 Comment', '% Comments', 'comments-link', '' ); ?></strong></h6>
</div>
```

Here, the capital 'S' is the ordinal part of the number. This gives you the 'nd' behind your 2 or the 'rd' behind your 3. To get this superscripted, you need to precede it with `\<\s\u\p\>` and follow it with `\<\/\s\u\p\>`. These are, respectively, the escaped opening and closing `<sup>` tags which will pass through the format string and make it into the final page output.

I also reduced the font size to 0.5em to make the ordinal text a bit smaller than the rest. The result is much nicer than the unstyled default version:

![WordPress meta info with superscripted ordinal](images/styled-meta.png)

---
title: 'Take the Upper Hand in Your Salary Negotiation'
date: '2020-06-19'
tags:
  - 'career'
  - 'freelancing'
  - 'negotiation'
  - 'salary'
coverImage: 'images/salary-negotiations-thumb.jpg'
description: 'Itʼs not easy to negotiate salary, especially when youʼre a new developer. In this video, you wonʼt learn ways to trick people into paying you more than youʼre worth. Youʼll learn to actually flip the power dynamic by being able to walk away if you donʼt get exactly what you want. 💰🏖⏳ '
---

https://youtu.be/q9ZEu2ktOCU

## Video Notes

It's not easy to negotiate salary, especially when you're a new developer. In this video, you won't learn ways to trick people into paying you more than you're worth. You'll learn to actually flip the power dynamic by being able to walk away if you don't get exactly what you want. 💰🏖⏳

<!-- If you're not sure how to start your freelance business, take my free course to teach you how to get everything set up. 👇

[thrive_leads id='2262'] -->

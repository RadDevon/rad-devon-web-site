---
title: 'Build an HTTP client and juggle Async calls in Node'
date: '2020-04-24'
tags:
  - 'asynchronous'
  - 'backend'
  - 'javascript'
  - 'node'
  - 'node-js'
  - 'nodejs'
  - 'tutorials'
coverImage: 'images/learnyounode-3-thumb.jpg'
---

We started learning [Node.js](https://nodejs.org) together in [a recent video](/articles/first-steps-with-node-js-part-1/) and followed that up by [building our own Node module](https://youtu.be/a35evCZbLoM). We'll continue through the Node.js workshop in this video by building an HTTP client and learning how to manage multiple asynchronous calls that are dependent on one another. We'll also get some practice refactoring by revisiting a solution that worked but wasn't very maintainable.

https://youtu.be/HnbTXHblARk

## Video Notes

❗️ If you missed the first two videos, go back and watch them before you try your hand at these exercises.  
📺 [Part 1](https://raddevon.com/articles/first-steps-with-node-js-part-1/)\- We dip our toe in learning how to deal with I/O and how to use core Node modules.  
📺 [Part 2](https://raddevon.com/articles/first-steps-with-node-js-part-2/)\- We use an array method to filter a directory listing and try our hand at building our own module.

📝 We're working through the excellent [Learn You Node workshop](https://github.com/workshopper/learnyounode) together.

📚 As you work through the exercises, use the [Node.js documentation](https://nodejs.org/dist/latest-v12.x/docs/api/) to help you understand how to use Node's core modules.

👋 Thanks for watching! Leave your questions [on YouTube](https://youtu.be/HnbTXHblARk) or [on Twitter](http://twitter.com/raddevon). Look out for upcoming videos in which we'll work through the remaining exercises together.

Hey, I've been making these tutorials. They're fun for me, and they're probably fun for you too. Just make sure you don't fall into the trap of working through [tutorial after tutorial](/articles/stop-doing-coding-tutorials/) while never getting any closer to your ultimate goal of becoming a web developer. One way to do that is by [**doing freelance work**](/articles/inverted-career-path/).

<!-- , but how do you price it? I wrote the book on the subject! 💰 Get a free chapter by entering your email address below. 👇

[thrive_leads id='2463'] -->

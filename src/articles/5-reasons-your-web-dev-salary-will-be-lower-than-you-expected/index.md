---
title: '5 Reasons Your Web Dev Salary Will Be Lower Than You Expected'
date: '2021-09-11'
tags:
  - 'career'
coverImage: 'images/5-reasons-low-salary-thumb.jpg'
---

You've heard about the ridiculous $300,000 salaries at FAANG, and you're here for that. You've probably also heard from that one friend who makes $15/hour and works 60 hour weeks maintaining some old-as-the-hills web app. You want to be as far away from that as possible.

Chances are, you're not going to hit that $300k mark on your first gig, but you _can_ do a lot better than $15/hour. Here are some factors that would keep your salary lower than it should be.

![Woman shocked about how little money she has while a man looks at her angrily](images/5-reasons-low-salary-thumb-1024x576.jpg)

1. **You're not in a tech hub.** Tech salaries are high, but they're much higher in the big tech hubs like San Francisco, New York, and Seattle. I grabbed some data from [Levels.fyi](https://www.levels.fyi/?compare=Google,Facebook,Microsoft&track=Software%20Engineer) and filtered it down by location. The average entry-level web developer salary on that site in Seattle was **$145k**. If I filter out all of the tech hubs and leave only other US cities, that drops way down to **$104k**. (Don't freak out. These numbers are much higher than average. They are self-reported by either high performers or good negotiators, primarily going to work for big tech companies. They _do_ give us a good basis to compare the salaries by locations though.)

   Even in this temporary age of enlightenment around remote work, **you still get paid more in the hubs**, especially by these companies... mostly because they intend to herd you back into the office one of these days. This doesn't mean you should move to a different city to make more money. Live where you want to live. Just realize that you'll probably be paid less if you live outside a tech hub. Your cost of living will probably be lower too, so hopefully everything balances out.

2. **You're not comfortable negotiating.** Companies give you an offer knowing you're probably going to [negotiate](/articles/take-the-upper-hand-in-your-salary-negotiation/). You don't get their best offer out the gate. If you take the first offer, that means you're leaving money on the table – possibly a good bit of money. We're talking $10,000 or more.

   So, either resign yourself to some negotiating or resign yourself to earning less. Negotiating isn't as intimidating as it could be. You might give yourself a few test runs where the stakes are lower. Negotiate at the furniture store, with someone providing you a serivce, or when you're buying something on Craigslist. You'll never know what you can get unless you ask.

   My best tip for negotiating is this: **most negotiations end up right in the middle of what the two parties are asking for**. If the other party has thrown out the first offer, add at least double the minimum you will accept. If they offer a $70k salary and you can't accept less than $90k (a totally reasonable floor in cities like the tech hubs mentioned previously), ask for $110k. Once you're this far, there's very little chance they're going to walk away from you unless you tell them something buck wild like you have to have $1 billion dollars a year. The worst that's likely to happen is they tell you they $70k is their high offer – take it or leave it.

3. **You don't know algorithms and data structures.** Let me tell you a quick secret: I don't really know that stuff either and I really haven't needed to. The sad truth, though, is that the highest paying companies are **shoving square pegs through LeetCode-shaped holes** as part of their interview processes. Carve yourself into that shape by practicing problems on your own or you won't make it past the second round, third round… whichever round it is, you won't make it past it.

   Yes, it's silly. Yes, it's impractical. Yes, it doesn't reflect what you're actually doing day to day. Everyone on every side of the equation knows this, and it's still the best way these company have come up with to filter through the hundreds of people applying to each of their openings. You can choose to play their game, [find a company that doesn't do this sort of thing](https://github.com/poteto/hiring-without-whiteboards), or skirt around it by focusing on reason 4.

4. **You don't have any experience.** If you have [zero experience](/articles/how-to-build-your-web-developer-resume-with-or-without-experience/), most companies won't look at you because they can easily hire someone who *does* have experience. If they do look at you, you're not going to be able to command the salary of someone who *has* some experience. More likely, you just won't get hired at all because **there's always someone out there with some experience**, and any experience is better than none.

   Some people learn this and believe this means it's impossible to get into the industry. It must be based on bloodline or something like that. The actual truth is that you can [get experience *without* a job](/articles/get-a-web-development-job-without-experience/). You can do this a lot of different ways, like by [volunteering](https://raddevon.com/articles/get-coding-practice-while-doing-good-by-volunteering/), through internships (which are kinda jobs but not quite), or by [doing your own projects](/articles/where-do-web-development-project-ideas-come-from/).

   But experience won't usually let you *skip* the line, so you're still stuck doing LeetCode… **unless** you [get your experience through freelancing](/articles/inverted-career-path/). In my career, I've done mostly freelancing, but I've also had two full-time positions. Each of those positions evolved out of freelance gigs. I didn't have to take tests or interview to get those gigs, and they evolved into full-time gigs when the clients needed more help. That led to them making me offers. That means I got great gigs without having to stumble through LeetCode exercises. It's the best of both worlds!

5. **You aren't job hopping.** This isn't our parents' (or, for you youngsters reading this, grandparents') world. People don't get a job at 22 and retire from it 40 years later. Most tech companies aren't loyal to employees and, for some reason, don't do a lot to retain them. What they *will* do is pay big bucks to poach them from another company. It makes no sense for us to wilt away on a substandard salary out of some one-sided sense of loyalty when there are plenty of fish in the sea.

   I don't do much job hopping myself because I'm not really interested in full-time employment as a rule, but if climbing the career ladder *was* my game plan, I'd probably be shopping my resume around every year or two, just putting out feelers to see what opportunities are available to me and to see how well my current salary stands up in the current job market.

   If you find something better, that doesn't necessarily mean you have to leave. If you love where you work, you might just want to use other offers as leverage to negotiate a salary increase.

[thrive_leads id='4451']

For some people, the highest possible number on their paycheck is important on on its own. For others, their salary represents more options. You don't have to try to min-max every one of these factors to get the biggest paycheck possible. (That's definitely not _my_ top priority.)

Even so, it's a good idea to be aware of the factors that determine your pay. It allows you to **pick and choose what's important to you** and to be sure you're getting paid what you deserve for your web development expertise.

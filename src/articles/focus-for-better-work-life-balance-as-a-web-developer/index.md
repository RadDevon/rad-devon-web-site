---
title: 'Build a Focus System for Better Work/Life Balance as a Web Developer'
date: '2018-05-07'
tags:
  - 'freelancing'
  - 'productivity'
coverImage: 'images/earthbound-not-good-to-work-too-hard.gif'
description: 'An unproductive day leads me to feel like I need to keep working. A focus system can help your days as a developer feel productive, freeing up your personal time for things you enjoy.'
---

_This article will teach you to build a system for staying focused on the task at hand. I make some software recommendations for macOS and iOS apps. I use the recommended apps every day, so I feel comfortable suggesting them. If you use another platform and have suggestions for alternatives, tell me about it [on Twitter](https://twitter.com/raddevon). I'll update the post with your suggestions._

_Let's get started!_

![Sage advice on work/life balance from Ness's dad](images/earthbound-not-good-to-work-too-hard.gif)
[//]: # (Make this text a caption)
Sage advice on work/life balance from Ness's dad

If you don't maintain some kind of **work/life balance** — even in a career as enjoyable and satisfying as web development — you're flirting with **burnout**. Burnout will leave you feeling disenchanted and unproductive for days or weeks, so it's better to head it off before it sets in by **proactively maintaining some balance** in your life.

This can be especially challenging for web developers because we can work from home. In this scenario, we can't escape the environment which, for much of the workforce, serves as a sort of container for their personal lives. What happens when that container and the workplace are merged?

**Focus** has been my best tool for maintaining the balance.

## Why Focus?

If you're never fully focused on one activity, you're going to have to work all the time _and_ do life all the time because you're not going to be effective at either. The better approach is to fully focus on each activity in turn.

Define your working hours and, during those hours, resist doing much else besides working. Once your daily work hours are over, allow yourself to focus on personal relationships, hobbies, fitness, and winding down. That doesn't mean you _can't_ do work-like tasks during your personal time. It doesn't mean you _can't_ do personal errands and tasks during your work hours. It means those instances should be the exception, not the rule.

## A System for Focusing

**The Pomodoro Technique** is an effective tool to help you. You divide your work into cycles which look like this:

1. Work without distractions for a number of minutes
2. Allow distractions for a much shorter period
3. Repeat

After 4 of these cycles, you should take a longer break. The prescribed times for the technique are 25 minutes of work followed by 5 minutes of break with longer breaks lasting 30 minutes, but you should adjust those to suit your work. I've found they don't allow me to get into "flow" properly. I'll sometimes double the times — 50 minutes of work, 10 minutes of break, and 30 minute breaks every 3 cycles instead of 4 — and, if I'm in the zone, I'll push the work time out even further, sometimes to 90 minutes.

If you want to build and start using your own focus system, download the focus checklist below. 👇

[thrive_leads id='1371']

## What does "without distractions" mean?

**Eliminate any technology that gives you notifications.** You generally shouldn't be taking calls, checking emails, checking Twitter, Facebook, or other social networks. I close all apps that generate notifications on my Mac, and I Option-click the notifications icon on the menubar to silence notifications in the OS.

![Demonstration of disabling notifications on Mac](images/turn-off-notifications-mac-os.gif)

I set my phone on vibrate and turn it over so I can't see the display. If you do it this way, you'll have to resist turning it over when it vibrates. You may instead want to use your phone's "do not disturb" mode so that you don't get any notifications at all and can still allow emergency calls from family through.

## Managing Expectations

The hardest part of this is **making it clear to the people around you** when you will be available for them and when you are working. It's best to do this ahead of time rather than to do it on the spot. (That leads to some frustration on both sides.)

When I'm working from home, I try to make sure my family knows when I'm about to start a Pomodoro. They know they can't bother me during that time unless they have an emergency. If they have something that isn't an emergency but can't wait until after work, they can catch me between Pomodoros or during a long break.

I like to use **a Pomodoro timer that ticks** to remind me when I'm inside a distraction-free work time. You _will_ stray and open your email or Twitter from time to time. The ticking should remind you to come back to the work zone. Just know **the next break is not far off** and batch up your distractions for those times.

## Focus in All Areas of Life

Focus needs to happen not just around work but in **your personal life as well**. When you're spending time with your family, be fully available to them. Again, don't be checking your phone, watching TV, or skimming your Facebook feed. If those things are important, set aside time to focus exclusively on them. **The less you divide your attention, the more effective and well-rounded you will be.**

## Tools

To keep the distractions from leaking into your work, you'll need **willpower**, **a timer**, and **music**.

### Willpower

You'll need some help putting focus to work for you. The first tool is **willpower**. Willpower is a muscle. You have to work it to make it stronger, and it gets tired. The Pomodoro technique is nice because you can remind yourself the next break for you to check your email and notifications is just around the corner. The ticking I mentioned before will remind you that you shouldn't be engaging with distractions right now.

Practice timeboxing your phone use not just during work time but during your personal time as well. Don't look at your phone every time you're bored. Set a specific time for Facebook, Twitter, email… whatever your digital compulsions happen to be. Exercise your willpower by freeing yourself from them outside their timeboxes.

### Fohkuhs

![Fohkuhs for Mac](images/fohkuhs.png)

This is the best Pomodoro timer I've found for the Mac. [Fohkuhs](http://www.fohkuhs.com/) has ticking, customizable timers, a configurable long break interval, a daily goal, and tracking for two weeks of stats. It's nice to look at and it's free.

### Music/Noise

Music can serve as a distraction, or, if used correctly, it can be a great focus tool. If you work in an open-plan office, this is probably essential unless you're great at filtering outside stimuli. Put on a pair of noise-canceling headphones (I use [the Sony MDR100ABN/B](https://www.amazon.com/gp/product/B01CQXGM5K/). Catchy name, right?), and block out your distractions.

As far as _what_ you should listen to, I'd recommend something without vocals. I find that, when I try to listen to music with vocals while working, it engages the part of my brain that processes language which hampers my ability to write code or read documentation. That effect is lessened if the music with lyrics is extremely familiar in which case, it's less likely to hook into me.

I've been listening to some [Darksynth](https://open.spotify.com/user/madjayspink/playlist/5FR3ZNcfdabJYF6cfNrq0y) music, which I'd describe as 80s synth with a horror vibe. Find something that appeals to you. I've also found great success with noise generators. I bounce between three different noise generator apps on iOS:

- [Noizio](https://itunes.apple.com/us/app/noizio/id960716999?mt=8)
- [TaoMix 2](https://itunes.apple.com/us/app/taomix-2-relax-sleep-focus/id1032493819?mt=8)
- [White Noise](https://itunes.apple.com/us/app/white-noise/id289894882?mt=8)

Gregory Ciotti wrote a great post on the [effect of music on productivity](https://www.helpscout.net/blog/music-productivity/) over on the [HelpScout blog](https://www.helpscout.net/blog/) that exposes some of the research on this topic and offers more music suggestions.

## Engage Fully With Every Task

The common ground all these suggestions share — the Pomodoro technique, eliminating distractions, working with the people around you, and the tools that facilitate them — is that they all help you **focus fully on one activity**. If you half-ass your work while watching TV and talking on the phone, the quality of your output will suffer. If you spend time with your family while checking Facebook or playing Clash of Clans, your relationships will suffer.

Decide what you're going to do right now, and do that thing. Make sure every action you take contributes to doing that thing better. This will allow you the room in your life for balance, and you will outperform "multi-taskers" every single time.

Ready to get your focus system set up? I made checklist to guide you through building your own system. It outlines everything you need to do to **get started**, what you need to do **every day**, and what you need to do **before each Pomodoro cycle**. Use the form below to get your copy.

[thrive_leads id='1371']

---
title: 'Learn Svelte by building a habit tracker app 🏎'
date: '2021-05-03'
tags:
  - 'css'
  - 'html'
  - 'tutorials'
coverImage: 'images/svelte-habit-tracker-thumb.jpg'
---

In this video, you'll work along with me to build a habit tracker app in Svelte. In under an hour, you'll learn:

- 🔸 Components
- 🔸 State
- 🔸 Rendering arrays
- 🔸 How reactivity works
- 🔸 Props
- 🔸 Stores (with auto-subscription)

Since Svelte is mostly vanilla Javascript, you'll want to make sure you're on top of your Javascript game. Grab my cheat sheet and keep it handy as you work.

[thrive_leads id='3815']

https://www.youtube.com/watch?v=ecGJ496lUFg

If you have any questions, please let me know [on Twitter](http://twitter.com/raddevon). Good luck on the build, and enjoy learning this awesome framework!

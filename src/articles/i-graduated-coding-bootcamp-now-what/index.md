---
title: 'I graduated coding bootcamp. Now what? 💸'
date: '2019-01-11'
tags:
  - 'bootcamps'
  - 'career'
  - 'freelancing'
  - 'start here'
coverImage: 'images/dejected-in-rain-wide.jpg'
description: 'Bootcamps are turning out more junior developers than the job market can absorb. If youʼre one of those, hereʼs what you can do to get into the industry despite all the competition.'
startHereDescription: 'You probably think you get a job by first training and then applying to jobs until you get hired. That can work, but you may be surprised how many you need to apply to before finding one. Iʼve talked to plenty of people who have put in hundreds of applications and still not gotten a bite. This article will calibrate your expectations, whether you learn with bootcamp, teach yourself, or even get a degree.'
startHereOrder: 1
---

What do you do when you've spent thousands of dollars on coding bootcamp, but now you can't find a job? Here's **a story of a friend of mine who was in this very spot**, **my take on why this is happening right now**, and **how you can beat the odds**. 🎰

![Person sitting on a bench in the rain looking dejected](images/dejected-in-rain-wide.jpg)

[Bradley Serlis](https://www.bradleyserlis.com/) was working in human resources at various tech companies in the Bay Area. He always felt like an outsider. The companies he worked for made really cool products, but **he was so far removed from actually building those**, he may as well have been working somewhere else. He longed to get closer to the product, to build something himself, and to feel like he was really part of the team.

His girlfriend got an awesome job offer in Seattle, and she took it. This put Bradley at a crossroad. He was going to be moving, and he’d need to find a new job in Seattle.

Rather than let his misgivings about his current line of work continue to gnaw at him, Bradley decided to take a leap. He would step outside his comfort zone in HR and change careers. **He wanted to be a web developer.** Seattle, being a major tech hub, seemed like the perfect place to do it. And, since he’d be looking for work anyway, this seemed like the perfect time.

He’d already talked to engineers at his company and told them he wanted to be a developer. They recommended *he* take the path *they* had. Bradley took their advice and **enrolled in a coding bootcamp**.

Bradley quit his job in January to prepare for the move. The next bootcamp cohort started in March. He paid roughly **$14k** toward admission and a new laptop. He then invested **the next 3 months** doing bootcamp full-time. Class ran 8:00am to 4:00pm every day. They usually stayed until 6:00 or 7:00 working on projects. Then, they’d design new projects on the weekends.

He was working more than he had in his full-time HR job, but he was fine with that. This would all be worth it to have the life he wanted: building things instead of doing HR. He’d finally get to get his hands dirty, and maybe even improve on his HR salary.

Bootcamp had prepared Bradley and his cohort that they might not get jobs right away. They said most people took 3 months. Some took up to 5 months to get those first jobs as developers. Bradley did everything they suggested after he graduated to maximize his chances of success. He’s nearly hit the 5 month mark, and **still hasn’t had a single offer**.

He’s had close calls. Interviews, recruiters showing interest, great conversations that seemed like they could go somewhere… but didn’t. These are almost harder to handle than the immediate “no”s since his hopes soar only to come crashing down.

There could be any number of explanations for Bradley’s difficulty. Maybe Bradley picked the wrong bootcamp. Maybe he’s focusing his efforts on the wrong things. Maybe he’s just not working hard enough.

Those explanations are seductively simple, but I’d argue the problem **goes way beyond Bradley, his bootcamp, and any other individual factors**. The numbers suggest **this problem is much deeper**.

### Bootcamps and Career Transitions

Despite the glowing placement numbers bootcamps tout in their marketing material, the job market is **not all sunshine and rainbows** for coding bootcamp grads. [According to StackOverflow’s 2018 survey](https://insights.stackoverflow.com/survey/2018#developer-profile-finding-a-job-after-bootcamp), only 33.8% of attendees came into bootcamp *without* developer jobs and were able to get developer jobs within 3 months of graduating.

Survey respondents are a self-selecting group of users who are still reading StackOverflow. It *doesn’t* include the grads who gave up altogether and went back to being baristas or working retail. Those grads wouldn’t have known about the survey. The problem **may be even more depressing** than the survey reflects.

The bottom line is that making a career transition by attending a bootcamp **is not as easy as putting down the cash and doing the work**.

### Peak Bootcamp 🗻

The problem is three-fold:

1. **No one wants to hire a junior developer.** Of about 157,000 developer jobs on Indeed right now, just over 200 of them contain the phrase “junior developer.”
2. **More developers are coming out of bootcamps every year.** In the 2015 StackOverflow survey, [3.5% of respondents were educated in coding bootcamps](https://insights.stackoverflow.com/survey/2015#profile-education). By 2018, [the percentage had skyrocketed to 10.3%](https://insights.stackoverflow.com/survey/2018#developer-profile-other-types-of-education).
3. **[Some employers believe bootcamp grads are not as prepared for work](http://blog.indeed.com/2017/05/02/what-employers-think-about-coding-bootcamp/)** **as computer science grads.** 17% of employers surveyed by Indeed in 2017 feel this way.

So, you have an extremely limited number of junior developer roles; you have more and more bootcamp grads competing for those roles; and you have employers who believe they aren’t as prepared as their university counterparts.

Put these three factors together, and it paints a clear picture of why **so few bootcamp grads are living the promise of new careers in web development** after they finish their programs.

### Death of the Bootcamp? ☠️

The promise of bootcamps is that you can change careers. If the chances of that actually happening are so low, the bootcamp is obsolete.

Not so fast. Despite the grim numbers, **I still believe in bootcamps**. They’re cheaper and more nimble than their university counterparts. It’s hard to understate the value of being nimble in this industry. Building a web application today looks very different than it did 5 years ago. Bootcamps can evolve quickly to reflect that while most university programs can’t.

Learning by yourself isn’t always a great option either. How will you know which resources are current and accurate? How will you know which skills to pursue next? Stumbling through your education alone **can leave you with significant gaps**, or, even worse, misinformation.

Bootcamps coupled with significant one-on-one mentorship **are still the best way to learn to code**, but you’ll have some challenges to overcome when you look for a job.

### The Odds Against You

Coming out of a bootcamp to get a full-time developer job puts you in a difficult position.

- You are being evaluated by other developers. You’re going to look deficient because **you have no experience** and their standards are high.
- Their motives for hiring are probably to **increase capacity on projects already in progress**. This is why they most often don’t want junior developers.
- You are being evaluated by developers **who went through gates you’ve chosen to bypass**. These people may still be paying off student loans on their computer science educations. You waltz in coming out of an $8k online bootcamp thinking you should start in the same position they started in. Some of them are going to be indignant that you would try.

The solution most people try is to overwhelm the universe with enough applications, cover letters, and schmoozing that you finally hit the career lottery and find a position. **I took a different path.**

### My Career Change Story

I graduated from a bootcamp myself about 5 years ago. Like most bootcamp grads, I hit the ground running, putting in applications. Like most bootcamp grads, I didn’t get any bites. _Un_like most bootcamp grads, I decided I might as well get some paying work while I waited for the right opportunity.

Freelancing was scary. I’d never gone without the apparent security of a full-time job with health benefits and the same paycheck every two weeks. I didn’t know what I was doing. I didn’t understand how to find clients, negotiate deals, position myself… **I was in unfamiliar territory.**

Once I got past that, though, I found freelancing exhilarating. I was doing what I wanted to do without asking anyone’s permission and without waiting to be picked. **I picked myself** and just started doing it.

A couple of months after I had started doing freelance work, I heard back from one of the companies I had applied to several months earlier. I went through their interviews, and they made me an offer. It was a remote position that paid $50k — **about 50% more than I was making in my previous role**in IT for a public school system, and **it was more than I was making freelancing**.

**I turned them down.** I considered the position, but it didn’t require much consideration. I had momentum in my freelancing business. I was doing work I enjoyed on my own terms. It wasn’t *all* amazing. I’m an introvert, and I don’t like selling myself. Freelancing requires a ton of that, but it was well worth the benefits.

They weren’t just asking me to do their job for $50k/year. They were asking me to do their job ***and* sell them my business**. The contract specified I couldn’t do other work on the side, but, even if it hadn’t, taking a full-time role effectively kills your freelancing business. (I know; I’ve tried doing both.)

It just wasn’t worth it, so I turned down the offer and kept going. **I was an accidental freelancer.** What had started as a side hustle to close the gap while I found a job had become the job. Aside from a short period of full-time employment, I’ve been enjoying the freedom and satisfaction from building and running my business ever since.

### Beating the Odds 🎰

What I didn’t realize right away is that I had unwittingly discovered a solution to a problem most bootcamp grads have to overcome: how to break into a new line of work with an intense barrier to entry and tons of skilled competition. **By picking myself, I short-circuited the gatekeepers that, for one reason or another, turn away bootcamp grads.**

The key was that **I changed the way I positioned myself**. You could very easily run into the same problem in freelancing. If you package yourself as a bundle of skills (“Hi! 👋 I’m React, Node, D3.js, HTML 5, CSS. Nice to meet you!”), **you’re only going to appeal to other developers**. They need more capacity temporarily for a project. You *might* be able to fill that… but first, let’s see if you can go to the whiteboard and write out a bubble sort. You’ve got **the same problem as before** (being evaluated by senior developers as a junior developer), but you’ve put it in a different context.

Even though I wasn’t up to a lead developer’s standards, I *knew* I could build things that people wanted. I just hadn’t found the right audience yet. I joined a BNI group and started hanging out in the local startup community.

These entrepreneurs had problems they needed solved, and, more importantly, they weren’t deciding based on whether my stack was going to match some legacy stack they already have in place or whether I’ve had 5+ years of Angular experience. Their problem is expensive, and **they only care that I can solve it for them**.

Besides that, none of the other bootcamp grads were talking to them. They were all busy submitting applications for yet another opening at Google, Apple, Microsoft, or Amazon.

### Stop Waiting to Be Picked 🙋🙋‍♂️

If this story resonates with you, I urge you to stop waiting to be picked and **pick yourself**. Here’s how to get started:

- **Find the people you want to work with and hang out with them.** You might have a very clear idea who that is (yoga studios, logistics companies, or realtors) or you may not be sure yet (in which case “business owners” or “marketing directors” might be as close as you can get). It’s fine either way. You can narrow it later. Go to where these people are, not to sell them anything, but to be friendly and be curious about what they’re doing. Listen for their struggles and offer help where you can.
- **Tell people what to do.** The more specific and relevant you can be, the better. Do not tell the owner of the carpet cleaning business that you write web apps using the MERN stack. You need something that’s meaningful to them. “I build marketing sites for small hotel chains,” or “I write software to automatically take actions based on emails you receive” are great because they’re understandable and relatable even though they’re not necessarily for the owner of the carpet cleaning business. They can refer you to their friends if they understand what you do.
- **Stop obsessing over technology.** Who is your audience? If you’re talking to engineers as a new grad, the chances you’ll work in the exact stack they want is slim. If you’re talking to people trying to run businesses, though, they don’t care what stack you run! Learn enough to build stuff that solves people’s problems. Then, focus on getting clients and getting even better at fixing those problems… not on learning every new framework and library that comes along.
- **Embrace the uncertainty.** [No one really knows what they’re doing](https://www.theguardian.com/news/oliver-burkeman-s-blog/2014/may/21/everyone-is-totally-just-winging-it). We’re all just figuring it out as we go. Be willing to look stupid and to make mistakes in service of building a better life for yourself. If you goof up, do what you can to make it right. Learn and get better the only way you can: by doing

<!-- If you’re ready to get going, I just launched **a week-long email course to help you launch 🚀**  **your freelancing business**. Sign up below 👇 to to get started.

LLC or S-Corp? What's the best business checking account? What should I do for a contract? This course will answer these questions and give you everything you need to start freelancing **in just a week**! I'll also share other helpful articles and resources to help you start your web development career! -->

---
title: 'Use Node.js to build a time server and a web server'
date: '2020-05-29'
tags:
  - 'backend'
  - 'dependencies'
  - 'javascript'
  - 'node'
  - 'node-js'
  - 'nodejs'
  - 'servers'
  - 'time'
  - 'tutorials'
coverImage: 'images/Learnyounode-part-4-thumb.jpg'
---

In this video, we're continuing through the Learn You Node workshop. We'll build a TCP time server which answers each TCP request with the current time and a web server that responds to HTTP requests with a file.

https://youtu.be/xya-s672b5M

## Video Notes

❗️ If you haven't done the other videos of this series, you might be lost. Start with the first video in the series.  
📺 [Part 1](https://raddevon.com/articles/first-steps-with-node-js-part-1/)

📝 If you need to install Learn You Node, you can get directions [on their repo](https://github.com/workshopper/learnyounode).

📅🕐 In the process of doing this, we'll also learn to use NPM to install and manage a dependency. We'll be incorporating [date-fns](https://date-fns.org/), a date utility library for client-side Javascript and Node.

👋 Thank you for watching and for working through the exercises with me! Look out for the final video in which we'll work through the remaining Learn You Node exercises together.

<!-- Now that you've gone through most of the Node tutorials, it's a great time to put your new knowledge into practice. If you're not sure what to build, I've created a project starter kit for a concept called Timeless. It’s a social network that allows only a single post per year. Get your starter kit below. 👇

[thrive_leads id='3587'] -->

---
title: 'Setapp is a software treasure trove for macOS web developers'
date: '2021-03-29'
tags:
  - 'macos'
  - 'software'
coverImage: 'images/setapp-thumbnail.jpg'
---

I've been testing [Setapp](https://setapp.sjv.io/c/2721426/344537/5114) for the past several weeks. It's a subscription that gives you access to a bunch of macOS software you'd otherwise have to buy individually.

I thought it might be cool, but I never anticipated what actually happened: **it has completely revitalized my workflow**. My workflow was your parent's 1980's kitchen. Setapp came through and ripped up the linoleum flooring, threw out all the Tupperware, and stripped off the wallpaper, swapping it all out for a more sleek modern look.

![Modern kitchen](images/setapp-thumbnail-1024x575.jpg)

Let me share with you some of the app gems I've found in Setapp and how they've improved my web development workflow – in some cases pretty dramatically.

## [DevUtils](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fdevutils.app)

This was the first app in the collection that surprised me. I was under the impression that most of the apps included would have broad appeal so they could try to sell the subscription to the broadest audience possible. I was pleasantly surprised to find some apps just for us developers.

![DevUtils screenshot](images/devutils.jpg)

This is a sort of Swiss Army Knife of developer tools. I'm constantly copy/pasting a bunch of JSON into one of those web sites that prettifies it for you or going to [Regex101](http://regex101.com) to test a regular expression. This app does those and more offline.

[Check out DevUtils](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fdevutils.app)

## [Proxyman](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fproxyman)

Proxyman is another tool that's great for developers. It lets you peek into requests your computer is making. This can be useful when you're developing an app and need to capture the requests it's sending to see if it's sending what you expect.

![](images/proxyman-1024x638.jpg)

It's also useful for capturing traffic from other apps. Maybe you need to make a request to an API that is poorly documented. The developer has an official app that works. You can capture the requests it's making and replicate them in your own app.

[Check out Proxyman](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fproxyman)

## [Sip](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fsip)

Sip is a global color picker. You can pick a color that's displayed anywhere on your screen. When you pick it, Sip will copy it to the clipboard in a format of your choosing. For me, that's usually a hexadecimal color code so that I can paste it directly into some CSS.

![Sip screenshot](images/sip-1024x640.jpg)

You can set a global shortcut key (I use Cmd-Shift-C.) that will bring up the color picker. I use this thing multiple times per week, and it makes my life a lot easier.

[Check out Sip](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fsip)

## [SideNotes](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fsidenotes)

What do you do if you're on a meeting or a phone call and need to capture a quick note? For the longest time for me, I would launch the note-taking app of my choice (Evernote for many years before Notion took over). The trouble with these apps is that they're not exactly lightweight. Even on a beefy MacBook Pro, they can take a few seconds to launch. That's enough time for the speaker to have already moved on to two or three additional ideas you also need to capture. More often than not, one or two of those is going to be lost or have to be revisited.

![SideNotes](images/sidenotes-1024x535.jpg)

SideNotes runs unobtrusively in the background. There are different ways you can summon the notepad. I prefer hovering over the edge of the screen and clicking on the small bar that appears. Once I do that, I have a quick and easy way to capture some text. It has some nice rich text support based on Markdown that is really handy when you need to structure the text. I'm using SideNotes almost daily to make sure I don't miss something important.

[Check out SideNotes](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fsidenotes)

## [ForkLift](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fforklift)

ForkLift is the app that got me to look at Setapp in the first place. It's a dual-pane file browser with some really nice features. Some you might expect, but my favorites are the pleasant surprises. For example, if you try to delete an app in ForkLift, it will helpfully find all the configuration files associated with the app and offer to delete them at the same time. People used to run entire apps to do just this, and now it's built right into your file browser.

![](images/forklift-1024x538.jpg)

I also stumbled upon the app's bulk renaming features which allows you to rename multiple files at once.

My most useful feature so far, though, is that the app doubles as a client for remote file management. It can connect to FTP/SFTP, Amazon S3, Backblaze B2, SMB shares, and several more to manage those files in the same UI you'd use for managing your local files. Just like you can favorite local file locations, you can favorite remote ones as well. You can sync files between your two panes, whether the locations are both local, both remote, or of mixed origin. Not only that, but you can favorite your syncs so that you can easily repeat them.

I was recently working on a project that was a pain to deploy, and this feature was a life saver. The deployment still wasn't automated like I might have wanted, but having those sync favorites set up gave me much less opportunity to screw things up.

It doesn't have all the bells and whistles of PathFinder (my previous favorite which is also available on Setapp), but it has been a much less buggy experience so far. I haven't once lost my favorites, nor have I lost my layout or view settings, all of which have been happening every 3-6 months in PathFinder. It seems like ForkLift's developers really have this nailed down.

[Check out ForkLift](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fforklift)

## [Session](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fsession)

Have you ever heard someone get really hyperbolic about how some piece of software "changed their life," and you can't help but think to yourself, "Really?" I'm about to become one of those silly hyperbolic people.

The pomodoro technique is a way of working that teaches you self-discipline. It teaches you that there is a time for working and a time for the distractions that leak into any cracks they can get into. It does this by asking that, during your pomodoro session, you not succumb to any distractions. It convinces you to do this by dangling the carrot of a break time at the end of the work session during which you can give into the distractions, again for a timeboxed period, before starting another working session.

I've had a lot of success with this technique. I [described the system I used](https://raddevon.com/articles/focus-for-better-work-life-balance-as-a-web-developer/) in an article a few years ago, and I was still using that one until recently. Well, I wasn't using it so much as I hadn't found a suitable replacement.

Two or so major operating system updates back, the app started throwing errors every time my computer starts. As far as I know, it's no longer actively maintained. Since I had access to all these apps anyway, I decided to give Session a shot.

![Session screenshot](images/session-1024x640.jpg)

Session has replaced my previous app, but it's also given me a lot of things I needed that I didn't know I needed. I like to have a ticking sound that runs during my session to serve as a subtle reminded to me that this is "no-distraction" time. Session has that. It also connects to my calendar so I can see my sessions alongside any events.

When I set my timer, it tells me when that session ends so I can plan ahead. By the way, it lets me set a default session time (My cycles are 50 minutes of work and 10 minutes of break – double the standard. I find this gives me a better chance to achieve "flow."), but it also lets me individually change the timing for a given session. This is handy when I have a meeting coming up in a half hour and want to squeeze in a 25 minute session beforehand.

I've been using the Waking Up app for guided meditations in the mornings. One thing the coach Sam Harris recommends is to make times of transition throughout your day times of mindfulness and awareness. I love this idea, but I can rarely execute on it… until I found session. The first two seconds of each session start with a circle that swells and then shrinks as it coaches you to breathe before the start of your session. This is enough to jog my memory and prompt me to make this a tiny moment of mindfulness. It's a fantastic feeling, both to reap the benefits of that and to be able to pat myself on the back for finally executing on it!

The last feature I'm going to gush about is super nerdy. Feel free to leave if you're already sick of reading this. It's not getting any better. 😉

Session has support for automation via AppleScripts. You can create scripts that hook into different times during your timers' lifecycle. I'm personally using the session_start and session_end scripts to perform actions at the times you'd suggest. I have a small colored smart lamp that lives outside my office. It's green to indicate I'm available and red to indicate I'm in a meeting. This lets my family know if they can come in and talk to me or not.

Using these scripts, my light, and a small app called [HomeControl](https://apps.apple.com/us/app/homecontrol-menu-for-homekit/id1547121417?mt=12) which allows me to script scenes in my smart home, my lamp now lights up orange when I start a pomodoro session and goes green when the session ends, indicating people can get my attention if they really need it, but they should avoid it if they can. It's been a real boon since my wife and teenage daughter are both at home, and we're all ticking time bombs just waiting to go off and interrupt the other's calls, meetings, or classes.

Since installing session, I've been massively more productive. I'm less distracted, my family has a greater awareness of what I'm doing at any given moment, and I'm able to stop for a second a breathe as I transition tasks. I can finish a workday feeling much better about what I've accomplished.

[Check out Session](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fsession)

## [Is it worth it?](https://setapp.sjv.io/c/2721426/343321/5114?u=https%3A%2F%2Fsetapp.com%2Fapps%2Fsession)

I'm a proponent of the idea that, since this is your job, you should spend money on tools that make it easier or better to do that job. I'm not in the "but I can do it for free, so I _must_ do that" camp.

Even so, it doesn't follow that every dollar you spend on tools is worth it or even goes the same distance. I've been shocked at how much I've gotten out of my $10/month for Setapp. I figured I would find maybe three apps that would be useful. Instead, I've got everything I listed above, and even about that many more that I didn't mention.

If you want to give it a shot for yourself, [Setapp](https://setapp.sjv.io/c/2721426/344537/5114) does offer a free trial before they start billing. Go ahead and give it a try before you commit. They do take your credit card up-front, but they're nice enough to email you before they start billing.

If you do give it a shot, but sure to try out Session and its implementation of the pomodoro method, if nothing else. [Mention me on Twitter](http://twitter.com/raddevon). I'd love to hear your thoughts on it or how you've tweaked it to work for you!

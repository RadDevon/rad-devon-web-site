---
title: 'How to Deal with Nightmare Clients 🎃'
date: '2020-10-16'
tags:
  - 'career'
  - 'freelancing'
coverImage: 'images/nightmare-clients-thumb.jpg'
---

I love freelancing, but you'll eventually come across someone who… giving them the benefit of the doubt, let's just say their idea of the freelancer/client relationship doesn't match yours. These nightmare clients can take an awesome project and make it a chore.

I'll share **four strategies** to help you deal with clients like these… and a new way of thinking that will help you eliminate this problem forever! (It's probably not what you think, but give it a shot anyway.)

https://youtu.be/fUbU6Hq1TTo

<!-- If you need some help getting your freelance business going, I built a course just for you. Check out my **Freelancing Crash Course**! It's free and comes straight to your inbox. 👇

[thrive_leads id='2262'] -->

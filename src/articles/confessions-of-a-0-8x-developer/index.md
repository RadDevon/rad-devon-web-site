---
title: 'Confessions of a 0.8x Developer'
date: '2021-07-06'
tags:
  - 'motivation'
coverImage: 'images/confessions-thumbnail.png'
---

![Pie chart showing how many X developer I am, which happens to look like Pacman. It's eating the ghosts which are other skills that don't look good on a resume.](images/confessions-thumbnail-1024x550.png)

This may be career suicide… but I feel like a lot of people probably need to see it.

I have a confession to make. I'm not a very good developer.

Maybe that's a little unfair to myself. Actually, I think I _am_ a good developer, but not because I'm especially clever or fast at building software.

There's nothing left to be said about the legendary 10x developer, particularly about whether or not such a beast exists. I won't weigh in on that. In the event you aren't familiar with the concept, though, I'll just briefly define it. A 10x developer is a developer who is 10x faster, 10x smarter, or generally 10x better than the typical developer.

Even though I can't say whether the 10x developer exists, what I can say that is that I am not one of them. I'm more like a 0.8x developer. Here's why.

**I don't write especially clever code.** If I'm feeling my oats and want to humble myself, I'll head over to [CodeWars](https://www.codewars.com/) to do a few challenges. Once I find a working solution, I like to browse the top voted solutions. When my solution is 16 lines, the best solution is often a one-liner. I tend to brute force everything. I write code in the way that makes sense to me. I can get it done, but do I get it done the best way? Usually not.

**I'm slow.** You may already know that I organize a meetup group for new and aspiring devs here in Seattle. Sometimes, we have events where we code together in small teams, each team having an opportunity to present and talk about what they've built. I'm frequently amazed at what people have been able to build. These are people without jobs who are able to build far more in 2-3 hours than I could, even though I've had years of experience.

**I'm not much of a computer scientist.** This isn't a huge deal because I'm not doing computer science most of the time. The only problem is that every developer interview tests you on computer science – implement a linked list, perform a bubble sort, or write a recursive algorithm.

You may be reading this and thinking, "These don't describe me. I'm way better than Devon." If so, that's awesome. I'm sure you'll find a ton of success. I'm happy to help you move forward into your career however I can, but I didn't write this for you. I wrote this for the person reading and saying, "Me too." I'm writing to let you know that none of these shortcomings mean you can't have a great career as a web developer.

Now, we're flipping to the other side of the coin. These are the things I do well.

**I write maintainable code.** One reason my code is generally more verbose than the top solution on CodeWars is that I've trained myself to write my code to be read by other developers. This is a skill I have honed for mostly self-serving reasons, those being that I can't understand clever code and write so that later I can read and understand the code. This has the nice side effect that, if I can understand the code, most anyone can.

**I am reliable.** I underpromise and over-deliver. If I say I will be at a meeting, I will be there. I will not tell you that your final delivered software will be 100% bug-free and secure because no software ever is. I will not tell you I will ship a feature next Tuesday unless I'm pretty damn sure I can make that happen.

**I communicate well.** You know that feature I was pretty damn sure I would ship next Tuesday? I now suspect I'm not going to hit the deadline because of a number of factors, and I'm currently writing my client an email detailing all of those and asking how we should proceed. That's because this is their business. Even reliable people miss deadlines, but reliable people also have empathy for the people they work for and give them as much time as possible to prepare for those deadlines to be missed.

**I care about what happens around the code.** I can't count the number of people I've heard say, "I just want to be left alone to write code," when confronted with meetings, estimating timelines, or other realities of running a business. I totally understand that impulse, and you can have that… _if_ you just want web development to be a hobby. If you want to get a paycheck, there's a business making the money to back that paycheck, and you should probably care about its continued success.

Now, I'm a capable person. If I wanted to, I could probably make an effort to develop those skills I don't have. I could write more clever code and write solutions that rank near the top of CodeWars. I could develop techniques to code faster. I could sit down and really learn computer science. I'd guess most people could if they really put in the effort.

But the things I do well are things that almost _anyone_ can do well if they make the effort and summon the empathy to care about factors orbiting around their work. These are skills that are incredibly valuable to businesses. I don't know this for a fact, but I'm pretty sure these are the reasons people want to work with me. They don't go on a resume, but they're factors people pick up on when they work with you, which is why I advocate so strongly for [starting with freelancing in your career](/articles/inverted-career-path/). [Showing is more powerful than telling](/articles/woo-clients-by-showing-instead-telling/). You can use freelance work to show people you have the qualities they want in a partner… or maybe an employee someday, if that's what you want and what they need.

As I'm reading back through this, doing some editing, it occurs to me that maybe my premise was wrong to begin with. Maybe I _am_ a 10x developer, just not measured by the metrics that are generally used to identify those. Or maybe I'm just a 1x developer… but that's still better than 0.8x! The skills I do have allowed me to build a career that's ~8 years strong at this point. Even though I'm not as fast nor smart as many web developers, it seems like some people, at least, have decided I'm good enough for them.

Thanks for reading. If this resonates with you and you're trying to get started as a developer, check out my free ebook on how to get use projects to get your career off the ground.

[thrive_leads id='4451']

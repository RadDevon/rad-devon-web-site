---
title: 'Make and Change Directory Shell Alias'
date: '2013-09-04'
tags:
  - 'aliases'
  - 'bash'
  - 'shell'
  - 'terminal'
  - 'workflow'
coverImage: 'images/folders.jpg'
---

If you've ever cursed the fact that you need to `mkdir <dirname>` followed by `cd <dirname>` to create and change into a new directory, this snippet is for you.

```bash
function mcd() {
    mkdir "$@" && cd "$@"
}
```

Just paste that into your shell configuration (.bashrc or .zshrc in your home folder), restart your shell, and start making and changing into directories with just `mcd <dirname>`.

[thrive_leads id='1366']

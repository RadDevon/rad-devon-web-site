---
title: 'Should I start by learning vanilla Javascript or a framework?'
date: '2019-10-04'
coverImage: 'images/vanilla-chocolate-soft-serve.jpg'
---

![Ice cream cone with vanilla and chocolate soft serve](images/vanilla-chocolate-soft-serve.jpg)

This is a common question among new developers, and it's a good one to think about. On one hand, robust applications are often built using front-end frameworks. If you want to start building robust apps, it would make sense to **cut to the chase** and learn the framework.

On the other hand, Javascript is the foundation of all of these frameworks. Maybe it's good to have **some knowledge of that foundation** before you try to learn something that's built on top of that.

When I was getting started in web development, front-end frameworks didn't have the same mindshare they do now, but I _did_ have to make a similar decision. I had to decide if I should learn vanilla Javascript or jQuery. **Here's what I ended up doing** and how it worked out for me.

## My "Cart Before the Horse" Story

For anyone who doesn't know, jQuery is a library that makes working with the DOM (the document object model) in Javascript easier. It also smoothed over a lot of browser compatibility issues that were a real pain to deal with 5-10 years ago. It's not so relevant these days since we rarely need to support those old browsers with our new builds.

It was so widespread that, even today — well past the peak of its relevance — it's estimated to be in use on **[88% of the top 10,000 web sites](https://trends.builtwith.com/javascript/jQuery)**. I was eager to get started building, so I decided to learn how to use jQuery with a very rudimentary understanding of Javascript.

This did allow me to get to work much more quickly. Since I could select elements and manipulate the DOM easily with CSS selectors and without having to worry about browser quirks, it let me dive right in. It worked out pretty well… **until I had a problem**.

## An Incomplete Toolset

I didn't understand a lot of what jQuery was doing because **I didn't have that foundational Javascript knowledge**. Why am I calling methods by chaining them off the original element I selected (and how does that actually work)? What is `this`? If I select an element and assign it to a variable inside a function, why can't I use that outside the function?

So, even though my toolset _seemed_ adequate while everything was working as expected, as soon as I went off-script, I would inevitably hit a problem I just didn't have the tools to solve. It meant I could code myself into a lot of problems that I couldn't get out of. It made me a worse developer because I was trying to take this shortcut to reduce the time it took me to become productive. Instead, **it was reducing my productivity**.

## Righting the Wrong

When I recommitted myself to web development a few years later, I realized this decision **had stunted my growth**. At the same time, the browsers causing many of the issues jQuery solved were becoming obsolete. I decided to properly learn Javascript.

Not only did this make me a better jQuery developer, but it made me a better developer overall. I understand what's going on behind the scenes with jQuery, so I can much more easily debug and solve problems when I use it. I can also make a conscious choice not to use it when that makes more sense. Before, that wasn't an option for me.

<!-- _Hey there. 👋 All the technical knowledge in the world won't make you a web developer. I help you bridge the gap between "I know how to build web sites," and "I get paid to build web sites."_

[thrive_leads id='1366'] -->

## What Should You Learn First?

I can't fault you for wanting to learn React, Angular, or Vue before you really have a good grasp on Javascript. After all, I did virtually the same thing years ago. It seems like you'll be able to get started on the projects you _really_ want to work on. In practice you're setting yourself up for **a lot of frustration and ultimately failure**.

What I'd recommend instead is, if you have a project that you feel needs a framework, either scale back to **a smaller project or build a smaller chunk of your existing project** with vanilla Javascript. You'll get your foundational knowledge, and you can always retrofit the project with the framework of your choice later.

Taking things a little slower now, at the beginning of your learning journey will pave the way for you to pick up serious momentum in a few months, once you have the basics down. For the impatient among us, it can be excruciating, but **the payoff is worth it**. Start with vanilla Javascript, and you'll be a better developer.

---
title: 'How do I compete with Wix, SquareSpace, and WordPress as a freelancer?'
date: '2019-08-23'
coverImage: 'images/abandoned-blockbuster-video.jpg'
description: 'None of us wants to go the way of Blockbuster video, and web site builders might look like our Netflix. Hereʼs what you can do to fight back.'
---

Web development is a dying profession. We're being squeezed out on the low-end by cheap and easy web site builders like Wix, SquareSpace, and WordPress.com. **Soon, there'll be no work for web developers like us**, and we'll be making pennies as support reps for these very same companies.

Or so goes the common web developer's lament. Even new freelancers who haven't had a single client ask me if this is something they should worry about. This market didn't disappear for them since easy web site builders have been around for more than a decade at this point, but they're concerned their chosen profession is slowly disappearing.

The worry is that becoming a web developer today is equivalent to **franchising a Blockbuster Video 10 years ago**.

![Abandoned Blockbuster Video store](images/abandoned-blockbuster-video.jpg)

Photo by Flickr user [Elliot Brown](https://www.flickr.com/photos/ell-r-brown/)

## The Problem

Web sites that would previously have required a web developer, who would have been paid a reasonable sum of hundreds or thousands of dollars, are now easy to build and host with SquareSpace and others for about $10 per month and some sweat equity.

The low-end of web development is now reserved for software instead of developers. For the most part, the only work still up for grabs in this space is for clients who don't have the time to build it themselves, even with the benefit of WYSIWYG software. For them, the price is no longer anchored to what developers _used_ to make to build these sites but instead to the $10 a month they would have paid SquareSpace.

## The Solution

We could spend our time grousing about how times have changed and we can't get the work we used to, but I'm more interested in how to move forward. The way forward is to **move on to more interesting projects**. The low-end of web development will never be the same, so the best way is to **find the work the software can't do**.

Freelance web developers still have tons of room to work. **Solving the specific problems businesses have will almost always require a developer's touch.** For prospects who are on a budget and need something simple, embrace the low-end alternatives.

Tell those prospects you _could_ help, but you would be expensive. If they have some time and their budget is limited, _they_ could build the solution themselves with one of the web site builders. **Nothing inspires trust like convincing people _not_ to hire you.**

Since trust is what gets you hired, this counter-intuitive strategy of turning away work actually opens the door to more interesting work later on, and makes it easier to sell when that work comes since you've already established trust. Not only is that work more fun, but **it pays better too**.

Almost every business needs a brochure site, even if they're just starting out. They're unproven, and they don't have as much money as established, successful businesses. That means, work for these clients won't pay as well.

If you send them away when they're at this early stage and help those who come back around for custom software once they're proven and making money, you're operating in **a much more profitable space**.

## Always Looking Forward

The low-end brochure site market for web development existed for a while because the web was young. It was bound to go away at some point because the needs are similar across all kinds of businesses. A generalized software solution was inevitable.

Deal with this by **keeping your skills sharp**, **intimately understanding the problems your ideal clients face**, and **building solutions that encapsulate that understanding**. If you can practice these three things, you'll never run out of work as a web developer.

If this article inspired you, I have a free course that can help you **translate that inspiration into action**. The Freelancing Crash Course teaches you everything you need to know to to start your business in a week's worth of emails.

[thrive_leads id='2262']

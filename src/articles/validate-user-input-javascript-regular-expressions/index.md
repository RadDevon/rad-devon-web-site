---
title: 'How to Validate User Input in Javascript with Regular Expressions'
date: '2018-03-05'
tags:
  - 'javascript'
  - 'regex'
  - 'regular-expressions'
  - 'tips'
description: 'Regular expressions allow you to check strings for the existence of patterns. Using them, you can validate input from your users. In this example, weʼll confirm your user has entered a zip code.'
coverImage: 'images/blue-lines-on-black.jpg'
---

_In this post, you'll learn some basic regular expressions syntax and how to use it to validate input from your user. This is by no means comprehensive but is intended as a short overview of regular expressions. It covers the Javascript flavor of regular expressions._

## What Are Regular Expressions?

Regular expressions define text patterns to find inside a string. They use a special syntax to define what text in the string being searched will match. They make it easy to search for characters in a string when you don't know exactly _which_ characters you're looking for.

Imagine you have an address form. You want users to enter a US zip code, and you want to validate they have entered a valid zip code. You could write a regular expression that would do the job. Here's a simple example that matches a 5-digit zip code:

```javascript
const zipCodeRegex = /^\d{5}$/;
```

## How Do They Work?

The regular expression above matches a 5-digit zip code, each digit being a numeric digit between 0 and 9. Here's a breakdown of the syntax:

The forward slashes (`/`) bookending the regular expression tell Javascript that the contained characters are a regular expression. In the same way you surround a string with quotes or an array with square brackets to indicate what type of data they are, you surround a regular expression with forward slashes.

The caret (`^`) is an **anchor**. It matches the beginning of the string. This causes the regular expression _not_ to match unless the match is at the beginning of the string. In this case, the regular expression would match `90210` but would not match `zip- 90210` since it has characters before the digits.

`\d` is a **character class**. This is the part of regular expressions' syntax that allows us to match a character when we're not quite sure what it is. We know a zip code has 5 numeric digits, but we don't know what those are. If we only wanted to match the Beverly Hills zip code, we could put `90210` right into our expression. Since we want to match _any_ zip code, we have to make it more general. `\d` matches any numeric digit.

`{5}` is a **quantifier**. It says that, for whatever preceded it, we want to match 5 of those. In this case, it's a numeric digit. So, we're looking for 5 numeric digits. `\d{5}` is equivalent to `\d\d\d\d\d`. Other common quantifiers are `*` which matches 0 or more of the preceding character, `?` which matches 0 or 1 of the preceding character, and `+` which matches 1 or more of the preceding character.

`$` is another anchor. This one anchors the end of the string. That means we only match if we have the beginning of the string, 5 numeric digits, and the end of the string with no characters before, after, or between.

## How Do I Use Them in Javascript?

If you want to use regular expressions for validation, the most straightforward way is through the regular expression's test method.

```javascript
const zipCodeRegex = /^\d{5}$/;

zipCodeRegex.test('90210'); // true
zipCodeRegex.test('apple'); // false
zipCodeRegex.test('902101'); // false
```

[thrive_leads id='1384']

## Caveats

This zip code validation is intended as a learning exercise and might not be what you'd need on a live site. Here's why:

- **It only works for US zip codes.** If you have users in other countries, their postal codes will not validate against this regular expression.
- **It's naïve.** It only validates the format of a 5-digit numeric US zip code. That means, no zip+4 and it doesn't know if a zip code is actually "valid." It just knows if it is in a valid _format_. For example, 00000 is _not_ a valid US zip code, but it _would_ pass this validation because it's a 5-digit number. We could cover these cases with the expression, but it would be complex for a beginner exercise.
- **Validating _any_ sort of user input can be tricky.** We often have assumptions about the data we're going to get that turn out to be incorrect or incomplete. If we write validation based on these assumptions, we're keeping valid data _out_ of our system, allowing bad data _in_, or both.

That said, the concepts you learn here _can_ be applied to your own more robust validation expressions to great effect.

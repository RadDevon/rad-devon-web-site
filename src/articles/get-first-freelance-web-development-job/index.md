---
title: 'How to Get Your First Freelance Web Development Job'
date: '2018-01-29'
tags:
  - 'business'
  - 'freelancing'
coverImage: 'images/work-from-anywhere-optimized.jpg'
description: 'Hereʼs what you need to make sure you have in place before you start looking for your first freelance web development gig and where to start looking when youʼre ready.'
---

![Job Hunt](images/job-hunt-transparent.png)

Although it's _easier_ to get freelance work than permanent positions, that doesn't mean it's trivial. It can still be challenging (and intimidating) to get started. Here are **my three favorite sources** to help you find that first contract.

## Wait! Before You Try for Freelance Work…

You need to have two things: **a portfolio of work** and **a contract**. I'll touch on each of these briefly.

### Your Portfolio

No one is going to hire you if you have nothing to show them. If you _don't_ have a portfolio, you need to start by being your own client. Build some cool stuff for yourself and put it up. If you can capture your thinking and the process in addition to the final product, even better.

Don't freak out about the word “portfolio.” You don't need to custom-build a web-based portfolio to display your work. You just need a few pieces of finished work people can look at. If code is your expertise, make sure you have a [Github](http://github.com) account with your work. If it's design, [Dribbble](https://dribbble.com/) will work. Find a hosted solution or throw together a site on [WordPress.com](http://wordpress.com) or [SquareSpace](http://squarespace.com). Just get some of your work up so you can show it off to prospective clients.

### A Contract

Make sure you have a contract and you insist on having a contract in place for every job you do. Small-time clients will be fine using yours. Bigger clients may have their own standard contracts. Whatever you do, make sure you have a contract in-place, and insist on changes if you don't like the terms. **Turn down jobs if they can't make the contract work for you.** Whatever verbal promises they've made are worthless, and whatever work they're offering isn't worth the fight that will almost certainly await you.

Don't sweat this either. Download the [Contract Killer](https://stuffandnonsense.co.uk/projects/contract-killer/) open-source contract. Modify it to suit you. If you have a little cash, run it by a lawyer to make sure it's still enforceable. If you _don't_ have the money, do this after you get paid for your first job.

## Where to Look

With your portfolio and contract in place, you're ready to start looking for work.

### Freelance Boards

These are the most enticing because they seem like they would be a very low-effort way to get work. Everyone else thinks this too which is why you'll be racing to the bottom to get any work. Skip the boards you've heard of like Upwork and Freelancer.com.

Since freelance boards are such minefields of empty promises and broken dreams, I tend to avoid them, but I have had luck on [Reddit's forhire subreddit](https://www.reddit.com/r/forhire/). Post titles on the subreddit are prefaced with either “\[Hiring\]” or “\[For hire\].” Feel free to post your own “\[For hire\]” post, but don't count on getting hired based on that. You're going to get work by replying to “\[Hiring\]” posts.

Many of the posters you message will never reply. Most of those that are left will get back to you but will select someone else. Very few will hire you. **And that's OK.** That's just the way this works. Just keep applying and you will find something that works for you.

### Small Business Networking Groups

In almost every city of any size, you'll find groups of small business owners who get together to help each other find business and solve other problems. [Check Meetup.com](https://www.meetup.com/find/career-business/) to see what's available in your area. Most small businesses need web sites, but their needs are modest making these a good place to find work as a beginner. Look for group members who have only Facebook pages. These will be small, low-paying jobs, but they'll help you make some money while fleshing out your portfolio.

### Web Development Conferences

By attending conferences, you'll accomplish two important goals. You'll…

1. learn new things and become a better developer.
2. meet people and make connections that might lead to work.

Also, going to a good conference has an effect of energizing you about the possibilities of your field. I highly recommend you do this. In my experience, it has always been worth way more than the cost to attend.

The work you'll find at a conference may require slightly more skill than what you'll find at business networking meet-ups. You're going to be surrounded by other freelancers and people who work for studios. They often need to augment their own staff to provide additional capacity or to fill skill gaps.

That said, serendipity can lead to other opportunities as well. I attended a web developer conference once in Columbia, South Carolina. I stayed in an AirBNB. My host was in the process of starting a business and needed some advice on web application development. It turned out to be a perfect fit. I was able to do some work for him and help him out. We continue to be friends to this day.

[thrive_leads id='1378']

## Roll Your Momentum Into the Next Job

Now that you've gotten your first job, use what you've learned to keep taking on more difficult jobs. Whether you want to continue freelancing or go get a permanent position, there's nothing like making some money while you continue to learn web development and build your experience.

_Need to pull together a portfolio before you go hunting for gigs? You may think that, as a web developer, you [need to build the whole thing from scratch](/articles/build-portfolio-site-from-scratch/). Spoilers: you don't. Read on to find out why._

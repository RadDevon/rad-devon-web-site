---
title: 'How do I find my web developer niche?'
date: '2019-04-12'
tags:
  - 'freelancing'
  - 'marketing'
  - 'niche'
coverImage: 'images/man-in-narrow-alley.jpg'
description: 'A niche is important for marketing yourself as a web developer. Now the question is, how do you choose one? Here are a few options to get you started.'
---

![Man in a narrow alley](images/man-in-narrow-alley.jpg)

Even though you understand [the value of selecting a niche](/articles/should-i-pick-a-niche-as-a-freelance-web-developer/) for your freelance web development practice, you may not understand how to go about actually choosing one. In this article, I'll share some **practical techniques for choosing your web development niche**.

## The Passion technique

Select a niche based on your interests. This technique requires a lot of restraint because you can't let passion be your only criteria for selecting a niche. (This is a good way to end up out of business.) You need to make sure your niche **can pay for your services too**.

Here's an example. Maybe you decide you're going to be a web developer for musicians since you love music. Unless you're able to target massive pop sensations, you're going to have trouble making any money from small touring bands. They just don't have the money to spend, and most of them aren't making a ton of money on the web. They make money by touring. They can't afford to invest in something that doesn't directly feed into that.

What you might be able to do instead, though, is to build software for record labels, recording studios, or agents who represent major acts. These are people who are making money in music, so you could charge enough to build a livelihood while also working in and around something you love.

Passion is **a powerful motivator** which is why this is a good way to go, but it can also blind you to reality. Make sure you go into the passion technique open to many different ways you might be able to turn your passion into a profitable niche. If you make your mind up exactly how this has to work before you start, you could find yourself with an abundance of passion but no cash for groceries and rent.

This technique is a tough balance, particularly if you're new to web development and to business, so **only go down this road if you really know what you're doing**.

[thrive_leads id='2262']

## The Funnel technique

Start broad and narrow based on which previous jobs you enjoyed the most. Anecdotally, this is the path I see most people taking, probably because it's the easiest to get started. You start out serving anyone. You figure out **which jobs you enjoy the most** and lean into those. It's a very organic approach to working into a niche.

The biggest advantages of this technique are that **you don't have to spend time and energy picking a niche from the start** and that **you'll know your niche is profitable** since you've already been making money from it.

The biggest disadvantage is that you're starting _without_ a niche, so **you lose all the advantages of having one**. It's harder to market. You don't know who you're talking to, and it's hard to tell people what exactly you do since it's so broad.

This is the technique I've used myself, and one major disadvantage for me is that it became easy to let my existing momentum carry me forward and never niche down. **The more entrenched you get in serving everyone, the more it dangerous it seems to do anything else.** "Do I need to turn away existing clients? How long will it take to get back to where I am now? Maybe I'll just keep doing what I'm doing instead…" These are the nagging doubts that block the change you want to make.

## The Existing Network technique

Make a list of all the people closest to you — your close friends and family — and the industries they work in. Pick one of these to explore further.

This technique solves two problems for you right off the bat. First, **you already know there's money to be made in this field** because someone you know is already making a living here. Your challenge will be to find out how to build web software to improve efficiency or multiply their efforts. Second, **you already have an "in."** If the person you know is knowledgeable about the part of the field you'd like to contribute to, you can start by talking to them. If not, they can probably make an introduction to someone who can help you understand the problems you might help solve.

It's hard to overvalue these advantages. It can take a long time to learn if there's actually money to be made in a field, and it can take a similarly long time to build trust to the point that people will talk to you openly about their struggles. Short-circuiting these two needs can **speed up your path to profitability**.

## Where to Start

If you're still not sure how best to get started selecting your niche, **I'd recommend trying [the existing network technique](#existing-network)**. It removes most of the risk and gives you a jumpstart on finding what you really need: a way to make money with freelancing.

By starting your freelance web development practice with a niche, you'll have a much easier time communicating why someone should hire you. Doing this effectively is **the key to getting contracts**. A niche is a great shortcut to make the hard part of freelancing — selling — a little easier.

---
title: 'Kill distractions while coding at home with this book'
date: '2020-05-08'
tags:
  - 'books'
  - 'career'
  - 'focus'
  - 'habits'
  - 'motivation'
  - 'self-improvement'
coverImage: 'images/books-make-time-thumb.jpg'
description: 'Learn about a book that can help you get more done with actionable advice to help you focus on whatʼs important.'
---

https://youtu.be/QQSlI5T0644

## Make Time

_Make Time_ is a fun read full of strategies for getting more out of your day. You'll be more productive at work and more present when you're not working. The strategies are helpful for anyone, but being disciplined is critical when you work remotely since there's no threat of a manager peeking over your shoulder.

_Make Time_ is a great companion to the last book I looked at, [Atomic Habits](https://youtu.be/GIDE3PXDHzc).

If you're interested in this book, I wrote an article about [my focus system](https://raddevon.com/articles/focus-for-better-work-life-balance-as-a-web-developer/).

Buy your own copy of _[Make Time](https://bookshop.org/a/2969/9780525572428)_.

<!-- I have my own method for building a custom focus system. Share your email, and I'll send it your way along with other resources to help you become a web developer. 👇

[thrive_leads id='1371'] -->

## Video Transcript

This is Rad Devon, and today I want to share with you a book that has made my work time a lot more effective. It teaches you to be more focused and to avoid- Actually, I'm sorry. Give me just a second to deal with this.

I'll- I'll be back in just a minute, just hang on, okay?

_Make Time_ is a book for people like us who have the blessing and the curse to have jobs where they can get distracted. And yes, it is a blessing because I've had jobs where I didn't have the freedom to get distracted. I had to ask permission to go to the bathroom. So that's why I refer to it as both a blessing and a curse.

This book is the perfect companion for the last book I talked about, _Atomic Habits_, which I will link you back to in case you missed that one. _Atomic Habits_ shows you how to form good habits and break bad ones. _Make Time_ gives you this whole list of habits you can try to help you improve your productivity. The habits are divided into four steps you're supposed to repeat every day. First step is highlight. The second one is laser. The third one is energize. And the fourth one is reflect.

Highlight is about deciding what you want to focus on. Laser is about eliminating distractions. Energize is about recharging. And reflect is kind of just what it sounds like. It's thinking back on how things went and trying to figure out how you can improve.

This book has a ton of tactics. I think it's got almost a hundred tactics. They don't intend for you to try to employ them all simultaneously. They're just different things you can try. And some of them will work for some people, others will work for others. You're supposed to go through the book and test them out and sort of mix and match and create your own productivity system.

The first thing I tried in this book that really helped me was my own variation of the distraction-free iPhone that Jake talks about in the introduction. He took it really far.

"Three, two, one."

He actually deleted all the apps off of his phone. I wasn't quite ready to go that far, but his message was pretty clear. Apps and their notifications are these little attention vampires, just sapping all the productivity out of your work day. Like I said, I wasn't ready to delete all the apps off of my phone, but I did take inspiration from what he did.

I took some of the apps that were especially distracting for me. And those were Twitter, a Reddit app, Reader, which is my RSS feed reader. And I moved them off of my dock onto the last home screen. That way there were much harder to get to. I also turned off notifications coming out of those apps, so they weren't screaming for my attention all the time.

\[annoying sound\] "Guys! Guys! Guys!"

Just taking those two simple steps, I immediately saw a pretty dramatic improvement. Here's a small selection of the other tactics that I think you should pay special attention to as a web developer.

Batch the little stuff. It's so easy to lose a day bouncing back and forth between your actual, productive work and email and Slack notifications and whatever other applications you use. Instead, save up all your email and Slack responses you need to do for a couple of times throughout the day. Devote 20 or 30 minutes or even an hour, if you need to, just to reply to everything in those batches. That way you can stay focused on your productive work throughout the rest of your day.

Just say no. This is one that's really important for new freelancers to hear because you don't know when work is going to come in. So you feel like anytime it does, you have to take it all. You have to say yes every time. And you'll probably do that to start out. But what you're going to find is there is an opportunity cost for saying yes. When you say yes to a project that's not a great fit, you automatically say no to a project that maybe needs to be done during that same timeframe that you can't do anymore because you're already doing the first project. Get used to saying no to projects that aren't a great fit or that don't help you move closer to your goals.

Ignore the news. I'm sure this one's probably a little bit controversial. I used to follow the news very closely because I thought part of being an adult was being well-informed on everything that was going on in the world. There's some peer pressure, too, some subtle peer pressure as a web developer because a lot of other web developers stay pretty up to date on current events. You kind of feel left out if you're not totally on top of everything that's going on. What I found is that knowing what was going on in the news, rarely ever changed what I was going to do that day. I wasn't really any better off for knowing it. It just took up a bunch of time to stay current. And it gave me a lot of anxiety. I've cut most of that out. And I find that the important stuff still bubbles up and gets my attention when it needs to.

Be slow to respond. If you're always quick to respond to emails or instant messages, people will come to expect that of you. If you're slow, you make room for yourself to enter that flow state and really do your best work.

Those are just a few I think you should look at, but pick up a copy of the book and build your own custom system from whatever makes the most sense to you.

I'm a real sucker for the playful style of this book. It has silly illustrations and lots of jokes with the authors' personalities coming through. I may be an adult, but I'm still a sucker for a book that's got some fun injected into it.

My one complaint is that this book, which was released in 2018, promised an app that was going to help you implement some of these techniques. I was really excited for the app, but it's never materialized, and I kind of doubt, at this point, it will. It sounded like it could be a great way to sort of reclaim your apps and notifications for good instead of for evil. But I guess we'll never know.

That is _Make Time_ by Jake Knapp and John Zeratsky . If you have this book or if you ended up getting it, I would love to hear which tactics work for you, especially if you put your own spin on them, so please share that in the comments. Thanks for watching, and I'll see you next time.

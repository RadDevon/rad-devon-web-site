---
title: 'How to Stay Current as a Web Developer'
date: '2019-08-30'
coverImage: 'images/person-reading-newspaper.jpg'
description: 'Web development is changing so fast, itʼs hard to keep up. Here are the resources I use to stay on top of things.'
---

![Person reading a newspaper](images/person-reading-newspaper.jpg)

I’ve talked about [why you should get off the tutorial treadmill](/articles/stop-doing-coding-tutorials/) and just-in-time learning as a more practical alternative, but, when the time comes that you need to build something outside your skill set, **how do you know which technologies you should reach for?**

Part of this comes from your connections. You ask a more senior developer if they know of any technologies you should be looking at for your current problems. The other part comes from maintaining a general awareness of the new technologies available to web developers.

My favorite methods of staying on top of new developments in software engineering are through various blogs, social sites where people share great articles, and podcasts. Here are a few of my favorites, but first…

## A Word of Warning

Staying on top of things can become an outlet for procrastination. It's sinister because you can convince yourself that it's work. It's really not even though, in moderation, it _can_ make your work better. To avoid this pitfall, **set aggressive limits** on how much of this information you take in.

Set a time limit on blogs and social media. 30 minutes per day is the absolute most you'd want to allow yourself.

Listen to podcasts while traveling, doing chores, or traveling. If you want to squeeze more into this time, creep your listening speed up to 1.5x or 2x playback speed.

This will take discipline to maintain, but it's worth it to be able to stay on top of your profession without distracting yourself from the really important work.

With that warning out of the way, on to the suggestions!

## Blogs

### [CSS-Tricks](https://css-tricks.com/)

Great writing and truly innovative ideas make [CSS-Tricks](https://css-tricks.com/) a can't-miss blog for web developers. The blog's founder Chris Coyier shares his own techniques and other accomplished developers bring their experiences to the mix.

The blog goes beyond just the CSS tricks the name might suggest and into other areas like Javascript, performance, and accessibility. You'll get great tutorials on libraries and tools you may not be familiar with. The content is primarily focused on front-end developers, but it occasionally ventures into back-end topics as well.

### [David Walsh Blog](https://davidwalsh.name/)

Through [his blog](https://davidwalsh.name/), David Walsh shares tutorials on how to do cool stuff with Javascript and CSS. His posts are somewhat infrequent, but they pack a lot of punch. One recent post about data URIs taught me I can use them to access the computer's web cam. Never know when something like that might come in handy, and that is exactly the purpose of this article!

### [Toptal Engineering Blog](https://www.toptal.com/developers/blog)

If you want a technical deep dive, the [Toptal Engineering Blog](https://www.toptal.com/developers/blog) is a great place to get it. Recent articles teach you to build a GraphQL API, integration testing with React, and what's new in the latest release of Rails. This is exactly the sort of knowledge you can draw on later to solve problems you and your clients or employers run into.

## Social Sites

### [Hacker News](https://news.ycombinator.com/)

[Hacker News](https://news.ycombinator.com/) is a reddit-like site where users post content of interest to programmers. Users vote, and the best content makes it to the front page. It's a great place to learn about new libraries or technologies. You'll also find great opinion pieces that can help you start to think about important issues in web development.

### [Sub-Reddits](https://www.reddit.com/)

[Reddit](https://www.reddit.com/) is a content-sharing community made up of thousands of smaller content-sharing communities. Don't make an account and read the default subscriptions. Instead, unsubscribe from all of them and build your own set of communities that are important to you. You can join communities around languages like [Javascript](https://www.reddit.com/r/javascript), frameworks like [Vue](https://www.reddit.com/r/vuejs/), or more general topics like [web development](https://www.reddit.com/r/webdev/).

It's a great place to stay on top of the latest libraries and techniques, especially as you start to drill down into more specific sub-reddits.

### [Twitter](https://twitter.com/)

[Twitter](https://twitter.com/) allows people to post short messages containing whatever content they want. Follow people who are thoughtful about web development, and you'll find yourself becoming more thoughtful and better informed too.

Here are some of the best accounts I'm following at the moment:

- [Ali Spittel](https://twitter.com/ASpittel)
- [JavaScript Teacher](https://twitter.com/js_tut)
- [CodeNewbie](https://twitter.com/CodeNewbies)
- [Jen Simmons](https://twitter.com/jensimmons)
- [Dave Rupert](https://twitter.com/davatron5000)
- [Scott Jehl](https://twitter.com/scottjehl)
- [Addy Osmani](https://twitter.com/addyosmani)
- [Julia Evans](https://twitter.com/b0rk)

[Follow me too while you're at it.](https://twitter.com/raddevon)

### [Dev.to](https://dev.to/)

[Dev.to](https://dev.to/) is a blogging community for developers. A lot of the content is focused on beginners, but even as you mature as a developer, you'll still find some nuggets here. Subscribe to the tags you like and follow the users that resonate with you.

[thrive_leads id='1366']

## Podcasts

### [Shop Talk Show](https://shoptalkshow.com/)

Each episode of [Shop Talk Show](https://shoptalkshow.com/) features a topic and a guest who is an expert on that topic. It covers primarily front-end web development, but they sometimes dip into other aspects of web development as well. The hosts are charming and funny.

### [Syntax](https://shoptalkshow.com/)

[Syntax](https://syntax.fm/) is another great front-end focused podcast. I could nearly copy and paste the description for Shop Talk except that Syntax generally doesn't have guests. I love them both and believe they're both worth subscribing to.

## Grow Your Someday Toolkit

The goal of taking in this information **is not that you need to have a deep understanding of everything you hear about** through these channels but to understand when a novel technology might be useful. Someday, if you come upon a problem that sounds like a use case for that tech, you can start learning it and build a better solution than you otherwise would have.

As long as you heed the warning to **timebox this content consumption**, having an awareness of new trends and technologies will make you **a more capable web developer**.

---
title: 'Last Second Gifts for Web Developers in 2019'
date: '2019-12-20'
tags:
  - 'books'
  - 'courses'
  - 'freelancing'
  - 'gifts'
coverImage: 'images/outatime.jpg'
description: 'Youʼre not going to be able to go back in time to have something shipped or to get to the store. Instead, you can buy these gifts right up until the last second. Not only that, but web developers will love them!'
---

![Delorean with "OUTATIME" license plate](images/outatime.jpg)

Wait, you're looking for a gift for a web developer _now_? Actually, it's OK. I've got you covered. You're not going to be able to go back in time to have something shipped or to get to the store. Instead, you can buy these gifts right up until the last second. Not only that, but web developers will love them!

## Books

When I talk about books for web developers, I'm rarely talking about the books people expect. I find that, for me, books aren't the best way to learn to code. If you buy a book today about the hottest framework, much of it will no longer be best practice in six months. The Internet is the perfect way to learn about and stay on top of technologies because it moves as fast as they do.

Books are an excellent medium for learning other skills you need as a web developer – understanding business, tips for staying productive, how to sell, etc. They're also great for a break from web development. Here are a few of my favorites that would level up any web developer.

[![10% Happier cover](images/10-percent-happier.jpg)](https://amzn.to/2PmHjra)

### [10% Happier](https://amzn.to/2PmHjra)

This was the first time I read a measured account of how meditation helps that wasn't predicated on a particular belief system. For _[10% Happier](https://amzn.to/2PmHjra)_ to be compelling, all you have to believe for this book to make its case is that

- you are stressed
- your emotions run away with you sometimes
- your brain keeps going whether you want it to or not

It's also great at holding back the hyperbolic claims about meditation being the cure for all the ails you. The more wild claims I hear, the less I'm likely to give it a shot. Dan Harris's only claim is that it improved his life by 10%. That's significant but still within the realm of possibility.

[![Anything You Want cover](images/anything-you-want-724x1024.jpg)](https://amzn.to/35nP6ul)

### [Anything You Want](https://amzn.to/35nP6ul)

Derek Sivers's experience starting, running, and ultimately selling [CD Baby](https://en.wikipedia.org/wiki/CD_Baby) flies in the face of much of the common advice around tech startups. Through [_Anything You Want_](https://amzn.to/35nP6ul), he shares his own learnings.

Not only can web developers learn a lot from the meta-lesson of the book (that there are many paths to success), but the lessons actually printed in the book can be used to run your own business as a freelancer, to help your employers on their paths to success, and to show you other paths in your personal life.

Derek's charisma and passion comes through in the book. He'll have you questioning your assumptions and making better decisions in no time. (Check out [this TED Talk](https://www.youtube.com/watch?v=1K5SycZjGhI) for an introduction to Derek.)

[![Make Time cover](images/make-time-678x1024.jpg)](https://amzn.to/35oN3GF)

### [Make Time](https://amzn.to/35oN3GF)

Who would have thought a book about productivity could be fun to read? [_Make Time_](https://amzn.to/35oN3GF) is easy to read – it feels more like a friend talking to you than a university lecture – and has fun little comics throughout which is always a plus if you ask me.

The advice here is really practical too. I especially enjoy the advice about coping with smartphones and the massive impact they can have on your productivity if you allow them to. If you always find yourself busy or if you work a lot and never get anything done, this is a must read. If you're new to managing your own time, this is a great place to start.

[![The Ultimate Hitchhiker's Guide cover](images/ultimate-hitchhikers-guide.jpg)](https://amzn.to/35nP6ul)

### [The Ultimate Hitchhiker's Guide](https://amzn.to/35nP6ul)

[The Ultimate Hitchhiker's Guide](https://amzn.to/2RQbQ2m) is just about the most fun you can have with a book. Where the other books on my list have been about making web developers better at their work, this one is about making web developers sides hurt from laughter.

This is for people who appreciate wordplay, clever humor, and the inherent funniness of characters with little to no self-awareness. Actually, you know what, if the person you're buying for likes to laugh, this is for them.

Quick bit of trivia: It's just been in the past few months I learned that the original form of The Hitchhiker's Guide was [the British radio play](<https://en.wikipedia.org/wiki/The_Hitchhiker%27s_Guide_to_the_Galaxy_(radio_series)>). I had always assumed the radio play was adapted from the books, but it was the other way around.

[![Freelance Web Development Pricing book cover](images/freelance-web-development-pricing-cover-642x1024.png)](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/)

### [Freelance Web Development Pricing](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/)

<!-- 🚨 Self-promotion warning! 🚨 I just finished my book [_Freelance Web Development Pricing_](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/), and I'm pumped to share it with you. -->

New freelancers don't know how to price their work. Heck, even veteran freelancers still deal with uncertainty when they quote a new project.

With my book on pricing, I focused on two things. First, giving you practical advice that doesn't require you to make a lot of decisions you're not equipped to make. Second, valuing your time by giving you exactly what you need to know without any fluff.

I hope you like it. I think it's the perfect starting point for your freelance business.

## Courses

Courses can be a great way to learn to code. I love video courses because nothing beats seeing what you're learning. It's easy for even the best teachers to take things for granted and leave something "minor" (yet critical) out of the text. You can't do that with video. What you see is what you get.

Today's best courses are updated and kept current. Here are a few I can recommend that would be great for a new developer. Some are technical; some aren't. All will make for a better web developer.

[![Git a Web Developer Job: Mastering the Modern Workflow course cover](images/mastering-modern-workflow.jpg)](https://click.linksynergy.com/deeplink?id=QPAcBn8XX*Q&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fgit-a-web-developer-job-mastering-the-modern-workflow%2F)

### [Git a Web Developer Job: Mastering the Modern Workflow](https://click.linksynergy.com/deeplink?id=QPAcBn8XX*Q&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fgit-a-web-developer-job-mastering-the-modern-workflow%2F)

Despite the title being a mouthful ([Git a Web Developer Job: Mastering the Modern Workflow](https://click.linksynergy.com/deeplink?id=QPAcBn8XX*Q&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fgit-a-web-developer-job-mastering-the-modern-workflow%2F)), the subjects of this course are often overlooked by self-taught developers. You can't be successful, though, without these skills.

Make sure the developers in your life understand not just the mechanics of using Git and writing CSS but how to actually use those technologies in a modern web development workflow. This course will get them there.

[![Modern Javascript course cover](images/modern-javascript.jpg)](https://click.linksynergy.com/deeplink?id=QPAcBn8XX*Q&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fmodern-javascript-from-novice-to-ninja%2F)

### [Modern Javascript](https://click.linksynergy.com/deeplink?id=QPAcBn8XX*Q&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fmodern-javascript-from-novice-to-ninja%2F)

Javascript is where most newbies start getting stuck and need a little extra push. [Modern Javascript](https://click.linksynergy.com/deeplink?id=QPAcBn8XX*Q&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fmodern-javascript-from-novice-to-ninja%2F) will provide that, taking developers through a 19-hour tour of the entire language. It's peppered with projects so you can put the techniques you're learning into practice as you go.

One word of warning: learning and taking courses is easy. Everything is laid out for you. You get a nice shot of endorphins when you complete a project or tutorial. But it won't get you there.

Doing real web development is hard. Make sure you try to pick up some paid gigs or go off-script with a project _you_ conceive. [Don't get stuck in tutorial purgatory](/articles/stop-doing-coding-tutorials/).

[![The Mom Test course cover](images/mom-test.jpg)](https://click.linksynergy.com/deeplink?id=QPAcBn8XX*Q&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fpractical-customer-development%2F)

### [The Mom Test: Practical Customer Development](https://click.linksynergy.com/deeplink?id=QPAcBn8XX*Q&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fpractical-customer-development%2F)

Code isn't all there is to being a web developer. [The Mom Test](https://click.linksynergy.com/deeplink?id=QPAcBn8XX*Q&mid=39197&murl=https%3A%2F%2Fwww.udemy.com%2Fcourse%2Fpractical-customer-development%2F) is a course on how to ask questions that get to the heart of what you're trying to learn about. Let's imagine a scenario.

You've spent some time doing one-off gigs building web-based tools for local event planners. Now, you want to build a single piece of software that you could sell over and over to event planners all over the country.

In order to find the right software to build, you're going to have to talk to some event planners. Some will be your previous customers. Others will be brand new to you.

You'll already have an idea of what you want to build. Your inclination will be to go to them and say, "Hey, if I built X, would you buy it?" The answer will always be "yes," because people want to make you feel good. That may or may not be the truth.

This course teaches you to ask the questions that get to the truth. It's a technique you can share with your clients and one you can use to develop your own products.

## More Ideas

I've got more books in [**my 2018 gift guide**](/articles/2019-gift-ideas-for-new-and-aspiring-web-developers/). If you want something a bit more substantial, [the 2019 gift guide](/articles/2019-rad-web-developer-gift-guide/) has gift ideas that will actually take up real space.

Give the gift of independence to the web developer in your life! Send them here to sign up for my free Freelancing Crash Course. In just a week, I'll give them the knowledge they need to start their freelancing business.

[thrive_leads id='2262']

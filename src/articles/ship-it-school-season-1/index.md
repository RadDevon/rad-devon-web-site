---
title: 'Welcome to Ship It School!'
date: '2018-11-09'
tags:
  - 'shipping'
  - 'video'
coverImage: 'images/sis-post-thumbnail.jpg'
description: 'Writing code and shipping code are two different things. If you donʼt ship your code is worthless. Ship It School teaches you how to go beyond writing code and start shipping it.'
---

![Ship It School logo](images/ship-it-school-logo-1000.png)

## The Problem: Writing Code Is Fun… But Shipping Isn't

Frequent readers know my mission here is to help people transition into web development careers. Part of my mentorship is **looking back** at how far the mentee has come, **looking ahead** to where they want to go, and using those touchstones to **find the next step**.

In working with my mentees, I've seen that loads of newbies **just need to ship something**. When you're learning, it's really easy to build stuff, but, since you don't have to ship anything, that piece often takes a back seat. **Shipping is hard**, so it gets tabled in favor of starting the next exciting project.

When you're doing this professionally, though, **shipping is the _only_ thing that matters**. When you show up at an interview with a bunch of half-finished projects, you might as well be there with nothing. When you're trying to get freelance gigs, if you can't give someone a URL so they can see something you've built, they'll have no confidence you can actually help them. **You're like a chef who has read a bunch of recipes but never actually cooked anything.**

## The Solution: Ship It School 📦

Writing code and shipping are **two completely different skillsets**. Coding is designing algorithms and writing them in the syntax of your language of choice. Shipping starts _earlier_ than coding by first answering, "What problem am I solving?" It ends _later_ than coding: when the software is available to the real world (whether that's consumers or a single client). Shipping is about coming up against **1,000 little barriers** and breaking each one down until your software is out.

**Ship It School** was created **to fill this gap** and to show everyone how software actually gets shipped: through **pragmatic compromise**, **critical thinking** about what is necessary for release, and **resourcefulness**.

## Season 1

## Bradley Serlis

![Bradley Serlis headshot](images/bradley.jpg)[Bradley Serlis](https://www.bradleyserlis.com/) is a recent bootcamp graduate starting his new career in web development. He carved out a niche for himself among his bootcamp cohort by **building his projects in React Native**. While they were building projects to run in a web browser, Bradley built for mobile.

He's been searching for jobs and wanted my advice on how to proceed. I told him what I tell nearly everyone in his position: [**go get some freelance work**](https://raddevon.com/articles/inverted-career-path/). The problem is this: Bradley wanted to do mobile development, but he hadn't yet shipped a mobile app to any mobile app store.

He could grab other web development gigs for now, and he may have even be able to pick up a gig doing a mobile app. The problem comes when his prospect asks to see some of his work. Showing an app running in the iOS simulator on a Mac **just isn't as powerful** as showing the app on an actual phone.

## JamMate

![Jammate splash screen](images/jammate.jpg)

JamMate is a social network for musicians. It helps you find people based on their favorite genres and the instruments they play. When I met Bradley, he had already put in quite a bit of work on the project, but it was missing that critical piece of the puzzle: **it hadn't yet shipped**.

So, Bradley and I started sprinting as hard as we could. My involvement was pretty minimal. I hardly wrote any code. (I'm a total newbie with React Native.) My primary role was to **help Bradley identify those barriers** I mentioned earlier and **figure out how to take them down**.

## The Barriers

1. Bradley wanted to implement **messaging between users** to allow users to kick off their musical collaborations.
2. JamMate uses Facebook for authentication, and Facebook requires you to **submit your app for review** when you use their API.
3. The database had **no security**. Any user could read or write anything.
4. The app needed to be **built and submitted to the app store** for review.

These were the biggest issues, but Bradley had a laundry list of other smaller issues and bugs too.

- The user list showed every user, even if they were too far away from you to make a quick meetup and collaboration practical.
- Related: Since he was using Firebase, he couldn't query to limit the user list. He would have to pull in all the users and filter them on the device.
- The user list showed every user… even you.
- The user's location was grabbed from the device as a set of coordinates but was then run through an API and stored as a zip code. That would eventually cost money after he hit the API's free limit. It would also make filtering by proximity a bit harder.
- It was impossible to delete your user account
- Parts of the UI were rough

## Breaking Through

Here are the steps we took to break through the barriers:

1. **Defined the goal**. What were we actually trying to accomplish here? Was it to build a wildly popular social network and get a billion users, or was it to see what it's like to ship an app? This will drive all the following steps.
2. **Ruthlessly trimmed the list of issues** to a list of issues we must fix before the first release.
3. **Found the blockers**. We looked through our list of critical issues and found any that might stop us in our tracks. The only major one we found was the Facebook review. This would have to happen before the app could be submitted to the app store.
4. **Dealt with the blocker first, then everything else**. By doing the absolute minimum we could to deal with all our blockers, we were able to ship in very little time. In a couple of weeks from conception of the Ship It School sprint, we were able to get the app submitted to Apple for review.

But there's way more to it than that. For instance, we found that we didn't need to get a Facebook review since the app only uses it for authentication (🎉), _but_ we learned we would need a public privacy policy for the app (💩 ). We learned hundreds of tiny lessons like this one along the way, but, more importantly, **Bradley is now super-close to having absolute proof that he can ship a mobile app**. (I'll report back with a post-mortem once the app is live.)

<!-- If this sounds like the kind of help you need to hit your web development goals, sign up for a free mentoring session!

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

If you want to see that learning happen in real-time and watch us power through exactly what it takes to ship, **watch the first season of Ship It School below**. 👇

https://www.youtube.com/watch?v=ZT04INboZ3s

https://www.youtube.com/watch?v=hlY0ucnLcY4

https://www.youtube.com/watch?v=T2CMatAM-iU

https://www.youtube.com/watch?v=4Tr707zef8E

## Look Out for Season 2

I'm already planning toward season 2 of Ship It School. If this kind of thing appeals to you, sign up to get notifications when I go live with a new episode.

[thrive_leads id='2075']

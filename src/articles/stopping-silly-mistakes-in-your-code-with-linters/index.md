---
title: 'Stopping Silly Mistakes in Your Code with Linters'
date: '2018-03-16'
tags:
  - 'code'
  - 'editors'
  - 'linters'
  - 'linting'
  - 'syntax'
  - 'tips'
description: 'If youʼve ever been frustrated by making a tiny syntax mistake in your code, you should be using code linters. With modern editors, itʼs easy! Iʼll show you how.'
coverImage: 'images/lint-roller.jpg'
---

## Allow Yourself a Few Silly Mistakes

New coders often spend a lot of time searching their code for simple problems like missing closing braces or tags, referencing variables that haven't been defined, or attempts to use strings without quotes. It's actually not such a bad thing to struggle with these kinds of issues a few times. Nothing helps you remember the rules of a languages syntax like beating your head against code that doesn't work for hours only to find out you were missing a curly brace.

Once you've had that experience a few times, though, you're going to want to move on. **Linters are the way forward.** They check your code to make sure the syntax is valid.

## When you're Done Searching for Commas, Try Linters

Linters come in many forms. Some are web-based, some are inside your editor. You can find linters for any language under the sun. Many of them can be configured so that you can decide how heavy-handed they are with their suggestions or even which way they fall on certain style decisions developers love to fight about ([tabs vs. spaces for indentation](https://softwareengineering.stackexchange.com/questions/57/tabs-versus-spaces-what-is-the-proper-indentation-character-for-everything-in-e), for example).

### Web-Based

We'll start by looking at a web-based linter to get a feel for how they work. [JSHint](http://jshint.com/) has a nice web interface. To use it, you just paste your code into the pane on the left to see your warnings on the right. The warnings will point out potential problem areas in your code.

![JSHint interface](images/jshint.png)

JSHint gives you a "Hello, World" app to start. For this example, I've simply removed the closing brace on that app's `main` function. The warnings show the problems this created.

The best way to use this when you're getting started is to copy and paste your code into the editor on the left and review any warnings on the right, making changes where needed. Ideally, you'll want to have as few warnings as possible, but some are safe to ignore. Linters are usually opinionated, and you don't have to agree with all their opinions.

Once you've sampled linters from the comfort of your browser, you'll want an easier way to use them while you're coding. For this, you'll want to use editor extensions.

### Editor Extensions

The most convenient way to use linters is right inside your editor. After installing a linter of your choice, you'll get real-time code checking as you write.

Now, instead of having to switch over to a browser when you're ready to check your code, **it will happen as you type**. Here's the same code from our JSHint example but checked inside VS Code:

![Linting in VS Code](images/linting-in-vscode.gif)

The editor underlines the problem area with a squiggly. Hover over it to get the warning text.

You can search for VS Code linter extensions and find tons of options. If you want to just use the ones I use, check out [my linter starter kit for web developers](/visual-studio-code-linters-starter-kit-for-web-developers).

## Linters: Helpful But Not Autopilot for Coders

You may be wondering why, in the previous case, the semicolon at the end of the file is underlined to notify you that you're missing a curly brace when the brace actually goes on line 3. The reason for this is that **the linter doesn't know where the brace should be**; it knows only that it's missing. It knows the brace should appear before the end of the file, so it underlines the last character in the file since that's the last place the brace could have been.

The web-based linter took a slightly different approach and instead underlined the _opening_ brace. **No linter can tell you where the brace should go**, but each has an approach to telling you that it's missing.

This highlights the fact that, although linting is extremely helpful in avoiding simple errors, **it won't write your code for you**. You still have to do some interpretation to understand the problem the linter is telling you about, and you'll have to figure out how to fix it.

Program errors take other forms too. If the problem in your code is **logic instead of syntax**, a linter won't help. Here's an example of what I'm talking about:

```javascript
let tenMore = (num) => num + 11;
```

This is a function called `tenMore`. Problem is, it actually adds 11. This is a logic error. The code is perfectly valid Javascript. The only problem with it is that it doesn't do what we expect. **No linter will catch this**, and, since logic is what makes your application work the way you intend, you can't write a good app without working logic.

This isn't to say linters aren't useful. They make sure you don't have to spend as much time with your magnifying glass out making sure you have all your commas, braces, and parentheses. You can instead direct that time and energy toward **making the logic sound and adding features your users want!**

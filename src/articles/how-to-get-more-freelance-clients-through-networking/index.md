---
title: 'How to Get More Freelance Clients Through Networking'
date: '2018-08-10'
tags:
  - 'freelancing'
  - 'networking'
  - 'sales'
coverImage: 'images/mr-wormwood.jpg'
description: 'Truth is, you canʼt build a career by pretending to be peopleʼs friend until they give you money. Hereʼs what you can do instead.'
---

**Just don't**. Don't try it. You're only embarrassing yourself. You're not fooling anyone. When your mindset is that you're going to "network" and find tons of business, **you're setting yourself up to lose**. Sure, you might pick up an occasional client with this. You might even find enough work to pay your bills... but **you can do so much better** if you change the way you think about "networking."

![Mr. Wormwood from Matilda](images/mr-wormwood.jpg)

Find people who are doing things you're interested in and **become friends with them**. Find them at meetups, conferences, parties… go to where the people you want to work with already hang out. Be interested in them and what they're doing. Be curious about their problems. Share your resources and contacts with them. **Help them in any way you can.**

<!-- [thrive_leads id='1378'] -->

This is not a sales tactic, but it will result in sales 💰. This is not "networking," but it will accomplish many of those same goals with better results and without the same feeling of ickiness. It's the journey, not the destination. Your primary goal should be **the friendships and the help you can provide**, not some flash-in-the-pan contract you get because you said the right things or "faked it till you made it" during that first meeting. That approach reeks of desperation, and the people you would actually *want* to work with can smell it from a mile away.

If you make it your mission to help people however you can, some of them will reciprocate. Not _all_ but some. This will be far more substantial than the quick WordPress site you're able to squeeze out of someone with a slick sales pitch. They'll bring *their* business to you. They'll refer their *friends* to you. **They'll take the same interest in your success that you took in theirs**, and both of you will be better for it.

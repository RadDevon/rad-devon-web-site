---
title: 'The Pitfalls of Chasing Black-Hat SEO'
date: '2013-08-21'
tags:
  - 'marketing'
  - 'seo'
coverImage: 'images/web-site-analytics.jpg'
---

I've always thought the whole SEO thing was a little slimy. Trying to reverse-engineer Google's ranking algorithm so that your web site can be engineered to exploit it just seems gross. Perhaps, then, you could argue my observations about SEO are simply confirmation bias. That may very well be true, and I won't even attempt to defend myself against those accusations.

Fortunately, it seems that, with every update to the ranking algorithm, my feelings are vindicated as Google punishes those who have exploited the algorithm. I recently heard an anecdote from an organization which makes significant effort to manipulate Google search ranking. One of their strategies is to pay for inclusion in online directories related to their industry. Since Google ranking is heavily influenced by the number of incoming links, this helped their ranking... for a while.

It was apparently a common enough practice that Google took notice. They updated their algorithm to, instead, _penalize_ any site listed in a paid directory such as this. At this point, the company instead needed to _remove_ their site from the paid directory. The paid directory, knowing it was on the fast-track to irrelevance, found a way to make a quick buck on the way out. They began charging sites to be removed from their directory. It's dirty, no doubt, but you're unlikely to get much sympathy if you're trying to pay for search ranking in the first place.

It seems like a fitting end to a story of unscrupulous attempted manipulation of an algorithm designed to measure actual value in the real world. That _is_ the intent of Google's algorithm: to measure the actual value of resources on the Internet to real users not to measure clever and morally confused opportunists' ability to figure out the machinations of said algorithm.

[thrive_leads id='1366']

All of this is not to say that no SEO knowledge is worth having. It's worth knowing, for example, that incoming links influence your ranking, that sites [need to be mobile accessible](http://blog.zobristinc.com/google-optimize-your-site-for-mobile-or-else-be-penalized/) (and redirect to the correct mobile pages when they aren't responsive), and that pages filled with h1s will not do well in search ranking.

Rather than treating every tidbit of SEO information as a sign to completely change your strategy, make your SEO knowledge a small part of your overall strategy. SEO should be considered but should never be the guiding principal.

Instead of chasing links, try these changes that will improve your ranking:

1. Make your site **a joy to use on mobile** as well as the desktop.
2. Produce the kind of **great and desirable content** that will organically make people link to you.
3. **Interact** with your people, whoever and wherever they are.

These tips, besides being more fun than min-maxing an algorithm, are less likely to be mitigated in future updates to the ranking algorithm. If you're delivering a great experience for your users, you're devoted to a practice Google and other search engines will strive to reward for years to come.

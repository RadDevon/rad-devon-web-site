---
title: 'Build a REST API in Node.js'
date: '2020-07-03'
tags:
  - 'apis'
  - 'backend'
  - 'dependencies'
  - 'javascript'
  - 'node'
  - 'node-js'
  - 'nodejs'
  - 'rest'
  - 'servers'
  - 'time'
  - 'tutorials'
coverImage: 'images/learnyounode-5-thumb.jpg'
---

In this final video of my Node.js Javascript series, we'll be building two different HTTP server projects: one that returns a string submitted by the user via POST request transformed into upper case, and a REST API that returns time passed in on a querystring as a JSON object.

https://youtu.be/0quNn4xLe_c

## Video Notes

Some of the things you'll learn:

- 📖 How to use the Node.js documentation
- 😖 How to find help using search (Google or my favorite DuckDuckGo) and StackOverflow
- 🧐 How to write readable, maintainable code
- 🗣 How to respond to an HTTP request
- ❓ How to parse a querystring
- 🛠 How to debug code that doesn't work
- 🤝 How to use a third-party library
- 🕰 Just what the heck is Unix time anyway?

If you missed the rest of the series, you may want to go check out the first episode and [get started with Node.js from the beginning](http://articles/first-steps-with-node-js-part-1/)!

You might also enjoy [my quick 30-minute Vue.js tutorial](/articles/build-your-first-vue-js-app/).

Thanks for watching! If you have any questions, please post them in the video comments [on YouTube](https://youtu.be/0quNn4xLe_c) or [email me](mailtp:devon@raddevon.com).

<!-- Now that you've gone through my final Node beginner tutorial, it's a great time to put your new knowledge to work. I have a project starter kit that will give you a boost into your first practical project. The idea is called Timeless. It’s a social network that allows only a single post per year. Share your email below to get a free starter kit to get you up and running quickly. 👇

[thrive_leads id='3587'] -->

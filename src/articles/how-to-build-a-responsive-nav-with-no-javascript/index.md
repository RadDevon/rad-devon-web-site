---
title: 'How to build a responsive nav with no Javascript!'
date: '2021-02-09'
tags:
  - 'css'
  - 'html'
  - 'tutorials'
coverImage: 'images/css-collapsible-nav-thumb.jpg'
---

In this video, I'll teach you to build fancy collapsible navigation without using any Javascript. Just HTML and CSS!

I love that we can make clever use of CSS and HTML to achieve something most people use Javascript for, but you won't get far as a web developer without Javascript. That's why I made this cheat sheet to help you learn it:

[thrive_leads id='3815']

If you want to work along with the video, start from this [pen](https://codepen.io/raddevon/pen/xxRVmOQ). Open that and click the "Fork" button in the bottom-right so you can get your own copy to work with.

https://www.youtube.com/watch?v=tFXbQS0zQe0

The CSS framework I used is [Milligram](https://milligram.io/). I like to use it because it takes just basic HTML and makes it look nice. I don't have to sprinkle a bunch of wacky classes around to get something that looks decent.

I talk about the "+" selector and refer to it in the video as the "sibling selector." More accurately, it's the _adjacent_ sibling selector. It only selects the UL if it is a sibling of the checked checkbox and also if the two are adjacent (meaning no elements between).

Hope you enjoy this tutorial! [Tweet at me](http://twitter.com/raddevon) if you have questions.

---
title: 'Web Developer Roadmap Step 1: Set Your Big Goal'
date: '2020-01-10'
tags:
  - 'goals'
  - 'motivation'
  - 'start here'
coverImage: 'images/thumbnail.jpg'
description: 'If you donʼt understand why you want to be a web developer, youʼll never be able to push through when it gets tough. Itʼs not just because you want to make more money. Dig deeper. Iʼll show you how.'
startHereDescription: 'This video series is a good overview of the approach I took to getting into web development with no connections and no experience. If youʼve tried applying and arenʼt getting anywhere, this might be the alternative path need. Follow the links from this video to the others in the series.'
startHereOrder: 3
---

https://youtu.be/taV-BsYqkos

## Other Videos In this Series

If you like this video, watch the others in the series for a good overview of the philosophy I used to break into web development with no connections or experience.

- [Step 2: Learn HTML and CSS](/articles/2020-web-developer-roadmap-step-2-learn-html-and-css)
- [Step 3: Learn Javascript](/articles/2020-web-developer-roadmap-step-3-learn-javascript)
- [Step 4: Learn Terminal, Git, and Deployment](/articles/2020-web-developer-roadmap-step-4-learn-terminal-git-and-deployment)
- [Step 5: Relationships Are Make or Break](/articles/2020-web-developer-roadmap-step-5-getting-work-through-relationships)
- [Step 6: Time to Get Paid!](/articles/2020-web-developer-roadmap-step-6-time-to-get-paid)

## Transcript

Hey, it's Devon, and I'm kicking off 2020 with **a series of videos walking you through my process for becoming a web developer**. Let's start with **setting a Big Goal**.

Understanding why you're doing this is the first step to becoming a web developer. In my last video about new year's resolutions, I talked about how you might arrive at a Big Goal, so if you want the full detail on this, [go back and check out that video](/articles/your-new-years-resolution-makes-it-harder-to-become-a-web-developer/). But just to quickly summarize what the process is:

First, you'll start with **a surface level statement of what you want to accomplish**, which is basically, "I want to become a web developer." From there, you're going to start asking yourself, "why?" "Why do I want to become a web developer?" Get the answer to that, and you're probably gonna need to **ask why again**. Just keep peeling back the layers of that onion until you get to the thing that you actually want – the reason you're actually here doing this – and that's what I call your Big Goal.

That's it. It's a simple process, but **it's deceptively difficult to actually peel back those layers** and get to what you really want, the real reason you're doing this. So if you need some help, I've got [a short email course](https://raddevon.com/stick-with-web-development/) that will sort of hold your hand and take you through the process, and you can always hit reply on one of those emails and ask me a question or run an idea by me and I'm happy to help.

Now that you have your goal, you've got to find ways to try to **keep that goal top of mind**. You want to be thinking about this all the time. The reason this helps is that, when you start getting frustrated – whether it's because you're having trouble trying to set up a development environment, or you just can't wrap your mind around Javascript closures, or maybe you're past all that stuff and you're having a lot of trouble getting your first freelance client – whatever that frustration is, **having a light at the end of the tunnel can help you push through that and keep working until you can break through**.

**You only have so much willpower.** Once that's exhausted, the next thing you know, you're spending every night on the couch in front of Netflix again. Having your sights set on something that would massively change your life helps you recharge that willpower and keep pushing towards your goal.

I use a couple of strategies to help keep my Big Goal top of mind, and the simplest one is sticky notes. **Just write your goal on the note, and put this somewhere that you're going to be looking on a regular basis.** Try the mirror that you look at when you're getting ready in the morning or… when I was going through this, I was on my computer all the time, basically, so I would put them on the bezel around my monitor so that I could always see them while I was working.

Sticky notes are a really low-effort way to remind yourself, but they don't do that much to make the goal seem real. I've mentioned on this channel before that my goal when I started down this path was to move to Seattle. When I got serious about that goal, **I started planning toward it even though I didn't have the means at the time to actually make it happen.**

I looked up all the neighborhoods in Seattle on sites like AreaVibes and WalkScore to try to find the ones that best fit the lifestyle that I wanted to live here. And then once I had found their neighborhoods that I thought would be good for me, I went to apartments.com, and I used their draw tool to draw a little circle around one of those neighborhoods and I'd see what apartments were available. If I found one I liked, I would do a 3D walk-around to get a sense of what it would be like to actually live in that space.

I did the same kind of thing on Google Maps. I'd search for one of the apartments I liked and grab the little Street View person and drag them over and drop them on the street and just take a walk around.

I even made a database in AirTable of all the apartments that I thought would work well for me and put in all the metrics that were the most important to me, like crime rates and walkability score, and of course rent and all those sorts of things, so that I could look through them and start to even rank them.

I started reading local blogs, and I even subscribed to a couple of email newsletters for Seattleites so I could feel like I understood the place and its people a little bit better.

This, of course is just an example of how you might go about making your goal seem really real and really attainable. You may not want to move somewhere, in which case these specific techniques would not work for you. On the other hand, you may want to move somewhere and maybe you're not quite as neurotic as I am, so all this planning ahead of time might make you anxious more than anything else. **You need to find some techniques that work both for your goal and for your personality.**

Now that you have a goal to propel you forward and to help you refill your willpower reserves along with some techniques to help you keep that goal top of mind, it's time to start building some of the skills you'll need to actually do this kind of work, in the next step of the roadmap, we're going to talk about learning HTML and CSS which are the foundational technologies of web development. I'm not going to teach you those in the video, but I will show you how you can get started learning them on your own.

Be sure to subscribe here on [on YouTube](https://www.youtube.com/channel/UCxgZYWJq2Cxk7vIePGiTxFw) so you don't miss the rest of the roadmap and check out my other articles for more help becoming a web developer. I'll see you in the next one.

---
title: 'Kill Your Web Dev Portfolio ☠️'
date: '2020-08-21'
tags:
  - 'career'
  - 'freelancing'
  - 'portfolios'
coverImage: 'images/kill-your-portfolio-thumb.png'
---

In this video, I share **why I don't have a web development portfolio** and **how I'm able to get gigs despite that**. We're hard wired to want to build things, and the desire to spend hours and hours crafting a portfolio plays right into that. Resist the urge, and take the advice in this video instead.

https://youtu.be/KaTLsg_-2Nk

This is primarily from the perspective of a freelance web developer, but there's some truth here if you're looking for a full-time gig as well. Sure, the company is probably going to want to see a portfolio, but it's not nearly as important as other means of building trust.

<!-- If you _are_ looking for full-time work and having trouble breaking into the industry, try freelancing first! You've probably noticed that even junior gigs require a couple of years of experience. How can you get that if you can't get hired? Freelancing is the way to get experience before you get hired. Get started here 👇.

[thrive_leads id='1378'] -->

---
title: 'Pushing Through: Stoicism for Aspiring Web Developers'
date: '2019-07-05'
tags:
  - 'career'
  - 'philosophy'
coverImage: 'images/marcus-aurelius.jpg'
description: 'Becoming a developer is tough. Youʼre going to reach a point where you feel like quitting. Stoicism can help pull you back from the edge of that cliff and get you moving again.'
---

You've decided to become a web developer. Awesome! For a few weeks or months, remaining committed to that goal will be easy. You'll be excited, constantly learning new things, and exercising creative muscles your current job may not be working at all. You'll feel **more alive** than you have in a long while.

It's not always going to feel this way, though, and whether you achieve this goal **hinges on whether you can push through when things are _not_ so rosy**. You can't get Webpack to build your project and you're not sure why. You've been applying to jobs for months and haven't even gotten a callback. You just posted a question on StackOverflow, and, instead of an answer, it got modded into oblivion.

Here come the Ancient Greeks to the rescue!

![Statue of Marcus Aurelius wearing a poorly Photoshopped purple cape](images/marcus-aurelius.jpg)

## What Is Stoicism (And Why You Should Care)

You might not have thought to look to ancient philosophy for the solution to this problem, but Stoicism is not just any ancient philosophy. Where many schools of philosophy deal with the ways society should be structured or the nature and meaning of the universe, Stoicism is all about **how individuals can deal with their problems**.

I'll take quotes from a few prominent stoic philosophers and explain how this can help you on your journey to becoming a web developer.

> Complaining does not work as a strategy. We all have finite time and energy. Any time we spend whining is unlikely to help us achieve our goals. And it won't make us happier.
>
> Randy Pausch

We have tons to complain about as web developers — as humans, really — but it's not useful. Figure out the way forward, and take it.

This is from [Randy Pausch's Last Lecture](https://www.youtube.com/watch?v=ji5_MqicxSo). He's a modern hero of Stoicism. Watch if it you haven't already.

> Fate leads the willing, and drags along the reluctant.
>
> Seneca

People ask how I feel about simple web site builders like SquareSpace, WordPress.com, and Wix. I've heard many developers and designers lamenting the existence of tools that make work they have traditionally done easier and more accessible.

I have a different way of looking at it. Web site builders are a great option for someone with basic needs and a low budget to get something great with just a few dollars a month and some sweat equity! Instead of being angry about these tools, **I'll actively refer clients to them**. When I do this, I build trust by giving them an option when I was too expensive or just not the right tool for the job.

Later, when they've had some success and need something more than their web site, **they'll come back to me**. These tools make things easier on my clients and clear up my schedule to work on **more interesting projects**. Awesome!

Web development changes fast, so you'll go through many similar transitions in your career where the thing you used to do is no longer valuable. You can sulk because the framework you dumped time into learning is no longer relevant, or you can find **a new way to add value** for your clients and employers.

> Cling tooth and nail to the following rule: not to give in to adversity, never to trust prosperity, and always take full note of fortune’s habit of behaving just as she pleases, treating her as if she were actually going to do everything it is in her power to do. Whatever you have been expecting for some time comes as less of a shock.
>
> Seneca

I've had my own version of this philosophy since long before I had heard of Stoicism: **Expect the worst, and hope for the best.** (Just ask my family; they're totally sick of hearing it! 😜)

You're going to need this when you're on the final round of an interview or when a freelance contract is almost signed. You will intuitively start to celebrate, and, just as you do, **everything will fall apart**.

Instead, **carry on as though you don't even _have_ this opportunity**. Keep lining up the next interview, the next gig… If the first one comes through, great! If not, you still have some bright alternatives to work toward.

> The chief task in life is simply this: to identify and separate matters so that I can say clearly to myself which are externals not under my control, and which have to do with the choices I actually control.
>
> Epictetus

You _can't_ control whether someone will hire you based on your résumé, but you _can_ control **how many desks that résumé is on**.

You _can't_ control whether you'll get a freelance gig when your bank account is almost empty, but you _can_ control **how many you've sent in proposals for**.

You _can't_ control whether a client will choose your favorite solution from the three options you present to them, but you _can_ control **what you say to educate them** on why they should.

Spend lots of time on the bits you can control and no time on the bits you can't.

> That’s why the philosophers warn us not to be satisfied with mere learning, but to add practice and then training.
>
> Epictetus

_This_ philosopher is telling you to **[stop doing tutorials](/articles/stop-doing-coding-tutorials/)**. I don't mean you should _never_ use them, but stop leaning on this crutch as the only way you'll learn.

Tutorials are fun because you follow a cool recipe and you end up with a working thing that makes you feel good. Unfortunately, that's not what development is. Development is having **nothing but a problem** and having to build a solution in software. It's choosing between **4 bad options**, each with caveats you think you can't accept… but you have no choice. Development is taking these challenges and **coming out the other side with a solution despite them**. Tutorials don't teach any of that, so, if you want to be a developer, use them sparingly.

> Don’t seek for everything to happen as you wish it would, but rather wish that everything happens as it actually will—then your life will flow well.
>
> Epictetus

> First say to yourself what you would be; and then do what you have to do.
>
> Epictetus

You want to be a web developer, or else you wouldn't be here. The more rigidly you define your goal beyond that, the less likely you are to achieve it. If you only count a success if you go to a university for 4 years, graduate with a computer science degree, get an internship with Google, and then get hired into an engineering role at the company working on analytics for YouTube, good luck. **You've defined exactly one way to succeed and left an infinite number of ways to fail.**

Set your goal loosely, choose a path you think might get you there, and start walking down it. Keep your eyes open to other ways you might meet your goal. I changed careers into web development, but I've known I wanted to build software since I was about 16. I thought I would go to college for 4 years and they would hand me off to a great company where I would work until I retired.

Instead, I worked at Walmart while I went to a community college. After that, I started going for my Bachelor's Degree at the University of Tennessee until my daughter was born. I didn't really have time to keep going to school, so I didn't enroll after that semester. I bounced around to other jobs, none of them in software until I set a goal to move to Seattle (yes, not even a goal to be a developer; [here's why](/stick-with-web-development/)) in my 30s. I was able to achieve that goal in part because **I didn't define exactly how it had to be achieved**.

> If a man knows not which port he sails, no wind is favorable.
>
> Seneca

Many people ask me [which language they should learn](/articles/which-web-development-language/), [which laptop they should buy](/articles/the-best-laptop-for-web-development-its-complicated/), or [which editor they should use](/articles/best-ide-beginning-web-developer/). I answer these questions with a single recommendation and minimal explanation even though my path to get to those recommendations was more nuanced. Why? **Because these questions are distractions.**

Even questions that seem bigger **really aren't**. If I freelance, should I incorporate or operate as a sole proprietorship? What kind of customer should I try to serve? Should I pay $500 to join this BNI group?

**The path of least decisions** is the one that will get you moving in the right direction. Pick a direction and go in it. Correct later. Any of these decisions can be changed **in almost an instant**. When you're looking back at all the opportunity you lost because you decided to be indecisive, that will be the one decision you _can't_ change.

> Be tolerant with others and strict with yourself.
>
> Marcus Aurelius

As you're submitting résumés, talking with recruiters, or trying to pick up freelance work, you're going to find that **people flake on their commitments to you**. People do this **a lot**. You _don't_ want to do this because it's a bad look for you, no matter what you're after, but, at the same time, you want to be understanding when others do it.

When I first got started, I was scheduling meetings with people left and right. Whenever someone stood me up at a time we had agreed upon, I would go home fuming. I would think to myself, "I can't believe they left me sitting there. What's wrong with people? Are they all just jerks?"

That lasted a couple of weeks until **I became the jerk**. I had scheduled a meeting, gotten preoccupied with a project, and totally forgotten to go. Since I know _I'm_ not a flake, that experience allowed me to **make room for other people to make mistakes**. Today, I can tolerate and forgive just about any mistake because I've made them all myself.

This doesn't mean you should keep relying on people who consistently flake. Give them a couple of chances with no strings attached, then move on. My goal is to be 100% reliable myself while allowing others the leeway I hope they will someday allow me.

> You could leave life right now. Let that determine what you do and say and think.
>
> Marcus Aurelius

Of course this isn't the right time to change careers because you've just had a new child or you're going to have one or you just moved or you're about to get a promotion. Life is always changing, and you've always just done _something_. If you wait until life slows down to change careers, **you'll never quite get there**.

Since your work life is such a big part of your life, you should be satisfied with it. Don't wait for the right time. **Take the plunge**.

## Steel Yourself for the Road Ahead

Use these tools to keep you going when it feels like you'll never become a web developer. If you want more tools to help, take [my Big Goal course](/stick-with-web-development/) for another important tool to **refill your willpower and keep you moving in the right direction**. You will get there, but it will take work, persistence, and a Stoic outlook to build your future.

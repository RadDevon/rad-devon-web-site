---
title: 'Web Developer Gift Guide for 2020'
date: '2020-12-11'
tags:
  - 'ergonomics'
  - 'gifts'
  - 'hardware'
  - 'software'
coverImage: 'images/gift-guide-2020-thumb.jpg'
---

Check out these gifts for a web developer in your life or for yourself! These will make any web developer more productive. Some of my picks will even make them safer through the magic of ergonomics!

The links to all my recommendations are below the video. 👇

https://youtu.be/TFIcUn51-6s

- [Logitech MX Vertical Wireless Mouse](https://amzn.to/3a0WOzF)
- [BRILA Wrist rest for mouse](https://amzn.to/3gBrf0B)
- [XDesk Terra standing desk](https://www.xdesk.com/terra)
- [A cheaper standing desk alternative](https://www.upliftdesk.com/uplift-v2-standing-desk-v2-or-v2-commercial/?product_config=4853)
- [Topo Ergodriven standing mat](https://amzn.to/3m6wIOi)
- [Ducky One 2 Keyboard](https://amzn.to/2Lf4qnV)
- [1Password password manager (for them)](https://1password.com/giftcards/)
- [1Password (for you)](https://1password.com/sign-up/)
- [Backblaze backup service (for them)](https://www.backblaze.com/partner/af9rzg?redirect=gift.htm)
- [Backblaze (for you)](https://www.backblaze.com/cloud-backup.html#af9rzg)
- [Sony wireless headphones](https://amzn.to/3gBA7DF) (not the ones I have, but the ones I would get today)
- [Logitech C920 webcam](https://amzn.to/3qEzk9w)
- [BZE tripod/selfie stick](https://amzn.to/2JRSXKN)

Share your ideas in the comments [on YouTube](https://youtu.be/TFIcUn51-6s). Thanks for watching!

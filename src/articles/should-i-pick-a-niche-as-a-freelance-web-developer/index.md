---
title: 'Should I pick a niche as a freelance web developer?'
date: '2019-02-22'
tags:
  - 'freelancing'
  - 'marketing'
  - 'sales'
coverImage: 'images/crowd.jpg'
description: 'People say picking a niche as a freelance developer makes marketing easier. But that means you have to turn down perfectly good work, right? Hereʼs my take on the question of selectig a niche.'
---

One of the scariest steps you can take as a web developer is to choose a niche. You draw a line in the sand that defines your ideal client. You're choosing who you're going to work for, but, at the same time, you're choosing **who you're _not_ going to work for**. Here's why it's still an important step to take.

![A crowd of people](images/crowd.jpg)

As much as we hate to admit it, marketing is critical to freelance web developers. Without marketing, you'll never have clients. Marketing is like standing in a huge crowd of people, shouting to them. **What can you say to get the attention of the people you want to hire you?**

Imagine yourself standing in a crowd like the one pictured above, trying to get your message across. You start by yelling "Hey!" A few people turn to look, but, as you start talking, they realize **what you're saying isn't relevant to them**, so they look away.

Now, you try again by yelling, "Hey, guy in the white shirt!" Now, 80% of the people in the crowd will _not_ look because they know you're not talking to them. There's a good chance that **60-70% of the people in the white shirts _will_ look** though.

You could take this a step further and yell, "Hey, Dave!" There may only be a single Dave or maybe two, but **every Dave is going to be looking**. You have their attention.

## Breaking Through Noise

This metaphor is useful for developers because, to get clients, you must first get their attention. Intuitively, we think that the bigger the audience we're trying to talk to, the more clients we can get. The counterpoint is that, **the more relevant you can make your message, the more likely people are to listen**. That's where having a niche can help. The "Dave" niche won't work in this case because it's too small, but the ideal niche is probably much closer to "Dave" than it is to "everyone."

So, what's more effective? A generic message to a mass audience or a tailored message to a niche audience? Let's do a quick thought experiment to find out.

You're a freelance web developer. In that role, you need lots of things like tax prep, a bank account, and maybe even office space. Check out the comparison below and think about which offerings you find more compelling.

### Grudge Match! 🤼

- Mass market appeal vs. Niche appeal

- “We make (tax) filing simple and easy.” vs. “I specialize in tax preparation and planning for independent contractors, freelancers and other sole proprietors.”

- “We help you stay in sync with your needs today — and tomorrow.” vs. “Fee-free small business banking that helps entrepreneurs start, grow, and succeed.”

- “Space that works for you.” vs. “A coworking and shared office space for independent entrepreneurs, developers, and creatives.”

When people are buying anything, they want to buy from someone **who understands their problems**. We don't even know yet what distinguishes these niche services from the mass market services, but we know they are talking to us. Once you learn that the tax service can typically get larger returns for freelancers thanks to a deep understanding of how tax law applies to them or that the bank offers invoicing baked right into the account, the decision becomes **a no-brainer**.

By slicing out a niche, you move out of the dangerous waters of competing for everyone's business and **into "no-brainer" territory** for your clients. You'll be able to offer additional value that speaks specifically to your ideal client's needs. Even though it will alienate potential clients who are _not_ in your niche, it will make those who _are_ love you even more.

Now, let's look at some practical tips for selecting the right niche for your web development services.

<!-- ![Bradley Serlis headshot](images/bradley-300x300.jpg)

## “How should I set my freelance web development Rate?”

— Bradley Serlis… and many others

![Freelance Web Development Pricing ebook on a tablet](images/freelance-web-development-pricing-ipad.png) [Read the First Chapter!](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/) -->

## Tips for Selecting a Niche

1. **Find a niche that makes money.** I've been getting really into browsing [pictures of people's custom mechanical keyboards](https://www.reddit.com/r/mechanicalkeyboards) over the last couple of weeks, but that would be a terrible niche since people generally don't make money from them. If they don't make money, they won't have the money to pay me for my services.
2. **Start with your connections.** Take inventory of the people you know. Do they own businesses? Where do they work? Make a list of the industries they work in and pick a niche from this list. It's way easier to talk to a friend or loved one to start getting the data you need to properly serve your new niche.
3. **Don’t wait for the perfect niche.** It doesn't exist. Pick something and get started. If it doesn't work out or you just don't like it, you can always change it later.
4. **Go narrower than you think you should.** When I advise people to pick a niche, they almost always try to start with "small businesses." This is too broad. You feel like you met your obligation, but you haven't achieved the goal of making it easier to talk to your niche. To make this concrete: small businesses is way too broad. Small _fitness_ businesses is still too broad. Yoga studios might be narrow enough, but you could probably go even narrower.

## What to Do With Your Shiny New Niche

Now that you've selected a niche, talk to lots of people in your niche to **get a good understanding of the common problems they have**. Knowing this stuff gives you **credibility** when you're talking to other people in the niche, but it also gives you **a problem to solve**. The solution to their toughest problems is actually what you're selling. Web development is simply how you build that solution.

You'll know when you have the right niche, the right problem, and the right solution. When you tell people in your niche about what you do, **you won't have to bring them to the sales conversation**. _They'll_ be asking _you_ how they can start working with you.

!["Shut up and take my money" meme](images/shut-up-and-take-my-money.jpg)

---
title: 'As a freelance web developer, what are you actually selling?'
date: '2019-04-19'
tags:
  - 'freelancing'
  - 'marketing'
coverImage: 'images/street-vendor.jpg'
description: 'Itʼs Javascript, Python, CSS, and React, right? RIGHT?!? Listen, I hate to break it to you, but… itʼs none of those things.'
---

To make money, you need to sell something. Most new developers think we're selling **Javascript, Python, CSS, or React**. That may be what you're _selling_, but that's not what anyone is _buying_.

![Street vendor selling trinkets out of a suitcase](images/street-vendor.jpg)

## What are they buying?

Whether you're a freelancer or an employee, the people and companies you're selling services to — we'll call them "buyers" — are not actually paying you because they want you to write code, even though they may present it this way.

It's an easy mistake to make. Job applications list out all the technologies you need experience with. Freelance gigs posted on sites like Upwork tell you exactly what you're going to be building and what tools you'll use to do it. This makes it easy to misunderstand what is valuable about our skills.

No one actually wants code, though. What good is code? It's the result of the code they want. If you want to make money, your result needs to make or save money for your buyer. The truth is that your buyer is actually buying **money**.

## What are you selling?

We've established that no one wants to buy code. They want to buy some result. Developers write code to create results, but the code is incidental.

That means, if you're selling **code**, you're making your buyer do a lot of the legwork for you. Here are the steps your buyer has to take before it makes sense to hire you to write code:

1. **Identify** a problem they need to solve.
2. **Calculate** the money they will make or save by solving the problem.
3. **Imagine** a software solution to their problem.
4. **Determine** the best technology stack for their solution.
5. **Find** the correct people to fill in each part of the stack. Hopefully, one of those will be you!

As what you're selling gets further removed from what your buyer actually wants, they have to overcome **increasingly difficult obstacles** to get to a place where hiring you makes sense. Fewer and fewer buyers will do this meaning you have a **much smaller pool of buyers** who could potentially hire you for your web development services. The service you're offering is **less valuable** because they already had to do the work of figuring out the solution.

The other problem with letting the client take all these steps is that, although they are an expert on their business, **they're probably _not_ an expert on software design and architecture**. They can easily make mistakes as they're conceiving of their software solution, deciding on a stack, and finding the best coders. That means, you might get hired for a job that's **doomed to fail** because one of those pieces is wrong. Once you work on a project that fails, **you won't be hired again,** even if it's not your fault.

## Achieving Alignment

Even if you're not selling the right thing, you will still find some buyers. Things really start to take off, though, when you can **align what you're selling with what the client is buying**. To do that, stop trying to sell code, and start selling solutions to problems.

If you're looking for employment, your focus on results will make you stand out from the conga line of coders coming through the door for interviews. Anyone can code, but not everyone can design and build software that makes money.

If you're looking for freelance clients, you move way up market when you start selling solutions instead of code. You're no longer a developer; **you're now a consultant**. (This is what consultants do: solve problems.) Not only that, but, instead of looking for people who want to hire coders, you can start looking for people with problems. Guess what? There are a lot more of those!

Instead of competing for scraps on Upwork, you can now talk to business owners and decision makers with **problems they don't even know can be solved with software**! You get to act as a consultant by offering them your solution. Once you've shown them the yellow-brick road, there's almost no way they're going to price shop for a cheaper developer to build it. **_You're_ the solution**, so you'll get hired without even having to compete.

<!-- *If you are ready to get started with freelancing, check out the Freelance Crash Course!* 👇

[thrive_leads id='2262'] -->

## How to Get Started

The best way to learn how to sell solutions rather than code is to get out into the world and **start talking to people who run businesses**. Find local meetups, chamber of commerce events, and other networking events for businesses. Use [my guide on how to network](https://raddevon.com/articles/how-to-get-more-freelance-clients-through-networking/) to make friends and discover problems. (It still works even if you're not interested in freelancing.)

Once you've discovered some problems, start thinking about possible solutions. Internalize this process of discovering problems and designing solutions to make sure you're always ready when you come upon a new problem in the wild. Changing your idea of what you're selling and exercising your problem-solving muscle are **the keys to igniting your career** as a developer.

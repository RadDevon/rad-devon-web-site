---
title: 'What Dungeons and Dragons 🐉 can teach you about getting your first dev job'
date: '2020-07-10'
tags:
  - 'career'
  - 'jobs'
  - 'networking'
  - 'relationships'
coverImage: 'images/d-and-d-thumb.jpg'
---

Turns out, it's not as easy as you'd think to get into your first online tabletop RPG campaign. You'd think if you're easy to get along with and you have the skills, someone would want to take a chance on you. Sound like your web development job hunt?

It sure sounded like mine.

https://youtu.be/o7fidT8WA7U

## Video Notes

My daughter decided she wanted to play a tabletop RPG over the summer. In-person is out of the question due to the pandemic, so she started looking for a group online.

She started the way many of you start looking for jobs: by shotgunning applications! 📃📃📃📃📃📃📃📃📃

If you've tried this approach in your job hunt, you may have predicted it didn't work. I want to share with you what _did_ work for her, and **how you can apply these lessons to your job hunt**. 🎉

Career transitions are hard. You need an anchor. Something to right the ship when you're desperate and it seems like things just aren't going to go your way. You need a Big Goal.

I can help. Take this free, quick email course to help define your Big Goal **so that you don't just quit** when things get tough. 👇

[thrive_leads id='1360']

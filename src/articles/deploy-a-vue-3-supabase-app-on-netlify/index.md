---
title: 'Deploy a Vue 3 Supabase app quick and easy on Netlify'
date: '2021-08-08'
tags:
  - 'deployment'
  - 'javascript'
  - 'netlify'
  - 'projects'
  - 'supabase'
  - 'tutorials'
  - 'vue'
  - 'vue3'
coverImage: 'images/timeless-supabase-deploy.jpg'
---

Several weeks back, [I built a social network using Vue and Supabase](/articles/build-a-social-network-with-supabase-and-vue-3/). At that time, I promised a video on the final piece of this app's puzzle: **deployment**. Now, that video is here! Watch and learn how to put your app live for the world to see. You probably won't get famous, but you will learn a critical part of the process that is often neglected.

If a tree falls and no one's around to hear, does it make a sound? If a developer builds an app and never deploys it, did they really build an app at all? In this video, we'll make sure we never have to answer these hard questions by just deploying the dang thing.

The best part? **It was super easy.** Watch and work along, and you'll see what I mean.

https://www.youtube.com/watch?v=ZhAORuOB3Dc

Please reach out [on Twitter](http://twitter.com/raddevon) if you have any questions.

<!-- If you want more resources on how to start your career as a web developer, you could do a lot worse than my ebook on **5 projects to get your career rolling on your own terms**. 👇

[thrive_leads id='4451'] -->

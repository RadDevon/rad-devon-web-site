---
title: 'Start Asking for What You Want'
date: '2019-06-21'
tags:
  - 'career'
  - 'freelancing'
coverImage: 'images/moving.jpg'
description: 'Asking for what you want sometimes leads to a surprising outcome: you just might get it. Hereʼs how to overcome your objections and start asking for exactly what you want.'
---

![Woman moving a box](images/moving.jpg)

I moved recently, and I hired a moving company to help because I'm a web developer with spaghetti arms. I paid them with a credit card, and their payment processing app asked me if I wanted to leave a tip. I was surprised when I saw this because I hadn't even thought about tipping the movers.

I promise I'm not a jerk. I tip well at restaurants, but, since I hadn't hired movers before, I hadn't considered the idea of tipping a moving company. By putting this option right in front of me, it made me stop and think. They had done a really great job. I'm still surprised they managed to get some of my IKEA furniture moved without it turning into a pile of particle board dust in the truck. Because they asked for it, I decided to tip, and, as a result, the moving company made **15% more money** than they would have if they hadn't asked.

I've written about how I started my career as a senior developer. This didn't happen because I'm an incredible 10x developer, but it didn't happen by accident either. It happened because, when a client came to me and told me they wanted to hire me, **I told them exactly what I wanted in return**.

## Why You Can't Do This

You absolutely _can_ do this, but here's why it _feels_ like you can't.

1. **You don't have enough experience.** This one may be a fair point. If you've been doing tutorials for the last 6 months, it's time to graduate to [real projects](/articles/10-great-web-development-learning-project-ideas/). If you've built a handful of personal projects, [start finding freelance work](/articles/get-first-freelance-web-development-job/) and build real projects for real money. If you've already done that, then this is _not_, in fact, you, which means…
2. **You lack confidence.** Confidence comes from years of coming up against problem after problem and being able to blow through them. Once you've done that for a while, you understand what you're capable of. Before you get to that point, you may have to take some leaps of faith. Think **not in terms of what you can do alone, but think also about all the resources you can draw support from**. Maybe you have friends who code. You can always hire someone on Upwork to help you out with a problem you absolutely can't solve. If you're on my email list, you can hit reply on any email from me to get advice and help. Even if you don't think you can do something, **you're never on your own**.
3. **You're not good enough to make $X or to have Y benefit.** Here's the problem with this thinking: ultimately, _you_ don't decide how much you're worth. **The person paying you does.** This is why you should ask for exactly what you want and let them say "yes" or "no." You're more enthusiastic when you get what you want, so, by robbing them of the opportunity to give you that, you also ensure you won't be as excited to work for them as you could have been.

## How to Find Leverage

You may find yourself thinking, "This is fine for some people, but **I'm desperate to get a job**! I can't go making wild demands!" Truth is, I've made some pretty big asks, and not one of them has shut down the negotiation _except_ in cases where the client budget is unrealistic. (Those are the ones you _want_ to shut down.) You can probably ask for more than you _think_ you can.

But, setting that aside, how can you feel comfortable making a big ask when you're just trying to get your shot? Leverage in negotiations comes from having two things: **something the other party wants** and **the ability to walk away**.

If you find yourself in that desperate place and unable to walk away from any opportunity, **stop**. Stop waiting for someone to invite you to be a web developer, and [**go be a web developer**](/articles/getting-started-in-freelance-web-development-without-experience/)! Every chamber of commerce, every BNI group, every meetup is full of people with problems. Those problems are making it harder for them to make money.

You just need to go out into the world and find one of those people. Let them know you can help, negotiate a fair price, and build them a piece of software. Congratulations! You're a web developer. No resume, cover letter, whiteboard interview, 8-hour take-home project… just a problem, a simple contract, and a solution. Once you've done that, go find another and another. You now have a business.

You also have people who know what you're capable of and who know you can solve their problems. They've been hiring you as a contractor, but, eventually, it might look like a good idea to have you as part of their team. **Now, you have leverage**.

## Make Your Demands

Take your leverage out into the world, and, if someone wants to work with you, **use it to make your demands**. If they meet them (or get close enough), great! Work with them. If not, keep going down your own path until someone comes along who will.

If you're looking at the steps you need to take to start asking for what you want and feeling overwhelmed, just take the first step. Step five will never get any closer until you take step one. In no time, you'll find yourself with the confidence to ask for what you want. **You'll also find yourself getting it more often than you can imagine.**

<!-- What's your first step toward finding leverage? Is it starting a freelancing business? If so, take the Freelancing Crash Course! 👇

[thrive_leads id='2262'] -->

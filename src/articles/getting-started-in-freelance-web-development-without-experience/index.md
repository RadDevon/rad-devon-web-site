---
title: 'Getting Started in Freelance Web Development without Experience'
date: '2019-05-03'
tags:
  - 'freelancing'
coverImage: 'images/pulp-fiction-the-wolf.jpg'
description: 'Many people I talk to believe freelancing is the domain of the super experts. Iʼm living proof thatʼs not the case. Hereʼs how you can get started as a freelance without being a veteran developer.'
---

When I talk to new web developers about freelancing, some of them tell me they will start freelancing once they are expert developers. Some go a step further and tell me they _know_ they need to be a veteran before they can start freelancing.

This happens because of a misunderstanding of what freelance web development is, or, at least, what it _can_ be. **Freelancing can be a great way to start your career.** I know because it's how _I_ got started as a web developer back in 2013.

When these people think of a freelance developer, they are picturing an elite 10x developer that Microsoft calls up when they're trying to hit a release date and their internal team is struggling with an issue. For $1,000 an hour, the developer whips the project into shape in time to ship and moves on to the next project. Remember The Wolf from Pulp Fiction? ([Here's a clip](https://www.youtube.com/watch?v=dTkg6wq6ma4) if you're unfamiliar; skip it if you're sensitive to language and/or gore.) A freelance developer does that except, instead of helping you clean brain matter out of car interiors, they solve problems in code.

![The Wolf character in Pulp Fiction](images/pulp-fiction-the-wolf.jpg)

## But that's not what freelancers are

Some freelance web developers are probably like The Wolf, but most of them aren't. Most of them sit at various other places on the experience spectrum and offer **the services they know how to perform to people who need them**. Not everyone needs a data scientist or an AI programmer. Some people just need a new web site or ongoing maintenance on their WordPress site.

As a new developer, you won't be able to be The Wolf, but that doesn't mean you can't do freelancing at all. In fact, **it can be easier to get started in freelancing than it is to find a full-time gig** because, depending on who you're trying to sell to, the bar could be much lower.

If you want a full-time job, you're probably going to be slotting into an existing engineering team. The job is going to be posted publicly so you're competing against everyone else who's looking for work. You'll be interviewing with a senior developer, and they'll be judging you on their standards. It's a relatively high bar with loads of competition, even if you're interviewing for a rare entry-level position.

If you start with freelancing and if you pick the right ideal client, you can avoid pretty much all of these landmines. Let's say you start going to networking events and talking to business owners. (_Psst. [You'll probably want to be defining your ideal client more narrowly](https://raddevon.com/articles/should-i-pick-a-niche-as-a-freelance-web-developer/), but I'm being general for this example._) You talk to people, earn their trust, and ask what they're struggling with. Maybe you find a company with problems you can solve for them using your web development skills.

You're no longer applying for a job. Instead, you're offering a solution to a problem you learned about organically. The business owner **may not have even known a software solution was possible**. That means you start with zero competition.

When you tell them about the solution you will build, they're not going to start asking you how many years of Java experience you have like the interviewer at the big company did; they don't even know what Java _is_. Instead, they'll judge you based on how credible you are — how much they trust you. Trust still takes time to build, but **not as much time as 5 years of Java experience**.

On the other hand, if you decide you're going to pick up freelance work from Google, Apple, and Amazon, the bar is even higher than it would be to get a permanent position.

## How do I get started?

You'll get hired because people trust you, not because of your experience. Experience can build trust, but it's not the only way to get there. Here are a few ways to start building trust with your ideal clients.

1. Go to their "watering holes." This is where they discuss their businesses. They can be in-person or online. Talk to people and keep showing up. Just being consistent helps them start trusting you.
2. Build a portfolio of projects that are relevant to your ideal client. This shows them you understand them and that you can solve their problems. Build a portfolio of other freelance projects, or become your own client building projects that look like what you will build for them.
3. Understand and talk about their problems. Help them by providing information for free. They'll come back with a check when they need more help.

<!-- If you're ready to get started, check out my short free course to get your freelancing business off the ground. 👇

[thrive_leads id='2262'] -->

## My Hack for Quickly Building Trust

The first gig I ever worked was posted on reddit. They wanted someone who had worked with a particular component of [jQuery Mobile](https://jquerymobile.com/).

I hadn't ever used jQuery Mobile, and I was scared to apply without knowing if I could do what they were asking for. I built a tiny proof-of-concept, just to prove to _myself_ I could do it. It didn't take very long, and it wasn't too difficult.

At this point, **I had a tiny project that proved I could do what they wanted**. I decided I might as well send it over to them as part of my application. In retrospect, this was the best thing I could have done, and I think it's **the single factor** that won me the job.

They didn't know me, so they had no reason to trust me. I had a small portfolio, but nothing proved I could do what they were asking. Doing a quick 30-minute project gave me the confidence I could do it and **short-circuited the process of gaining trust**, all at the same time.

## Trust Wins Work

When you're new, the barriers to get hired onto an engineering team are daunting. Freelancers have a different set of barriers, but they're easier to negotiate. As a freelancer solving problems for businesses, **it's trust, not experience**, that wins work.

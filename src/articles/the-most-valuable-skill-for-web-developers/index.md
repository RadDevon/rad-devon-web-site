---
title: 'The Most Valuable Skill for Web Developers'
date: '2018-04-14'
tags:
  - 'business'
  - 'freelancing'
coverImage: 'images/most-valuable-skill.png'
description: 'The most valuable skill for web developers isnʼt Javascript, algorithms, or data structures. Instead, itʼs something thatʼs directly tied to why we get paid to do this.'
---

As web developers, we tend to focus on our technical skills. Although these are important, they can only get us so far. What's _more_ important to the people who hire us is **whether we can take a business problem and build a solution for it**. How do we do that?

## Take a Step Back

![Developer planning](images/design-sketches-1200.jpg)

One of my biggest problems is that I want to get to code as quickly as possible. I've had to train myself (still training, in fact 😉) to step back and understand the problem I'm trying to solve. That goes beyond the task you've been assigned. **What will doing this accomplish?**

## Is This the Best Solution?

Now that you know the result you're trying to achieve, is your current task the best way to get there? If not, a massive piece of the value you bring to your employer or your client is in **suggesting better alternatives**.

How insistent you are about your solution comes down to your relationship with the client/employer, your conviction that your solution is the right one, and your personality. If you're working with someone who is hostile to your suggestions, that's a good sign you should move on.

Besides the obvious benefit of getting to a better solution, it shows you care enough to understand the problem first. That signals that you care about the business and business outcomes. The people who sign the checks care about these things too, and they'll sign more checks payable to you if they think your goals are aligned with theirs.

## Only Now Can You Build

Once you've stepped back from the prescribed solution, thought about the problem, figured out the best solution, and gotten stakeholders on board, you're now ready to bring your technical acumen to bear building out the **real** solution.

Your clients or boss will be impressed that you saved them the cost of building the wrong solution. You'll have a great story to tell when you want to get more work in the future. Your friends will be impressed and everyone will love you. All because you took the time and care to evaluate the solution before building it.

[thrive_leads id='1378']

_Since you're interested in non-technical skills for web developers, [learn how I use a focus system to help maintain work/life balance](/articles/focus-for-better-work-life-balance-as-a-web-developer/). I'll teach you to create your own focus system too!_

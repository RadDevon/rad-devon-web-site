---
title: 'Remote Work Tips for Developers'
date: '2020-04-03'
tags:
  - 'remote'
coverImage: 'images/remote-work-tips-for-developers-thumb.jpg'
description: 'Weʼre all working remotely these days, but even if youʼve never known another way, I have some tips that can help make you more productive and healthier as a remote developer.'
---

https://youtu.be/1loasaeDR4g

## Video Notes

We're all working remotely these days, but even if you've never known another way, I have some tips that can help make you more productive and healthier as a remote developer.

### Routine

- Get up and get dressed.
- I use the [Sleep Cycle alarm clock app](https://www.sleepcycle.com/) to wake up in the morning. Don't hit snooze!

### Communication with Your Family

- Be clear about your work time and what you need from others in your household during that time.
- If you want to know more about the pomodoro system or about staying focused in general, read my article about [building your own focus system](https://raddevon.com/articles/focus-for-better-work-life-balance-as-a-web-developer/).
- I use [Google Calendar](https://calendar.google.com/) for my shared calendar, but there are lots of similar options. You could try [Zoho Calendar](https://www.zoho.com/calendar/).

### Communication with Your Team

- [Slack](https://slack.com/) for real-time communication text.
- [Whereby](https://whereby.com/) for video conferencing or [Jitsi Meet](https://meet.jit.si/) for larger meetings.
- The Bluetooth headset I show in the video is a slightly older model. The [WH1000XM3](https://amzn.to/33Zk644) is a newer model that's similar. I've also used this cheaper [on-ear headset by Jabra](https://amzn.to/2UQy8Rn) and really liked it.
- [Trello](https://trello.com/), [Asana](https://asana.com/), or [Todoist](https://todoist.com/) for project management (even if you work alone).
- Keep email sparse.
- Figure out what kind of communication belongs where (Slack vs. video conference vs. email).

### Mental Health

- I use the [Waking Up app](https://wakingup.com/) for guided meditation. I've seen lots of recommendations for [Insight Timer](https://insighttimer.com/) as a free alternative.
- See my video about [The Obstacle Is the Way](https://youtu.be/qKmcMPbZ65k) to get started with Stoic philosophy.
- Take time to play. Watch my video about [video games you can play that strengthen some of the muscles you use in web development](https://youtu.be/ihdPbuwbQbI) for some suggestions. (Make sure you play some just to unwind too. Not all play should be tethered to your career aspirations.)
- Walk (which is good for both mental and physical health).

### Physical Health

- Snack responsibly. This is hard when the kitchen is just down the hall.
- Drink water.
- Move! (Pro tip: if you have a VR headset, try Beat Saber!)
- Pay attention to ergonomics. I love this [short introductory video about workstation ergonomics](https://youtu.be/bLBKUbnLYTs).

Thanks for watching! Post a comment [on YouTube](https://youtu.be/1loasaeDR4g) or mention me [on Twitter](https://twitter.com/raddevon) if you have questions about working from home or more tips I missed.

If you're liking this whole remote work thing, you might be wondering how to keep it going even after everyone else is back in the office. Freelancing is one way to work wherever you want. My free crash course will take you through a one-week lesson to teach you how to start your freelance business.

[thrive_leads id='2262']

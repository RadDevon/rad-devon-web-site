---
title: 'How I Get Web Development Job Offers Without Interviewing'
date: '2019-05-17'
tags:
  - 'career'
  - 'freelancing'
coverImage: 'images/pet-monkey.png'
description: 'Interviewing sucks and no one wants to do it. I say, why bother? Hereʼs my method for getting job offers without ever interviewing.'
---

I've been a web developer for about six years at the time of writing this article. I've done freelance work throughout my career and I love the freedom that affords me, but I've also held full-time positions on two occasions.

I wasn't looking for these positions, and **I was perfectly happy continuing to do the freelance work I was already doing**. This in part was what allowed me to negotiate some of the great terms I got in these offers. They were the terms new developers dream about: six-figure salaries, fully remote, flexible schedules, and loads of autonomy.

In this article, **I will share how _you_ can get job offers like these** without any whiteboard interviews or coding tests — in fact, without even applying for them!

![Man with a pet monkey](images/pet-monkey.png)

## Start with Freelancing

If you wanted to start a band, would you get the members together, record a demo, send it out to every record label, and then wait, hoping someone signs you? **Absolutely not!** You'd get out there and start playing shows.

Start your freelance career like a punk rocker, doing the thing you want to do **instead of waiting for someone to give you permission**. All you need is an instrument (your computer) and an audience (your clients), even if it's a small one.

This is the one decision I made that underlies everything else. I won't go much into it in this article since one of my early articles was devoted entirely to the idea of [starting your career with freelancing](/articles/inverted-career-path/). (Go read that one for more detail.) I will say that **this was what set me up to take the upper hand in negotiations** to get the terms I wanted. This was the linchpin of what worked for me.

<!-- If you're ready to try freelancing, I have a course that will help you get started. 👇

[thrive_leads id='2262'] -->

## Work With Teams

I have mixed feelings about this advice, so I'll try to share both sides of my internal monologue here.

If a company has an existing engineering or development team, that means **they already put a high value on software as a driver of their business**. If they grow, there's a decent chance they would want to increase their capacity to produce that software. If you're already working with them at that point, you'll be at the top of their list.

The problem is that, if they have an engineering team, **you're going to be evaluated by or against that team**. That's a slightly tougher road to take starting out than coming in and doing one-off jobs for companies who need some help but can't justify having full-time developers on-staff.

In those cases, you likely will be evaluated by someone without development expertise. Their requirements will be less stringent as a result. Do they believe you can solve their problem? **If so, you're hired.**

The difficult thing about the one-offs is that, **if they haven't needed a single developer on-staff up until now, it's less likely their business will suddenly evolve to need or even be able to support that**. It may be easy to get in the door for a contract, but they're less likely to make you an offer to take a permanent position.

As a result, if your goal is to transition into full-time work, **you're best off targeting teams**; design and dev studios or small companies with web software as a component of what they do are good targets.

If you're brand new to professional web development and you'd like to evolve a freelance practice into a full-time role, **start building your business with the one-off jobs that are easier to get**. Once you've done a dozen or so of those, start looking for contracts to integrate into teams that might eventually hire you.

## Do Great Work

If you want someone to eventually hire you, you need to do great work. When they're looking to expand, you want to make the idea of a big recruitment effort look like **a waste of money**.

But what is "great work?" What does that mean for a freelancer? Does it mean clever algorithms all over the place? Does it mean being super fast? Maybe those are factors for some companies, but the most important factor that makes a freelancer's work great is **communication**.

Make sure your clients know what progress you're making. Make sure they know when something is blocking you. Make sure they know if you're going to miss a deadline. They're giving you their hard-earned money, so **make sure they know that money is well-spent**. Make sure they know what they're getting.

Communication comes in more forms than direct correspondence with your client. Since you are working on code with a team, making sure you write **code that communicates its intent** is a critical part of being a responsible team member.

Variables with names like `a` and `qq` might make you feel smart for being able to keep all the context in your head, but they're going to have your team members **cursing your name** the second they have to touch your code. When it comes time to hire, if anyone brings up your name, the rest of the team is going to have some negative things to say about the idea.

Variable names like `elapsedTimeInSeconds` and `backupEmail` might not fit the Hollywood ideal of a developer hammering out cryptic code that no one else can decipher, but they are much easier to reason about. Your teammates will appreciate good code that makes maintaining the code easier.

## Be Ready to Seize Opportunity

You've stacked the cards in your favor, but that's the most you can do. The next steps are out of your control. If the company grows, you're set to be their first choice of engineering hires.

Just keep doing what you're doing, and be prepared for the opportunity if it comes. Even if that never happens, you still have your freelance work.

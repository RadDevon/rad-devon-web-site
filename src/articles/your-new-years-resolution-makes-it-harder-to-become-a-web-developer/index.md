---
title: "New Year's Resolutions Won't Help You Become a Developer"
date: '2019-12-27'
tags:
  - 'career'
  - 'motivation'
  - 'resolutions'
coverImage: 'images/thumbnail-opt.jpg'
description: 'It feels like youʼre finally making your goal to become a web developer official, but youʼre really shooting yourself in the foot. This video explains why and what you should do instead.'
---

https://youtu.be/ilSHY1DQTGs

It feels like you're finally making your goal to become a web developer official, but you're really shooting yourself in the foot. This video explains why and what you should do instead.

One thing you might try is my free (and quick!) Big Goal course. It will help you discover what you're actually trying to accomplish and help you keep those willpower reserves full even when your transition gets difficult (and it will). Check it out below. 👇

[thrive_leads id='1360']

## Transcript

I'm sure a lot of you aspiring developers are thinking that with the new year coming, this would be a perfect opportunity to formalize your desire to become a web developer. Today, I'm going to tell you why that will actually **make it harder** for you to succeed and to reach your goal of becoming a web developer.

So, what's wrong with New Year's resolutions? The first thing is that it creates **a set of artificial boundaries** around when you can make a big change in your life. If you decide you want to become a web developer in October, instead of getting started right away, you might just wait until January. Since the new year is just around the corner, you can start on that journey then when everyone else is going to be starting on their own New Year's resolutions.

The other side of that coin is even more insidious. If you fail in March and you realize you're not going to get there, then you may just say, well, okay, **I'll wait until next year** and then start again, when really you should just get started right away or really never stop. These artificial boundaries are a big problem and you don't really want them getting in the way of your goals.

The next reason you should not use a New Year's resolution to try to propel you towards your goal of becoming a web developer is a little bit morbid, but it draws on what I've learned about stoic philosophy. We are dying all the time, not just on New Year's Day. So, **you shouldn't only be _living_ on New Year's Day** either, and living – at least, a _good_ life, as I would define it – is about **thinking about where you are and where you want to be** in relation to that, and starting to make changes so that you can get there. And that shouldn't only happen on New Year's Day.

The last reason I want to talk to you about is that **we have normalized failing at New Year's resolutions**. It's far more common than succeeding at them. So, why would you want to take this goal that's really important – this could genuinely change just about every aspect of your life – why would you want to take something like that and give it a label that has **so much baggage**? Resolutions have a history, and it's not a good one.

So, instead of giving your goal that label, break down the steps you need to take to get there and **start taking them**. One of the first questions I ask people when they talk to me is, "what is your goal?" Almost everyone tells me their goal is to be a web developer. **Almost none of those answers are actually accurate.** People want to see some sort of change in their life and they think becoming a web developer might be a way to get them there, but **what they actually want is the change**.

When I set out to become a web developer, my goal was to live in a different city, and that in itself had other goals attached to it, like there were things I would gain by living in another city. But that was where I set my sights and that gave me the motivation to do the hard things I needed to do to make this career change.

If you're not sure what your actual goal is, you can use a technique called **the five whys**. What you do is you take the answer to the question, "what is your goal?" For a lot of you, that's going to be to be a web developer. Then ask yourself, "why?" Maybe it's because I want to make more money. So then ask yourself again, "why?" Why do I want to make more money? And the answer to that might be, I'm tired of living paycheck to paycheck. Or maybe it's just I'm not comfortable in my apartment. Or maybe it is my car is breaking down, and I don't want to have to go buy another beater off some used lot somewhere.

If you use this technique, the five whys, usually by the time you get to the answer of the fifth level of why, **you'll have a really good understanding of what your goal actually is**. That goal will actually propel you forward. (Check out [the Big Goals course](/stick-with-web-development/) for more help figuring out your actual goal. It's free!)

Becoming a web developer is not a good goal to set your sights on because **it's going to get really hard**, and it's not actually what you want to accomplish. If you can set it at the place where you gain something that's extremely gratifying and life changing out of it, that can be a real motivator to push you forward.

One of the ways you can stay strong when things get hard is to think about the life you're trying to leave behind. I would even go a step beyond that and say, hold a grudge against your old life. We all hit these inflection points in our lives where something happens and we think to ourselves, "this is not working. I can't do this anymore." It's usually painful. It's sometimes sad. It can be a strong motivator. It can really push us forward into something better. The problem is, it's easy to forget about those things and **time heals all wounds**.

In two weeks, that fresh pain that you felt when your boss yelled at you or whatever it was, when you had to call the mortgage company and ask them to defer your payment because you couldn't _make_ the payment – whatever that thing is, that pain in the moment is going to wear off. That pain, if you can harness it, is something that's really valuable to you to keep you moving forward and to show you that the relatively insignificant pain it takes to become a web developer, to learn web development, and then to go get a job and so on and so forth – all the things you need to do to make that transition are nothing compared to this pain over here that I'm trying to leave behind. **Write down what happened to you that pushed you to that point**, and, not only just the mechanical thing that happened, but write down how it made you feel. That can serve as a really powerful motivator to keep you going.

The last technique I'm going to share that beats the crap out of. New Year's resolutions is to imagine your new life – the new life you're working for. Don't just to imagine it, but **make it as real as you possibly can**. When I was trying to move here to Seattle, I was living in Knoxville, Tennessee, not making very much money. There was absolutely no way for me to get here.

I had an idea of a way I thought I could get here, and that was by becoming a web developer. So, what I did to make this future seem more real is I looked at apartments. I went to Google maps and dropped my little Street View guy on a street here in Seattle and walked around. I subscribed to email newsletters for locals and kept track of what was happening here in Seattle. All those things made it really easy – or at least easier – for me to imagine being here. I sort of describe it as **making your mouth water for the life you're trying to get**. It made it so visceral and so real for me that it gave me a little extra motivation when I felt like I just didn't have it in me to continue anymore.

I want to thank you for reading the articles I've put out this year and watching my videos. I hope your New Year's celebration is amazing and awesome, but **don't wait for it to start making the change you want to make**. Dig in and get started right now. Stay here at Rad Devon for help along the way, and I will see you in 2020.

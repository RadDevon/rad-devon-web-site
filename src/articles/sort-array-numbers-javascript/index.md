---
title: 'How do I sort an array of numbers in Javascript?'
date: '2018-02-18'
tags:
  - 'arrays'
  - 'javascript'
  - 'sorting'
  - 'tips'
description: 'In this article, Iʼll share how you can sort an array of numbers in Javascript. Itʼs not as simple as you might think.'
coverImage: 'images/colored-thread.jpg'
---

In this article, I'll share how you can sort an array of numbers in Javascript. It's not as simple as you might think… but it's not too difficult either. Just like the language itself! 😜 (Grab [my free Javascript reference](#javascript-by-example) while you're here to make things a little bit easier.)

Let's say you have a Javascript array of numbers like this:

```javascript
let numbers = [85, 83, 29, 70, 4, 0, 17, 8, 55];
```

You want to sort this array. You might think you can do this using the array's `sort` method like this:

```javascript
numbers.sort();
```

If you do that, though, the results aren't what you might expect.

```javascript
[0, 17, 29, 4, 55, 70, 8, 83, 85];
```

## Default Behavior of sort()

Everything looks OK until you get to `4` and realize the numbers are sorted _alphabetically_ instead of _numerically_. The `sort` methods default behavior is an alphabetical sort, but, fortunately, it can be customized to sort by any criteria you want.

## Customizing the Array Sort

To customize the sort order, you **pass it a function to compare two items in the array**. `sort` will pass to your function two items in the array (conventionally the parameters `a` and `b` in your function). `sort` looks for one of three different outcomes from your function:

- If your function returns a value less than 0, **`a` comes first**.
- If your function returns a value greater than 0, **`b` comes first**.
- If your function returns 0, **the order of these two items remains unchanged**.

In order to sort numbers, you'll need to write a function that returns a negative number if `a` is less than `b`, returns a positive number if `b` is less than `a`, and returns 0 if the numbers are the same.

This can easily be accomplished by **subtracting the numbers**. Subtracting the two numbers will yield 0 if the numbers are the same. Which number needs to be subtracted from which to yield a negative number if `a` is smaller? I believe that's going to be `a - b`, but I like to confirm that by testing it with some actual numbers.

Need help remembering how to write a function? (Don't be embarrassed. It happens to us all.) This might help. 👇

[thrive_leads id='3815']

## Testing Our Solution

Let's say `a` is `1` and `b` is `2`. We'd want our sort function to return a negative number in this case. Insert the sample values into my proposed formula of `a - b` and you'll have `1 - 2` which yields `-1`. Looks good so far! Now, let's see if it yields a positive number when `a` is larger.

This time, we'll make `a` equal `2` and `b` equal `1`. Plug that into the formula and we have `2 - 1` which yields `1` — a positive number. That's exactly what we want.

This means all our function needs to do is return the result of `a - b`. We can do our sort **by passing an anonymous function** into the sort function like this:

```javascript
numbers.sort((a, b) => a - b);
```

but if we're going to be sorting multiple arrays in this way, we probably want to **declare a function and pass it into the sort** instead like this:

```markup
const numberSorter = (a, b) => a - b;
numbers.sort(numberSorter);
```

Now we can pass `numberSorter` to any array's `sort` method to sort it numerically. Here's the array after the sort with our custom sorting function:

```javascript
[0, 4, 8, 17, 29, 55, 70, 83, 85];
```

## Sorting Other Data

Even though the default sort doesn't work in every case, by writing custom sorter functions, it becomes very flexible. You can write **virtually any type of sort you want** by setting up the sorter function to produce [the three possible outcomes](#outcomes) we discussed above. Use this example to get your data organized in whatever order you need!

Are you a little hazy on functions? Grab [a free Javascript reference](#javascript-by-example) for quick glanceable code examples of functions and many other foundational Javascript concepts.

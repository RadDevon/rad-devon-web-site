---
title: 'How to Bounce Back After Being Turned Down for a Web Developer Job'
date: '2018-12-28'
tags:
  - 'career'
  - 'motivation'
coverImage: 'images/bounce-back.gif'
description: 'Getting turned down for a web developer job can feel like youʼre back at square one. Realizing that you will get rejected and that it doesnʼt mean youʼll ultimately fail is an important step. Hereʼs how to get over the rejection and keep pushing.'
---

![Homer Simpson bounces back](images/bounce-back.gif)

Getting turned down for a web developer job can feel like you're back at square one. In the worst cases, it can make you decide you're not cut out for this work and seek refuge in the 💩 job you're more familiar with.

The problem is the same whether you're trying to get a permanent position or trying to find freelance gigs. I've developed a few strategies to **minimize the impact** of rejection on your career.

<!-- While you're here, one of the best ways to push through rejection is to remember your Big Goal. I can help you find yours. 👇

[thrive_leads id='1396'] -->

## Stop Believing in Fate

You're not destined to become a web developer. If you get turned down for the one job you applied for, it doesn't mean you're destined _not_ to be a web developer. Whether or not you will have success comes down to three factors:

1. Can you [**provide a valuable service**](/articles/the-most-valuable-skill-for-web-developers/) to someone? (Hint: If you can build for the web, the answer is yes.)
2. Can you [**stick with it long enough**](/stick-with-web-development/) to get someone to pay you to do it (and endure the rejections in the meantime)?
3. Can you get yourself **enough exposures** to land the luck you need to get there?

That last one deserves some more explanation.

## Understand Luck (and Make It Work for You)

Think about playing the lottery. If you buy a single ticket, you have to be extremely lucky in order to win. If you buy tickets with every single possible number, **you'd no longer have to be lucky at all to win**. Buying every number is not feasible, but buying a single ticket and thinking you're going to win is just silly.

What if you bought _two_ tickets? You've now **doubled your chances** over buying a single ticket. Buy four tickets and you've quadrupled them. You can't control the role of luck in your lottery chances, but you can control **how many opportunities you have to get lucky**.

The same goes for your web development career. Think of each job you apply for, each relationship you create, and each project you build as **an opportunity**. Each one of these opportunities has very little chance of becoming your new career. That's why you have to **collect as many of the opportunities as you can** to maximize your chances that one of them will pay off.

You'll never apply for every job or meet every business owner, but you should strive to find as many opportunities as you can.

## Act Like You've Already Failed, <em>Inter</em>act Like You've Already Succeeded

**You will not know whether you've succeeded until the contract is signed.** Everything may look perfect. You can't imagine a way this could go wrong. Just one more executive needs to sign off before they can make you an offer. They're waiting on a check to clear before they accept your proposal.

Your brain tells you these jobs will definitely close in your favor. In reality, **80% of them will evaporate**. When you're talking to your interviewers or prospects, you should be confident and interact as though they're definitely going to work with you. Otherwise, you need to behave as though the deal definitely won't happen… **because it probably won't!**

If you're looking for full-time work, that means keep applying to jobs, keep going to meetups, keep refining your resume… everything you would do if you didn't have a single interview.

If you're a freelancer, keep contacting leads and making new relationships. Don't rest because the contract is almost signed, or you'll be kicking yourself when it falls through.

**Pursuing new opportunities is hard.** That's why it's so easy to convince ourselves we don't need to do it anymore. We don't even need a contract signed to believe we've already succeeded. Fight against that impulse, and you won't be so crushed when an opportunity slips through. Pretend you've already lost the opportunity, even if you know you've nearly got it. That will align your behavior with the most likely outcome.

You _can_ get closer and closer to sealing a deal or having a particular job. This makes your brain relax but, in reality, getting a job is *not* a continuum. It's binary. Either the contract is signed and the deal is done, or **you have no deal**, no matter how close it seems. Until you have a deal, you should **keep pursuing _other_ deals**.

## Define “Opportunities” More Broadly

This advice is specifically for the full-time job junkies among you. If you believe the only way to your web development career is by applying for jobs and waiting for someone to pick you, **think again**. You can pick yourself. Hire yourself as a web developer [by doing freelance work](/articles/inverted-career-path/).

When you get turned down for that job after four rounds of interviewing, that's a great opportunity to **go pick yourself**. Join your local Chamber of Commerce and start going to their events. Talk to people and find out what they're struggling with. Look for opportunities to help them, and offer your services.

If you can show these people you're valuable, you have a good chance of getting hired… and you can skip all the questions about how many years experience you have, whether you know Docker, Vagrant, React, Angular, Vue, Backbone, Ember, continuous integration, data science, Big O, and whatever else (because, as you probably know, all posted positions require at least 5 years experience in all of these 😉).

If you're helping business people make more money, **they don't care what stack you use**. They don't care if you have umpteen years experience. Heck, most of them don't even care about your test coverage.

None of this means you can't go have a full-time job later, but why not pick yourself and start being a professional web developer rather than **waiting for someone to give you permission**.

> “There is no better moment to pick yourself than right now. Because, after all, if you're not willing to pick yourself, who will?”
>
> — Seth Godin

## Focus Energy on the Factors You Can Control

Imagine you're coaching a team, and your advice to them is to "win the game!" Sounds reasonable, right? That's the outcome you want, so why not define that as the goal?

You tell the team this and one of your players says, "Coach, how do we do that?" Oh, crap. You've given your team a goal that's too abstract and that they don't have full control over. The other team stands in the way, and we can't predict what they will do.

What you need to do instead is figure out **which tangible actions will increase your chances of winning**. Coach your team to do those things. When we set a goal like "win the game" or "get the job," we end up giving up because we don't know how to do that and can't do everything it takes to make it happen. If you're looking for a full-time job, here are some important inputs you _can_ control:

- How many applications you submit
- How many people you meet (other developers, CTOs, business owners)
- The quality and relevance of your resume
- Your skills
- The projects in your portfolio

_Note: Be careful optimizing those last three. Once you have a reasonable baseline of skills, a resume that shares them effectively, and a portfolio with 2-3 unique projects, they're not anywhere near as important as the first two._

Here are the important inputs for freelancers looking for gigs:

- How many people you meet
- How well you have honed your pitch
- How well you know your value proposition

_Note: Again, the first input is far more important than the last two._

Work hard on the pieces you have control over, but don't sweat it after that. At a certain point, you have to put your work out there and let the machinations of chance take over.

## Takeaways

1. You're not fated to have any outcome. **You have to create the outcome you want.**
2. Don't worry about whether or not you're lucky. Learn how to work with it by **giving yourself loads of chances to get lucky.**
3. Keep going even when you have a good prospect. **Most good prospects don't work out**, whether it's for a job or a freelance contract.
4. **Pick yourself.**
5. There are many factors you can't control. The same is true for everyone else too. **Focus on the factors you _can_ control.**

Use these tips to **get back on your feet** and keep pushing toward your web development career!

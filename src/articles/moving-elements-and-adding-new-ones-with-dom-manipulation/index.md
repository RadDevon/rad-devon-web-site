---
title: 'Moving Elements and Adding New Ones with DOM Manipulation'
date: '2020-11-13'
tags:
  - 'dom'
  - 'javascript'
coverImage: 'images/dom-manipulation-2-thumb.jpg'
---

If you haven't watched the first video in my series on basic DOM manipulation, you'll want to catch up with that first.

In this video, we'll dig a bit deeper into selecting elements. I'll also show you how you can move your elements around in the DOM and how to create brand new elements in Javascript and render them onto your page.

https://youtu.be/4vy8TAVZ8B0

<!-- If you want to learn Javascript faster, download my Javascript By Example cheatsheet. It's like a regular Javascript cheatsheet but without all that "foo" and "bar" garbage.

[thrive_leads id='3815'] -->

---
title: 'Doing Web Development for Experience or Exposure'
date: '2019-03-22'
tags:
  - 'free'
  - 'freelancing'
  - 'rates'
coverImage: 'images/suspicious-woman.jpg'
description: 'Someone will eventually suggest you do work for them for exposure or experience. Hereʼs the way to think about these offers.'
---

As you start your freelance web development practice, someone is bound to suggest you do work for them **for experience and/or exposure**. If the person is good at selling, this might sound like an attractive prospect.

Experience and exposure are two things you desperately need as a new developer. Let's talk about **the reasons you should _not_ get them by doing free work for someone else**, and how you should get them instead.

## Reason 1: Their Offer Is of Dubious Value

![This woman is suspicious of the value you're offering](images/suspicious-woman.jpg)

Successful transactions are exchanges of value — ideally equal value. Each party trades something that is valuable to the other. In most freelance web development work, this is **money for services**. (Actually, it's money for the _result_ of the services, but I'll dive deeper into that idea in a future article. 😉)

In a "work for experience" or "work for exposure" arrangement, the value of the services you're exchanging is much clearer than the value of the experience or exposure you're receiving. Maybe you build something that flops in the end, but the beneficiary of your work **has at least decided they want that thing built**. You're building it. That satisfies a perceived need. See? Value.

Maybe you'd argue that you also want the experience or exposure. That's probably true, but how can you measure **the value of the experience or exposure you'll get from this particular project**?

If you want experience, you should focus on the right kind of experience. The chances that the experience you'll get from a random project conceived by someone else is exactly what you need **are slim**.

If you want exposure, **it's not going to come from doing a random project for someone who doesn't want to pay you for work** — at least, not in any meaningful way. If you did a project for Microsoft or Apple or Coca-Cola, that would give you a big hit of exposure, but you won't do it for free. Those companies pay for their work because they take it seriously.

If they are really confident of the value of the experience or exposure you'll receive, get them to sign a contract stating they will pay the full amount for the project if your experience or exposure hasn't produced the desired results within, say, 6 months. Would they take you up on it? Of course not! **They're not confident in the value they're bringing to the table**, and you shouldn't put any stock in it either. (Note: Don't actually do this because no one will take you up on it. I'm merely making a point here.)

## Reason 2: They Get All Their Value Before You Get Any of Yours

![This cat is your cousin and a Nigerian Price](images/prince-cat.jpg)We'd all love the Nigerian prince scam if you could meet the person face-to-face and hand them your $5k as they simultaneously passed you the big bag with the dollar sign on it (💰) containing your millions. **The reason the scam works is because you have to deliver _your_ value before they deliver _theirs_.** They take your money. They disappear from the face of the earth. Everyone lives happily ever after… except for you.

Working for exposure or experience is no different. Before you can get the experience, you have to do the work. Before you can claim the work to get the exposure, the work has to be delivered and live for the "client."

Once they have the thing they want — the web site, app, or whatever it happens to be — **why would they waste time and effort doing something for you?** Ideally, they would be out there in the world singing your praises to anyone who will listen, recommending you anytime they hear someone needs software developed. Problem is, they have literally no incentive except for preserving their reputation. In my experience, people who want free work are rarely concerned about their reputations.

Even the best intentioned people who really believe they can provide you value in exchange for your free work can't help but be affected by incentives. Once they have your work and the incentive to provide you something in return has faded, **will you take precedence over the hundreds of other tasks competing for their time?**

## Reason 3: You Can't Go Pro By Working for Free

If your goal is to make money as a web developer, **you're never going to get there by doing free work**. Everyone you talk to in this camp will have a unique and excellent reason why doing free work is really going to pay off for you _this_ time. Every situation will be sold to you as a uniquely fantastic opportunity. The only common thread is that you'll get little to nothing out of the deal almost every time.

Check out this before and after comparison of typical income growth before and after charging for your work 😜:

![Chart showing no income if you don't charge for your work](images/free-chart.png)![Chart showing income growing if you do charge for your work](images/charge-chart.png)

If you're ready to make money, the only way to get there is **by charging people for your work**. When you engage with a person who doesn’t intend to pay, it's a double whammy for you. Not only are you not getting paid for \_this\* job, but you're losing time you could be looking for paying work. **Stop wasting time with the people who _won't_ pay and keep looking until you find those who _will_.**

<!-- Not sure how to break through and start getting paid work? Maybe I can help. Schedule your free session, and we'll figure out your next step together!

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

## Exceptions: When to Work for Free

I don't believe in absolutes… except for that one. That means that, yes, sometimes doing free work is perfectly reasonable.

### Charity Work

If you want to do some work for a charity or a cause you believe in, go for it! Just make sure you can pay the bills first.

### Work for Friends

Use this one sparingly. You don't want to become the sucker all your friends come to for free web work. Again, make sure you can pay the bills or politely decline.

### If You Know You'll Probably Never See Anything Out of It and You Don't Even Care

If you're so excited about this project that, even though you understand you will most likely never see a dime from any experience or exposure it provides, you _still_ want to do it… that's on you. Maybe it will work out, but it probably won't.

## What to Do Instead

### For Experience

Charity work is fine for experience, but **doing your own projects is even better**. Become your own client. Build a project that is similar to the kind of work you want to do. Build something that is useful to you or solves a problem you have. Build the startup idea you've been dreaming about for years. If you don't know what to build, take inspiration from [my project ideas](/articles/10-great-web-development-learning-project-ideas/).

In any of these cases, you're still doing the work for free. The difference is that **you have created an asset**. You have a site tailored to your ideal client to put in your portfolio. You have software that makes your life easier. You have the beginnings of a software startup. Any one of these is better than nothing.

### For Exposure

In my experience, exposure rarely comes from a single project anyway. If you want exposure, become known as an expert in solving the problem you want to solve for your clients. You can do this by doing lots of work that looks like the work you want to do or by making yourself visible while talking about the problem.

The first approach creates a bit of a chicken-and-egg problem. "I can't get the experience I need to be known as an expert because I don't have the experience I need to be known as an expert!" That's why I like the content approach.

This starts with knowing your ideal client and understanding their problems. Easy, right? 🙄 Once you have those down, start creating content around solutions to those problems. Don't just set up a blog and toss content out into the void. Find other people who already have the audience you want to talk to and pitch them on blog posts, podcast appearances, videos, or whatever kinds of content you can deliver.

This helps them because they're trying to continually feed content to a hungry audience. It helps you because you get an implicit endorsement from them to their audience. It's a great win-win! This exposure beats the pants off whatever you would have gotten from a handful of free projects, and it's enduring in a way those could never be.

## Do Your Free Work for You!

Doing free work is essential, but you should be doing it **for you**. If a prospect says you should do _their_ work for free, it doesn't mean there's no one willing to pay you. It just means **you need to keep looking**. Most likely, you're eventually going to find someone who will.

The other side of the coin is that you may not have enough experience yet to get work. Getting a web development contract is about building trust and proving you can do the work. If you haven't done that yet, **assign yourself some work and go do it**. The next time you talk to a prospect, show them what you've done. You'll have experience tailored to your clients' needs, and they'll appreciate it.

Build your exposure **by showing people you're an expert**. Show them you care about their problems and that you have a solution. Go to where your customers are and wave your expert flag. They'll come to you when they want the solution done for them, or when their problems are unique in some way.

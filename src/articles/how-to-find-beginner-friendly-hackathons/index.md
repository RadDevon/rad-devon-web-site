---
title: 'How to Find Beginner-Friendly Hackathons'
date: '2019-06-28'
tags:
  - 'naming'
coverImage: 'images/fcc-seattle-hackathon.jpg'
description: 'One common misconception people have about freelance web development is that you need to be an elite coder to try it. A lot of new coders hold this same misconception about hackathons. They can be for newbies too. Hereʼs how to find one for you.'
---

![Two people working on laptops at a hackathon](images/fcc-seattle-hackathon.jpg)

One common misconception people have about freelance web development is that you need to be an elite coder to try it. A lot of new coders hold this same misconception about **hackathons**.

My first hackathon wasn't really a hackathon. It was just another event put on by a meetup I was regularly attending, and part of the event was devoted to working on a simple project. It's still, to this day, **the most fun I've had** as an attendee at a meetup event. I can't even remember what we built, but the feeling of meeting new people, forming an ad-hoc team, and combining each of our strengths into a tightly scoped project was exhilarating.

Now, [as the organizer of a meetup myself](https://www.meetup.com/free-code-camp-sea/) (in my hometown of Seattle; please join us if you live here!), I do my best to facilitate these kinds of connections and give my members these experiences. Our most popular event by a good margin has been our mini hackathon, so I'm not the only one energized by this idea.

The trouble is that hackathons friendly to beginners aren't always easy to find. Here are some tips I've used to find them in the past.

## Join Meetup Groups

Join groups on [Meetup](https://www.meetup.com/) that interest you, and keep watch on their events. Some of them may be listed as hackathons but you may find some **stealth hackathons** among the events as well. Read all the event descriptions and you may find something that lets you work on projects with others. That's effectively a hackathon.

## Join Local Slack Groups

Find local developer Slacks and join them. Many of them will have **an "events" channel** or something similar where people can publicize upcoming community events. Watch these channels for upcoming hackathons.

[thrive_leads id='1360']

## Search Meetup and Eventbrite

Even though you've already joined groups on Meetup, you can also search for "hackathon" and find events you might want to participate in that aren't part of the groups you're already in.

[Eventbrite](https://www.eventbrite.com/) is another platform where you'll find a lot of local hackathons. Read through the descriptions to find those that are beginner-friendly. Here's an excerpt from an upcoming local VR hackathon under the heading "Who can participate?"

_You don't have to be an expert. Attending this event is a great introduction to working with VR and AR, and our organizers and workshop presenters are all dedicated to helping you no matter what your skill level._

If you can't find something like this in the hackathon's description, **the more general the hackathon's focus, the more likely it will be good for beginners**. For example, a "web developer" or "Javascript" hackathon is more likely to be a good fit for beginners than a [three.js](https://github.com/mrdoob/three.js/) hackathon.

## Hack and Meet People with Confidence

Once you find the right hackathon, go into it confidently. Many of the people in attendance will be at or below your level, but, more importantly, **many will be ahead of you and willing to share what they know**. It's a great opportunity to meet new people and level up your skills!

---
title: 'Freelancers: You Are Not Toilet Paper'
date: '2019-03-15'
tags:
  - 'freelancing'
  - 'marketing'
  - 'sales'
  - 'skills'
  - 'value'
coverImage: 'images/toilet-paper.jpg'
description: 'Youʼre not a commodity, so stop selling yourself like one. You are not toilet paper.'
---

![Toilet paper](images/toilet-paper.jpg)

You're fresh out of bootcamp, university, or your own self-lead learning program, and you're understandably excited about all the technologies you've learned. You proudly tell people you meet that you know HTML, CSS, Javascript, and a few others. You do this as you start your freelance career, and you've unwittingly turned yourself into toilet paper. That's right. Toilet paper. Here's how.

If you didn't already have a favorite brand of toilet paper, **how would you choose what to buy**? "I definitely want two-ply. Oh, wait. Three-ply? I didn't even know they made three-ply. This one's cheaper _and_ it says it doesn't leave any debris. But wait, what's the price per sheet?"

When you start talking to potential clients about the skills you have, you've given them only enough information to compare you to the alternatives the same way they would compare toilet paper based on what's written on the package. They're going to look for **the cheapest freelancer who ticks all the boxes they need**… _unless_ they find someone with a better approach to selling. Let's explore what that might look like through another analogy.

[thrive_leads id='2262']

![iPhone X](images/iphone-x-1000-300x300.jpg)Regardless of how you feel about Apple, you should pay attention to them as you're thinking about selling your freelance web development services. **No one buys an iPhone because of the processor speed or how much RAM they have**. I have an iPhone, and I don't know either of those specs, even though I'm the kind of person who used to geek out over that kind of stuff.

Apple taught me that **I don't really care about the specs**. In fact, I'm much happier not thinking about them. They trained me to stop digging for those things and trust that they can solve the problems I'm looking to solve with a smart phone. They did this by consistently delivering products that just work. They get my business, not because they have bigger numbers on the box or because they're cheaper, but **because I trust them and feel like they understand me**.

In doing this for me and millions of others, Apple is able to make way more money with less. Theirs aren't the most powerful phones. They don't have the most impressive specs in almost any category. They are rarely the first to do anything. They're definitely not the cheapest.

Training their customers in this way allows Apple to hang on to the lion's share of smartphone profits even though they sell fewer smartphones than the competition. According to a [Counterpoint Technology Market Research report](https://www.counterpointresearch.com/iphone-x-alone-generated-35-total-handset-industry-profits-q4-2017/) on the quarter of the iPhone X's release, the iPhone X captured **35% of all smartphone profits**. Across all models, Apple captured **86% of all smartphone profits** in that quarter. Of the top 10 smartphones in that quarter by profit share, 8 of them were Apple phones.

As the countless other smartphone manufacturers compete by selling faster processors and more RAM, Apple makes all the money by selling something entirely different: **solutions**.

Now, we've bisected the smartphone market into Apple who takes 86% of the profits and everyone else competing for the scraps. Freelance web development and really almost any other market can have this same split. The question is, **which group do you want to belong to**?

Do you want to lose jobs because someone else has another year of experience with React or because you were undercut by $5/hour? Or would you prefer to win clients with bids that are extremely profitable for you because **they trust you** and **you focused on understanding their problems and proving you could solve them**?

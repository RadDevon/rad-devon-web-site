---
title: 'Your biggest fears about transitioning to a web dev career, addressed! 😱'
date: '2020-10-09'
tags:
  - 'career'
  - 'motivation'
coverImage: 'images/biggest-fears-thumb.jpg'
---

I talk to a lot of people who are transitioning into web development careers. These are the four questions they that reveal your greatest fears about changing careers.

https://youtu.be/0k2dFN6x4jg

## 🧰 Resources 🧰

- 🔧 [Read more about age bias in hiring web developers and how to overcome it](https://raddevon.com/articles/am-i-too-old-to-become-a-web-developer/)
- 🔧 [Learn to get experience before you get your first job](https://raddevon.com/articles/get-a-web-development-job-without-experience/)

<!-- If you're sold on freelancing having watched the video, or even if you're just curious to know more about what it would be like to start freelancing, sign up for my **Freelancing Crash Course**! It's free and comes straight to your inbox. 👇

[thrive_leads id='2262'] -->

---
title: 'How to Build Your Web Developer Résumé (Even with No Experience)'
date: '2019-01-25'
tags:
  - 'career'
  - 'resumes'
coverImage: 'images/reviewing-a-resume.jpg'
description: 'Build a résumé that gives them all the information they need and shows why youʼre the right choice. Iʼll even give you a copy of my résumé. Itʼs not perfect, but it works well enough for me.'
---

Even in a field like web development where you have loads of opportunities (see [Why Web Development Is a Great Career](/articles/why-web-development-is-a-great-career/)), competition for permanent positions is still fierce. Companies filter through tons of résumés for each opening. Most applicants get filtered out before they get a single interview. The only thing you want from your résumé is **to get you through that initial filter** so you can start getting interviews. Keep reading, and I'll teach you how to write a résumé that **gets through the filter** and takes you **one step closer to gainful employment**.

![Reviewing a résumé](images/reviewing-a-resume.jpg)

After I decided to leave my last full-time gig, I thought I wanted to go directly into another one. Well, I thought that for a day, at least. 😜 On that day, I sent out one résumé, and I ended up doing four rounds of interviews with the company. I didn't get the job, but the résumé certainly did _its_ job by getting me into the building. Here's the résumé I submitted:

![Résumé I submitted with my most recent job application](images/my-resume.png)

I show this **not as an example of the perfect résumé** — it definitely isn't — but as an example of one that worked. Let's go through and examine the aspects of this particular resume that succeeded and those that could be improved.

## Less Résumé Is More

My example résumé is a single page and glanceable. You can quickly see what tech I know, where I've worked, and what projects I've worked on. If you think from the perspective of the company who is trying to filter through 100-200 of these, this is exactly what you'd want to see.

I could have written several paragraphs of flowery prose about my career objectives, but that wouldn't have made this any better. My goal is **to communicate why I would be valuable without wasting people's time**.

## Make It Relevant

I've been working since I was about 15 years old. I could have tried to catalog everything I've ever done in the "Experience" section. Instead, I included **only what was relevant** for the position I was applying for. They don't care that I served time as a Walmart cashier or that I was paid to produce IDs for students at my high school.

One thing I might have done better is to **break out individual projects I've done through RadWorks**. As it's written, my freelancing practice is a sort of black box where I did "front-end development," whatever that means. I wish I had used the bullet points to point out specific projects that would have been interesting to them.

### If you don't have relevant experience…

Let's unpack that a little. Do you mean that you have **no experience whatsoever** that is relevant to the job you're applying for, or do you mean that **you don't have _professional_ experience** (meaning you haven't been paid for this kind of work)?

If your problem is that you don't have _professional_ experience, that's fine. Instead of listing irrelevant work experience, **list _projects_ you've completed that are relevant**. Provide links to code and working deployments of the projects if that's practical.

If your problem is, in fact, that you have zero experience that's relevant for this job, **you probably need to fix that instead of applying**. You can apply for this job anyway, but you're unlikely to get it. Instead, go build a project or two that gets you some experience in the area of interest and list those on your next application for a similar opening. _(You might be interested in reading about [the best types of projects for learning](/articles/the-best-projects-for-learning-web-development/) and even getting some inspiration from [my list of 10 sometimes wacky project ideas](/articles/10-great-web-development-learning-project-ideas/).)_

It's easy to get cynical and start poking fun at the fact that entry-level positions require 5 years of experience, but that won't help you get hired. Think about this from the company's perspective. They have a problem they need to solve or else they wouldn't be hiring. You're asking them to hire you **even though you have no experience solving the problem they need solved**. Meanwhile, they have a stack of résumés, many demonstrating the skills they need. Why would they even give you a second look?

You can mitigate some of this by [starting your career with freelancing](/articles/inverted-career-path/). The barriers to entry are lower, and you can do some learning on the job. It's no silver bullet; you're not going to quit your job driving a forklift today and set up shop freelancing tomorrow hoping to learn _everything_ as you go.

<!-- (Here's a resource to help you get started 👇)

[thrive_leads id='1378'] -->

What it *will* do is allow you some more leeway to build from your foundation of web development fundamentals (HTML, CSS, and Javascript) into more esoteric stacks with the motivation of a paycheck to drive you forward. If you don't want to live in the freelance world forever, showing successful freelance projects on your résumé make you a much more attractive prospect.

## Focus on Results You've Generated

Most developers focus on technology. The résumé is a contest to see who can list out the most languages and frameworks. That's not going to do you any favors.

Your résumé should let people know that you're proficient in the technologies they need (_if_, in fact, you are), but **it shouldn't list single technology you've ever touched** unless that list is both short and relevant. My résumé does a good job of keeping it lean.

Instead, you should focus on **what results you've created** for you previous employers and clients. This is the weakest aspect of my résumé. It doesn't really talk about results at all. A giant list of frameworks is meaningless. Does that mean you're a master of all of those? Does it mean you've at least gone through a tutorial for each one? Maybe it just means you've heard of them all.

**Results speak for themselves.** If you built a new backend that allowed you to cut infrastructure costs in half or if you built a UI that reduced churn (customers canceling a service) by a quarter, those are powerful. They're powerful because they equal more money for businesses you work with. Presenting them in your résumé is powerful because it shows you understand the value of the work you do.

## Show What Makes You Unique

You have to walk a fine line with this one. If you start telling about your weekly pick-up basketball game or listing your pet names, most people are going to get annoyed.  Talk about interests or projects that aren't strictly related to the stated job requirements but **provide a fringe benefit** to your prospective employer can help you stand out though.

In this résumé, I've done this with my side projects. I mentioned Rad Devon because I'd like them to know I enjoy mentoring new developers. I talked about my writing experience because I want them to know I'm good at communicating. I also drop the fact that I think a lot about writing readable, maintainable code, which is very important when you're working on a team.

## Write Your Résumé for the Reader

![The perfect résumé Venn diagram](https://raddevon.com/wp-content/uploads/2019/01/ideal-resume-venn-diagram.svg)

Most résumés are written for the writer. We compile a massive list of every job we've ever had and another of every skill we've ever encountered. We look at the 6-page document with a feeling of pride. Look at all we've accomplished and all we've created.

This makes the _writer_ feel good, but **it doesn't accomplish the goal of getting the job**. These are the résumés that go directly into the garbage. **The résumé written for the reader is the résumé that gets you the interview.** Some quick advice:

- Cut out anything that's not relevant.
- Instead of writing the 6-pager, re-read the job posting. Most of them tell you exactly what they want from you.
- Make sure you tell them that you have those skills they want.
- If you can, share a result you generated with those skills.
- Share something about yourself that could be valuable in this position.

Writing for the reader is the advice that underlies all the other advice. **If you can orient yourself around the goal of finding how you're valuable to the company instead of the goal of getting the job, the job will follow.**

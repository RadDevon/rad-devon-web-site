---
title: 'Things You May Not Know About Freelance Web Development Pricing'
date: '2019-08-09'
tags:
  - 'freelancing'
  - 'pricing'
coverImage: 'images/people-taking-notes.jpg'
description: 'As youʼre pricing your services, keep in mind these gotchas. Make sure youʼre charging enough to account for all of them!'
---

![People taking notes](images/people-taking-notes.jpg)

The first thing you need to do if you want to be a freelance developer is to set your rate. You can figure out the minimum rate you need to charge with a simple calculation, but if you’re not aware of all the variables, you’ll come up short.

If you’re coming from the world of full-time employment, it’s easy to miss some of the benefits your employer paid for. You also probably won’t think about all the new jobs you’ll have to do as a freelancer and the unbillable time you’ll spend doing them. If you don’t consider these when setting your pricing, you’ll probably set a minimum rate that is below the minimum you need to stay afloat.

Let’s take a look at how much time you can actually expect to bill (spoiler: it’s not 40 hours a week) and the costs you could easily overlook when you’re thinking about your freelance rate.

## How Many Hours You’ll Actually Bill

If you work as a web developer for a studio or in-house, you never have to worry about where the work is coming from. With a studio, you’ll usually have someone else selling the work you will later build. Working in-house, you’ll have product people who drive development forward, hopefully guided by your users. In either case, the person responsible for bringing in work isn’t you.

The opposite is true in freelancing. If you sit around waiting for more work to come in on its own, you’re going to have a rough go of it. You need to get out there and sell your services. Build relationships, screen potential clients, maybe even build proposals or estimates. These things take time, and you can’t bill anyone for that time. (Well, some-times you can bill for some of it. More on that later.)

You’ll need to wear other hats too. You will probably do some of your own bookkeeping. You’ll build a website for yourself and maybe have other marketing activities.

Maybe you’ll spend some of your time mentoring others.

Expect to spend about 1/3 to 1/2 of your time on activities you can’t bill for.

## Things You’ll Have to Pay For (That You Might Not Have Considered)

Some people won’t try freelancing because they would “lose” health insurance or life insurance. Truth is, you never got any of these things for free. If your employer didn’t pay for them, they could add that money to your salary. It’s not free just because it doesn’t hit your paycheck. The good news is you don’t have to lose them either.

As a freelancer, your employer still pays for all your benefits. The only difference is that your employer is you. Choose the benefits that are meaningful to you and buy them.

As the party responsible for your own benefits, you now have more choices and you get to see the money before it’s spent on the benefit.

### Taxes

If you’re employed, your employer pays half of your Social Security and Medicare taxes. As a freelancer, you’ll pay both halves. This amounts to about 15 percent of your income as of this publication. Your state may also have a self-employment tax that you need to pay.

Now that we’ve gotten the biggest tax surprise out of the way, be sure you also save for your income taxes. Your employer will typically withhold an amount from your check to cover income taxes. (If you withhold more than you owe, this is how you end up with a tax refund.)

Now, that’s all on you. You’ll need to save for income taxes on your own so you don’t get caught with a tax obligation you can’t cover at the end of the year.

I saved 20 percent of my freelance income last year, and that wasn’t nearly enough. This year, I’m saving 35 percent of what I make to cover both my self-employment taxes and my income taxes.

### Vacation and Holiday Pay

When you take a vacation from your job, or if you’re off for a holiday, you get paid just as though you worked that day. You can have this as a freelancer too … but you’re the one paying. That means you have to make enough on the days you do work to cover the days you don’t. That includes vacation, holidays, and sick days.

### Retirement Savings

Some employers save for your retirement on your behalf, but most have a matching program. You contribute whatever portion of your pay you choose to your retirement, and you employer will match a percentage of your salary.

That means you’re already used to paying for your own retirement benefit. The only thing you “lose” as a freelancer is the employer match. You’ll need to make enough to cover that portion your employer would have contributed. Three percent is a common match.

### Health Insurance

This is the scary one for most people. If you want health insurance (and you probably do), you’ll have to find a plan and buy it yourself.

A quick search of my state’s health care exchange at the time of this writing shows family health insurance plans ranging from $850 to almost $1,700 per month, depending on coverage and deductibles.

### Life Insurance

Life insurance is important if you have a family who depends on your income. Many employers buy your life insurance coverage. Fortunately, life insurance can be very cheap. You can get term life insurance (coverage expires after 20 or 30 years) for a few hundred dollars a year. Whole life coverage never expires but costs more.

### Other Benefits

Think about anything else your employer pays for that you value. Some employers offer gym memberships, money for ongoing education, transit passes, free lunches, and other perks. Make sure to include headroom in your rate to replace the fringe benefits that are valuable to you.

### Your Tools

Most employers will buy you the tools you need to perform your work. Fortunately, as web developers, we don’t need super-expensive equipment, but the costs still add up.

As a freelancer, you’ll be footing the bill for these. Here are a few tools your employer might have paid for along with ballpark prices:

- Computer (~$2k)
- Keyboard (~$100)
- Mouse (~$70)
- Monitors (2x ~$400)
- Desk ($300 – $2k)
- Chair ($100 – $500)

You’ll need tools as a freelancer that you employer wouldn’t have provided because you wouldn’t have needed them.

- Hosting/cloud services (~$300/year)
- Accounting and invoicing software (~$50/month)
- Sales and marketing tools (You could spend anywhere from nothing to thousands of dollars per month on these.)

You can pay way more or way less than any of my ballpark prices. I tend to estimate a little on the high side because I believe you should invest in excellent tools if you take your work seriously. Even if you’re not bought into that idea, budget for it so that you can go down that road if you choose to later. If you’re able to make it work on a budget, you’ll be able to pay yourself a nice bonus.

This list is by no means comprehensive. You may have completely different needs.

For example, if you don’t have space at home for an office or if you prefer to work somewhere else, you might want to buy a desk as a co-working space. That’s going to run you about another $350 a month. Make your own custom list so you understand exactly how much you’ll need for tools.

### “How am I going to charge enough to cover all these expenses?”

Your freelance clients should expect that your hourly rate as a freelance web developer will be more than that of an equivalent full-time employee. If they have employees, they’re fully aware of the additional overhead of hiring someone as a permanent employee. They know you’ll have to absorb those costs yourself.

It’s worth paying a premium to work with someone you can walk away from when you no longer need the services they provide. That’s the value your clients get for the premium that lets you keep your business running and profitable.

This post was an excerpt from [my book 📕 *Freelance Web Development Pricing*](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/). If you want to dive deeper and be able to confidently price and estimate your projects, this book will get you there. Check out the full book to learn everything you need to know about pricing!

<!-- ![Bradley Serlis headshot](images/bradley-300x300.jpg)

## “How should I set my freelance web development Rate?”

— Bradley Serlis… and many others

![Freelance Web Development Pricing ebook on a tablet](images/freelance-web-development-pricing-ipad.png) [Learn Everything About Pricing](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/order/) -->

_This post originally appeared on [Simple Programmer](https://simpleprogrammer.com/freelance-web-development-pricing/)_

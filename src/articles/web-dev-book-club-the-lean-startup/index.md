---
title: 'Web Dev Book Club: The Lean Startup'
date: '2020-03-13'
tags:
  - 'books'
  - 'business'
  - 'career'
  - 'philosophy'
  - 'self-improvement'
coverImage: 'images/books-lean-startup-thumbnail.jpg'
description: 'Learn about how businesses work, and youʼll have a massive advantage as a web developer. The Lean Startup is a great intro to how modern tech startups are built.'
---

https://youtu.be/mNE8mKfK3XA

## The Lean Startup

Grow as a web developer by reading [_The Lean Startup_](https://amzn.to/2PZUCxP) by Eric Reis. Yes, it's a business book, but you'll be a much better web developer after reading it.

The old way of starting a business was to come up with an idea, go away for months to build it, and finally release it **with your fingers crossed** 🤞. _The Lean Startup_ teaches you a new method that **removes a lot of the risk** and **makes building a new business cheaper**. Here's what you'll learn as a web developer:

- 💸 Building everything at once is a bad (and expensive) idea. Build your projects incrementally and save your clients/employers money by recommending they do the same.
- 💼 Learning the techniques of The Lean Startup is critical if you're going to be working with startups (because they're most likely building their companies on the foundation of this methodology). If you're not working with startups, you can start to share these lessons with your clients. They'll see you less as a "developer" and more as a "consultant." This means you have a greater chance of delivering them success and you can charge more money.

Grab your copy [on Amazon](https://amzn.to/2PZUCxP).

<!-- Now, it's time to stop reading about web development and make it your career. Start with freelance. My Freelance Crash Course will teach you how.

[thrive_leads id='2262'] -->

## Transcript

This is another one of the books that made me. This is _The Lean Startup_, by Eric Reis. And I want to share with you how this business book can make you a better web developer. Let's go.

_The Lean Startup_ teaches you how to employee what's basically the prevailing methodology for building a tech startup and what has been the prevailing methodology for probably the last 10 years or maybe more. Instead of the old way of coming up with an idea and building a giant piece of software right up front what you want to do is build it a little bit at a time. Build the least you can, and then work with your customers to make sure that piece is right and to help inform what you should build next to best solve their problems. This is gonna definitely be required reading if you want to work with startups because a lot of them are going to be using this methodology and you need to have an understanding of it to be able to work with them.

Even if you're not planning to work with startups, this is a really valuable tool to have in your toolkit. This is probably something you're going to want to use in your own development as you're building out software, both for other people and even maybe software that you want to sell yourself.

For example, maybe you build a piece of software for someone and you suddenly realize, oh, wait a second. there are tons of other businesses that look just like this one, who have the same problem. I bet I could sell this software to all of them. That may very well be true. And by using _The Lean Startup_ methodology, you can start to build out that software without building something that really nobody wants.

That's one way _The Lean Startup_ can be helpful to you as a freelance web developer. Another way is by bringing the methodologies it teaches down to other sectors, then maybe you wouldn't have exposure to them. The method that _The Lean Startup_ teaches is really common in Silicon Valley, and it's really common in other tech startups, even outside of Silicon Valley.

But if you're working with people in other sectors, you can be the person that brings these innovative ideas to them. And the great thing about these ideas. Is that they will save your clients money. Clients who wouldn't have been able to afford to use your service maybe can because they're building smaller, iterating on that small core as they go,  instead of trying to build a gigantic, monolithic piece of software from day one.

"I want a horn here, here, and here. You can never find a horn when you're mad, and they should all play La Cucaracha."

"Can do, Mr. S."

"And sometimes the kids are in the back seat. They're hollering. They're making you nuts. There's gotta be something you can do about that."

"Maybe a built in video game would keep them entertained."

"You're fired! What is my brother paying you for?"

"What about a separate soundproof bubble dome for the kids with optional restraints and muzzles."

"Bullseye! And another thing: when I gun the motor, I want people to think the world is coming to an end. Vroom! Vroom! Vroom!"

When you have a client come to you and ask you to build a gigantic piece of software and you come back to them and say, I could take all your money, but what I would rather do is, I would rather save you money. I would rather you have a smaller investment. If you come to somebody and say that you have short circuited a lot of the process for earning their trust because nothing earns trust more quickly than to tell someone, I don't want to take your money. By learning some of these lessons of _The Lean Startup_ and bringing them down to some of your clients that maybe wouldn't have had exposure to them otherwise,  you're moving up the food chain. You've graduated from being a web developer into more of a consultant. Now you're involved in some of the business decisions.

_The Lean Startup_ is an acknowledgement that our hypotheses are not always right.

"They cannot arrest the husband and wife or the same crime."

"Yeah, I don't think that that's true, Dad."

"Really? I've got the worst <beep\> attorneys."

That's true, whether we acknowledge it or not. So using a process that _does_ acknowledge it means you're going to have less waste. If you can be the web developer that understands the importance of that and knows how to minimize that and cares about that, then you will automatically gain an advantage over other web developers who just want a specification so they can sit down at the computer and build something for two or three months, and then toss it over the wall to the client and hope for the best.

That's _The Lean Startup_ by Eric Reis. Check it out to understand how modern startups are built. Thanks.

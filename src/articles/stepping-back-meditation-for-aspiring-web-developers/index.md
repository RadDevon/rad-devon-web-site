---
title: 'Stepping Back: Meditation for Aspiring Web Developers'
date: '2019-10-25'
tags:
  - 'career'
  - 'mentalhealth'
  - 'philosophy'
coverImage: 'images/man-meditating.jpg'
description: 'Meditation is about getting out of your own head. Hereʼs how it can help you become and thrive as a web developer.'
---

![A man meditating in the traditional "meditation" post with legs crossed and hands held out on his knees](images/man-meditating.jpg)

When searching for images for this post, almost every image I found looks like the one above: a person with legs crossed on top of a mountain, hands propped on their knees, held in what I can only call "that meditation pose" with a finger touching the thumb of each hand.

Just being perfectly honest, when I look at stuff like that, it turns me off a little. Like meditation needs to be some kind of ritual. I jump to conclusions that it probably makes bold claims about what it can accomplish and delivers results that are suspect at best.

But I tried it anyway, because **a lot of people I like and respect were talking about the benefits** — people who I trust to be skeptical and not fall victim to "[woo](https://rationalwiki.org/wiki/Woo)."

## What Meditation Actually Is

I meditate on a fairly regular basis, and **here's what it looks like**. I'm usually sitting, on a bed or maybe on a chair. My legs aren't crossed. My hands aren't in any position. I'm not chanting or anything like that. My sessions are generally 10 minutes.

I generally use **guided meditation**. In this style of meditation, you listen to a recording that coaches you through the meditation session. In addition to telling me what I should be doing, this helps bring me back when I get distracted and my mind starts churning over my concerns of the day.

The app I use coach me through different areas of focus in each session. In one session, I might focus on the sounds I'm hearing while in others, I might focus on sensations I'm feeling. My eyes are closed in some and open in others.

The object of the meditation (i.e. _what_ you're focused on) is not important — whether it's sight, sound, or touch. The goal of meditation is **gain some control over your own mind**. For most people, their mind is barraging them with anxieties, problems, ideas, emotions, and other miscellaneous thoughts throughout their day. It's valuable to be able to take a step back from that and just observe it for a moment.

You can practice meditation regardless of your beliefs, and realizing the benefits don't depend on any belief system either. **If you have a mind and would benefit from being in better control of it**, meditation can help.

## Why Taking a Step Back Is Valuable

Most of us have experienced a tricky problem or a problem that consumed all of our available headspace. It's exhausting. If you have exercised the control over your thoughts that meditation can provide, you can lean on that in these times to gain some relief from whatever has you stressed.

It's also great for acting in a more measured way. Emotional decisions are rarely good ones. In a moment when you're about to be overcome with emotion, you'll have the ability to **look at the response the emotion is creating in you**, as if you're on the outside looking in on your emotional state. Having that awareness about what the emotion is doing gives you some space to **make decisions outside of it**.

Here's a real scenario you might find yourself in. You turn out your finest work — a beautiful web site that's accessible, offers a thoughtful user experience, and has clean, readable code. You hand this site off to your client who comes back angry. They claim it's not at all what they ask for, and they refuse to pay. Your heart rate increases and you feel your face get hot.

Your natural response might be to scream obscenities at the client and let them know they'll be hearing from your lawyer. Spoilers: this is not likely to help the situation. Even if you don't typically fly off the handle like this, you might still be inclined to respond in a way that isn't productive.

If you've practiced meditation, **you'll have the willpower to take a deep breath and make a more measured response**. You can calmly probe the client with questions that might reveal what the problem is. You can disarm them by being calm and trying to get to the root of the problem. It's still hard to say what the outcome will be, but it's going to be far better than the "scorched earth" approach.

It doesn't even have to get this dire for your meditation skills to be useful. Let's talk about a much more mundane situation — something that happens to me all the time: people not showing up to appointments on time.

The first time I scheduled an appointment with someone and they missed it, **I was furious**. 🤬 I didn't go into a fit of swearing at them, but I _did_ end the working relationship there. I fumed over it for hours. I had taken time beforehand to prepare. I went out of my way to meet them at a restaurant at the time they chose… and they never showed up. I took it very personally.

Now, I'm able to get some distance from those emotions and take a more measured approach. It's made me a better freelancer, a better developer… _and_ a better person.

## How to Add Meditation to Your Toolkit

Some people say you shouldn't use apps for meditation. That's all well and good, and I understand the philosophical justification behind it. Having said that, I'm a technophile, and I know I won't do it unless there's a fun shiny app telling me to. 😉 I've tried several apps and found a couple of favorites.

[Headspace](https://www.headspace.com/) is a good one, but my absolute favorite is [Waking Up](https://wakingup.com/). I find it has good variety in the areas of focus for the sessions. It also has some sessions where the narrator is mostly silent to break things up a bit.

The narrator does a great job of telling you what to do during meditation and why you're doing it. Some of the apps I've tried do a great job telling you what to do but leave out why, meaning you don't actually accomplish anything during your meditation except practicing a bunch of techniques without knowing what the goal is.

Both of these apps offer paid subscriptions. I don't get paid no matter what you decide to use to help with your meditation, so feel free to experiment with apps to find the one that works for you. You can also find YouTube channels (the Headspace app I mentioned earlier [has a good one](https://www.youtube.com/channel/UC3JhfsgFPLSLNEROQCdj-GQ)) that will teach you meditation and guide you through practice. If you have the discipline to go back to the channel on a regular basis, that's another great option.

To get started, **grab your app of choice and set aside 10 minutes each day** to practice. You may find you're practicing for a while without noticing any results. If so, hang in there and sustain your practice for a month or two. You'll soon find yourself making better decisions and going through your day with a greater sense of calm. It's a small improvement that can make a big difference in all aspect of your busy new web developer life.

If you liked this article, you might also want to check out my article on [stoicism](/articles/pushing-through-stoicism-for-aspiring-web-developers/) and how it can help you become (and remain) a successful web developer.

<!-- You generally understand that becoming a web developer is more than just writing code. Good for you! I help people close the gap between "I know how to code," and "I get paid to code." Sign up below for a free mentoring session. 👇

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

---
title: 'Learn Version Control with Git and GitHub: Part 1'
date: '2020-08-07'
tags:
  - 'git'
  - 'version-control'
coverImage: 'images/learn-git-part-1-thumb.jpg'
---

Git is one of those universal essential technologies that all web developers need to know. I ran a live Git workshop several months back to teach all you rad people how it works. We made some great progress, but we didn't finish the workshop in the two hour stream.

I've circled back to finish the remaining exercise and edit the live video together into a much tighter package. Over the next two weeks, you'll get those videos in two parts!

You want to:

- ❗️ be able to turn back the clock on your codebase if you accidentally write code that breaks everything.
- ❗️ have backups of your code off-site in case something happens to your computer
- ❗️ have an easy way to collaborate with other developers so that you aren't all stepping on each other's code

Git and Github give you all these things! We'll learn to use them by working through a free open-source git workshop called Git It.

## What You'll Learn in this Video 🎓

- ⚡️ How to install Git
- ⚡️ How to initialize your first repo
- ⚡️ How to stage and commit changes
- ⚡️ How to set up your GitHub account
- ⚡️ How to link your GitHub account to your local Git install

https://youtu.be/wi8vQsaBqy0

## Resources 🧰

- 🔨 [The Git It workshop](https://github.com/jlord/git-it-electron)
- 🔧 [GitHub Desktop](https://desktop.github.com/)

Ask any questions you have [in the YouTube comments](https://youtu.be/wi8vQsaBqy0) or to me directly [on Twitter](http://twitter.com/raddevon). Now, get out there and start protecting your code!

In the same way Git enables you to go back to good code when you accidentally break something, setting a goal to remind you **why you are learning web development** allows you to revert back to the time when you were driven and enthusiastic to learn!

Listen, as you go through the long journey of learning web development to transition into a new career, **you're going to lose some steam**. Your drive is always strongest at the beginning, before you've confronted the reality of what this transition takes. You're going to get frustrated. You're going to feel like you're not cut out for this. Maybe whatever it was in your life that made you want to make a change gets a little less intense and you forget why you're making all this effort.

Having a Big Goal allows you to get back to why you're doing this. You can revert your enthusiasm, your drive, and your willpower back to nearly what it was when you first started. Take my free email course to figure out your Big Goal which will keep you from losing all your progress half way through.

[thrive_leads id='1360']

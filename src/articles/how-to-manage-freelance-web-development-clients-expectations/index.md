---
title: "How to Manage Freelance Web Development Clients' Expectations"
date: '2019-02-15'
tags:
  - 'clients'
  - 'communication'
  - 'freelancing'
coverImage: 'images/planning-meeting.jpg'
description: 'Setting expectations is the first step toward making your clients happy. Even the outcome itself isnʼt as important to your clientsʼ satisfaction as setting the expectation about what the outcome might be. Hereʼs how to do it.'
---

![Two people having a planning meeting](images/planning-meeting.jpg)

Setting clear expectations for your prospects and clients makes them feel at ease. 😌 Not knowing what to expect from an engagement is a source of **anxiety**. 😱 People want to make plans, and they need to make them around what you're going to do for them.

How long will it take? What will it cost? What result should they expect? Without knowing these things, your clients can't plan effectively for what comes next. This leaves them **feeling lost and out of control**. They can't move forward until you do your work, and they're not even sure what that _is_ yet.

Here are some of the key inflection points where you should set expectations and some strategies to put your clients at ease with clear expectations.

## At First Contact

If you meet someone who is interested in your services, set a clear expectation about **what you will do next and when you will do it** during your first contact with them. This gives your prospect comfort as you start the relationship. When you actually make good on your promise, you start building a track record of reliability. That builds trust.

Here's an example scenario. You meet a prospect at an event. Through talking with them, you learn they use two different CRM (that's customer relationship management) applications because each one has features they can't live without. Unfortunately, these two apps don't talk to each other. Each customer has to be entered twice. Besides wasting time, this leaves the door open for human error, which can cause the company to lose clients.

You think you can fix this problem for them, but you need to do some research into the APIs offered by the two apps. To set proper expectations, you say, "I might have a way to solve that for you. Let me do a little research, and I'll get back to you next weekend." This gives your prospect **a clear expectation** for when they're going to hear back.

The first meeting is a good time to **give your prospect a ballpark price, or to get a ballpark budget** from them. Nothing here is binding, and you want to make sure the prospect understands that. For the previous example, you may want to start by understanding what the problem costs them.

In other situations where the solution is more well-defined, you could say something like, "My services start at $X. Is that within your budget?" If you want to start from the prospect side of the equation, you could ask something like, "Could you ballpark your budget for this project?" If they resist, asking "Is it closer to $1k, $10k, or $100k?" might get them to open up a bit more.

If you can't serve your client within their budget, you'll **save everyone time and earn their respect** by making that clear at the beginning. Even if the client can't afford you right now, they'll have an expectation about what budget they need to be able to hire you later.

[thrive_leads id='2262']

## Before the Project

Talk with your prospect before the project begins and make sure you are both in agreement on the terms of the project. Get aligned on the expectations for both you and your client. Once you've ironed out all these details through conversation, make sure you both **sign a contract that formalizes all these expectations**.

Things will undoubtedly change as you go. Project requirements may shift. Deadlines may be missed. Technology you're working with might change. Your contract serves as a baseline for the project going forward.

## When Starting the Project

Just after a new client has paid your deposit, they're feeling very vulnerable. They're out on a limb: they've given you some value (in the form of money) while you've given them none (or relatively little). This is a great opportunity to **let them know what you'll do next, when they will hear from you with an update, and what you need from them**.

The last one is of critical importance to remember. We tend to think only of the expectations on us as web developers, but our clients also have responsibilities to make sure our projects succeed. Without their help, we can't do our work.

## While Working on the Project

The major expectations should be laid out in a contract, but you should reiterate them as you move through the project. Not every expectation will be laid out in the contract, and those smaller expectations need to be set as you go. "I'll email you this Friday," or "I should be wrapping up the first phase tomorrow," are great ways to let the client know their money was well spent.

## When Something Changes

You're going to miss a deadline at some point. The best time to let the client know this is going to happen is not the day of the deadline but **as soon as you know you're going to miss the deadline**. If you've done a good job [vetting your client](https://raddevon.com/articles/how-to-get-fewer-freelance-clients/), they should be understanding and appreciative that you're keeping them in the loop.

Even if the client does get upset, giving them some lead time to prepare for the missed deadline is better than springing it on them at the last second. They'll remember how you handled the situation the next time they need help from a web developer.

Like expectations, changes don't flow in only one direction. Clients often want to make changes to the project mid-stream. When this happens, a simple "Yes, I can do that," isn't enough unless the change is small enough that it doesn't materially impact any of the terms of the deal. In most cases, **you'll need to set new expectations** around cost and timeline.

## When Things Are Going as Planned

You'll be tempted to keep rolling when things seem to be going well. Taking time out to communicate your progress steals execution time. Here's the problem though: what if you think everything is going well but your product doesn't match up to your client's vision? You want to find this out after a week of work instead of at the completion of the project.

The way you get there is by regularly communicating and showing your progress. You can do this with emails and screenshots, but I prefer either showing the live site or, when that's not practical, sharing a screen recording. I use [ScreenFlow](https://www.telestream.net/screenflow/overview.htm) to record a demonstration of my work so far. Then, I share that with my client via Google Drive.

## Communicate!

The key to managing expectations is to communicate! **It's better to communicate too much than to communicate too little.** This doesn't mean you want to share every little thing. Consider what your clients care about. Your client doesn't care that the version of the API you were using was deprecated… _unless_ that deprecation will cause you to miss a deadline.

If you select clients that are reasonable and respect you, they will be glad to hear from you, even when things go wrong. They will understand their choice is not between a developer who runs into problems and one who does not, but between a developer who hides problems until the last possible moment and one who communicates them early and openly.

---
title: 'Books That Made Me: The Obstacle Is the Way'
date: '2020-01-03'
tags:
  - 'books'
  - 'business'
  - 'philosophy'
coverImage: 'images/thumbnail.jpg'
description: 'As youʼre on your way to becoming a web developer, not everything is going to go the way you expected. How you respond to that will determine whether or not you ultimately succeed. This book positions you for success.'
---

https://youtu.be/qKmcMPbZ65k

## The Obstacle Is the Way

The Obstacle Is the Way teaches you how to be more in control of your emotions and to be prepared for the worst outcomes. These two simple tweaks make life so much more livable. They make you a much more pleasant person to work with too! [Buy The Obstacle Is the Way on Amazon](https://amzn.to/377VZ3m) to learn how you can put these tips into practice.

<!-- While we're on the subject of books, if you'd like a free chapter of my own book, _Freelance Web Development Pricing_, share your email address below. You'll learn everything you need to know about pricing your projects confidently, estimating projects, and turning a profit.

[thrive_leads id='2463'] -->

## Transcript

In this episode of The Books That Made Me, I want to share with you a book that's changed almost every aspect of my life. This is **_The Obstacle is the Way_ by Ryan Holiday**. Let's go take a look.

This is one of the books that made me, it's _The Obstacle Is the Way_ by Ryan Holiday. This book definitely has made me a better web developer, but going way beyond that, **it's made me a better person** in general. Hardly a day goes by that I don't apply something I learned from this book in my work and in my personal life. It's been so helpful and changed the way I approach life in so many ways. It's taught me to be less emotional and respond to things in a more measured way.

It's also reinforced some ideas I already had. One thing my family is utterly sick of hearing me say is, "expect the worst and hope for the best." It's a philosophy I have repeated until everyone is sick of it. This book basically espouses that same philosophy and a bunch of other related ones that are similarly useful and puts it in the context of a framework that will help make your life easier to live.

The book is about stoic philosophy, which is an ancient Greek school of philosophy that fell out of favor in contemporary times. It's not the kind of philosophy you think of when someone says "philosophy" – like these speculations about life and the nature of life and the world that are, at the end of the day, sorta useless. Interesting, definitely. I love to study philosophy, but not especially practical. Stoic philosophy is **extremely practical**, and it gives you tools to make your life easier. I want to share some of those with you now.

Here's a chapter that deals with the little nugget of wisdom I just shared with you about expecting the worst and hoping for the best. This one is on anticipation, thinking negatively.

> A CEO calls her staff into the conference room on the eve of the launch of a major new initiative. They file in and take their seats around the table. She calls the meeting to attention and begins, I have bad news. The project has failed spectacularly. Tell me what went wrong. What? But we haven't even launched yet.
>
> That's the point. The CEO is forcing an exercise in hindsight in advance. She's using a technique designed by psychologist, Gary Klein, known as a pre-mortem. In a post-mortem, doctors convene to examine the cause of a patient's unexpected death so they can learn and improve for the next time a similar circumstance arises.
>
> Outside of the medical world, we call this a number of things: a debriefing, an exit interview, a wrap up meeting, a review, but whatever it's called, the idea is the same. We're examining the project in hindsight, after it happened. A pre-mortem is different. In it, we look to envision what could go wrong, what will go wrong, in advance, before we start.Far too many ambitious undertakings fail for preventable reasons. Far too many people don't have a backup plan because they refuse to consider that something might not go exactly as they wish.
>
> Your plan and the way things turn out rarely resemble each other. What you think you deserve is also rarely what you'll get. Yet we constantly deny this fact and are repeatedly shocked by the events of the world as they unfold. It's ridiculous. Stop setting yourself up for a fall.

It continues on with a couple of other similar anecdotes that reinforce this idea that **you will be better prepared for a negative outcome if you imagine it before it occurs**. This is something that I cannot help but do, and I originally thought of that as a bug but now I think of it as a feature. It allows me to be more prepared for these negative outcomes when and if they do actually materialize.

Some of the other chapters that are really applicable for web developers:

- **Control Your Emotions**\- That's great when you're dealing with clients and people are angry about something not working out the way they hoped it would, or if _you're_ angry about things not working out the way you hoped they would.
- **Alter Your Perspective**\- This is great when things don't go your way and you think everything is ruined.
- **Finding the Opportunity**\- This is really good to use when you feel like you are shut out of something.

Here in Seattle, one thing I've noticed is that we have so many coding boot camps and they're turning out so many grads and there are so few junior developer roles and it feels like it's impossible. You're up against impossible odds. You either know someone and you can get in or you don't know someone and you apply and you apply and you apply and nothing ever happens. This is why I teach people to use freelancing to get their foot in the door and prove to people that they know what they're doing. That's what worked for me. I didn't have any way in either. And using this stoic philosophy, I was able to sort of **find another way around** that other people weren't exploiting.

- **Follow the Process**
- **Do Your Job, Do it Right**
- **What's Right Is What Works**

I love that it's practical and that you can apply it, like I said before, in your personal life and your job. It'll improve your relationships. It'll also improve your chances for success and give you better outcomes when you fail.

That is, again, _The Obstacle is the Way_ by Ryan Holiday. It's a short read. I highly recommend you pick this up and read through it or grab the audio book and listen to it. I think it will **change the way you think about the world and the way you look at success and failure**. I think it will increase your chances for your own success.

Thanks for watching and I'll catch you in the next one.

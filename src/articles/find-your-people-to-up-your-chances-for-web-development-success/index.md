---
title: 'Find Your People to Up Your Chances for Web Development Success'
date: '2019-09-06'
coverImage: 'images/pax-west-2019.jpg'
description: 'Finding the people you click with is important, even for introverts. Hereʼs why itʼs important by way of a story about my first experience at a video game convention and how it changed me.'
---

With all the resources available to learn on your own, you can become a web developer on your own… but you shouldn't. I attended an event this weekend that's not web development related, but it did remind me of how important it is to **find your people**.

![Two women and two men laughing while playing a game at PAX West 2019](images/pax-west-2019.jpg)

Last weekend was [PAX West](https://west.paxsite.com/): a gaming convention in Seattle. It brings together over 70,000 gamers of various ages and backgrounds for a weekend celebrating the hobby.

Over the course of the four days of the event, I got to play unreleased games and buy fun t-shirts. I got to listen to people who work in the industry talk about interesting topics. But the secret sauce is **everything that fills the gaps between those programmed events**.

My first PAX was 10 years ago. As silly as it may sound, I give that event a huge slice of the credit for making me the person I am today. As a mid-twenties guy living in East Tennessee, I wasn't too sure if it was OK to be an adult who plays games. I was low on confidence but full of doubt.

Over that weekend, though, **I transformed**. I went from being a socially awkward person who no one understood to someone who could walk up and talk to anyone. I made relationships I maintain even all these years later, and I actually got up in front of a room packed with people to tell them about some of my experiences. I learned that **I was OK** and that there were people just like me.

This feeling of belonging and camaraderie is important to find when you have hobbies that other people around you don't share, but **it's critical when you're trying to make a difficult transition** that people in your life may not even understand. If you want to become a web developer, you've got a much better chance of making that leap if you find people who have been there and understand the struggle.

<!-- One way to make the road from 💩 job to web developer a little less lonely is by finding a mentor. I offer a free session to new readers. If you like it, you can subscribe to ongoing sessions at a frequency that makes sense for you. Check it out. I look forward to chatting with you! 👇

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

You also need people you can bounce problems off of when you're not sure how to move forward on a project. Online resources are great, but they do a horrible job of dealing with the case where you don't know what question to ask. If you're coming to StackOverflow or reddit with a question, you generally want to have all the pieces in place except one. If you just have a vague idea of where you want to end up and need some direction, **you'll be moderated into oblivion**.

That's what friends are for. **They care about whether you succeed**, and they want to help. Maybe they've been web developers for years, or maybe they're new just like you but they have tackled this particular problem before. Whatever the case, if you're on your own when you get to a crossroad like the one I've described, the best outcome you can hope for is to muddle through slowly and painfully.

Once you're more established and you're ready for work, whether that's a permanent role or freelance work, **these relationships you've built with your people will act like tentacles**, allowing you to extend your reach in many directions at one, grabbing success while those less connected continue to struggle. Three of your friends got hired by great companies and will put in a good word for you. One of them just got their biggest freelance gig yet and needs someone they can trust to help out. Another works for a company with a small team who has a big project coming up and wants to augment their staff with a contractor for the next three months.

Fortunately for you, the means to find your people as a web developer are already in place. If you've been flying solo, [find a local developer meetup](https://raddevon.com/articles/which-meetups-should-i-attend-as-a-web-developer/) and attend their next event. (Obligatory plug: [Join my meetup](https://www.meetup.com/free-code-camp-sea/) if you're in the Seattle area!) If you've dabbled in meetups, find a local conference or [hackathon](/articles/how-to-find-beginner-friendly-hackathons/) and buy your ticket now before you have a chance to talk yourself out of it. If you're in a more remote area, find online communities where people actually talk and get to know each other. Slack and Discord both have great communities for developers. ([LadyLeet on Github](https://github.com/ladyleet/) has [a great list of Slack communities](https://github.com/ladyleet/tech-community-slacks). Scroll to the bottom for dozens that are not locality-specific.)

Taking these steps will unlock the magic of **finding your people**. They help you feel less lonely, pull you through the difficult times, and position you to have greater success than you ever could alone.

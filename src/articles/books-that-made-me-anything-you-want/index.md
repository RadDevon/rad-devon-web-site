---
title: 'Books That Made Me: Anything You Want'
date: '2020-02-21'
tags:
  - 'books'
  - 'business'
  - 'career'
  - 'philosophy'
  - 'self-improvement'
coverImage: 'images/anything-you-want-thumb.jpg'
description: 'Stop waiting for permission after reading this book filled with common-sense advice from a down to earth guy.'
---

https://youtu.be/A-i13eXn_5A

## Anything You Want

Derek Sivers started [CD Baby](https://en.wikipedia.org/wiki/CD_Baby), and he didn't do it in the typical tech startup fashion. He wasn't touring Silicon Valley, pitching VCs with just a dream for what he wanted it to be. He built it for himself, and it evolved organically from there.

Anything You Want is a tight little package of lessons Derek learned making his own path in the business world. Many of these business lessons can be applied directly in your web development career. Most of the ones that can't have an underpinning philosophy that developers who didn't graduate from a fancy engineering school can put to work as they make their way into the industry. [Buy Anything You Want on Amazon](https://amzn.to/3bDK4NU) to start cutting your own contrarian path into your new career.

<!-- While we're on the subject of books, I'd like to share with you my own book, _Freelance Web Development Pricing_. Share your email below, and I'll send you a free chapter so you can start pricing your projects confidently, estimating projects, and turning a profit.

[thrive_leads id='2463'] -->

## Transcript

Today, I want to share with you another one of the books that made me. This one is [_Anything You Want_](https://amzn.to/3bDK4NU) by [Derek Sivers](http://sivers.org). Let's go.

_Anything You Want_ by Derek Sivers really packs a punch for a book that's under a hundred pages long. It's full of anecdotes about Derek's time starting his own business, which is CD Baby. CD Baby was a CD store on the Internet because iTunes didn't exist and all these other digital music stores didn't exist yet.

The thing I really love about Derek is he is a contrarian like me. There are so many wonderful nuggets in this little book. They're not just about business, although they are framed in that context. You can take the lessons from this book and you can apply them to other aspects of your life, including your journey to become a web developer. A lot of these lessons I did apply. And they were really informative.

I want to share a couple things with you that are interesting out of this book. Derek talks about ideas just being a multiplier of execution. What that means is an idea itself isn't really worth anything, but a _good_ idea can make your business better if you also have the execution behind it. He says, an awful idea is like a minus one multiplier. If you have great execution and an awful idea, that minus one multiplier is probably still gonna land you in the hole.

If you just have a weak idea, , that's not that great, that's a 1X multiplier. So if you bust your butt and really make it happen, you can still succeed with a weak idea. If you have a _good_ idea, then that's a 10X multiplier.

I think that's an interesting way to look at ideas, and if you run in startup circles at all or in developer circles, you're probably not new to the idea that ideas are not especially valuable on their own, but it's still important to hear.

It's a good thing to be able to convey to your clients, especially if you're working with people who maybe want to bring an idea and get you to build it and split it 50/50. If you can sort of articulate why that doesn't really make sense, then that'll help you maybe turn what could just be a sour deal in into a friendship.

This book is organized into these little stories, each just a page or two long about various lessons Derek has learned through his business experience. Another one I really love that I think is useful for developers is the strength of many little customers. Derek talks about how people want to land Apple or Google or some other massive customer, and that's really hard to do.

Whereas you can get lots of smaller customers and still have a great career and maybe even have more fun. And I think this can easily be applied for developers. A lot of developers, they have these dreams of going to work for Google or Apple or Microsoft, and nothing less will do, but that's pretty hard to do.

Those are companies with big, massive barriers that keep you from getting in the door. You can go out and find a hundred clients as a freelancer who would hire you before Google or Apple would hire you. So, why not go work for those people? Probably you're going to have more influence over what the final product looks like than if you're just a cog in the machine.

Derek talks about the most successful email he ever wrote. This was an email he wrote to send out to people who ordered from CD Baby, and I'm going to share it with you now because it's really fun. It says, "Your CD has been gently taken from our CD Baby shelves with sterilized contamination-free gloves and placed onto a satin pillow. A team of 50 employees inspected your CD and polished it to make sure it was in the best possible condition before mailing. Our packing specialist from Japan lit a candle and a hush fell over the crowd as he put your CD into the finest gold lined box that money can buy. We all had a wonderful celebration afterwards, and the whole party, marched down the street to the post office where the entire town of Portland waved bon voyage to your package on its way to you in our private CD Baby jet on this day, Friday, June 6th. I hope you had a wonderful time shopping at CD Baby. We sure did. Your picture is on our wall as customer of the year. We're all exhausted but can't wait for you to come back to cdbaby.com.

He says that one silly email sent out with every order has been so loved that if you search Google for private CD Baby jet, you'll get almost 20,000 results. Each one is somebody who got the email and loved it enough to post it on their website and tell all their friends.

That to me says that you don't have to pretend to be some giant company to be successful and really, people just want to do business with other people, and even the other businesses you're wanting to work for as a web developer, those are all filled with people and they want to do business with people too.

So you don't have to be this faceless, homogenous process that you churn people through. You can be a human being. Some people actually appreciate that more.

Out of all the stories in this book, the one to me that is most relevant for web developers is called "Start Now. No Funding Needed." It's a call to entrepreneurs to say, you don't need to go raise money. You don't need to go to Silicon Valley and appeal to venture capitalists and try to get $100 million so you can go build your idea. Whatever problem you want to solve, you can just go do it.

And that's really the message that I try to deliver through my site. It's that you don't have to wait to be hired.

You don't have to get permission to go do what you want to do. You want to be a web developer, so go find somebody who needs web development. They may not know it's web development they need, but go find somebody with a problem that you can solve for them and do it. And then you're a web developer.

Maybe someday you get hired by a big company. Maybe you don't, but from that first day you're doing it. You're already a web developer, and I think this is something a lot of people miss because they get this rigid idea in their heads of not only where do they want to end up, but exactly how they want to get there.

And if they can't get there that way, then they failed. And that's just too rigid. If you're building a building and you want it to be able to withstand an earthquake, you don't build it so that it's so rigid and so tough that the earthquake can't shake it down. You build it so that had bends and flexes if or when the earthquake happens. It doesn't try to stay rigid in its exact position in its exact shape. It moves and bends and lets the earthquake move it. That's going to happen. There's nothing you can do about it.

You need to build that sort of flexibility into your career so that you can be prepared and be able to take advantage of opportunities that don't look exactly like what you thought they would. Sometimes those are the ones that will take you on a winding path that ultimately leads you to the end point you wanted to be at to begin with.

Those are just a few of the awesome anecdotes you'll pick up from this book – a few of the great lessons that Derek is ready to teach you. I highly recommend you pick this book up and read through it. It won't take you very long and it's well worth it. He is an extremely charming person. He's very genuine and authentic, and I think that all comes through in his book and I think his lessons are not only different from a lot of the business lessons you may have heard, but also extremely valuable to hear.

That's [_Anything You Want_](https://amzn.to/3bDK4NU) by Derek Sivers. Thanks for watching. I'll catch you in the next one.

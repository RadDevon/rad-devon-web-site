---
title: "Why Freelance Developers Shouldn't Do Their Own Taxes"
date: '2019-11-22'
tags:
  - 'freelancing'
  - 'outsourcing'
  - 'taxes'
coverImage: 'images/tax-prep-papers.jpg'
description: 'You may be tempted to do your own taxes. It will save you a few bucks… right? Actually, hereʼs another way to think about it.'
---

From the very first day I started doing freelance web development, I knew I wanted an accountant to prepare my income taxes. Most new freelancers I talk to, though, don't.

I always thought of this as nothing more than a personal choice until I was writing a check to my accountant for my tax prep last year. Because of the thoughts that ran through my head as I was signing that check – thoughts I'm about to share with you – I'm now 100% convinced that, no matter who you are, **you should never do your own tax prep as a freelance web developer**. Here's how I woke up.

![A coffee and pen with a folder full of tax papers](images/tax-prep-papers-1024x683.jpg)

As I was paying my accountant last year during tax season, I took a moment to be thankful for the fact I didn't have to fill out all these forms and have a robust understanding of tax law. I paid $400 for the tax prep, and, even though that may seem like a lot of money, I was happy this is an option that's available to me.

To try to put the cost in perspective, I thought about it in terms of my own rate for web development services. It will take me a few hours of work to recover this amount in billings to my clients. On the surface, I'm trading money to the accountant in exchange for the tax prep, but, what I'm _actually_ trading is some of my own time doing web development.

When you put it in these terms it's a no brainer. You mean to tell me **I can do a few hours of web development (which I love doing) and, in exchange, I get all my tax prep handled for me?** You better believe I'm doing that!

But wait. Even *this* slightly more enlightened way of looking at it assumes I would be as competent at tax prep as my accountant. **I assure you I would not.** So, not only do I get to trade a few hours of doing what I enjoy in exchange for not having to prepare my own tax return, but doing it this way means I have **an expert working on my behalf** to make sure I'm paying exactly as much as I owe and no more. This deal gets more incredible the more you break it down.

While my accountant is doing a stellar job prepping my tax return, I'm not just building web sites and having fun. I'm also:

- sharpening my web development skills
- learning new tricks
- building relationships with new clients (which will lead to more work later)
- strengthening relationships with my existing clients

…and, when it's all said and done, I'm a more fun person to be around because I haven't spent hours trying to decipher tax law, catalog expenses, and fill out cryptic forms.

<!-- While you're here, sign up for my Freelancing Crash Course. In one week, I'll teach you to set up your own freelancing business and take control of your future! 👇

[thrive_leads id='2262'] -->

If you're doing your own tax prep because paying someone else to do it is "so expensive," take another look at **the total cost of doing it yourself**. That cost can be quantified in overlooked deductions, missed opportunities for experience doing what you actually want to do, and, most important of all, your own happiness and well-being.

---
title: 'Learn Docker 🐳 for more consistent and portable dev environments!'
date: '2020-10-31'
tags:
  - 'docker'
coverImage: 'images/docker-thumb.png'
---

Docker is a fantastic technology that lets you create repeatable and portable environments for development or even for production.

What does that mean? That means other developers working on the same project will be running it in the same environment as you. No more fun bugs to hunt down only to find out it's because your colleague is running Node 8.x while you're running 11.x.

The definitions of your Docker containers can be included in your repo to make it super easy for any new developers to bring up an identical version of the dev environment. It's a huge boon to developers when it comes to collaboration.

In this course, I'll teach you the basics by working through the [Play with Docker classroom training](https://training.play-with-docker.com/dev-stage1/) together.

https://www.youtube.com/watch?v=XllRCpBxYko

Quick question while you're here: why are you learning web development? I don't mean the surface level answers – "I want to be a web developer," and "I want to make more money." What's really driving you?

If you're not clear on that, you won't make it very far. Sign up for my Big Goal course, and I'll help you get the clarity on your "why" that will help you persevere through the challenges of becoming a web developer.

[thrive_leads id='1396']

---
title: 'Test Web Sites in Chrome for Android on a Mac'
date: '2013-09-05'
tags:
  - 'android'
  - 'browsers'
  - 'chrome'
  - 'emulators'
  - 'google-chrome'
  - 'mobile'
  - 'testing'
coverImage: 'images/android-mascots.jpg'
---

_This article is really old and probably isn't relevant anymore._

On iOS devices, all browsers must use Webkit to render pages. Alternative "browsers" are no more than wrappers around the default browser's rending engine. When testing Android, proper display on Chrome, Firefox, and others doesn't necessarily follow from proper display on the default browser.

Once you have your Android emulator configured and running, it's not at all obvious how to install other browsers. You're already set to test the default Android browser, but that's only half the battle — perhaps even a smaller fraction of the battle than that!

In order to install an Android not bundled with the OS, you'll need to get that app's [apk](<http://en.wikipedia.org/wiki/APK_(file_format)>) file. Unfortunately, this requires Google searches leading to really shady sites that primarily exist to enable Android software piracy. Since Google offers no official path to Chrome for Android testing on the Mac, this is the only way.

Once you have procured the Chrome apk, you'll have to load it up on your device. First, start the emulator with the command `emulator @<avd-name>` from the terminal. The emulator should remain running while you run the install.

Pull up another terminal and run `apb install <path-to-apk>`. This should push the apk over to your emulated device. Launch the app by clicking the bottom-center button on the emulated screen to bring up the full app list. Then, simply click the app. If the browser runs and works as expected, you're done!

This process isn't especially difficult, but I found myself hitting two different roadblocks as I ran through it. First, I could not run apb from the terminal because its location was not on my PATH variable. To fix this, bring up your shell's configuration file (~/.zshrc for some, ~/.bashrc for most). Add this to your configuation:

```bash
PATH="$HOME/bin/android-sdk/platform-tools:$PATH"
export PATH
```

Substitute your path to the SDK's platform-tools directory when you add this to your config.

My second problem was a bit more difficult to nail down, but the solution was simple. I was able to get Chrome installed easily, but the rendering engine would not display. The browser would load web pages, but it would never display them. The interface also displayed odd behavior. The address bar would sometimes become invisible, among other things.

[thrive_leads id='1366']

After trying several different Chrome versions along with the latest build of Chromium, I found the solution hidden in my device configuration. Run android at your shell to bring up the SDK manager. Go to Tools > Manage AVDs... Edit your AVDs turning on the host GPU option.

![Use Host GPU setting](images/use-host-gpu-setting.png)

I also recommend you change your device to the Nexus S. Its lower resolution in comparison to many of the other devices make the emulator perform much better.

Now, you're ready to test your site against Chrome for Android on the Mac. The road may have been circuitous, but the payoff is assurance your site looks great in one of the most popular mobile browsers!

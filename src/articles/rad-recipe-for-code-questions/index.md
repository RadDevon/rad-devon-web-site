---
title: 'The Rad Recipe for Code Questions'
date: '2018-09-03'
tags:
  - 'business'
  - 'clients'
  - 'freelancing'
  - 'invoicing'
  - 'videos'
coverImage: 'images/rad-recipe-for-code-questions-thumbnail.jpg'
description: 'The internet is chocked full of grognards ready to rebuke questions asked by newbies for minor clerical errors. Hereʼs how to slip your questions through the filters and get useful answers.'
---

As a new developer, I got thoroughly rebuked many times trying to ask questions. You're going to run into problems with your code, and sites like Stack Overflow seem like the silver bullet. You'll quickly find, though, that **if you don't ask your question the right way, you'll be ignored at best or admonished at worst**.

You want to **give enough information** and **tee up your question** to make it easy to answer. I've laid out **the 4 ingredients** that make a good question. Here is the Rad Recipe for Code Questions.

https://youtu.be/Qcv4L8A6bI8

## Resources

I checked out 4 questions in this video:

- [Why can't Python code execute!?!?](https://www.reddit.com/r/learnprogramming/comments/9c3qso/why_cant_python_code_execute/)
- [How to change color of button with javascript/jquery when input (radio buttons) is selected](https://stackoverflow.com/questions/52130522/how-to-change-color-of-button-with-javascript-jquery-when-input-radio-buttons)
- [$ not defined issue](https://www.sitepoint.com/community/t/not-defined-issue/305661)
- [Image upload to Imgur API via JS Fetch API is canceled by browser](https://stackoverflow.com/questions/48513163/image-upload-to-imgur-api-via-js-fetch-api-is-canceled-by-browser)

In the video, I mention a couple of tools you can use to ask better questions. These will let you build working examples of your problem to make it easier for people to experiment and find a solution to your problem.

- [CodePen](https://codepen.io/)
- [JSFiddle](https://jsfiddle.net/)

<!-- _Need more help than Stack Overflow can provide? I give away **4 free 30-minute mentoring sessions** each week. Click below if you're interested._ [thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

## Video Transcript

Hey, this is Devon. In this video, I'm going to show you how to ask better code questions online.

When you're asking for help online with your web development questions, you want to do everything you can to **make it as easy as possible** for people to answer your questions. The thing is, nobody's being paid to help you. They're all doing it for free out of the kindness of their hearts, and their incentives are usually pretty weak.

If your question's too hard to answer, there are loads of other easier questions that they can go answer if they just want a quick hit of maybe ego or something like that from helping somebody. What you want to do is you want to make your question so great that, once they land on it, it's really quick and easy for them to help you.

I'm calling this my Rad Recipe for Code Questions. So, your question needs to have four ingredients roughly in this order.

The first one is **what you want to happen**. What's the desired result of the code you wrote.

The second ingredient: **What is the context?** What is around the code that might be important — other code in your application that might interact with this code or things about your environment, maybe your operating system or some version of some software that you're running that might impact the way it would fail or succeed.

That one can be a little bit tricky when you're new because you don't always know what all the context is. If that's the case, then just do the best you can and, if people follow up with you and ask for more, then come back around and post it.

The third ingredient is **what you tried**. This is going to show people that you've put in an effort, and, if somebody is giving you information and help for free, they definitely want to see that you have also put forth an effort to try to figure it out yourself. It also shows people the mental model you have of the way things work which may be wrong and they may be able to correct you and set you on the right track as far as that goes. And it shows how you're approaching the solution so people can tell you if you have the wrong approach or, if your approach is good, it's just maybe some kind of syntax problem or something along those lines. This gives people the tools to give you a good answer that can not only fix your problem, but teach you something else in the process.

And then the fourth ingredient is **what actually happened**. So, this is the flip side of the first ingredient what you want to happen. This is what actually happened as a result of what you did, what you tried.

You might have **multiple attempts at fixing your problem**. If so, then your third and fourth ingredients, you may have multiple instances of those and that's great. If you do, try to explain every one of them: the thing you tried and then the result from that attempt.

Now, we're going to **take a look at a few questions that I found online and evaluate them against this recipe,** maybe make it a little easier to see what is a good question and what is a bad question.

Okay, the first question we're going to look at is this one on Reddit on the learn programming subreddit. And the title of the question is "Why can't python code execute?"

"For some reason my Python code can't function. It is saying that it has some kind of gateway error. I have the code. It just won't run. I'm using Sublime Text 3 as well. Can someone help me out?"

Let's start with the first ingredient: so, what you want to happen.In this particular question, it's pretty vague what the user wants to happen. All we can really tell is that they want the code to run. We don't know what the code is supposed to do, so we kind of don't know what should happen

For the second ingredient — the context: in this question, the only thing they give us really about the context… well, that's not true. I was going to say the only thing is Sublime Text 3, but they also tell us it's Python code. So, I guess we know two things. Still that's not very much to work with.

For the third ingredient what the person tried. We can kind of assume they've probably tried _something_ and that it failed but we don't really know _what_ they tried. They tried to run their code, whatever that means to them.

And the fourth ingredient: what actually happened it. It's very vague. We know there was "some kind of gateway error." That's all we really know, and, since we don't know what they tried, maybe that doesn't mean as much to us as it _could_, but, regardless, we don't have very much to go on.

So, this question fails on all four ingredients of the recipe. It would be **really difficult** to try to give a good answer to this question without asking some follow-up questions. That's going to take extra effort, so, for a lot of people that are looking at these questions, that's probably going to be enough for them to move on to the next one and never come back to this one.

**The one thing this user does right** or at least _tries_ to do right is to **circle back around with the resolution of the issue**. So, if we scroll down a little bit that you can see some of the comments are basically echoing the problems I had with this post. "We don't have enough information." "We need more to give you an answer." And the original poster is responding to say, "I figured it out. I've got a solution." And then here they posted their own comment that says, "This is what I did. I went to desktop and created a new folder to store python code from there. I added the python file from Sublime Text 3 into that folder and I opened the document in pycharm. From there the code started working."

So, they _did_ give us a follow up which is great. You should definitely do that. If you solve a problem yourself, if you come back around and post the resolution, then someone else in the future may be searching for the same problem. They may stumble upon your post, and you might have a great answer there a great solution for them.

The problem here with this circle back is. We still don't really know what the problem is and they don't give us a lot of detail about the resolution either. So I don't think we can do much with that. If if we're the person that stumbles upon this post and says, "Oh my gosh, I have that same problem," I don't know that this resolution gives us really any direction to take that.

Let's move on to our next question. This one is on Stack Overflow. It is called "How to change color of button with JavaScript jQuery when input radio buttons is selected."

This one gives us some mark up at the top. It's basically three standard buttons and then three corresponding forms. Each with three radio buttons

and they go on to say this: "If input with id id1 is selected with form… _in_ form with ID formid1 then color of button with id id1 should change and so on and so forth — the same thing for forms 2, button 2 and form 3, button 3.

Now, let's evaluate this question against the recipe. OK, the first ingredient: what you want to happen. They actually have a pretty good description of what they want. I understand it, at least. I know that they've got three forms. Each one corresponds to one of their standard buttons. When you select an option in one of those forms, it should change the color of that corresponding button. Pretty… pretty straightforward and a good explanation of what they want.

Ingredient two: what's the context? This one gives us the context we need. In fact, this **shows the importance of sharing that context** because if I'm coming to answer this question, I'm going to try to give an answer best I can but I'm also going to mention a few other problems I see with this code that might help that person as they're trying to move forward with this project.

They have multiple elements with the same ID. That's not valid HTML, so they need to do something about that. The names they've chosen for things are not very descriptive of what those things are or should do: id1 id2 formid1… that's going to make this really hard to read later, so that… that is something they could fix.

And then the values are all the same. Maybe that was just copy pasting and and not going back and changing it. But that is something that probably needs to be fixed.

So it does give us the context we need. We've got all the HTML for all the elements. At the same time, it doesn't go overboard. If you're asking a question about a large application, you want to give **just enough of the context** but not show all 27 of your script files and and every HTML document you have in the entire project. You want to narrow it down to **just the stuff that applies to your question**.

Let's look at the third ingredient: what they tried. This is actually the weakest part of this question. It doesn't really show that they put forth **any effort at all**. They don't say anything about what they tried. If somebody is going to come to a forum or Stack Overflow or Reddit and help you for free, they want to first see that you've put forth some effort. They want to see what have _you_ tried.

If they had tried some solutions and shared them, it also would have created **a better opportunity for them** because we as the people coming to the page to answer the question, could have tried to help explain why those solutions didn't work and potentially fix some broken mental models that the question asker has. It's a great opportunity for learning and the poster of this question completely missed out on all of that.

Let's move on to the fourth ingredient: what actually happened. This one is completely missing because they didn't try anything. We don't know what happened. We don't know if they even tried so there this one doesn't even apply, unfortunately, in this case.

This question has a real "do my homework for me" kind of vibe and it probably won't get very good answers as a result. The people who are trying to help in the forums tend to get annoyed when these sorts of questions pop up when you're just showing what you have and just asking for a solution. Since there are more questions those people can answer, they're probably just going to skip over this one and move on to another one where someone has shown more effort.

Now we have a question on SitePoint. It is called "$ not defined issue."

And what we have here is some Javascript. And then the questioner asks, "This line is generating $ not defined issue. Where am I going wrong?" And then they reiterate the line that is actually causing the problem.

OK, let's look at the first ingredient: what they want to happen. This one doesn't really tell us what the code should do. It may or may not be relevant to the answer, but it's always good to at least give a quick sentence or two that tells what the code should do.

It may be that the reason you're having trouble solving it the way you _are_ is because there's a better, much simpler solution that you've just overlooked, and, if you can tell people what you're trying to do, they may be able to point that out for you and you might be able to just **go around your problem altogether** and and avoid it.

The second ingredient: what is the context? We have some context here. We've got some code, but I'm not sure that it's enough. And when you copy and paste code, that's one way to give context but **the best way is to build a working example** and you can do that on a site like CodePen or like JSFiddle. Those will let you build out an example that shows your problem. Other people can come in and look at that and see a working example of it without having to leave their browser or pull up a console and try to run your code which in this case wouldn't work anyway, because you're not going to have the same markup that they had that they're running this against. This JavaScript is interacting with the DOM, so you'd have to try to rebuild their HTML page and it would just be too much of a hassle. So, if you can create that minimal… that example for them, that just makes it easier.

I mentioned earlier that figuring out the context that's important is kind of tough when you're new, but one great way to help yourself and make it a little bit easier is to **try to create a minimal example.** So, go to CodePen or JSFiddle and build a site that's just the pieces that you need to show the problem. **This can sometimes lead you to a solution on your own** by eliminating some of the other pieces of your application that might be interfering or helping you better narrow down exactly what is causing the problem. It _definitely_ makes it easier for people to answer your question. So it's useful even if you _don't_ come to a solution in doing that.

The third ingredient in the Rad Recipe: what did you try? So, we see the code that caused the problem, but we don't really see any other attempts. I guess that's _one_ attempt but it's nice to show that you tried a couple things or at least you thought about what the solution might be. Showing something like this can get you a much better answer.

And the last ingredient: what is the actual result? They did a pretty good job of showing the result. They tell us the exception they got and the line that triggered it, so that helps us narrow… as a potential _answerer_ to this question, that helps us narrow what we need to look at.

This question partially succeeds with each ingredient but it's still a little bit lacking. The reason this question is difficult to answer is that, even though we have _some_ context, **we probably don't have exactly the context we need** to see what the problem is. That's something you'll get to if you build… _try_ to build a minimal example, you'll figure out what is the context that causes this to happen.

OK, for the fourth question, I'm going to cheat a little bit and use one of my own questions. I don't normally pat myself on the back, but I do think it's a pretty good question just because I tried to give everyone the tools they need to help me answer it. The title of the question is, "Image upload to Imgur API via JS Fetch API is cancelled by browser." If you don't understand what that means. Don't worry about it. It doesn't really matter. We're not going to discuss the technical details of this question.

I won't read _everything_ in this question, but it's basically a description of the problem here, something that shows the issue as it manifested in Chrome, and then some code that surrounds the problem area, and some solutions that I tried.

OK, let's try the first ingredient: that is, what I wanted to happen. And I sort of built two layers of that into this question. So, here in the first sentence, I tell **about the _application_** — the overall application. "I wrote a small application on CodePen to upload to a specific Imgur album using JavaScript Fetch API."

So, that's sort of an overarching, "what is this supposed to do?" and then I follow that up with, "the application is failing to upload and I'm not sure why since I'm actually getting… I'm _not_ actually getting a response from the server." So that is… **this _feature_ that's causing a problem, what is it supposed to do**?Well, it's supposed to upload something.

OK, the second ingredient: the context. I've actually given that in two different ways in this question. The first is **I've linked to the application on CodePen** so you can click through on that link and get a working copy of it. You can fork it, play with it… do whatever you want to do.

Then, if we scroll down, I have a little bit of code. So, **this is the code around the part that's causing the problem**. As someone who's going to try to answer this question, if you look at the code and you can see what the problem is right away, you never have to go away from here. You don't have to go anywhere else. But if you read that and that's not quite enough and you'd like to be able to play with this and see exactly what's happening, you have the CodePen link up at the top that you can click through so that you can play with it and try to discover a solution on your own.

The third ingredient: what have I tried? And that's basically what this paragraph is. "I'm not sure if I need to be reading the file or if I can pass the file object directly into the form data." So, this is just sort of going through some different things I've tried and the results of those.

And then the fourth ingredient: the actual result, and that's sort of encapsulated here in the first paragraph. "The application is failing to upload and I'm not sure why. The browser appears to be cancelling the request,"  and then also an image so that you can see. OK, well, here's what I _believe_ is happening based on what I've seen and then here is _actually_ what I'm seeing. And the reason I did it that way is it's just **a recognition that my interpretation might be wrong**. So, here's what I saw. Maybe you interpret it differently. It gives me another chance to learn from this question.

It has all the ingredients. Maybe that's cheating since I wrote the recipe. I'll let _you_ decide that, but I still think it's a good question and I think I have framed it in such a way that I can potentially learn more from it.

OK, that's the Rad Recipe for Code Questions along with a few examples of questions all across the spectrum, from good to bad. And I want to leave you with this: the next time you've reached your limit and you decide to take your question online, **try checking for these ingredients before you post: what you want to happen, what's the context, what you tried, what actually happened.**

Thanks for watching. Talk to you in the next video.

Thanks everyone for watching. I hope you found this video helpful. And if you want more stuff like this go to [RadDevon.com](/) where I teach people how to leave their 💩 jobs and become web developers.

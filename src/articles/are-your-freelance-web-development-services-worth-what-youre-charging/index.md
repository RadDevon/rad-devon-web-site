---
title: 'Are Your Freelance Web Development Services Worth What You’re Charging?'
date: '2019-09-20'
coverImage: 'images/sticker-shocked-monkey.jpg'
description: 'How much you need to make is important, but how much what youʼre building is worth is going to be really important to your clients. Hereʼs how to figure out if youʼre worth what youʼre charging, and what to do if youʼre not.'
---

![A monkey sticker-shocked after seeing a web developer's estimate](images/sticker-shocked-monkey.jpg)

If you’re getting started as a freelance web developer, you’ve probably already considered what rate you want to charge. Maybe you plugged some numbers into a calculator—your monthly expenses, the billable hours you plan to work in a week, the time off you want to take—and got a number you’re comfortable with.

You’re going to start talking to prospects and sharing your rate with them, and what you may learn is that **although your rate works for _you_, it doesn’t work for _them_**. Your prospect may not want to pay your rate either because **they don’t understand the value of what you’re selling** or, even worse, because **what you’re building for them actually isn’t worth what you’re charging**.

And so, before you get to that point, **let’s do a reality check**. We’ll start by answering a few questions. By doing this, you’ll be able to confidently present your rates and convince your prospects you’re worth it … or you’ll know your rates for this particular solution are too high and be able to adjust accordingly.

## What Are You Selling?

When you make an offer to someone, what is the actual product you’re selling? Is it “web development services”? A new website? Automation?

If you answered “yes” to any of these, **[you’re likely selling the wrong thing](/articles/as-a-freelance-web-developer-what-are-you-actually-selling/)**. The first step in doing a reality check on your rates is to figure out how much what you’re selling is worth.

If your offer is one of those I mentioned above, who really knows what that’s worth? How much more money will a business make after it buys some “web development services,” a new website, or some automation? I don’t know, and I bet you and your client don’t either.

In order to determine the value of your offering, you need to **frame it in terms of the result** rather than what you’re putting into it (e.g., the nebulous “web development services”) or the technical details of how the result will be delivered (e.g., a website or automation).

If your “web development services” are going toward building a booking system for a chain of barber shops, what you’re selling is **efficiency**, **reliability**, and the **enhanced reputation** that follows, as well as **better customer experience** and **more loyalty** resulting from that. The fact that you’re solving this with web software **is now incidental**. Instead of “web development,” you’re selling the _benefits_ of the solution. Those benefits are offerings you can much more easily put a price on because they result in **more dollars in or fewer dollars out**.

## What Is It Worth?

Now that you’re dealing with a product described in terms that are much closer to the result to your client, it’s time for you to bridge the remaining gap.

**What is this thing I’m selling actually worth?**

The easiest way to find this out is to **ask some of your ideal clients** what it would be worth for them. No one knows better than they do how their business works.

Failing that, **use your research skills** to try to connect the dots and figure out the value on your own. If you’re lucky, you might find a case study another developer put together on a similar app and make some assumptions based on the results they report. This is a poor substitute for doing your own research, though.

Here’s a quick example of how discovering the value works: I’m working on an app to automatically generate seating arrangements at weddings. This will save tons of time for my client who is a wedding planner. To understand the value of this software, I need to know **how long this process normally takes** and **how much their time is worth**.

Beyond that, I need to estimate how long using the app will take, since it won’t reduce their time commitment to zero but will dramatically reduce it. With these three data points, **I can put a solid number on the value of my seating arrangement software**.

## Are You Charging Less Than the Value?

If you’re not, this is a big problem. Businesses don’t want solutions that allow them to break even or that lose them money, so the cost to them **must be less than the value**.

Even if you could fool your prospect into paying more than the value for a piece of software, it’s a bad move in the long run. Once they realize they paid more for the project than it made or saved for them, **they won’t call you back**. They might even tell other people that they worked with you and that it didn’t go well. You risk hurting your reputation.

Even though needing to charge more than the value of your solution is a problem, it *doesn’t* mean you need to start sending out résumés. You have a few options.

**Are there parts of the project you can eliminate?** Start with the part of the project that generates the value and work backward. Maybe you’re spending a ton of time implementing intricate designs that aren’t necessary (even though we all love to create beautiful software).

Maybe the app you’re building has three key features which each take ⅓ of the time to build, but one in particular is the source of 80% of the value. Trimming your solution can help you **bring the value and cost back into balance**.

**Are you working for the right client?** Let’s revisit the conversion rate example I discussed earlier. Improving a conversion rate by 10% conveys a completely different value for the hoagie shop down the street than it does for Amazon. Those are the extremes, but the principle holds true. **Your solution may be just fine; maybe you’re not targeting the right ideal client.**

Some clients, though, are just not a great fit if you want to earn a living as a freelance developer regardless of what you’re building.

For every musician who makes money playing music, there are 10,000 who don’t. For every YouTube elite, there are 100,000 channels out there struggling to get a few hundred views. For every pro Fortnite player, there are a million kids just looking for a way to put off their homework a little bit longer. If you try to specialize in these spaces, you’ll have to serve the 0.1% because **if your client doesn’t make any money from the project, *you* can’t either**.

**Are you building the right project?** The counterpoint to the advice to find the right client is this: You may already be targeting a perfectly suitable client but selling them **the wrong solution**. If your solution were more effective, it would create better results. Better results means more money for them *and* more money for you. Getting those better results may be easier said than done.

Maybe you’re solving the wrong problem. If you’re trying to build software to solve problems that are easy to work around or not very expensive, **the software won’t be worth much**. That’s where the value comes from, after all: the money your solution makes or saves.

## Charge With Confidence

At this point, you’ve either confirmed that your rate does, in fact, make sense in the real world, or you have some strategies to bring the stars back into alignment. If you're ready to learn more, check out a free chapter of [my book on pricing your freelance web development services](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/).

<!-- 👇

![Bradley Serlis headshot](images/bradley-300x300.jpg)

## “How should I set my freelance web development Rate?”

— Bradley Serlis… and many others

![Freelance Web Development Pricing ebook on a tablet](images/freelance-web-development-pricing-ipad.png) [Learn Everything About Pricing](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/order/) -->

_This post originally appeared on [Simple Programmer](https://simpleprogrammer.com/freelance-developer-pricing/)_

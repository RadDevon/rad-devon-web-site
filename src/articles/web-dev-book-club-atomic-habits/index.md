---
title: 'Web Dev Book Club: Atomic Habits'
date: '2020-04-17'
tags:
  - 'books'
  - 'career'
  - 'habits'
  - 'self-improvement'
coverImage: 'images/atomic-habits-thumb.jpg'
description: 'You donʼt change careers by doing one amazing thing a single time. It happens by doing many small things over and over. This book will help you do that.'
---

https://youtu.be/GIDE3PXDHzc

## Atomic Habits

You don't change careers by doing one amazing thing a single time (like running into Mark Zuckerberg in an elevator and convincing him to hire you). It happens by doing many small things over and over (like writing some code every day of the week). [_Atomic Habits_](https://bookshop.org/a/2969/9780735211292)  ⚛️ is the story of how to keep the small things going (and how to stop the bad ones).

The Guardian has a great article about [the Polgar sisters](https://www.theguardian.com/sport/2012/nov/12/judit-polgar-everything-chess) and their mastery of chess. ♟

Buy your own copy of _[Atomic Habits](https://bookshop.org/a/2969/9780735211292)_.

<!-- Getting your habits straight is key, but it was also important for me to understand why I was doing what I was doing. That tied everything together and kept me going when my brain was telling me, "Stop! This is too hard!" This course will help you find your "why." 👇

[thrive_leads id='1360'] -->

## Video Transcript

These things are really good. Salt is really addictive. I mean, it's super addictive. I generally don't have a problem with overeating, but if you give me open-access to a salty snack, there is no serving size that's going to slow me down. I discovered these awesome plantain chips. Got to the point where I was eating about a bag of them every single day, until I read _Atomic Habits_. Stick around and I'll tell you what happened next.

I put off reading _Atomic Habits_ for a long time because it was a bestselling book. I think most people would probably do the opposite, but I have this tendency to root for the underdog and not want to jump on the bandwagon. That's probably why I ended up with a [Microsoft Zune](https://en.wikipedia.org/wiki/Zune) instead of an iPod, but it was actually a really good MP3 player. That tendency of mine doesn't always work out quite as well as it did with the Zune.

And this book is one case where I wish I had just followed the crowd _Atomic Habits_ by James Clear is a bestseller for a reason. Using what I've learned from this book, I've actually broken old habits and made new ones. For the 15 or 20 dollars I paid for the book, that's pretty high praise to say that it can actually improve your life.

But why would you even care about habits as a web developer? A lot of times when we think about habits, we're thinking about things like quitting smoking or flossing every night. If those are the only kinds of things that come to mind, you're putting habits in too small a box. They're all the things you do on a regular basis. Your habits are the tiny things you do over and over that compound over time to make you into the person you're becoming.

Let's put this in some really concrete terms. If you want to become a web developer, you're going to need to write code regularly. That means you're going to need to be in a habit of writing code regularly.

If you want to be successful as a freelancer, you're going to want to be in a habit of forming and cultivating relationships, even when you already have enough work. If you want to succeed at basically any kind of work, you need to be out of the habit of watching YouTube videos while you work. That's actually one I really need to work on.

Take the person you are right now, layer your habits on top. Those two factors, plus the passage of time, will make you into the person you're going to be tomorrow, next month, and next year. Habits are the secret, almost invisible, difference makers.

You feel like you got lucky because you showed up at a meetup and the CTO who eventually hired you was there. When, really, it was your habit of going to two to three meetups every single month for nine months leading up to that day. It may seem like you wrote one line of code and then your client paid you $50,000. Magically. What really happened is your habit of writing code every day toward this same project meant that one day you were able to write that last line of code and get to your payday.

The advice in this book centers around the author's four laws of behavior change: make it obvious; make it attractive; make it easy; and make it satisfying. The book really boils down to this, if you want to make a habit, do those four things. If you want to break a habit, do the opposite of those four things.

Here's how I used this to fix my chip problem. First, I had to recognize there was a problem. _Atomic Habits_ helps you do this by having you create a habit scorecard, which is a list of all the habits you have and a score, indicting whether you want to keep the habit or you want to get rid of it.

I made my own habit scorecard as I went through the book and one of the items I noticed on my list was snack while making lunch. When I was trying to decide if this was a good or a bad habit, I actually realized that there was a little more to it than that. I was definitely snacking while I was making lunch. But then after I was finished with lunch, I was taking the entire bag of chips back to my workstation with me and munching on it for the rest of the day until the bag was empty. This wasn't really a habit I wanted to maintain because it was sort of just automatic. I wasn't making a clear decision about how much I wanted to eat. I was just eating until the bag was empty. This is where I started applying the four laws.

I kind of look at this as a two sided habit. I've got, on one hand, the bad habit I want to break, which is eating the entire bag of chips. And I've got the good habit on the other hand, that I'd like to form, which is controlling my portions of chips. By taking the whole bag of chips back to the workstation with me, I was making it too easy for me to overeat. What I really needed to do is take a single serving back. That way. If I wanted to eat more than a serving, one, I had to go off all the way into the kitchen to get more. And two, I had to face the decision about whether I actually wanted another serving. When the bag was just there, there was no decision. I just reached my hand in and grabbed more and ate them.

The first time I measured out the serving size, I realized something kind of useful. We have these ramekins that are basically about the right size for a serving of chips. This discovery makes it obvious how much I need and it makes it easy to measure out that amount. Instead of counting the chips out to determine if I've got a serving or not. I can just eyeball this ramekin, fill it up about how full I know it should be for a serving ,and I'm done. A normal person probably would just take advantage of that, but I still kind of like to count out the chips anyway.

Being precise when I can give me some satisfaction. That's just a personality quirk. And yes, I realize that a chip is not a standard unit of measurement. Even though it was satisfying for me to keep counting out my chips, to measure a serving. I didn't have a good way to make this overall habit immediately satisfying. It's very satisfying when we go to the grocery store now and only need to buy half as many chips, but I don't think that satisfaction payoff really has the immediacy it would need to have a meaningful impact on my habit.

The nice thing is, even though we have four laws of behavior change, we don't have to pull on all of those levers every time we want to make or change a habit, the more we can pull on the better, but even just using one of them can be enough to change a habit.

_Atomic Habits_ has this quality I've noticed in a lot of good self-improvement books. After you're done reading it, you're left asking yourself, why did I need to pay somebody to tell me this? It all seems so obvious. The truth is, that question only gets asked after you've read the book, and it gets asked because the book does such a great job of making everything seem obvious and making the right connections so that this will stick. In a weird way, reading it almost feels like deja vu.

The book accomplishes this by pairing these great stories of practical applications of each law alongside the descriptions of the laws. One of those great stories is in the second section, which is on the make it attractive law.

It's the story of a Hungarian husband and wife, who before they even had children decided they wanted to raise their children to play chess.

They ended up having three girls and the girls were brought up in a culture of chess. They were encouraged to play chess. Their parents played chess. In their household, chess was cool, and it was important.

Two of the three ended up becoming chess champions. One of those became the youngest Grandmaster ever and was the top ranked female chess player for 27 years.

Here's just a little piece of that story from the book. "The childhood of the Polgar sisters was atypical to say the least. And yet, if you ask them about it, they claim their lifestyle was attractive, even enjoyable. In interviews, the sisters talk about their childhood as entertaining rather than grueling. They loved playing chess. They couldn't get enough of it. Once, Lazlo, the father, reportedly found Sofia," she's the middle daughter, "playing chess in the bathroom in the middle of the night. Encouraging her to go back to sleep. He said, 'Sofia, leave the pieces alone,' to which she replied, 'Daddy, they won't leave me alone.'"

"The Polgar sisters grew up in a culture that prioritized chess above all else. Praised them for it, rewarded them for it. In their world, an obsession with chess was normal. And as we are about to see whatever habits are normal in your culture are among the most attractive behaviors you'll find."

This is one reason I recommend finding developer meetups and developer communities online. If you can immerse yourself in a culture that values the thing you're wanting to achieve, the habits you need to build to get there will become much easier to build.

There is so much to love about this book. It's organized really well around those four laws I mentioned earlier. Each chapter ends with a summary of key points that you should have taken away from it. And I found that really helpful. The stories make the concepts so obvious and intuitive and are going to make this material stick with me in a way that other habit books just haven't. Forming the right habits and sticking to them will make it possible for you to build a career as a web developer. _Atomic Habits_ is a masterclass in how habits work and how to make them work for you.

Post in the comments any success stories you've had with your own habits. I'm always looking for new ways to improve, and I'm sure some of you have tackled habits that I probably need to be paying attention to and maybe that I need to adopt for myself. Thanks for watching and I'll talk to you in the next one.

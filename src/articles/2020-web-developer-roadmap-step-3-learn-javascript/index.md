---
title: 'Web Developer Roadmap Step 3: Learn Javascript'
date: '2020-01-24'
tags:
  - 'javascript'
coverImage: 'images/thumbnail-2.jpg'
---

In this video, I'll share my favorite resources for learning Javascript in 2020. I've also created [a free reference](#javascript-by-example) that gives you glanceable code examples of all the basic Javascript concepts like functions, loops, and scope. Keep it handy as you're learning to jog your memory and make the process much faster. Now, onto the video!

https://www.youtube.com/watch?v=BKxna-Yr9Vo

## Video Notes

Good news! You have access to an awesome array of resources for learning Javascript. Here are the ones I would go with if I were starting in 2020.

If you want to learn how to use a Big Goal to propel you forward into your new career, watch [the first video in the roadmap](/articles/2020-web-developer-roadmap-step-1-set-your-big-goal/). After that, you should learn HTML and CSS which I covered in [the previous roadmap video](/articles/2020-web-developer-roadmap-step-2-learn-html-and-css/).

The two courses I recommend if you like learning from books and written tutorials:

- ⭐️ [The Modern Javascript Tutorial](https://javascript.info/)
- ⭐️ [Eloquent Javascript](https://eloquentjavascript.net/)

If you're more into video:

- ⭐️ [Beginner Javascript](https://beginnerjavascript.com/) (by Wes Bos)

Other links mentioned in the video (mostly in order of mention):

- [React for Beginners](https://reactforbeginners.com/)
- [Javascript 30](https://javascript30.com/)
- [10 Web Development Project Ideas](https://raddevon.com/articles/10-great-web-development-learning-project-ideas/)
- [MDN's Javascript documentation](https://developer.mozilla.org/en-US/docs/Web/javascript)
- [Learn Node](https://learnnode.com/)
- [Free Node course with the terrible name](https://github.com/workshopper/learnyounode)

Thanks for watching! Feel free to ask questions or leave your thoughts in the comments [on YouTube](https://youtu.be/BKxna-Yr9Vo) or to me directly on Twitter. [Subscribe on YouTube](http://www.youtube.com/channel/UCxgZYWJq2Cxk7vIePGiTxFw) so you don't miss the rest of the roadmap!

## Other Videos In this Series

If you like this video, watch the others in the series for a good overview of the philosophy I used to break into web development with no connections or experience.

- [Step 1: Set Your Big Goal](/articles/2020-web-developer-roadmap-step-1-set-your-big-goal)
- [Step 2: Learn HTML and CSS](/articles/2020-web-developer-roadmap-step-2-learn-html-and-css)
- [Step 4: Learn Terminal, Git, and Deployment](/articles/2020-web-developer-roadmap-step-4-learn-terminal-git-and-deployment)
- [Step 5: Relationships Are Make or Break](/articles/2020-web-developer-roadmap-step-5-getting-work-through-relationships)
- [Step 6: Time to Get Paid!](/articles/2020-web-developer-roadmap-step-6-time-to-get-paid)

## Transcript

Hey, this is Devon. You're watching step three of the web developer roadmap. In this step, we're going to talk about learning Javascript.

In the last video, I gave you some resources and advice on learning HTML and CSS. If you're just getting started down this path and you're watching this video as it is released, I want to let you know that it is _not_ my expectation that you would have already learned HTML and CSS in the time since that last video came out. So don't sweat that. Just keep going where you are. Come back to this video later when you're ready to move on from HTML and CSS. Once you can confidently mark up a page and write a good CSS selector, it's probably a good time to at least start learning Javascript.

My two favorite free resources are both text-based in this case. One of them is The Modern Javascript Tutorial, and the other is a book: Eloquent Javascript. Of those two, I would recommend most people start out with The Modern Javascript Tutorial. Eloquent Javascript is a little bit more comprehensive, and the Javascript tutorial holds your hand a little bit more.

Both of these resources have sample code. The Modern Javascript Tutorial has code that you can actually run on the page. Eloquent Javascript does not, but it has a few different projects that you can work on throughout the book so that's a pretty cool way to learn.

If video is more your speed, I would recommend you check out Wes Bos's Beginner Javascript course. It is not a free resource, and this is not an ad. I've been a big fan of Wes's video courses for several years now. When he released his first course on Sublime Text, I picked that up and learned how to use Sublime Text when that was the hottest editor, and since then I've also used his React for Beginners course to get a basic grasp on React.

His courses are just really fun to watch. They're easy to listen to and work through, and the projects he gives you to work on are nice looking and a lot of fun to do. I always like to point out free resources because I know those are going to be accessible to everyone, but I firmly believe that if you're wanting to make a change, it's okay to invest in yourself and pay some money for a resource that may work better for you.

Whichever route you decide to take to learn Javascript, just like I advised you in the HTML and CSS video, you're going to eventually need to break out of the walled gardens these courses will have you in and start building your own projects to apply your learning and lock it in.

Wes Bos has another course you might look at. It's a free course called Javascript 30, and it's a series of 30 projects you can build in Javascript. This is a really cool way to get started and it can sort of introduce you to the idea of applying Javascript. The only problem is that it too is a walled garden, so eventually you're gonna want to break out of that as well and move into your own projects.

You might revisit a project you worked on when you were learning HTML and CSS. Try to think about how you can layer in some interactivity. Once you've exhausted those opportunities, you may want to move on to a brand new project where you can practice your Javascript.

It's going to work best if you come up with your own project because that will make you excited about it and you're more likely to see it through, but if you're having trouble with inspiration, I will link you to my own list of 10 web development projects that can give you a little push. If you want to just grab one of those and run with it, that's fine. If you want to tweak it, that's fine. If you just use it as inspiration and come up with something new, that's also great.

Once you understand the fundamentals, you're going to want to have easy access to some sort of reference so that you can look up parts of the language that you don't remember. And again, like with HTML and CSS, MDN is your best bet. It has a great Javascript reference. I use it almost every day to look up various array methods or other parts of the language that I just don't want to keep in my brain.

These resources I've shared with you so far are covering primarily frontend Javascript – that means Javascript that runs inside your user's web browser. This is going to give you a ton of capabilities that you didn't have with just HTML and CSS, but if you only learn frontend Javascript, you're still eventually going to hit a wall.

Just to give you an example of what that might be: you can't use HTML, CSS, and frontend Javascript alone to build an application that allows users to share data with each other. For something like that, you're going to need to build a backend for your website or web application. Ten years ago, writing a backend would have meant you needed to go out and learn another language and write your backend in that – maybe PHP or maybe Pearl or Python or Ruby or something like that.

You are lucky for not taking this on 10 years ago because now you get to write your backend code in Javascript too if you want to. This is made possible by a technology called NodeJS, which essentially allows you to run Javascript outside of the web browser. This lets you do some really cool stuff like talking to a database or reading and writing files. You can build authentication flows for your users so that people can come to your website and log in. Basically almost anything you can imagine you'll be able to do once you have this knowledge of backend development.

If you liked Wes Bos's frontend Javascript course, he also has a NodeJS course that you should probably look at. If you're looking for a free resource instead, I found this course called… let me see if I can get this right. Learn You NodeJS for Great Win. Is that right? Oh, close. It's Learn You The NodeJS for Much Win. Yeah. I'm not in love with the name, but it's a pretty cool interactive way to learn Node.

Once you've completed whatever Node course you decide to look at, loop back around and do your independent project, you can either augment an application you've already been working on to add some functionality you couldn't add before with only frontend code or you may want to come up with a completely new project that utilizes your new NodeJS skills.

Whatever you do, keep it small. This is not the time to go build your Facebook killer. This is the time to build a small project, get a quick win, and apply those skills you've learned.

Next up in the roadmap, we're going to talk about how to get actual paying work doing this stuff you've just learned. Here's a secret that not a lot of people know. You don't need to be a master or an expert, whatever those even mean to go out and start getting work doing this stuff. You just need to know enough to solve a problem for the company that's paying you. Subscribe to be sure you don't miss that next step, and I will see you next time.

## Next Steps

Here's what you should do next:

1. Start using the resources mentioned in the video to learn the basics of Javascript.
2. Download [your copy of Javascript by Example](#javascript-by-example), my free reference with code examples of basic Javascript concepts.
3. Start a project to put what you're learning into practice, or incorporate what you're learning into a project you're already working on.

---
title: 'Validate Forms with Event-Driven Javascript'
date: '2020-12-04'
tags:
  - 'forms'
  - 'javascript'
coverImage: 'images/event-driven-js-thumb.jpg'
---

Validating form input on the front-end is a great way to make your users' experience better. When _I'm_ the user, I'd rather know immediately that something is wrong with my input than having to wait until I submit the form and get the results back.

In my previous video, I showed [how you can access a form's values](/articles/working-with-html-form-fields-in-javascript/) which is one piece of the front-end form validation puzzle. This video completes the puzzle.

If you want to work along, get the project starter using the form below. 👇

[thrive_leads id='4193']

https://youtu.be/hv20XIUFXiM

Before I teach you to write Javascript form validation, I'll teach you ways to validate your form _without_ writing custom JS. HTML5 forms have some validation capability baked right in, and using this is preferable to custom validations.

Later, we move on to a scenario where built-in validation either wouldn't work or would be more difficult than the custom variety. We'll start by hooking into an event. This allows us to start running some Javascript when the user interacts with our form. Then, we'll check the user's values to see if they are valid. If they aren't, we'll stop the form from submitting.

---
title: 'Introducing The Thinkful Flaskcast!'
date: '2013-12-27'
coverImage: 'images/studio-microphone.jpg'
---

I started [Thinkful](http://www.thinkful.com)'s online Python bootcamp a while back in an effort to beef up my back-end development skills. As the course was wrapping up, I realized I wasn't going to be able to get my final project to where I wanted it to be before the end of the course. As I thought about how to fix this problem, I also heard they were looking for ways to tell prospective students what their live mentoring sessions are like. I pitched the idea that I could continue in the course and record my sessions so that they could show exactly how mentoring works.

From this, the Flaskcast was born. The app I'm building is a control interface for my local maker space, the [Knox Makers](http://www.knoxmakers.org). This will control a remote monitoring system they're setting up in their hackerspace. I'm embedding the first three episodes of the Flaskcast here for your enjoyment. Many more episodes are still to come.

[thrive_leads id='1354']

For more information about the contents of the videos, click through to their Vimeo pages where you'll find clickable timestamps with the various discussion topics.

_The Flaskcast is no longer available to watch on Vimeo._

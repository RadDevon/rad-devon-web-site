---
title: 'First Steps with Node.js Part 1'
date: '2020-03-28'
tags:
  - 'backend'
  - 'javascript'
  - 'node'
  - 'node-js'
  - 'nodejs'
  - 'tutorials'
coverImage: 'images/learnyounode-1-thumb.jpg'
description: 'Node lets you write Javascript code that runs outside the browser. This allows you to do wild stuff like automate tasks or even run web servers! In this video, Iʼll teach you how to start working with Node.'
---

https://youtu.be/hRXFobkJN1M

## Video Notes

[Node.js](https://nodejs.org) allows you to run Javascript code outside your web browser. That makes it possible for you to take what you already know about Javascript and apply it to backend development as well as frontend.

📝 We'll be working through the [Learn You Node workshop](https://github.com/workshopper/learnyounode) together.

📚 As you work through the exercises, you'll want to reference [the Node.js documentation](https://nodejs.org/dist/latest-v12.x/docs/api/).

👋 Thank you for watching and for working through the exercises with me! Look out for upcoming videos in which we'll work through the remaining exercises together. If you have questions, please comment on the video [over on YouTube](https://youtu.be/hRXFobkJN1M) or mentioning me [on Twitter](https://twitter.com/raddevon).

As fun as it is to work through these exercises and learn Node together, don't forget that **code isn't everything when you want to become a web developer**. **If you don't know _why_ you're here**, you're not going to have the staying power to keep going and complete your career transition.

I have a short email course that will help you find your Big Goal, giving you the most important tool you need to make sure you can finish the job… **by _getting_ the job**. 👇

[thrive_leads id='1396']

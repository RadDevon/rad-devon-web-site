---
title: '2019 Gift Ideas for New and Aspiring Web Developers'
date: '2018-12-07'
tags:
  - 'freelancing'
  - 'gifts'
  - 'hardware'
  - 'software'
coverImage: 'images/confetti.jpg'
---

If you know a developer with a birthday coming up or even if you just want to treat yourself to something nice, I've got a great list of gifts that any web developer would love.

![Confetti](images/confetti.jpg)

## Books

Most aspiring developers want to learn nothing but how to code. It's exciting. It's gratifying. It's in the zeitgeist right now. (Everyone is talking about how everyone should learn to code.)

**You need to know more than just code to be a great developer.** I'll recommend some books about software development, but I'm going to start with books that help with the skills and knowledge most developers lack.

[![The Personal MBA book cover](images/personal-mba.jpeg)](https://amzn.to/2PqSNXG)

### [The Personal MBA](https://amzn.to/2PqSNXG)

You may wonder what business has to do with web development. Truth is, **no one is going to pay you do build web sites if there isn't a business reason behind it**. Understanding how business works **is key to being a good web developer**. Instead of just building whatever people tell you to build, you can dig into the reasoning behind the project and decide if this project is the best solution to the problem being solved.

**[The Personal MBA](https://amzn.to/2rv2oGP)** is a readable way to get up to speed quickly. It will introduce you to the business vernacular, help you understand what drives a business, and show you how the pieces fit together.

[![](images/lean-startup.jpeg)](https://amzn.to/2QxK3Ub)

### [The Lean Startup](https://amzn.to/2QxK3Ub)

If you want to work with startups, understanding the lean methodology is critical. Even if you _don't_ want to work with startups, you can look really innovative by sharing bits of it with your employer/client through casual conversations and formal proposals. Once you save someone thousands of dollars by convincing them not to build the product they _think_ their customers want, you'll become indispensable to them.

**[The Lean Startup](https://amzn.to/2YI86Bz)** teaches you how to build a product incrementally as you test it on your customers. Help your clients apply these lessons and everybody wins!

[![How to Win Friends and Influence People book cover](images/how-to-win-friends.jpeg)](https://amzn.to/2KYqLSD)

### [How to Win Friends and Influence People](https://amzn.to/2KYqLSD)

I seriously felt like I needed a shower after my first time reading through this book. It felt like it should be titled, "How to Manipulate People to Get What You Want." It has really stuck with me, though, and I've come around on it. I still feel like parts of it are gross, but **I don't believe it's a bad thing to be deliberate about trying to please people in your interactions with them**.

After reading [How to Win Friends and Influence People](https://amzn.to/2P4d880), you will be more aware of the way you are perceived by people.

[![Clean Code book cover](images/clean-code.jpeg)](https://amzn.to/2rmpBHu)

### [Clean Code: A Handbook of Agile Software Craftsmanship](https://amzn.to/2rmpBHu)

Some parts of programming are relevant no matter what languages a developer uses or what kind of software they write. Web developers of all stripes could stand to think more about properly naming variables, writing code that's readable instead of unnecessarily terse, and refactoring code that's going to be hard to maintain before it gets out of hand.

**[Clean Code](https://amzn.to/36hi3Z4)** reminds you that we are actually writing code not for machines but for other humans to read.

[![](images/little-schemer.jpeg)](https://amzn.to/2Ui2J9j)

### [The Little Schemer](https://amzn.to/2Ui2J9j)

I had no clue how to write recursive code or make sense of recursion until I read this book. It's a gentle introduction to the concept by way of teaching a language called Scheme.

**[The Little Schemer](https://amzn.to/2P9hfzX)** transcends the language it focuses on (which I still have never seen in the wild) and, as a result, remains my favorite way to learn to write recursive functions.

[![](images/kindle-paperwhite.jpg)](https://amzn.to/2QCutX7)

### [Kindle Paperwhite](https://amzn.to/2QCutX7)

Web developers are constantly learning, but who has room for all those books? Ebooks are the way to go. As much as people pine for the smell and feel of a paper book, the conveniences are undeniable. As many books as you want in a tiny device. Books are searchable, and you can highlight important text for easy reference later. You lose the ability to quickly leaf through the book, but you gain a lot more.

The **[Kindle Paperwhite](https://amzn.to/2QCutX7)** has a built-in light so it can be read any time. It's a touch screen so you can highlight by touching the text. The battery lasts darn near forever. It's really a simple device, but it's a joy to use and a great gift for a new web developer.

## Hardware

One of the hardest things to learn as a new web developer — particularly if you're coming from a mindset of scarcity — is that you need to spend money on the tools of your craft. A carpenter could save money by using a rock to drive a nail instead of buying a hammer. Problem is, they'll waste time, do inferior work, and be less satisfied with their job.

Web development is no different. We have tools we use every day that could be better, but we're inclined to settle because we're coming from low-paying careers where we're used to making do. Make sure the web developers in your life have **the tools they need to do great work**.

[![Das Keyboard Professional 4](images/das-keyboard-4-professional.png)](https://amzn.to/2PlhW5W)

Das Keyboard 4 Professional

[![](images/kinesis-advantage2.jpeg)](https://amzn.to/2rr2ICV)

Kinesis Advantage2

### [Das Keyboard 4 Professional](https://amzn.to/2PlhW5W)

The keyboard is the piece of gear a web developer is hardest on. We pound away on the keys for hours every day. A web developer's keyboard should be comfortable, convenient, and built to last.

The **Das Keyboard 4 Professional** ([Windows](https://amzn.to/2sb4aNr)/[Mac](https://amzn.to/36l4Qyy))has been my tool of choice for 5 years now. It's beautiful, built like a tank, and it has some cool convenience features like an integrated USB 3 hub, media controls, and a volume knob. I love the feel of the clicky mechanical key switches, and that's the one reason that keeps me from switching to a different keyboard. It makes typing faster by registering a small bump when you actuate a key.

_Update: I have trouble recommending the Das Keyboard since mine failed last year, just after the warranty expired. I'd expect a keyboard like this to last damn near forever. Honestly, I would expect that USB3 would become obsolete and I'd have to get rid of this thing because I have no way to connect it to the computer rather than because the thing just randomly stopped working._

_Lucky for you, the Advantage2 is still a solid pick, and I have a new pick to replace the Das in [my 2019 web developer gift guide](/articles/2019-rad-web-developer-gift-guide/)._

If not for my addiction to clicky key switches, I would switch to a **[Kinesis Advantage2](https://amzn.to/2ruRgd2)**. The one place the Das Keyboard falls down is in ergonomics. That's exactly what Kinesis is known for. I owned one of these keyboards for about a week, and the difference was immediately noticeable. I could feel my hands and wrists were less strained. If you don't care about clicky switches and have the budget, this is the way to go.

Whichever keyboard you decide on, make sure you get the correct version for your OS so that the keys are properly labeled.

[![](images/logitech-mx-vertical.jpeg)](https://amzn.to/2PoEu5Q)

### [Logitech MX Vertical](https://amzn.to/2PoEu5Q)

A mouse is easy to take for granted, and it doesn't seem consciousness around ergonomics has matured as much in the world of mice as it has in keyboards. In keeping with the ergonomics trend, let's look at a mouse that will help you minimize the risk of repetitive stress injury.

The **[Logitech MX Vertical](https://amzn.to/2Psyb3i)** is the only item on my list I haven't used, but I just put in on my wish list. I've been slumming it for years with a very basic mouse, but I can just twist my wrist to the angle of this mouse and immediately feel relief.

[![Dell S28 28" 4K monitor](images/dell-s28.png)](https://amzn.to/2rqYPh8)

### [28" Dell 4K monitor (S28)](https://amzn.to/2rqYPh8)

Screen real estate allows a web developer to work more efficiently. I can see the code I'm writing and the results it's producing at the same time without having to switch back and forth between applications. With an additional monitor (or two) I can just turn my head.

I have a couple of 27" Dell 4K monitors which are no longer in production, but this **[28" Dell 4K monitor](https://amzn.to/2LDSF7M)** is a nice stand-in. The size and resolution together gives a web developer tons of room to run whatever applications they need and to see them all simultaneously.

[![](images/rain-mstand.jpeg)](https://amzn.to/2EkuBVR)

### [Rain Design mStand Laptop Stand](https://amzn.to/2EkuBVR)

Back to ergonomics land with a laptop stand. For developers who use a laptop primarily on a desk as I do, a laptop stand is a great simple way to improve life by putting the laptop's monitor at eye level. Reduces neck strain and makes it more pleasant to put in a solid day's work.

The [**Rain Design mStand Laptop Stand**](https://amzn.to/38pQWg8) is a great laptop stand although really almost any of them should work. I recommend this one because it's the one I use, and it's been great. All a laptop stand needs to do is be stable while holding your laptop at eye level. Pick the one that you like the most.

[![Sony WH1000XM3​ headphones](images/sony-wh1000xm3​.jpg)](https://amzn.to/2Qlvu6H)

### [Sony WH1000XM3](https://amzn.to/2Qlvu6H)

The hardest thing about working from home is staying in "work mode" with tons of things happening all around to lure you into distraction. Headphones are a great way to disconnect from your surroundings so that you can focus on getting things done.

Check out the [**Sony WH1000XM3**](https://amzn.to/2DYJZVA). This is another case where the model I have is older and it makes sense to recommend a newer one instead. The key feature with these is the noise canceling. That's really what allows you to tune out those distractions and listen to some white noise, sounds of a coffee shop, or even instrumental music to keep you focused. (I wrote in detail about [my focus system](https://raddevon.com/articles/focus-for-better-work-life-balance-as-a-web-developer/) if you're interested.)

## Software/Services

Oddly, people who write code sometimes have the most trouble paying for it, even if it would make their jobs and lives a lot easier. Even worse, some developer fool themselves into thinking they should just write their own solutions instead of buying excellent software others have written.

Please don't allow any beloved developers in your life to go down this road to ruin. **Show them how great a premium piece of software can be** by gifting them a useful piece of software or a few months of a service. They'll thank you for it.

[![1Password logo](images/1password.png)](https://1password.com/giftcards/)

### [1Password](https://1password.com/giftcards/)

Web developers have to manage passwords. Boy, do we have to manage passwords. You think it's bad having to keep track of your Apple password, your bank, and Netflix? I have over 3,000 passwords for various services that I and my clients use. If I had them all written down, I would need an extra room in my apartment.

The fix is a password locker. It's a piece of software that keeps track of all your passwords and makes it easy to get to them when you need them. I know there are several options, but I've never explored any of them besides **[1Password](https://1password.com/giftcards/)** because it works so well. While you're at it, [grab a subscription for yourself](https://1password.com/sign-up/). It makes your digital life so much easier.

[![](images/backblaze-1024x406.png)](https://www.backblaze.com/partner/af9rzg?redirect=gift.htm)

### [Backblaze](https://www.backblaze.com/partner/af9rzg?redirect=gift.htm)

Have you ever hear that you should keep a backup of all your files? This is especially important if those files are your livelihood. Back in the day, we would have external hard drives containing a copy of all the files. The problem was keeping it up to date. No one did, so, if you lost your files for some reason, you'd have a snapshot from 6 months ago to recover from.

Today, this is an easy problem to fix. [Backblaze](https://www.backblaze.com/partner/af9rzg?redirect=gift.htm) runs in the background on my computer and keeps a current backup of all my files in the cloud. This is way better than the manual backup for two reasons. First, it's always current. Second, it's off-site, so if there's a fire, I'll still have my files and won't have to let down my clients. You can download your backup, or, if you're in a hurry, they'll send you a hard drive with your entire backup on it.

This is another one [you might want to grab for yourself too](https://www.backblaze.com/cloud-backup.html#af9rzg). It's very cheap for the peace of mind you get.

---
title: 'The Inverted Career Path for Developers'
date: '2018-08-23'
tags:
  - 'career'
  - 'freelancing'
  - 'start here'
coverImage: 'images/difficult-path.jpg'
description: 'I didnʼt start my web development career by going to a prestigious college or even by graduating bootcamp and shotgunning job applications. Those paths donʼt work for me. If that sounds like you, check out what I did instead.'
startHereDescription: 'Now that you know what youʼre in for starting a career in web developer, hereʼs how you can avoid that horrible fate. My inverted career path approach can help you get around all the soul crushing rejection of applying for hundreds of jobs without getting one. You just need to flip the power dynamic.'
startHereOrder: 2
---

![A difficult path through the woods](images/difficult-path.jpg)

Sometimes, the path that will lead you to success doesn't look very inviting. Other people don't choose that path, and they'll look at you crazy when you tell them you're trying it.

## 0 to $100K💰 in 1 Step

To be a web developer, you…

1. take out loans and go to a university
2. get an internship for some real-world experience.
3. **search and search** for a junior role until someone agrees to hire you.
4. work as a junior for a few years until you're able to start climbing the ladder, **one rung at a time**.

Unless you don't. 😉

My first web development job was a senior role that paid **$100k a year**. You might say this is low for a senior position. That is true… *if* you're in a coastal city with a high cost of living. I was not. If you're in Knoxville, Tennessee where [the average developer salary is $70,000 a year](https://www.indeed.com/salaries/Web-Developer-Salaries,-Knoxville-TN), it's significant.

So, how do you go from 0 to $100k on your first job with no professional experience? **You don't**. What you *can* do is flip the entire process of starting your web development career on its head. Get your professional experience ***before* you get the job**. You might like this method so much, **you may find you don't even need a traditional "job" at all**.

## The Inverted 🙃 Career Path

I preach the gospel of **a *new* path** — one I stumbled upon as I started my web development career. In the **Inverted Career Path,** you *start* as a professional web developer and look for a job only if you want to. My path is non-linear and looks something like this:

- **Learn HTML and CSS**. While you’re doing that…
  - Start **a pet project** and build whatever part of it you can with what you now know
  - **Find freelance jobs** that let you practice your new skills
- **Learn basic front-end Javascript**. While you're doing that…
  - **Build whatever part of your pet project you can** with your new skills
  - **Find freelance jobs** that let you practice your new skill

Now, you have the basics of front-end development. From now on, you’re going to **let your freelance gigs and your pet project** **drive what you learn next**. Choose a direction based on the needs in front of you. This ensures you’re always learning something that is **meaningful to you.** It’s either making you **more money** or helping you **complete your pet project**.

Keep this cycle going. Learn until you can build the next piece of your project or take on the next tier of freelance work. Work until you hit another barrier. Rinse. Repeat.

At this point, you don’t *have* to look for a job because **you’re already getting paid for your freelance work**. If you decide you need the structure and stability of a permanent position, you can get that much more easily on the back of **your proven freelance track record**. You’re no longer some random stranger who wondered in out of the nearest coding bootcamp looking for a job. You’re already **a proven professional web developer**. (Note: The additional stability of a permanent position is an illusion.)

Besides this, you’re actually earning money as you continue to learn. Instead of *paying* to learn, you’re getting *paid* to learn.

## A Pain-Free 😌 Hiring Process

By becoming a professional developer before you look for a job, **you may not have to look at all**. This is what happened in my story. My first employer as a web developer was initially a client in my freelance web development practice.

I worked with them for almost two years as a freelancer. When they needed the permanent help of someone to do more of what I was already doing, ***they* came to *me***. Their alternative was to [**waste thousands of dollars**](https://devskiller.com/true-cost-of-recruiting-a-developer-infographic/) finding a suitable candidate. They would hope all the barriers they built into the hiring process had the desired effect. (Even the best hiring process in the world can't ensure you don't end up with a dud. If you do, you start back at zero.)

Offering me the job was a no-brainer. I allowed them to short-circuit that whole painful process. **All the uncertainty** of hiring a candidate just because of their whiteboard code and charm was gone. They *knew* I could do the work I was doing **because I was already doing it**.

It was great for me too. Everyone hates whiteboard interviews and take-home coding projects (i.e. unpaid work). I didn't have to do any of these things. My coding project was **the two years of work I had already done for them**. *They* won because they got **actual useful code** instead of a throwaway project. *I* won because **I effectively got paid for the coding interview**.

## Inverting the Power 🔋 Dynamic

The hiring process was simple and low-risk on both sides. They made me an offer. We negotiated a bit. We met in the middle. I accepted the offer and kept doing what I was already doing but for more hours each week.

By removing the risk and costs of hiring and on-boarding new talent, I was **in a much better bargaining position**. This allowed me to ask for a salary I could never have received in a million years as an unknown quantity wandering in off the street to beg for a job.

<!-- ## But, Freelancing Is Scary! 😱

That's what I'm here for. I'll be your mentor and your guide to get you through the scary parts. Sign up below to get **great resources** and **ongoing mentorship** (just hit reply to any email!) as well as **my 3 best tips for new freelancers**.

[thrive_leads id='1378'] -->

---
title: 'Git Deep Cuts'
date: '2013-06-03'
tags:
  - 'git'
  - 'version-control'
coverImage: 'images/pavement-crack.jpg'
---

A friend and I have been working together to build [a JavaScript game](http://raddevon.github.io/onion-or-not/ 'Onion or Not'), and we've run into quite a few different roadblocks with Git version control. The software is incredibly flexible and powerful, but, with that flexibility, it brings a mind-boggling repertoire of commands that create a haystack of options around that needle that will have exactly your desired effect.

I've collected a few of the most common functions we find ourselves having to do on a regular basis and the somewhat unintuitive commands that will accomplish them.

[thrive_leads id='1366']

**Track a new remote branch**

Frequently, one of your collaborators will start a new branch and push it to the remote repository. Now, you'd like to have a local branch tracking it. `git checkout -t <remote>/<whatever>`

**Undo a merge attempt with conflicts**

If I try a merge that I thought would be simple, and find that it comes back with tons of conflicts, I may instead opt for a different merge strategy. Before I can try this, I'll have to undo the merge in which I'm now entangled. `git merge --abort`

**Make a branch just like another**

I feel there should be (and most likely is) a better way to do this. I want to be able to overwrite one branch with another. You could always delete the branch and then re-create it from your source branch, but that seems like a hassle. This is my current preferred method.

Switch to the branch you want to change (e.g. `git checkout <destination_branch>`) `git reset --hard <source_branch>`

**Delete a remote branch**

Now, why didn't I think to push to the remote and put a colon before the branch name in order to delete the remote branch? It's all so clear now!

`git push <remote> :<branch>` Note the colon before the branch name.

**Stage a deleted file for commit**

I wish this were as simple as adding the file as you would for a changed file. Instead, you need to remember a separate command to stage deleted files. `git rm <filename>`

---
title: 'Stop Doing Coding Tutorials'
date: '2019-03-08'
tags:
  - 'learning'
  - 'motivation'
  - 'projects'
  - 'tutorials'
  - 'start here'
coverImage: 'images/code-tutorial.jpg'
description: 'Tutorials are great, but they can also keep you from taking the next step: going off script and solving unique problems on your own. That mistake will kill your career before it starts. Hereʼs how to kick the habit.'
startHereDescription: 'As you start learning, youʼre bound to get caught in a trap that everyone gets caught in: the sweet siren song of coding tutorials. Like a Lego set, theyʼre easy, fun, and satisfying. It feels like youʼre learning, but, by the end of it, you havenʼt learned anything except how to complete the tutorial. Here Iʼll teach you to avoid the trap and what to do instead.'
startHereOrder: 4
---

When you start learning to code, you'll probably start with a tutorial. It's a quick way to get up-to-speed with a new concept. Learn by following a step-by-step guide for building a canned project. You can quickly start to see how the technology works and feel the gratification of building something real.

That loop of reading the next step, typing out some code, and seeing it work **is addictive**. It's too easy for newbies to get stuck in that loop.

![Man doing coding tutorials with a cookie and coffee](images/code-tutorial.jpg)

## What's Wrong with Tutorials?

The very thing that makes tutorials so addictive is also what stunts your growth as a developer: **each tutorial is a solved problem**. The difficult decisions are already made, the hard problems already worked out, and everything is laid out for you on a silver platter.

This is good for learning how to write code, but it's not good for learning to **solve problems**. If you want to be a developer, writing code is an almost incidental part of what you do. People are actually paying you to solve problems. If you're learning only how to write code, you won't be able to be a web developer. It's like the difference between paint-by-number and painting on a blank canvas. No one is going to buy your paint-by-number "masterpiece," but someone _might_ buy a work of art you painted from scratch.

## What You Should Do Instead

As soon as you can, **break away from tutorials** and **start applying what you've learned on projects**. These can be pet projects you want to build or they can be freelance projects you do for clients. The goal is to get out into the messy real world and start dealing with all the unknowns.

The nice thing about your own projects is that [they provide a kind of motivation](/articles/the-best-projects-for-learning-web-development/) to stretch your learning that is hard to come by in other ways. If you're really excited about a project and driven to complete it, you'll figure out new concepts, technologies, and techniques you might not have just for the sake of learning them. That's because you now have **a _reason_ to learn them**. They've become applicable to your life and critical for accomplishing your goals.

Instead of going out and trying to learn everything you might need to know before building anything, start building. When you hit a wall, go find some information on the one thing you need to know to break through. Apply what you've learned and keep moving. Rinse. Repeat. I call this "just-in-time learning."

If you need some inspiration, check out [my list of web development project ideas](/articles/10-great-web-development-learning-project-ideas/).

<!-- If you're not sure what to do next, sign up for a free mentoring session. We'll work together to figure out your next step toward becoming a web developer! *👇*

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

## When Should You Do Tutorials?

If you don't know anything about HTML, CSS, or Javascript, you can't very well start a project using those technologies. Just-in-time learning only works from **a solid foundation**. Avoid the temptation to re-define "foundation" to encompass everything you haven't learned. "I just need to learn React and I'll be ready," is the battlecry of the coding procrastinator.

The way you build that foundation can be through tutorials, books, mentorship, bootcamps (in-person or online), or other kinds of courses. Tutorials have a niche to fill, but **that niche is much smaller than most people want it to be**.

If you understand HTML and CSS, **you can start a project**. You may find parts of the project you can't yet build, but you should start anyway. Here are the steps to take if you only know HTML and CSS:

1. **Start building your UI**, applying what you've learned about HTML and CSS
2. **Continue building your Javascript foundation**
3. **Layer in interactivity later** after you've learned the basics of Javascript
4. **Layer in data persistence** later still (_if_ your project needs it) after you've built a foundation in back-end development
5. **Learn the next skill, technique, or concept** your project demands

If you try to learn all these things without actually using any of them, by the time you get to the end, **you'll have to circle back and re-learn HTML and CSS** before you can start a real project. Once that's done, you'll have forgotten everything you knew about Javascript. Welcome to the vicious cycle of learning web development.

## Practice How You Play

How many elite athletes go pro by watching other players and mimicking their every move in contrived scenarios? How many world-famous pop stars play only faithful covers of other artists' songs? You can't expect to go pro as a web developer by following tutorials or doing what an instructor told you to do. **People need to know how you perform in the real-world.**

The only way to show them is to **abandon tutorials** for the messy, dirty, and uncomfortable realities of **real-world projects**.

---
title: 'Learn the Basics of SQL 📺'
date: '2020-07-31'
tags:
  - 'databases'
  - 'sql'
  - 'tutorials'
coverImage: 'images/learn-sql-thumbnail.png'
---

You may think SQL is old-fashioned, and that you'll build a career using only those new-fangled noSQL databases. If that's what you have your heart set on, I hope it happens for you, but it's not very likely.

Almost every Internet giant you've ever heard of – we're talking Facebook, Google, Amazon, Stripe, eBay, Wikipedia – all have relational databases in service somewhere. The small players use SQL too because noSQL isn't the right solution for every problem. The way to interface with these databases? **SQL**.

That's why it's so important to learn. If you couldn't make the live SQL workshop, no worries. It's all captured in glorious video! Watch and learn the magic of SQL.

https://youtu.be/fzJ2soEsErI

You're digging deep to understand all the technical skills you need to become a web developer (like SQL). Good on you! Don't forget that _just_ technical skills won't get you to where you want to be. You need to take it a step further and **start applying those skills to solve problems** that make people money. That's why I love freelancing as a starting point for new developers. It puts that fact in sharp focus.

<!-- If you want to try freelancing, take **my free crash course below** 👇. It will teach you how to set up your business so that you can start taking on clients and applying what you know to earn a living!

[thrive_leads id='2262'] -->

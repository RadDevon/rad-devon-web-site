---
title: 'The Ultimate Guide to Pricing Freelance Web Development'
date: '2018-09-30'
tags:
  - 'fees'
  - 'freelancing'
  - 'pricing'
  - 'rates'
coverImage: 'images/neon-dollar-sign.png'
description: 'Pricing your services is a scary proposition. I share three different methods for pricing and the basics of how each one works, including the pros and cons.'
---

Even though I've been doing freelance web development for years now, pricing is still tough. Sending out pricing to a prospective client always triggers a lot of doubt. Did I price the project too high? Did I leave money on the table?

Having talked with other freelancers, I'm not sure these feelings ever entirely go away. It does get easier with experience. This doesn't help _new_ freelancers, though, since they need to start pricing projects quickly and with confidence

In this guide, I'll outline **different ways to price your services** and **pricing considerations new freelance developers are likely to miss**.

_Note: Most of this advice will be applicable to freelancers everywhere, but I only have experience freelancing in the US. This guide is written from that perspective._

![Neon dollar sign](images/neon-dollar-sign.png)

## Ways to Price

### Time

Pricing for the time you spend on a project is straightforward and easy to spin up. You can chunk your time however you want: by **hour, day, or week** are common, with hourly being the most common.

#### Setting a Rate for Your Time

People tend to work backwards into a rate either by starting from **a full-time developer salary** or by starting from **their own personal target salary**. Avoid using a full-time developer salary as the basis for your rate. It's apples to oranges. As a full-time employee, you have several benefits that go away as a freelancer:

- health insurance
- retirement benefits
- life insurance
- workspace
- consistent work and pay

and others. You can have all of these as a freelancer, **but you have to buy them**, either with your money or your time. If you _do_ use a full-time salary as the basis for your rate, make sure you account for all of these _and_ self-employment taxes.

##### Taxes

As a US employee, your employer will pay a portion of your Social Security and Medicare taxes and you will have money withheld from your paycheck to pay your income taxes. **You'll have neither of these as a freelancer.** You'll have to pay all your own Social Security and Medicare taxes, and you'll pay your income taxes out of whatever you have saved yourself. Make sure you **factor this into your rates** and that you **save for it**. I've generally saved 20% for taxes, but last year that wasn't enough for me. I'm saving 35% this year. Find a good accountant and talk to them about how much you should save. It's worth the small investment.

If you're going to work backwards from your desired salary, make sure you're covering all of the items mentioned above. You'll also need to account for the fact that **you're not going to be billing 40 hours per week** (a common misconception if you're coming from being an employee). In addition to doing billable work, you also have to keep your sales pipeline full so that, when _this_ project is done, you'll have another project ready to replace it. Your desired freelance salary should be **50-100% more** than the salary you might expect from an employer.

Now that you have a number, you'll have to **check this number against market rates** for the skills you bring to the table to make sure it will make sense for at least some clients. Don't pay attention to these things:

- **rates for freelancers outside your region**\- If you live in a region where rates are higher, you can still find plenty of clients who will pay more to hire someone in their own region.
- **the absolute lowest rates**\- You're not trying to be the cheapest. You just want to be aware of the average rates and make sure you're not coming in at 3x that unless you can justify the cost to your clients.
- **rates on freelance marketplaces like Upwork**\- Many of the people on these sites _are_ paying attention to the two items above, and their rates are unreasonably low as a result.

#### Pros

##### Easy to get started

It's quick and easy to figure out what you want to be paid and start quoting people that rate.

##### Easy to understand

Billing for time is common and widely understood by clients.

##### Works well for on-going engagements

If a client has several projects they want you to work on, you can sign a single contract to work at your rate and jump from project to project on the same contract.

##### Less risky for you

Even if you underestimate how much time a project will take, you'll still get paid for all your time.

#### Cons

##### Misaligned incentives

You are incentivized to work on a project as long as possible to get paid more. The flip side of this is that you're penalized for being efficient by being paid less.

##### Encourages micromanagement

Clients will want to second-guess how long things should take since that's what determines how much they will pay.

##### Time tracking

You have to make sure you're always tracking your time which can be a pain.

##### You're positioned as a commodity

If your prospective clients find a developer with similar skills who is charging a lower rate, they're likely to go with them.

### Project Pricing

Giving clients a flat price for a project is great for them since they know exactly how much they're going to spend. It's nice for you since **you get rewarded for being faster**.

#### Setting the Project Price

Project pricing, for the purposes of this guide, is **still based on time**. The only difference is that you're trying to guess exactly how much time the project will take _before_ you start the project. You'll talk with the prospective client, gain an understanding of the project scope, and do your best to estimate how long it will take. You'll then come up with a price that represents **the most the client will pay** for the project you've defined together.

For most projects, it's hard to do this looking at the project as a single monolithic unit, so you'll probably start by **breaking the project apart** into smaller pieces you can better estimate. You'll estimate each part, add them altogether, and add some time for communicating with the client and for the inevitable problems you'll run into. It's fairly common for people to **double** the sum of their original estimates.

Besides just accounting for the total number of hours you work, you should also be paid for taking on more of the risk. The client will never pay more than the agreed amount, and that reduction in their risk is valuable. Make sure you charge for it.

#### Pros

##### Makes clients feel warm & fuzzy

Clients know exactly how much they're going to pay and exactly what they will get for it. This makes them more likely to work with you since you've taken some risk off of them.

##### Easier to understand

Client pays X and gets Y built for them. This is as easy as shopping on Amazon.

##### Rewards speed & efficiency

If you can finish a project in less time than you estimated, your hourly earnings rise.

#### Cons

##### Misaligned incentives

You are incentivized to complete each project as quickly as possible. This may mean cutting corners or delivering a sub-standard product. which will hurt your business in the long run.

##### It's difficult to estimate (especially when you're new)

It's extremely difficult to anticipate all the problems you might run into with a web development project. It's damn near impossible if you don't have a ton of experience.

##### You own all the risk

If your estimate was too low, your hourly earnings drop. You have to deliver the application or web site promised at the price agreed upon no matter how long it takes.

##### New contract for each project

Since you're pricing for a project, your contract needs to define exactly what is being delivered. If the client wants to continue working with you on the _next_ project, you'll need a new contract for that.

##### You're (still) positioned as a commodity

If your prospective clients find a developer to complete the project for less, they're likely to hire them.

### Value Pricing

This is like project pricing in that you give a single price for a single project. The difference is in **how you reach that price**. With value pricing, you're estimating **how much your project is worth to your client**. You'll anchor your price to that number instead of to the amount of time you spend. Value pricing is the holy grail of freelance web developer pricing.

#### Setting the Value Price

Setting a value price is **much more difficult** than setting a price based on time. Before you can set the price for the project, you need to understand the value. This usually happens during the initial conversations about the project.

Here are some questions you might ask:

- _"How do you currently work around the problem?"_ This will help you understand how much they are spending right now.
- _"How much value does a single sale/prospect represent?"_ Estimate how many of these your solution can be expected to deliver and multiply to get the value of your solution.

Now that you know the value, what should you do with it? You'll charge **a percentage of the value** as your fee. The solutions you build will probably produce results on a recurring basis. Instead of the value being $X, it will be $X per month or $X per year. Be sure to factor this into your pricing too.

Value pricing requires you to **change how you think about what you're selling**. When you're selling your time, you're selling web development. When you price based on value, you need to be selling a result that makes or saves the client money. For example, you'll want to talk about how much time your software is going to save their marketing department or about how many additional leads their new web site will bring in every month. **The entire conversation is centered around the result and its value.**

##### More on Value Pricing

Value pricing is a massive subject. If you want a better understanding of how it works and why you should consider it, check out the free ebook _[Breaking the Time Barrier](https://www.freshbooks.com/wp-content/uploads/2018/02/breaking-the-time-barrier.pdf)_ by the co-founder of Freshbooks, Mike McDerment. It's a great intro to the concept and helps you shift the way you think about your services. _(Note: That link goes directly to the PDF. Freshbooks used to have a page introducing the book, but they don't seem to anymore.)_

#### Pros

##### Not anchored to your time

In both the other pricing methods, you make more if you work more and make less if you work less. If you're already working a full week and want to make more, you have to raise your rates. This will only get you so far. This is not the case with value pricing. This also means you never have to worry about _tracking_ your time.

##### Clients get excited about your work

When you're selling the value instead of hours, it's easy for clients to get excited. Instead of envisioning a time clock and monthly invoices, they get to dream of more customers and less waste.

##### Potential for massive profits

If you're an expert in an obscure technology billing hourly, you might be able to push your rates up to $300/hour. If you can build a web application that adds another $100,000 a month in revenue for your client, there's no reason you can't charge them $500,000 for it, even if it only takes you 100 hours to build. How does $5k/hour sound? 😉

##### Incentives are finally aligned!

When you bill on value, you have to provide that value to get paid. That means you succeed when your customers succeed and vice versa. Sure, on a given project you may be able to trick someone into thinking your solution works when it doesn't, but word will spread and that will quickly catch up to you.

#### Cons

##### Sometimes difficult to find the value

Businesses may not want to give up the average value of a new customer. They may not want to tell you how much it costs them to work around a problem you intend to solve.

##### Need some experience to understand the value

Even if the client _will_ tell you how valuable each lead is to them, you have no basis for deciding how many of those your application can deliver unless you've done a few projects like it before.

##### New contract for each project

You're still pricing for a given project, so you'll need a new contract for each project.

##### Bigger project prices take longer to sell

In general, the higher your price the more time it will take to close the sale. I've heard your sales cycle will grow by 1 month for every $10,000 in price.

[thrive_2step id='1842'] Need more help with freelancing? [/thrive_2step]

### How do I start?

Now that you understand a little bit about each method of pricing your web development services, here's a practical roadmap for getting started:

1. **Start billing hourly**. It's quick and easy to spin up. Bill **no less than $40/hour** if you're in the US.
2. **Keep your eyes on the prize: value pricing**. Start each job with a questionnaire **as if you're going to price on value**. This will get you in the habit and give you some additional tools to use when you _do_ actually start value pricing projects. As you work with your early clients, ask them to let you **keep track of the impact of the web software you built for them**. This data will be instrumental when you start pricing on value.
3. **Look for opportunities to raise your rates.** Raise your rates after each successful project until you get to around $75/hour. If you still have more prospects than you can handle, keep raising rates until you reach equilibrium.
4. **Transition to value pricing** as soon as you have enough data to start quantifying the value of your web development projects.

You may have noticed this roadmap doesn't include project pricing at all. That's mostly because that pricing method puts all the burden on you to estimate accurately and you absorb all the risk if you don't. Its exclusion doesn't mean you should never use it though. If you come upon a project you think you can estimate pretty accurately but you're not quite ready to try value pricing, project pricing can be good practice since it's similar to value pricing in some ways. If you have a great process for a specific kind of project that allows you to finish it quickly, project pricing could be a good way to capture additional revenue.

Just as with all the pricing advice here, you have to make your own decision based on the situation. Don't feel like you have to follow this guide to the letter. You're welcome to do so, but it's intended instead to… well, to **guide** you as you steer your own freelance web development business in the direction you want it to go.

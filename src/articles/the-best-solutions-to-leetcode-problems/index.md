---
title: 'The best solutions to LeetCode problems'
date: '2021-09-23'
tags:
  - 'career'
  - 'interviewing'
coverImage: 'images/leetcode-solutions-thumb.jpg'
---

I've always thought of LeetCode (and previously HackerRank) as a lame way to decide who to hire. I personally have never been hired because I solved some kind of programming puzzle ([and _you_ don't have to go down this path either](/articles/inverted-career-path/)), but I know that many people _are_ hired this way. The inertia LeetCode has as the method of choice for filtering candidates doesn't seem to be slowing anytime soon.

If you've resigned yourself to the fate of solving the riddles of the sphinx to get the job you want, you're going to need some resources to help along the way. I looked at several of them, and this one will put you on the fast track… *if* you're the right person.

![The sphinx saying “Sure, you can be a dev, but first… do a bubble sort!”](images/leetcode-solutions-thumb-1024x576.jpg)

I discovered this [YouTube playlist of LeetCode solution tutorials](https://www.youtube.com/playlist?list=PLi9RQVmJD2fYXgBAUfaN5MXgYPUTbzqeb) on a [reddit thread where people were discussing the platform](https://old.reddit.com/r/cscareerquestions/comments/gxjh9e/back_on_the_leetcode_after_4_yearsits_awful/). The creator Kevin Naughton, Jr. hasn't been so active on his YouTube channel in recent months, but that's OK because he spent a couple of years before that recording **over a hundred tutorials on popular LeetCode problems**. This playlist collects all of those in one place, but they're not for everyone.

## Who are they for?

I really like these tutorials, but you'll need to clear a couple of hurdles to make them useful. Here's what you need to know.

### It's written in Java (No, wait! Come back!)

I've got no beef with Java, but I doubt many of you reading this are writing Java. If you *are*, you're in luck. These solutions are written in your primary language. If you're _not_ writing Java, you're going to need to be able to translate the concepts presented into your language of choice.

If you're a brand new coder, this sounds terrifying… and it is. You're already trying to learn a new language and a bunch of new concepts. Now, you're being asked to take novel applications of those concepts and translate them to a _different_ language as you're still learning them. It's probably going to be too much for you.

If you've been doing this long enough to write code in two different languages, though, you already know that, **once you learn the fundamentals, they're not all that hard to translate into different languages**. You can just as easily take these solutions and translate them to your language of choice.

If you're very comfortable with your language, you should at least try to watch a few of these and see what you can get out of them. I remember being surprised the first time I tried to pick up a new language at how easy it came. You might be too, and this resource is good enough that **it's worth trying to translate**.

### His explanations are pretty brief

Kevin's explanations are very clear. He's also very brief. If you can follow along, this is perfect because he doesn't waste any of your time. If you have trouble tracking with him, you're probably not going to get much out of these videos.

Part of this is that he doesn't take a ton of time to explain how he approaches coming up with the solution. He explains the solution, but not how he got there. If you're looking for help understanding how to break a problem down and then start solving it, one step at a time, this isn't your resource. If you just need exposure to clever ways to mix and match the fundamental tools you're already using, Kevin's videos will give you that.

If you can get past these two caveats, these videos do a lot of things right

## What they do right

- Kevin has a casual style with his videos that feels like curling up with a warm blanket. It makes his videos easy to watch and the content less intimidating.
- Explanations are quick and to the point. They don't waste any time. He gets in and gets out.
- He walks you through each solution step by step.
- Instructions and code are easy to see in the video (at least on my 15" laptop)

## What could be improved

- More detail about why the solution was chosen. For example, Kevin frequently says that a hash map is the right data type for a solution, but I've not yet heard him explain why. Maybe that happens in an early video, but even if so, it would be nice to have that context in every video.
- Related to the above but even more broad: it would be cool if Kevin would talk through his decision making process as he devises the solution. This would make the videos longer, but adding chapter markers could still serve viewers who want to cut to the chase and see the solution.
- Especially for _my_ readers, it would be cool to see solutions in other languages. That way, people who are new to programming and know a different language would be able to get at the meat of these solutions without having to simultaneously translate to their primary language.
- Audio levels seem to be a bit uneven. Some videos are easy to hear. Some need to be turned way up. Turning them way up means the next video or ad might blast you out of your seat.

## A great way to crack that technical interview

Even though Kevin's videos are not going to be the perfect resource for everyone, they're pretty dang awesome. If you can get over that Java hump, you'll have access to a treasure trove of over 100 solutions, explained and solved with you.

If this is not the right resource for you – maybe because of the language or maybe video isn't your jam – check out these alternatives.

## Other materials to try

I haven't used these myself so I can't make a personal recommendation, but these are the other resources I see recommended frequently:

- [_Grokking Algorithms: An Illustrated Guide for Programmers and Other Curious People_](https://amzn.to/2ZaAZu9)\- I love the illustrations in this book. Rather than trying to teach you to solve specific interview questions, this book teaches you about some common algorithms more generally. It doesn't matter which languages you're familiar with, this book can help you get a better understanding of algorithms which will make it easier for you to solve LeetCode problems.
- [_Cracking the Coding Interview_](https://amzn.to/3nQcMnc)\- This has to be the most recommended book for coding interviews. Any time the subject comes up, someone is around to toss out this title. Be aware that the solutions in the book are in Java. If you didn't like Kevin's videos for this reason, this book is not for you.
- [Algorithms on Coursera](https://www.coursera.org/learn/algorithms-part1)\- You can take this Princeton course for free on Coursera. It's a very traditional style course. So traditional that, not only are you going to be submitting code in Java, but you'll be submitting it by zipping it and uploading it to Coursera. Feels a bit like I've stepped back to the early 2000s, but the content is apparently excellent.
- [The Ultimate Guide to JavaScript Algorithms](https://scotch.io/courses/the-ultimate-guide-to-javascript-algorithms)\- This is a nice written introduction to algorithms in Javascript. It has good explanations of the solutions. I like the casual style, although I'm not crazy about the memes. I definitely prefer this presentation to the Princeton course, and it's the first resource I've covered that's in Javascript!

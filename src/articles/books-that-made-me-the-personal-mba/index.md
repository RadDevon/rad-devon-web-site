---
title: 'Books That Made Me: The Personal MBA'
date: '2019-11-29'
tags:
  - 'books'
  - 'business'
coverImage: 'images/books-personal-mba.jpg'
description: 'As web developers, we can only make a living by selling our services to businesses. So, shouldnʼt you understand how they work and what they would want to pay you to do in the first place? This book can help with that.'
---

https://youtu.be/Un-5Dax4Ap0

## The Personal MBA

If you want to learn the basics of business, [buy The Personal MBA on Amazon](https://amzn.to/2KPM8GU).

If you'd like a free chapter of my own book, _Freelance Web Development Pricing_, share your email address below. You'll learn everything you need to know about pricing your projects confidently, estimating projects, and turning a profit.

[thrive_leads id='2463']

## Transcript

In this first episode of The Books That Made Me, I'm going to introduce you to one of my favorites. It's The Personal MBA by Josh Kaufman. Let's take a look.

This is one of the books that made me the web developer I am today. This is The Personal MBA by Josh Kaufman. When I got this book, or really when I started in web development in general, I didn't know anything about business, and I didn't think I needed to because I'm just writing code, right? What I realized, though, is that I had no experience, so **no one would hire me**.

What I did with this is, instead of waiting for someone to take a chance on me, I started doing freelance work, and that alone is what got me to where I am today. Every full time job I've had, I got through that freelance work. Every single dollar I've made on web development, I did through freelance work. And one of the reasons I was able to do any of that is because I realized pretty quickly that, **in order to be a freelancer, I was going to have to run a business**, and **in order to run a business, I was going to have to understand how businesses work**.

I don't have an MBA. I never even took a business class. I don't remember where this recommendation came from, but I'm so glad that I stumbled upon it.

This book is so readable. You would think that a book about business would be incredibly boring and dry, but this one is just a fun read. It's not very dense. It's almost like a dictionary of business terms, but **it gives you enough context** and **tells stories around each of the terms** that you're being taught and that sort of makes everything concrete and makes it more interesting to read.

The Personal MBA teaches you the things that people who are running businesses care about, and that's really important because **those are the people who are going to hire you**. Those are the people who are going to pay you to do your work as a web developer. If you understand what they're looking for, you can find ways to use your skills to give that to them.

You can also understand **how they talk about what they're looking for**, so that when you enter into a conversation with a new person who might someday become a client, you can talk in a way that makes you seem credible, like you understand on a deeper level what problems they're experiencing and maybe you could solve them.

If you come up to somebody you meet cold, and you're trying to win them as a client and you're just talking about building websites, they have to connect all the dots to understand why they care about that. **If you understand how business works and if you can talk to them on that level**, and instead of telling them you build websites, you can say, "I solve this problem for this kind of business," they don't have to connect as many dots. So that puts you much closer to getting a sale - to closing a new client for your business.

I'm going to read you a quick section from this book. This is from chapter four on sales, and this is about trust.

> Here's a proposal. Send me a certified bank check for $100,000 right now, and in 10 years I'll give you the keys to a brand new 10,000 square foot villa on Italy's Amalfi coast. _(Devon's note: I probably butchered that pronunciation.)_ You can't see any example villas. You won't hear from me again until the villa is ready, and there are absolutely no refunds. Deal?
>
> Unless you're an extremely trusting soul with cash to burn, probably not. After all, how can you be sure I can actually build you a seaside mansion for such a paltry sum? How can you be sure I won't just take the money and disappear?
>
> You can't, which is why you shouldn't cut me or anyone else at check for a villa on the Mediterranean you've never seen.

That's about half of this definition of trust, so you can see how short, they are, how readable they are, and in this definition of trust, Josh has highlighted other key words that he defines in the book, like trust, transaction, reputation. So, if you see a bold word in one of these definitions, you know that that word is also defined in the book. So you can cross reference that if you're not sure what that is.

Trust is a concept I talk about a lot in my website. I don't think you can make sales as a freelancer unless you have trust. I think that's a critical component, and that is something I was first exposed to in the context of business through this book, The Personal MBA. It does such a great job of giving you the foundation you need to understand business and **tying everything together** and doing it in a way that's enjoyable to read.

I really feel like this is a book that every new freelance web developer should get and read through, just to have **that baseline understanding of what business is and how it works** so that you can run your business and so that you can make other people's businesses better by doing the work you want to do.

This is a new video series I'm experimenting with, so I'd love to hear what you think about it. If you like this, give me a shout [on Twitter](https://twitter.com/raddevon) and let me know. I've got several other books that were really formative to me in getting started as a web developer, and I'd love to share those with you if that's something you're interested in.

In any case, check out my other articles and video here on the site and you can find lots of other material that will help you become a web developer and change your life. Thanks for watching, and I'll see you in the next one.

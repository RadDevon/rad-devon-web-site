---
title: 'Where do web development project ideas come from?'
date: '2019-03-29'
tags:
  - 'ideas'
  - 'inspiration'
  - 'learning'
  - 'projects'
coverImage: 'images/man-checking-map.jpg'
---

![Man checking a map](images/man-checking-map.jpg)

How do people come up with the ideas they get for cool web development projects? In this article, I'll teach you some techniques you can use to develop **your _own_ project ideas** that give you the motivation and enthusiasm to keep learning!

Let's start by laying the groundwork.

## What does software _do_?

If you're a web developer, you may think of what you build as a "web site." A "web site" is like an empty building; who knows why it was built, but we're pretty sure it will be useful to someone someday.

What we _should_ be building instead is **software that solves people's problems**. That software might be in the form of a WordPress site or even static HTML, but I'm lumping those all together as "software."

I mentioned that the software needs to solve problems. That's what software does. That's the only reason it exists. If we want a software project, we need to first figure out **what problem we're trying to solve**.

## How do you find problems?

Discovering problems is a valuable skill to hone as a developer. It's literally just a matter of **awakening yourself to the problems that are all around you**. Then, you need a system for capturing them and the imagination to think about how you might solve them with software.

Time for your awakening. Start thinking deeply about everything you do as you go about your day. Which parts are difficult? Which parts could be improved? This can be tough because **we're trained to be consumers**. We should accept the flaws of our world and live with them… until someone in the mythical creator class builds a solution for us to buy.

Congratulations, by the way, on joining the mythical creator class! As a developer, **you can now build your own solutions** and solutions for others. As a result, you can start seeing your daily struggles less as an inescapable part of accomplishing tasks and more as tiny opportunities for you to exercise your newfound creative muscles.

Problems at work (yours or others) are great because there's money changing hands. The problem you identify could be making the money come into the business more slowly than it otherwise might. If you build solutions to those problems, **you can charge some of the money your solution saved**.

If you're looking for learning projects, **any problems will do, even personal ones**. Maybe your kids aren't doing a great job keeping track of their chores. Maybe your favorite video game doesn't have a built-in world map. Maybe every night when you get up to get a drink of water, you step in a sticky drink someone spilled the day prior.

You'll find problems that are easy to solve with software and others that are less so. Start by cataloging everything. Filtering comes later.

## How do you record problems you find?

You need a way to **capture** all these problems you're going to be encountering every day. It doesn't matter what it is. I like to keep digital notes because my writing muscles have seriously atrophied, but, if you love pocket journals or even literal back-of-napkin notes, by all means use those.

I created a notebook in [Evernote](https://evernote.com/) called "Ideas." ("Ideas" instead of "problems" because the problem is not the thing. The _solution_ is.) Each time I find a new problem, I make a new note, with the idea as the title. I then take down any detail about the problem in the body of the note. Later, I'll go back and do my solution brainstorming, right inside the note.

I use Evernote because it has robust sharing features, but I don't love everything about it. If I didn't need to share notes, I might use [Bear](https://bear.app/) instead (but that one only works if you're in the Apple ecosystem). I love the look of [Notion](https://www.notion.so/product) except for the cost of a plan that supports sharing (at the time of this publication, $8/month per user; so $16/month for me and my wife). You can easily use your phone's built-in notes app, [OneNote](https://products.office.com/en-us/onenote/digital-note-taking-app), [Google Keep](https://www.google.com/keep/), or, like I said, ink and some kind of notebook.

## How do you turn the problems into project ideas?

Sort through your problems at regular intervals — maybe once per week — or when you're looking to start a new project. Pick a handful of your favorites and **brainstorm some solution ideas**.

Your **criteria** for which problems to select will change depending on your goals for this project. If it's strictly a learning project, pick either the one you find most exciting or the one that stretches you to try a technology you want to learn. (Just don't get caught up with shiny object syndrome, always chasing the latest languages and frameworks [to add to your résumé](https://raddevon.com/articles/how-to-build-your-web-developer-resume-with-or-without-experience/).) If you're trying to make money, pick the one with the most expensive and painful problem. Do some research to find out which that is.

Now, it's time for brainstorming. Your success here will depend on **your ability to imagine solutions that don't exist and haven't even been conceived but are still practical**. It's OK to make these big pie-in-the-sky solutions. Know that later you're going to pare that down to a much simpler version of your grand vision when you actually start building.

Try to come up with **multiple solutions to each problem**. This way, it's harder to fall in love with any one solution. You'll find a much better solution if you come up with just three options and evaluate them than if you try to hit a home run on your first at-bat.

You're now ready to pick, not between your problems, but between the best _solutions_ you brainstormed for them. Keeping your project goal in mind, **pick the solution that best fits your criteria for this project**.

Jackpot! 🎰 Now, you have a web development project, filtered directly through **your own brain** from **your own experiences**!

<!-- If you're trying to go pro with web development, I'd like to help. Let's figure out your next step in a free mentoring session!

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

## Notes on Picking

I've asked you to make a choice at several points in this article: choosing a way to capture problems and potential solutions, choosing a handful of problems, and choosing the best solution. Everybody loves choices, right?

My mom can tell you from her experiences of waiting for me to find the perfect action figure at the Toys 'R' Us that **choice can also be a curse**. You have the tools to come up with your own projects, but you can easily let the inflection points in the process **derail** everything you were trying to accomplish.

Here's the pep talk you need to get out of your own way and pick something!

- **This is not the last pick you will make.** If this one turns out to be bad, you can pick another one.
- **The options you didn't pick won't hold it against you.** You can come back and pick them later.
- **Doing the wrong something now is better than doing the exact right thing never.**
- **If someone beats you to your idea, it generally doesn't matter.** A dozen people may have already beaten you to it, but they did a bad job at something and disappeared into obscurity. Your challenge is rarely to do it first but to do it better. (Facebook > MySpace, iPhone > [IBM Simon](http://time.com/3137005/first-smartphone-ibm-simon/), Google > Lycos/AltaVista/Hotbot/Webcrawler/etc.)

Pick your project like you'd pick your nose 👃: fast, before someone sees you! Progress comes from building your project, not from deliberating over which to build.

_If you still need a little push, check out my list of [10 fun project ideas for web developers](/articles/10-great-web-development-learning-project-ideas/)._

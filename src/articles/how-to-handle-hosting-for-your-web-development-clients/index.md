---
title: 'How to Handle Hosting for Your Web Development Clients'
date: '2019-02-08'
tags:
  - 'clients'
  - 'freelancing'
  - 'hosting'
coverImage: 'images/server-cabinets.jpg'
description: 'Once you build a web site for a client, where are you going to put it? Iʼll walk you through the options and the trade-offs for each of them.'
---

![Server cabinets](images/server-cabinets.jpg)

You have several options for hosting your clients' sites depending on **what you built** and **how involved you want to be in hosting**. I'll share a few of the options and tell you **how I host sites for my clients**.

## Ways to Provide Clients with Hosting

### They "Host"

The client isn't actually hosting the app, but **they own the account the app is hosted on**. If you don't want to be involved at all in hosting, this is your jam.

That doesn't mean you can't still make some money from the deal. You're providing the host with access to a valuable asset: **your customers**. You've taken time to build a relationship and trust with them. Most providers are willing to pay you for access to that customer. When your client signs up (or once they've been hosted for a few months), you'll get a one-time payout for the referral.

**Don't refer your client to a provider just because they offer the biggest payout.** The relationship you have with them going forward is worth far more than a quick buck in the near-term. That trust you've spent time cultivating can be gone in an instant if they have a horrible experience with the provider you recommend. My approach is to research the best option for a client _first_ and only _then_ seek out an affiliate program. That way, my choice isn't clouded by the referral payout.

To find these programs, look for a "referral," "partner," or "affiliate" program. You'll usually have to sign up and be approved, so do this now rather than waiting until you're up against a deadline.

<!-- ## Not sure how to get started with freelancing?

LLC or S-Corp? What's the best business checking account? What should you do for a contract? This course will answer these questions and give you everything you need to start freelancing **in just a week**! I'll also share other helpful articles and resources to help you start your web development career!

- Name\*
- Email\*
- Country\* Country Afghanistan Åland Islands Albania Algeria American Samoa Andorra Angola Anguilla Antarctica Antigua and Barbuda Argentina Armenia Aruba Australia Austria Azerbaijan Bahamas Bahrain Bangladesh Barbados Belarus Belgium Belize Benin Bermuda Bhutan Bolivia Bonaire, Sint Eustatius and Saba Bosnia and Herzegovina Botswana Bouvet Island Brazil British Indian Ocean Territory Brunei Darrussalam Bulgaria Burkina Faso Burundi Cambodia Cameroon Canada Cape Verde Cayman Islands Central African Republic Chad Chile China Christmas Island Cocos Islands Colombia Comoros Congo, Democratic Republic of the Congo, Republic of the Cook Islands Costa Rica Côte d'Ivoire Croatia Cuba Curaçao Cyprus Czech Republic Denmark Djibouti Dominica Dominican Republic Ecuador Egypt El Salvador Equatorial Guinea Eritrea Estonia Eswatini (Swaziland) Ethiopia Falkland Islands Faroe Islands Fiji Finland France French Guiana French Polynesia French Southern Territories Gabon Gambia Georgia Germany Ghana Gibraltar Greece Greenland Grenada Guadeloupe Guam Guatemala Guernsey Guinea Guinea-Bissau Guyana Haiti Heard and McDonald Islands Holy See Honduras Hong Kong Hungary Iceland India Indonesia Iran Iraq Ireland Isle of Man Israel Italy Jamaica Japan Jersey Jordan Kazakhstan Kenya Kiribati Kuwait Kyrgyzstan Lao People's Democratic Republic Latvia Lebanon Lesotho Liberia Libya Liechtenstein Lithuania Luxembourg Macau Macedonia Madagascar Malawi Malaysia Maldives Mali Malta Marshall Islands Martinique Mauritania Mauritius Mayotte Mexico Micronesia Moldova Monaco Mongolia Montenegro Montserrat Morocco Mozambique Myanmar Namibia Nauru Nepal Netherlands New Caledonia New Zealand Nicaragua Niger Nigeria Niue Norfolk Island North Korea Northern Mariana Islands Norway Oman Pakistan Palau Palestine, State of Panama Papua New Guinea Paraguay Peru Philippines Pitcairn Poland Portugal Puerto Rico Qatar Réunion Romania Russia Rwanda Saint Barthélemy Saint Helena Saint Kitts and Nevis Saint Lucia Saint Martin Saint Pierre and Miquelon Saint Vincent and the Grenadines Samoa San Marino Sao Tome and Principe Saudi Arabia Senegal Serbia Seychelles Sierra Leone Singapore Sint Maarten Slovakia Slovenia Solomon Islands Somalia South Africa South Georgia South Korea South Sudan Spain Sri Lanka Sudan Suriname Svalbard and Jan Mayen Islands Sweden Switzerland Syria Taiwan Tajikistan Tanzania Thailand Timor-Leste Togo Tokelau Tonga Trinidad and Tobago Tunisia Turkey Turkmenistan Turks and Caicos Islands Tuvalu Uganda Ukraine United Arab Emirates United Kingdom United States Uruguay US Minor Outlying Islands Uzbekistan Vanuatu Venezuela Vietnam Virgin Islands, British Virgin Islands, U.S. Wallis and Futuna Western Sahara Yemen Zambia Zimbabwe Country

<iframe style="display:none;width:0px;height:0px;" src="about:blank" name="gform_ajax_frame_14" id="gform_ajax_frame_14">This iframe contains the logic required to handle Ajax powered Gravity Forms.</iframe> -->

### They Host (For Real This Time)

No, really. This time, the client actually hosts the thing you built for them. In general, this makes everyone's life harder. **Some enterprise clients will require that applications be hosted on-prem** (on premises, meaning they can't be hosted in the cloud). They may want to deploy the app themselves, or they may want to give you remote access to deploy the app. Enterprise clients pay well, so the hassle should be worth it.

### You Host

You can host client's sites by owning the hosting or cloud account where your clients' applications live. You'll pay the fees and bill them through to your clients, usually with a bit of a markup which covers maintenance, support, or whatever recurring services you want to provide your clients. This creates some **recurring income** to help keep you out of the feast and famine cycle freelancers have to deal with.

### A Hybrid Approach

Some hosts offer **white-labeling**. This allows for a hybrid of they host/you host. With white labeling, you re-sell a provider's services, just like if you host. Instead of paying you, though, the client pays the host directly. You'll typically either earn a percentage of the monthly fee or you can choose to mark up the monthly service, receiving the difference as a payment from the host.

The client pays the host directly, but the host's logo never appears on invoices, admin panels, or anything else. (That's the white-label part.) You can instead put your *own* logo on everything. This allows you to own the relationship with your customer while the host takes care of all the hosting.

## What _I_ Do

As enticing as recurring income is, **I never want to get a call at 2am because a site or an app is down**. I want to make sure my clients understand who is responsible for making sure their site is up so they know who to call when it's down. That handoff happens by way of a referral. They sign up for the service level I recommend and I deploy the site for them.

I've considered trying "hosting" or even going the white-labeling route, but in either case, I would be the face of the hosting for my client. I enjoy building software, but I don't enjoy supporting infrastructure. I've made **a conscious choice to do what I like** instead of making the most money.

That's what I do for static sites, WordPress, and other PHP CMS-backed sites. In every case other, my clients have already had cloud accounts where they wanted applications deployed. If and when this comes up for me, I'll utilize partner programs so that I can get paid for the referral and walk away from supporting the infrastructure.

I still want to help my clients any way I can. If they call me, I'll do whatever I can to help. My goal with this approach is just to make sure I'm not the _first_ person all my clients think of when their site goes down for a few minutes.

This model works great for me. I get to work on the things I like, and I get paid when I refer a client to a host. I'm probably leaving some money on the table, but **that's a sacrifice I'm willing to make**.

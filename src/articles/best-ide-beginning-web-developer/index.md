---
title: 'The Best IDE for a Beginning Web Developer'
date: '2017-11-24'
tags:
  - 'editors'
  - 'software'
  - 'tools'
  - 'visual-studio-code'
coverImage: 'images/visual-studio-code.png'
description: 'The most important piece of software youʼll use as a developer is your editor. Since you spend so much time here, the right one can make your life a lot easier. Hereʼs my pick for new developers.'
---

When you decide to learn web development, you have quite a few decisions to make. One of those is what software you should use to write code. It's easy to be paralyzed by decisions like these and end up not doing anything at all.

That's why, rather than comparing a bunch of editors, I have a simple, clear recommendation: install [Visual Studio Code](https://code.visualstudio.com/). Here's why.

1. **It's available for all major platforms.** Download for Linux, macOS, or Windows.
2. **It's easy to install.** As a newbie, you have plenty of barriers to cross. Installing and configuring an editor doesn't need to be one of them.
3. **It's a good balance between a simple text editor and an IDE.** An IDE (integrated development environment) is usually a text editor with a debugger and some other features built right in. They're nice but often more difficult to configure than a simpler editor. VS Code is a nice balance, perhaps leaning a little toward the IDE side of the spectrum. Configuration is easy though which eliminates another barrier.
4. **It's free.** Most of the best editors available today are, but some are not. VS Code is among the former.

![Visual Studio Code screenshot](images/visual-studio-code.png)

If selecting an editor is getting in the way of your learning, go ahead and get Visual Studio Code. It's a great editor for getting started, but it will continue to grow with you as you mature as a developer.

<!-- [thrive_leads id='1366'] -->

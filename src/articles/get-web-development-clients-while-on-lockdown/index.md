---
title: 'Get Clients While on Lockdown'
date: '2020-05-15'
tags:
  - 'clients'
  - 'freelancing'
  - 'remote'
coverImage: 'images/get-clients-while-on-lockdown-thumb.jpg'
description: 'Getting clients while on lockdown may seem impossible, but I have a method to keep your pipeline full even when you canʼt get in front of people.'
---

https://youtu.be/hSulX92JQM4

## Video Notes

OK, everybody. We're going back to the drawing board. I tell new developers to start looking for freelance clients in-person. (By the way, if you're not yet sold on freelancing, read [why I recommend it as the best way to start your career](/articles/inverted-career-path/).) I would _never_ recommend that right now. Instead, I have some other ideas about how you can still move forward, even when you can't get in front of people.

Here are the four steps I describe in the video:

1. **Decide who you will work for.** This is counterintuitive, so stay with me for a second. In order to get clients during the pandemic when you can't go charm people in person with your wonderful personality, you need to pick a niche. That's right. I want you to get clients by reducing the potential pool of people you can work for. Why you'd want to do this will hopefully make sense when you watch the video.
2. **Discover their problems.** Learning what's important to them lets you talk to them in a way they can understand. It also gives you some ideas of what you could build for them.
3. **Get in front of them.** Not literally. They need to know you exist even though you can't literally get in front of them. I'll share some ideas for how you can do this while staying safe.
4. **Show them your offer.** You've learned what their problems are. Now, offer to fix one of them using your magical web development skills. Again, you can't do this in person, but I can show you how.

<!-- Once you've worked through the advice in this video, you'll need a price for your offer. I've got you covered. Get a copy of the first chapter of my book on pricing web development services by sharing your email below. 👇 I'll send you other resources to kickstart your career too!

[thrive_leads id='2463'] -->

## Video transcript

I always tell people to pick up your freelance clients by going out into the world and meeting people face to face. Now, during the COVID-19 pandemic in 2020, we're not really in a place that we can do that. Let me tell you what I am doing instead.

The reason I tell people to go meet people in person and be face to face with them, talk to them person to person is that that goes a long way toward short circuiting trust building. In order to get somebody to hire you, they have to trust you first. So you have to build some process around building up that trust from nothing if you don't know the person.

It's easier to get to know somebody in person. I don't really know why, it just is. When you can't do that, you might be tempted to just go set up your portfolio website and say, "Hey, I take all comers. Here's what I can do, and I want to do it for you. Send me your checks."

That's usually not going to work because you are tossing that website onto the giant pile that is the internet of every other freelancers website that says pretty much that same thing. And then the people you're trying to serve are not searching for anything like that. They have some problems they're trying to solve, and they're searching for solutions to those, but your website isn't talking about that.

Partly because you probably don't have a good idea of who you want to work for and who you should be talking to. That's why this moment in time, when we're all sitting at home, and we can't responsibly go out into the world and meet people face to face, it's even more important to think about who is your ideal client?

Once you know who that is, you're still not done. You've got a lot of steps to go before you can start building that trust, but it's going to be easier to build that trust if you can talk to someone in a way that they understand, and in order to do that, you have to know who they are.

So first step is think about who you want to do work for. As part of that consideration, you want to figure out who can pay you to do work for them. Who will you do work for that makes them able to earn more money? Those are the people that you can be working for.

The answer to that question might be completely different now than it was six weeks ago. So keep that in mind too, as you're picking that target audience.

The second thing you're going to want to do is you're going to go find where those people hang out online, where they are talking about their businesses, and find what their problems are.

Those are the things that you can talk about to let them know that you understand them, and they're also the things that you can solve to show them that you have something to offer that is valuable to them.

This is a piece of the puzzle that a lot of new web developers miss. They think the value is in being able to write code, but it's really not. The value is in being able to connect your skill, which happens to be writing code, to something that helps people do business. Once you can do that, you'll have a product or service that is valuable. And once you can explain that to them, you'll be able to sell it.

The third step in this process is to put yourself in front of them so that they can see that understanding. So that they can see the solutions you can offer to their problems. And there are lots of different ways you can approach that. You can do that by going back to the places you found your ideal audience, the forums, the subreddits, wherever you found them talking about their problems, and you can go start answering their questions. That gives you some visibility.

You can also start your own blog and post answers to those questions. If you do that consistently enough, you'll eventually hit on something or maybe several things that really resonate with your audience, and will start to bring people back to you through search engines or through social media.

Another way is by borrowing other people's audiences. There's someone out there already talking to the people you want to work for. What can you offer those people? So if they run a podcast, you could be a guest on their podcast, offer a new angle on some problem that maybe they haven't considered.

If they run their own blog, you could write a guest post. As someone who runs a blog myself I can say those types of things can be very enticing because not only does it give your audience a new perspective, but it gives you a little bit of time off that you're not having to just crank out new content all the time.

They might have a YouTube channel, in which case you could possibly do some sort of collaboration with them, and also provide them content just like you would with a podcast interview or a guest blog post.

The fourth and final step once you've established in their minds that you're an expert in solving problems in their field, you need to show them what paid offerings you have. What can they pay you to do? And just make that process as easy as you possibly can.

If you started a blog, maybe this is a form on your site where people can onboard as new clients. Maybe it's just a phone number you post somewhere that people call to get in touch with you, or an email address. Whatever it is now you've done the work to build the trust, or at least to start building the trust. Sometimes it takes multiple exposures to your expertise before they're ready to hire you, but your goal needs to be to make that as easy as possible, once they do reach that point of readiness.

I hope that you find this helpful as you're trying to build up a client base, even in this crazy mixed up world we're in. If you have any questions, I would love for you to post those here on YouTube in the comments below, or you can message me on Twitter. I'm raddevon there.

That's all for this time, stay safe, stay healthy, and I will talk to you in the next video.

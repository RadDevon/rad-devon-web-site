---
title: "Don't Chase the Hottest Technologies"
date: '2020-05-22'
tags:
  - 'career'
  - 'frameworks'
  - 'languages'
  - 'technologies'
coverImage: 'images/dont-chase-technologies-thumb.jpg'
description: 'Itʼs easy to get distracted and try to chase each hot new tech stack that comes along. Hereʼs why that makes it harder for you to get your first gig.'
---

https://youtu.be/118Tj-ETYBc

## Video Notes

[COBOL](https://en.wikipedia.org/wiki/COBOL) has been in the news recently because tons of people are applying for unemployment and it's breaking these ancient systems that were built in — you guessed it — COBOL.

I had to make this video after I saw [a post on Reddit](https://www.reddit.com/r/learnprogramming/comments/g5zvpa/psa_dont_try_to_learn_cobol/) giving new developers advice NOT to chase that COBOL money (if there even is any money to chase).

If you haven't even heard of COBOL, you'll probably be just fine without knowing about it. I took a COBOL course in college. That was almost 20 years ago, and COBOL was ancient even then. Today, it's almost 60 years old. To give you a frame of reference, Python, which is a very mature language, is 30 years old. Javascript is about 25.

The commenters' arguments for why you shouldn't chase down that COBOL money are great. My only critique is that you could take them even further.

My advice: stop chasing down every job requirement you see – frameworks, languages, dev ops tools… all of it. Keep learning, but do it on your terms instead of letting poorly written job listings pull you from stack to stack.

In this video, I explain why it hurts your chances of getting a job rather than helping them.

If you'd like to hear about another potential distraction that might delay your goals, check out [my article about the seductive powers of the coding tutorial](/articles/stop-doing-coding-tutorials/)!

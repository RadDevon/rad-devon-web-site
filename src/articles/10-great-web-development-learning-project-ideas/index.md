---
title: '10 Unique Web Development Learning Project Ideas'
date: '2018-07-26'
tags:
  - 'learning'
  - 'motivations'
  - 'projects'
  - 'tutorials'
coverImage: 'images/man-working-on-project.jpg'
description: 'If youʼre trying to practice web development but arenʼt sure what to build, youʼve come to the right place. I have a notebook full of web app ideas you wonʼt find anywhere else that Iʼm opening up to you!'
---

In this post, I share 10 project ideas straight out of my notebook. These are not your run-of-the-mill to-do list apps. Each one will be a fun challenge to build, and, as a bonus, they won't bore you to tears! 😂 If you want a jump-start on your project, grab [**your free project starter kit**](#timeless-starter-kit-form) which gives you a logo, wireframes, HTML, and CSS so you can get to building quickly!

![Man working on craft project](images/man-working-on-project.jpg)

The best way to learn web development is to find [a project that will push you forward](/articles/the-best-projects-for-learning-web-development/). Tony Mastrorio has [a great article about getting out of "tutorial purgatory."](https://medium.freecodecamp.org/how-to-escape-tutorial-purgatory-as-a-new-developer-or-at-any-time-in-your-career-e3a4b2384a40) Everything he says in it is spot-on.

> …if you’re going through tutorial after tutorial just for the sake of learning and not as part of a larger project you’re working on, you’re probably learning a lot less than you think.
>
> Tony Mastrorio

## What's wrong with Tutorials?

Tutorials are nice and neat. Buttoned up. If you find a good tutorial, everything **just works** as it should. They're comfortable like a warm blanket. You know you can complete them if you just follow the steps. That's not the reality of web development. **It's messy.** You have to compromise, cut features, and hack your way past the finish line. No client will ever give you step-by-step instructions on how to complete their task. If they could do this, **they wouldn't need you**. Tutorials _feel_ like you're making progress. Instead, you're not learning most of the skills you'll use in **real work**. Projects are not like this. They're loosely defined. No one has done the exact thing you're doing, so there is no script. Once you've learned the very basics through books and tutorials, **it's time to get your hands dirty** with a project.

## What Kinds of Projects Are Best?

The best project ideas solve your own problems (I never wake to an alarm. I need something better.) or fulfill your own desires (I want a place to hang out with other people who collect video game strategy guides.). Sometimes, you'd like to work on an idea that inspires you, but you're not sure what yet. This list will help get your creative juices flowing.

[thrive_leads id='4451']

## 10 Great Project Ideas

Think of these like writing prompts. Take these ideas and contort them into your own beautiful disasters. If you *really* want, you're welcome to build them as described here, but, if you do, you're missing the point. Your project should be your own.

### What the Stock

Flip card for description A stock photos site with only weird or unique photos. Crying clowns, abandoned malls, giant wicker chairs… that sort of thing.

### Promo Club

Social network where people submit things they've created like articles they've written, products they've released, or YouTube videos. You view a list of them and promote the ones you like on your social media accounts. More promotions of other people's stuff buys your own posts a higher position on the site.

### Fog of Explore

A gamified GPS exploration app. You see a real-world map, but most of the map is covered in fog. (Gamers will recognize this as the "fog of war" common in real-time strategy games before you have explored an area.) As you explore the real world, the map is revealed. Users can define an area and see the percentage of that area they have explored.

### Pinboard Reader

Anyone use Pinboard? It's a great social bookmarking app that picked up the pieces after del.icio.us dissolved. They have a cool "to read" feature where you can bookmark an article and mark that you want to read it later. Pinboard Reader is an Instapaper/Pocket-style app that hooks into their API.

### Tangled Web

A blogging platform which automatically links between users' posts. It matches words and phrases common in posts, preferring longer matches. For example, if this project description were a post on the platform, the words "blogging platform" might automatically be linked to another user's post on the top blogging platforms in 2018. Links change over time.

### The Fam

OK, so the names of these aren't all home runs. 😉 Did you ever try Miitomo? Nintendo's social network? I loved the way it used prompts to spark interaction between friends. The Fam does this for families, getting them communicating, learning more about one another, and becoming closer through smart, engaging prompts.

### Timeless

The social network that only lets each user post once per year. Better make it good!

### Note Log

A notes app with multiple entries per note. When the user launches the app, they select a note. This adds a new entry for that note automatically tagged with the current date, time, and location. Entries can be sorted and filtered on this metadata. Great for resolving messy problems and tracking what you did.

### Exit Strategy

A widget or plugin that pops up a modal on exit intent. (You've probably seen these before. You're about to close a page when a modal pops up giving you a "final offer" of sorts to keep you from leaving.) The twist is that the content of the modal is customizable based on how long the user was on the page.

### Out on a Limb

A daily “exercise” app for entrepreneurs. Instead of a physical workout, this gets users to step outside their comfort zones through one activity per day. This helps build the risk-taking muscles entrepreneurs need and erodes their fear of rejection.

My only ask is that, if you build one of these, please share it with me. I'd like to become one of your early users. 😜

## I thought these were for web development practice. Some of them say “app” right in the description.

"App" could mean a web app or a mobile app. Even building a mobile app, you can still use the same tech you'd use to build a web app. Check out [React Native](https://facebook.github.io/react-native/).

## These seem hard. I don't think I know enough to build them.

Some are harder than others. What the Stock might be the simplest one on the list.

That said, the point of choosing a personal project is to pick something that **reaches past your current skill level**. This is what drives your learning. Now, instead of learning the things someone _told_ you to learn or the framework that is mentioned most on Hacker News, you're learning the things that get you closer to your own personal project goal.

When you find your personal project, **start building the part you can build given what you already know**. Once you get to a barrier — a piece you can't build yet — learn that next.

## Where to Start If I Know…

### Nothing

Now, this is when it's perfectly valid to read some books or do some tutorials. Go get a baseline understanding of HTML and CSS. Come back here when you're done and start working on a project.

If you prefer interactive tutorials, Codecademy's [Learn HTML](https://www.codecademy.com/learn/learn-html) and [Learn CSS](https://www.codecademy.com/learn/learn-css) offerings are pretty good. (The free lessons are fine. No need to spring for the pro stuff.) If books are your jam, [Head First HTML & CSS](https://amzn.to/2LtFgBb) is a good way to go.

### HTML & CSS

You can build the user interface for any of these projects with just knowledge of HTML and CSS.

Here are the projects you can come closest to finishing with just this knowledge:

- **What the Stock**\- You could just write this one as a static site with just HTML and CSS although doing payments for images could be tough.
- **Out on a Limb**\- Build this as a simple HTML site with a list of exercises.

### Front-End Javascript

If you've learned front-end Javascript, you can now start adding some interactive elements.

You can also save data, but it will live in the user's browser. That means, every user of the browser will have the same data. If you were to build Note Log this way and I tried to use it on your computer, I would see your notes instead of mine. My notes would be saved in my browser on my own computer.

At this point you can potentially complete almost any of these projects. For those that require a back-end, you can use a back-end as a service like [Firebase](https://firebase.google.com/).

These projects are a home run if you know HTML, CSS, and Javascript:

- **Pinboard Reader**\- Since this one will use [Pinboard's API](https://pinboard.in/api/), it doesn't matter much that you don't know how to build a back-end.
- **Note Log**\- With a little Javascript, you can create a version that saves notes in the browser only or use your back-end as a service to save notes server-side
- Out on a Limb- Now, you could either randomize the exercise you show or show a specific one each day.

### React/React Native

Knowing React opens you up to mobile app development by way of React Native. This is a great time to try your hand at **Fog of Explore**.

### Back-End

The back-end lets you save data into a database. This is how social networks and other web apps let you have settings and content that follow your user account on that site. If you log into Facebook on any computer, you'll always have the same friends. If someone else logs into Facebook on your computer, they get their friends, not yours. The database makes this possible.

Having all the site's data in a single place allows you to show users content created by _other_ users. This is how Facebook shows you posts written by your friends. Without a centralized place to store the posts, Facebook couldn't do this.

A back-end also lets you do computation on the server instead of in the user's browser. Once you understand back-end development, you can tackle any of these projects and do it justice.

## What to Do Next

Be sure to grab [your free starter kit](#timeless-starter-kit-form) while you're here so you can quickly get up and running. Procrastination is the enemy of learning. You could download the kit, go through the wireframes, and start writing code within 5 minutes.

Building projects is way better than having your hand held through a tutorial, but what's even better is building a project you've conceptualized that has a special meaning or significance to you. Read my article on [how to generate your own project ideas](/articles/where-do-web-development-project-ideas-come-from/).

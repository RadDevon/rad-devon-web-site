---
title: "Let's Write Our Own Module in Node.js"
date: '2020-04-10'
tags:
  - 'backend'
  - 'javascript'
  - 'node'
  - 'node-js'
  - 'nodejs'
  - 'tutorials'
coverImage: 'images/learnyounode-part-2-thumb.jpg'
description: 'Iʼll teach you to filter and list the contents of a directory and to create and use your own Node module!'
---

https://youtu.be/a35evCZbLoM

## Video Notes

We started learning [Node.js](https://nodejs.org) together in [a recent video](/articles/first-steps-with-node-js-part-1/). We'll continue through the Node.js workshop in this video.

📝 In this part, we'll work through exercises 5 and 6 of the [Learn You Node workshop](https://github.com/workshopper/learnyounode) together. Exercise 5 is to list the directory contents filtered for an extension. Exercise 6 lets us try creating and using a Node module. This is an essential part of being a Node developer!

📚 As you work through the exercises, you'll want to reference [the Node.js documentation](https://nodejs.org/dist/latest-v12.x/docs/api/).

👋 Thank you for watching and for working through the exercises with me! I'm going to keep this series going and work through the remaining exercises in coming weeks. If you have questions, please comment on the video [over on YouTube](https://youtu.be/a35evCZbLoM) or mention me [on Twitter](https://twitter.com/raddevon).

Don't fall into the trap of working through [tutorial after tutorial](/articles/stop-doing-coding-tutorials/) and never getting anything done. **Make sure you apply these skills** toward your ultimate goal of becoming a web developer.

<!-- One way to do that is by [**doing freelance work**](/articles/inverted-career-path/), but how do you price it? I wrote the book on the subject! Get a free chapter by entering your email address below. 👇

[thrive_leads id='2463'] -->

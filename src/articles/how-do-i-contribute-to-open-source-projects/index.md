---
title: 'How do I contribute to open source projects?'
date: '2018-12-21'
tags:
  - 'career'
  - 'open-source'
  - 'projects'
coverImage: 'images/swiss-open-cultural-data-hackathon-2015.jpg'
description: 'Contributing to open source, besides being a great way to get experience, can also bring some welcome attention to career switchers. Here are 4 steps you can take to start contributing.'
---

![Swiss Open Cultural Data Hackathon 2015](images/swiss-open-cultural-data-hackathon-2015.jpg)

Photo used under CC license from Swiss National Library; Simon Schmid, Fabian Scherler

## The Mechanics of Contributing

Contributing to open source can be done in 4 steps.

1. Find an opportunity
2. Fork the repository
3. Make changes on your fork
4. Submit a pull request

The first 3 steps have been covered ad nauseam. If you're interested in the mechanics of making a pull request, Kent C. Dodds has created [**a great resource on submitting your first PR**](http://makeapullrequest.com/).

The mechanics of submitting your pull request are, unfortunately, the easy part. They're the same no matter who you are or what project you're contributing to. The _real_ magic happens at step 1: **finding an opportunity**.

## 2 Ways to Find an Opportunity

Aspiring web developers are interested in making contributions to open source because they believe it makes them look better to prospective employers. I ran a quick informal poll on Twitter to find out if this is actually the case. The responses were interesting and maybe a little surprising.

(Some of the responses have been deleted since this article's publication.)

https://twitter.com/stylianipantela/status/1064524496659468288

It looks like, at least for most of the people who responded, open source contributions are _not_ a factor they consider. For the one respondent who said they do look at it, I get the sense you wouldn't be disqualified if you _hadn't_ contributed to open source; it's just a bonus if you have.

Even if it may go unnoticed in your job search, contributing to open source is a worthwhile pursuit. You can **help the developer community**, **make yourself more visible**, and **solve some of your own problems**. Here are the two ways you might approach finding where _you_ can contribute as a new developer.

### Seeking Out Opportunities

This is the approach most new developers take since they want to start touting that contribution in their interviews. In my experience, **this is the more difficult approach for newbies**. Ideally, you're going to pick a library you've used before so you're at least somewhat familiar with it. Then, you'll browse the repository's open issues to find one you think you can tackle.

If you want to contribute code, you'll need to gain some familiarity with the codebase first so that you know where you need to be working to add the desired feature or to fix the bug you're targeting. It's a pretty big ask for a new developer: grab an issue, figure out the codebase, _and_ write a pull request to address the issue. **Reading code is _not_ an easy skill** and is a big part of this process.

If you're going to go this route, here are some ideas and resources to make your life easier.

- If you want to contribute to a library you're unfamiliar with, **start with the documentation instead of the code**. This is a natural fit for you since you'll be using the documentation to learn the library anyway. You're bound to find weaknesses. When you find them, clean them up for the next person who reads the docs.
- Libraries often have labels to identify newbie-friendly issues. React, for example, has a label called "[good first issue](https://github.com/facebook/react/issues?q=is%3Aissue+is%3Aopen+label%3A%22good+first+issue%22)." Seek these out to find an issue you can tackle.
- Many sites curate good opportunities for newbies to contribute to open source. Check out [Up For Grabs](https://up-for-grabs.net/#/) or [the Awesome First PR Opportunities repo](https://github.com/MunGell/awesome-for-beginners) for a couple of good examples. Since the awesome list is itself a repository on Github, you could actually contribute to it by adding other good newbie opportunities you find. So meta!
- [Hacktoberfest](https://hacktoberfest.digitalocean.com/) is an annual event encouraging contributions to open source projects. The event helps you find great opportunities and rewards you with a t-shirt for!

Since this is the hardest road to take and since it doesn't seem to have much impact on your job prospects, I'd avoid this approach in favor of a more "organic" approach to finding opportunities to contribute to an open source project.

### Waiting for an "Organic" Opportunity

When you stop trying to force things, you'll find it much easier to contribute to an open source project… although it might take more time for the opportunity to present itself.

My first merged pull request was for a library called [Telescope](http://www.telescopeapp.org/) (now [VulcanJS](http://vulcanjs.org/)). It was a framework built on [MeteorJS](https://www.meteor.com/) to make it easy to set up Reddit-style link voting social networks. I was using the framework to build a news sharing site for indie game developers called GameDev News.

Telescope made my life a lot easier, but **it lacked one feature I really needed**: it didn't expose comments through the API. When you made a request, you would get the posts but no comments.

This was a great opportunity for me to start contributing to open source for a few reasons.

1. **I was already familiar with the codebase.** I had been using the framework to build my site for a few weeks already.
2. **It was solving my own problem.** That's loads of motivation to dig into someone else's code and make a contribution.
3. **I didn't have to go hunting for an opportunity.** The opportunity came to me.

I wrote a few lines of code to add the feature and submitted the pull request. The maintainer got back to me with a problem he saw while reviewing. I fixed that, and [**the pull request was merged**](https://github.com/VulcanJS/Vulcan/pull/378)!

All of my contributions have fit this mold. In one case, I found a library that sounded like it would do what I needed. The only problem was, **it didn't work**. It simply failed to do what it claimed to do! I checked out the code to see if I could find the problem. It was really simple: the library was looking for a function to return a truthy value, but that function didn't return anything.

I fixed it with two lines of code and [submitted a pull request](https://github.com/w-m/pleonasm/pull/6). That one still hasn't been merged, (The library appears to be completely abandoned.) but I was able to use my fixed fork in the project I was working on.

If you keep building and keep your eyes open, **you'll find many of this kind of opportunity** for you to fix your own problems while also contributing to open source.

Need some help navigating all this and increasing your chances to become a professional web developer? Set up a free mentoring session, and we'll figure out your next steps together!

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step]

## Don't Sweat the Timing

If the responses to my tweet are any indicator, you're probably not going to get a job on the back of your contributions. That means **the timing isn't important**. Instead of trying to force it, just let the opportunity to contribute to an open source project come to you. It's a fantastic goal, but you don't have to accomplish it today.

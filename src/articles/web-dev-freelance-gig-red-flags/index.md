---
title: 'Web Dev Freelance Gig Red Flags 🚩'
date: '2020-08-28'
tags:
  - 'career'
  - 'freelancing'
coverImage: 'images/freelance-gig-red-flags-thumb.jpg'
---

In this video, I'll take you through a few postings for freelance web development gigs on Reddit's [forhire subreddit](https://reddit.com/r/forhire). We'll find which postings sound like they could be a problem for one reason or another.

https://youtu.be/Yj2FNAig98k

You shouldn't necessarily disqualify one of these gigs immediately just because you find a problem with it. Just make sure you're aware of potential red flags and that you factor that into your decision about whether to pursue and then ultimately take on the gig.

<!-- If you're brand new to freelancing, these tips are a great starting point! 👇

[thrive_leads id='1378'] -->

---
title: "COVID-19 can't stop you from becoming a web developer"
date: '2020-03-20'
tags:
  - 'career'
  - 'motivation'
coverImage: 'images/covid-19-thumb.jpg'
description: 'This virus may change a lot of things, but it ultimately doesnʼt change what you need to do to see the transformation you want.'
---

https://youtu.be/C7kRDqIPeL8

## Resources for the End of the World

I hope you're OK, and I hope you can find the strength to keep pushing forward. This virus may change a lot of things, but it ultimately doesn't change what you need to do to see the transformation you want.

Here are the resources I mentioned in the video to help you keep going:

- 💌 First things first. Here's my email address: [devon@raddevon.com](mailto:devon@raddevon.com)  
   Email me if there's any way I can help you, if you need to get something off your chest, or just if you need a friend. We're in this together.
- If you're not sure what to do right now, I recommend these three things for starters:
  1. 📗 Check out my video on [_The Obstacle Is the Way_](/articles/books-that-made-me-obstacle-is-the-way/). It's a gentle introduction to stoicism that's never been more relevant than it is right now.
  2. 🧘‍♂️🧘‍♀️ Try meditation. It can teach you to disconnect from your brain when it's filling you full of anxiety and doubt. [Waking Up](https://wakingup.com/) is a great guided meditation system that's a little pricey, but it has worked better for me than any of the alternatives. Find whatever works best for you and run with it. (P.S. I don't actually meditate in the lotus position like in the emoji, but who knows? Maybe you will. 😉)
  3. 💻 Code. 🔄 Every. ☀️ Day. Don't take a break. Don't wait for the economy to straighten out. Don't wait for the virus to blow over. Just keep practicing.

Feel free to reach out using the email above, [in the YouTube comments](https://youtu.be/C7kRDqIPeL8), or [on Twitter](https://twitter.com/raddevon).

## Transcript

\[Bicycle skidding.\] I'm okay!

Hey, this is Rad Devon. I'm out for a walk tonight. I joke with people that I'm an avid indoorsman, but I've been staying in even more than normal. So this is a good night for a walk.

I wanted to share some thoughts with you on this thing that we're all sort of collectively dealing with right now. I assume you're watching this because you have some sort of goal that you're working toward.

You probably feel like the world has thrown a wrench into that goal in the last few weeks. The world you were trying to transition careers in just maybe a month ago is not the world we're living in right now. And you might feel like your brain is telling you that you need to stop and let all this pass.

I find that my brain tries to shift me off of difficult paths and get me back onto the easy path when I'm trying to do something that's hard. We can't let it, even when the situation we find ourselves in is something that we've never experienced before. Don't try to wait this thing out. We don't know when the world is going to be back to normal or if it ever will be.

Your job now is the same as it's always been. You need to show people that you can solve problems for them using web development. You need to show them that you can make them, or save them, some money. That hasn't changed and it's not going to change if you're not sure what you should be doing right now, I want you to try these things.

First, read [_The Obstacle Is the Way_](/articles/books-that-made-me-obstacle-is-the-way/). If you've already read it, this is a great time to read it again. It teaches you about stoic philosophy and that can help you get through this time.

Meditate. I like to use the Waking Up app for guided meditation, but you can use anything that works for you. Meditation can be a great tool for helping you get through difficult times.

And last but not least, keep writing code. Make sure you write code every day and keep learning your craft. Now, before I lose this last bit of sunlight, I want to show you the awesome sunset.

I was meditating a few days back to start my day. I had all these crazy worries and anxieties running through my head. Is this just the way things are now? Am I still going to have work in six months? Is my family going to have a place to live? Are we just turning into The Walking Dead? And while this was all circling around in my head, I started hearing a bird outside the window singing.

It's still cool in Seattle, so we're not, we're not really used to hearing that yet. And it made me realize that the world is just still going and it doesn't really realize what we're dealing with. Just like this awesome sunset we have tonight that's still incredible and amazing. In spite of the fact that all of humanity is dealing with this massive health crisis.

Your life is going to keep going, too. Don't stop making progress toward the life you want. The world around you is never going to be stacked fully in your favor. So your time might as well be now.

Stay healthy, take care of each other, reach out if I can help or if you just need a friend. Thanks. And I'll talk to you next time.

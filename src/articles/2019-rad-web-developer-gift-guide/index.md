---
title: '2019 Rad Web Developer Gift Guide'
date: '2019-12-13'
tags:
  - 'freelancing'
  - 'gifts'
  - 'hardware'
coverImage: 'images/santa-looking-at-rad-devon.jpg'
description: 'I still use most of these gift ideas today. Any of them would be great for the web developer in your life (or for yourself if thatʼs you).'
---

![Santa looking at Rad Devon](images/santa-looking-at-rad-devon.jpg)

Looking for gifts to give the web developer in your life (or even ideas for some gifts for yourself 😉)?  Here are a handful of ideas that will make a web developer's life easier and their work even better!

(If you don't find something you like here, I made [a web developer gift guide in 2018](/articles/2019-gift-ideas-for-new-and-aspiring-web-developers/) with some ideas that will still work great this year. I tried not to recommend the same items this year except when they are meant to replace last year's pick.)

## For Any Web Developer

[![Ducky One 2 mechanical keyboard](images/ducky-one-2.png)](https://amzn.to/2sXYqH1)

### Ducky One 2

[Last year](/articles/2019-gift-ideas-for-new-and-aspiring-web-developers/), I recommended the Das Keyboard Pro as my keyboard of choice. This year, I have to retract that. Not only is the Das Keyboard pricey, but mine broke after just a few years of use when I would have expected a much longer life out of it.

I ended up replacing it with this curiously named [**Ducky One 2**](https://amzn.to/2sXYqH1) which is less than half the cost. Despite that, it's still a high-quality keyboard with a nice hefty weight and clicky Cherry MX key switches that I love.

The only thing I miss is the fancy volume knob on the Das Keyboard, but the volume keys on the Ducky One 2 do the same thing… they're just not as fun to fiddle with. 😉

[![Audio Technica mic](images/audio-technica-mic.png)](https://amzn.to/348gL0Y)

### Audio Technica Cardioid Condenser USB Microphone

Why would a web developer need a microphone? I'm glad you asked. Here are a few of the things I use mine for:

- meetings with clients
- recording progress update videos (great for showing progress as you move through a long project)
- mentoring other developers
- content marketing (Podcasts and YouTube videos are great ways to get more clients.)
- capturing the by-products of my work (Learn something cool while doing client work? Make a course out of it and sell it on Udemy!)

It's a versatile tool with lots of handy uses. A good one doesn't cost all that much but makes a huge difference in sound quality. This [**Audio Technica mic**](https://amzn.to/348gL0Y) has worked great for me over the past year!

## For Web Developers on Newer Macs

[![HP Thunderbolt Dock](images/hp-thunderbolt-dock.png)](https://amzn.to/2saQhyF)[![Portable MacBook Pro dock](images/macbook-dock.png)](https://amzn.to/348gL0Y)

### MacBook Pro Docks

Newer MacBook Pros have just a few USB-C ports. To fully utilize these machines, you need some kind of dock to give you more connectivity options. I use an [**HP Thunderbolt Dock**](https://amzn.to/2saQhyF) at home and this [**Wassteel dock**](https://amzn.to/36s1rxX) on the road.

When I'm at home, the HP dock allows me to connect a single cable to my MacBook Pro to connect two DisplayPort monitors (see [2018's guide](/articles/2019-gift-ideas-for-new-and-aspiring-web-developers/) for more on those), a bevy of USB devices (most connected through the aforementioned monitors), and power. It makes my transition from mobile to desktop workstation super quick and easy.

The Wassteel dock just gives me more options when I'm out. I have HDMI, an SD card reader, and several USB ports, not to mention I can charge the computer through the dock, which is very convenient. I actually use this at home too just because the card reader is so convenient.

## For Standing Desk Owners

[![Topo standing mat](images/topo-standing-mat.png)](https://amzn.to/2YBEZzz)

### Topo Standing Mat

I stand at least eight hours a day, every day. I have a motorized standing desk, but I only use the motor maybe five or six times a year. I don't even own a desk chair; that's how committed I am to the standing lifestyle.

I do own this nice [**Topo standing mat**](https://amzn.to/2YBEZzz), though, and it gives me several different ways to stand to keep me moving and healthy. If you stand all day like me, you need to change your stance throughout the day to keep your feet from getting fatigued. It's simple, but it has changed the way I stand at my desk… in a good way!

## For Developers _Without_ a Standing Desk

[![Uplift standing desk](images/uplift-standing-desk.jpg)](https://amzn.to/2YBEZzz)

### Standing Desk

This is easy! Get them a standing desk! Sure, it may be a pricey gift, but it's an investment in your web developer's health. The [risks of extended sitting](https://www.mayoclinic.org/healthy-lifestyle/adult-health/expert-answers/sitting/faq-20058005) have been documented: it represents a risk of death similar to smoking or obesity. A standing desk gets the web developers we love back on their feet and, in a very real way, could give them longer lives.

I got myself a **NextDesk (now XDesk) [Terra](https://www.xdesk.com/terra)** about 5 years ago, and I love it. It's one of about five pieces of furniture that made the move with me from Knoxville to Seattle. It's sturdy, smooth to raise and lower, and it looks good too.

The standing desk landscape has changed dramatically though. Where it used to cost thousands of dollars to get a good standing desk, you can now get one for hundreds instead. If I were buying a new desk today, I'd go with the [**Uplift V2**](https://www.upliftdesk.com/uplift-v2-standing-desk-v2-or-v2-commercial/). It's by all accounts an excellent desk for a very reasonable price.

## More Ideas

I shared **[some great last second gift ideas](/articles/last-second-gifts-for-web-developers-in-2019/)** that you can buy and send immediately. You should also check out [**my 2018 gift guide**](/articles/2019-gift-ideas-for-new-and-aspiring-web-developers/) for more ideas that are still totally relevant today: a great mouse, monitors that will make any web developer more productive, a headset that will keep your audio from echoing back into the microphone when you're holding remote meetings, and more.

<!-- Give the gift of independence to the web developer in your life! Send them here to sign up for my free Freelancing Crash Course. In just a week, I'll give them the knowledge they need to start their freelancing business. -->

[thrive_leads id='2262']

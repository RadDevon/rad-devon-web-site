---
title: 'The Magic Freelance Rate Formula'
date: '2019-12-06'
tags:
  - 'business'
  - 'freelancing'
  - 'pricing'
coverImage: 'images/cook-up-your-rate.jpg'
description: 'This magic formula will help you set a reasonable rate for your freelancing service with some very simple division. You wonʼt even need a calculator!'
---

![Magic student cooking a potion](images/cook-up-your-rate-1024x621.jpg)

If you're brand new to freelancing as a web developer, one of the first questions you need to answer is, **"How much do I charge?"** The fastest, easiest way to answer this question is using the magic freelance rate formula. It's a two-step process:

1. Think of a yearly salary **that would allow you to live comfortably**. If you were looking for a full-time job, what would you want to be paid?
2. Divide that yearly salary **by 1,000**.

That gives you your hourly rate. If your ideal salary is $40,000, you need to charge $40 an hour. If you want to make, $58,000 a year, charge $58 an hour. If you want to cross that six-figure mark and make $100k, charge $100 an hour.

The magic formula **isn't perfect**, but it gets you pretty close to a rate that should work for you. It has enough headroom built in to account for many of the [costs and factors you might not think of](/articles/things-you-may-not-know-about-freelance-web-development-pricing/) when trying to determine your own rate, including allowing you to pay for [your own benefits](/articles/how-do-freelance-web-developers-cope-with-losing-their-benefits/).

If you're looking for more accuracy, or if you want to be absolutely sure you have all your bases covered, the magic formula may not be for you. Check out my book [Freelance Web Development Pricing](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/) instead.

<!-- It's the definitive guide to all things pricing when it comes to freelancing as a web developer. Drop your email address in below to read a free chapter! 👇

[thrive_leads id='2463'] -->

---
title: 'How to Stay Motivated While Learning Web Development'
date: '2017-12-03'
tags:
  - 'motivation'
coverImage: 'images/sisyphus.jpg'
---

import {MailerLiteEmbed} from '../../components/mailerlite-embed';

Web development is difficult to learn, especially when you're first getting started. It's easy to get stuck on a problem and end up quitting. **Many people go through this cycle several times** before they can harness the motivation to see it through to the end. Even more people _never_ get that far. ![](images/sisyphus.jpg)

## My Breakthrough

I went through this cycle myself, and I discovered a technique that transformed me from someone who had dabbled in HTML, CSS, and Javascript into a professional web developer in a very short time. Once I put my technique into practice, I was able to quickly move from making a modest salary of $30,000 a year in a job I didn't particularly enjoy to starting my own business doing freelance web development making roughly twice that — all while working from wherever I wanted, picking exactly who I would work for, and taking vacations whenever I chose.

I wasn't done yet though. A couple years after that, I accepted an amazing full-time position while continuing to offer my own services on the side.  I now have much greater job satisfaction than before. My life is unrecognizable from the one I lived for many years.

Now, I'm writing this post from Seattle and working on achieving some even bigger goals that would have seemed totally out of reach just a couple of years ago.

## Using a Big Goal for Leverage

The solution to keeping my motivation up wasn't magical, but it was so effective for me, it may has well have been. I set a Big Goal — that is, something I wanted to achieve that could be achieved in a year or two but would have a huge impact on my life. For me, that goal was to move from my hometown of Knoxville, Tennessee to Seattle, Washington ([where the cost of living is roughly double](http://www.bestplaces.net/cost-of-living/knoxville-tn/seattle-wa/50000)). An achievable goal but by no stretch of the imagination easy. I knew learning web development could get me there.

My reasons for wanting to move aren't really important, but they were compelling enough that I thought about it all the time. Knowing every aspect of transitioning to a this career would be tough (learning, finding work, setting rates, billing, evaluating job offers...), I came up with ways to remind myself of the goal. I frequently did fantasy apartment shopping on Craigslist. I made posters for my work area to remind me of my goal. I subscribed to email newsletters about Seattle. I did anything I could to keep my goal fresh in my mind when times got tough. It gave me the leverage I needed to push through any obstacles.

## Your Next Steps

Now, it's your turn. Stop failing to achieve your goals. Here are three simple steps to put you — and more importantly *keep* you — on the track to becoming a web developer:

1. Discover your Big Goal
2. Find ways to constantly remind yourself of your Big Goal
3. Start learning!

Let me help you out with the first step. Sign up below, and I'll help you find your own Big Goal so you can keep going when the going gets tough.

<MailerLiteEmbed formUrl="https://dashboard.mailerlite.com/forms/90414/60928458118661464/share" height="401px" />

---
title: 'Web Developer Roadmap Step 4: Learn Terminal, Git, and Deployment'
date: '2020-01-31'
tags:
  - 'frameworks'
  - 'git'
  - 'self-improvement'
  - 'shell'
  - 'terminal'
  - 'vue-js'
coverImage: 'images/thumbnail-3.jpg'
description: 'Thereʼs way more to a development role than writing code all day. You need to have a toolbelt full of other technical skills too. Here are some of the big ones.'
---

https://youtu.be/fn4kJcbk72Q

## Video Notes

This step is all about the technical pieces around the code you'll write as a web developer. I almost forgot to make this video, but you shouldn't forget to learn this stuff. Watch to learn why it's important.

### Terminal

- ⭐️ [Jesse Showalter](https://www.youtube.com/user/JesseAtomic)'s **[How to Use the Command Line](https://www.youtube.com/watch?v=5XgBd6rjuDQ)**
- [A Designer's Guide to the Terminal](https://react.design/terminal) (Don't worry. Even though it says it's for designers, it's still a useful resource for use devs.)
- For Windows 10 users: [How to Activate Bash in Windows](https://www.laptopmag.com/articles/use-bash-shell-windows-10)
- For other Windows users, you can get a Bash shell when you install Git for Windows. Just make sure you have "Git Bash" selected in the installer.
- P.S. "Bash" is just a particular Unix shell. Actually, it's sorta the standard Unix shell. A lot of times, "shell," "terminal," and "command line" are used interchangeably.

### Git

- ⭐️ **[Git It](https://github.com/jlord/git-it-electron)** interactive course. Follow the instructions in the "[What to Install](https://github.com/jlord/git-it-electron#what-to-install)" section of the readme to get started.
- I featured some footage from [a sweet Git Bisect video](https://www.youtube.com/watch?v=P3ZR_s3NFvM) by [Andy Van Slaars](https://www.youtube.com/channel/UC1EPLJ19SMAUpu0LOt8a4IA).

### Deployment

There's no single course that's going to teach you deployment since it's different depending on your target. Your best bet is to check out the documentation wherever you're going to deploy. Here are a few options:

- [Netlify](https://docs.netlify.com/#get-started)
- [AWS S3](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html)
- [Github Pages](https://help.github.com/en/github/working-with-github-pages)
- [Heroku](https://devcenter.heroku.com/)
- [AWS Elastic Beanstalk](https://docs.aws.amazon.com/elastic-beanstalk/index.html)
- [DigitalOcean](https://www.digitalocean.com/)
- [Vultr](https://www.vultr.com/resources/)

Hope you enjoyed this video! Feel free to ask questions or leave your thoughts in the comments [on YouTube](https://youtu.be/fn4kJcbk72Q) or to me directly [on Twitter](https://twitter.com/raddevon). [Subscribe on YouTube](http://www.youtube.com/channel/UCxgZYWJq2Cxk7vIePGiTxFw) so you don't miss the rest of the roadmap!

## Other Videos In this Series

If you like this video, watch the others in the series for a good overview of the philosophy I used to break into web development with no connections or experience.

- [Step 1: Set Your Big Goal](/articles/2020-web-developer-roadmap-step-1-set-your-big-goal)
- [Step 2: Learn HTML and CSS](/articles/2020-web-developer-roadmap-step-2-learn-html-and-css)
- [Step 3: Learn Javascript](/articles/2020-web-developer-roadmap-step-3-learn-javascript)
- [Step 5: Relationships Are Make or Break](/articles/2020-web-developer-roadmap-step-5-getting-work-through-relationships)
- [Step 6: Time to Get Paid!](/articles/2020-web-developer-roadmap-step-6-time-to-get-paid)

## Transcript

Hey everybody, this is Devon coming to you with a video that almost didn't get made. I looked back at the videos I already had and realized this one was missing. Let's take some time to talk about the Unix terminal, Git, and deployment.

There are a lot of skills that make a web developer besides just being able to write code, and it turns out that's true in the technical sense and not just in the soft skills sense. Later on in this series, we're going to be covering some soft skills that are really important, but there are also some technical skills around just HTML, CSS, and Javascript that make a fully fledged web developer.

Today I'm going to talk about three of those that I think are really important to understand and have some familiarity with before you start trying to actually get work. Let's start off talking about the Unix terminal. A lot of web servers run on Linux. If you need to jump onto your web server and make some configuration changes, or maybe even in rare circumstances, just live update your code, you'll need to have an understanding of the Linux command line and how to use that in order to make those things possible for you.

Just a quick example of something you'll need to use the command line for in just your day to day development tasks. If you want to start a React app, you're going to do that using the React command line interface, or CLI, and you'll do that by running the command "create-react-app" that will bootstrap your new React project.

You'll also use the command line for something we're going to talk about later in this video, and that is version control using Git. You don't have to use the command line to use Git version control, but if you use a GUI instead, you're going to miss out on some of the features and you're probably not going to understand what's happening under the hood quite as well as if you're using it from the command line.

A lot of people would probably argue that understanding the Unix command line is not absolutely necessary to becoming a web developer and I can see those arguments, but my counter would be that if you… unless you know that you're going to be all Windows all the time, I think it's probably a good skill to have.

If you _do_ know that you're going to be all Windows all the time, then what are you doing watching these videos? You probably don't need me anyway. It was a little bit challenging to find good resources on the Unix command line, but I did find a great one. It's a short video on YouTube by a guy named Jesse Showalter, and it's called "How to Use the Command Line."

He walks you through, in about 15 minutes, some basic commands, and he helps you understand how to navigate through your computer's file system using the command line. That's sort of one of the foundational concepts, one of the foundational skills you need to have to effectively use the command line. It's a really gentle introduction, and I think he does a good job of exposing people to this who maybe haven't had any exposure to a Unix command line.

I also found a really nice textual reference for using Unix command line. It's called "A Designer's Guide to the Terminal." We're not designers here, but I still think it does a good job of exposing these concepts and giving you a quick reference for people who are maybe not so familiar with the command line.

Eventually, this mode of operating on your computer is going to be so second nature that you won't need any of this, but when you're starting out, it's nice to have these resources to help you along. If you're on Windows, you're going to have a little bit harder of a time because Windows does not ship with the Unix command line already set up and ready to go.

If you're running Windows 10 you can activate it, and I'm going to provide a link in the description with some information on how to do that. If you don't have Windows 10 you can get a Bash command line when you install Git for Windows. So when you install, Git for Windows, you'll get something called Git Bash, and you can use that Unix command line to do any of the things you would do with a native Unix command line for the most part.

The key to learning the terminal is just to put in the time. It's going to feel uncomfortable, so you're going to have to push through that and just use it for things you would otherwise use your GUI for. Use it to do some file management tasks when you're working on a project – deleting files, deleting folders, creating new folders, renaming folders, renaming files, whatever the case is. Use it for some of that stuff, even though you're going to be slower and it's going to feel… kind of bad.

The next skill I want to talk to you about is version control. Specifically, we're going to be talking about Git because it's the most popular version control system right now, and if you learn that you're probably in good shape. You probably don't need to worry too much about any others.

Version control is like insurance for your code. You can certainly write code and develop websites without using any sort of version control, but let me give you a couple of scenarios where it's really useful to have your projects under version control.

Imagine that you want to try something a little bit experimental with a project and you're not quite sure how it's going to turn out. If your project is in version control, you can create what is called a branch and that is like a separate line of work on a project. You can use that branch to explore your experiment and try it. Maybe it works. If it works, then you can merge it back into your main branch and everything's great. The beauty of version control though, comes in if that experiment doesn't work because if it doesn't work, if you don't have version control then hopefully you saved a copy of all the files in your project before you started the experiment. Or hopefully you have tons of levels of undo and your editor and you can undo each of the files you changed back to the point they were at before, and hopefully you know what that point looks like and can actually get there.

With version control instead of having to worry about all that – either managing a separate set of files that represents the previous version or trying to undo, undo, undo and get back to where you were – I've been tunneling for decades, searching every instance from yesterday to tomorrow – you just switch over to your main line branch and it is exactly you where you left it.

All of your experimental work is on the new branch. You can return to it later and maybe you figure out something that makes it work and then you can complete that work and maybe merge it back in. But if you don't, you still have your main line branch that works the way it did before you started the experiment.

That's the first scenario. The second scenario is maybe even more common. You're adding a feature or fixing a bug that is not experimental, but you give up. This happens all the time and you break something critical in your application. If you don't use version control, you're in trouble at this point because you have to try to get back to where you were before.

If you're using version control, you can look through your logs at the various commits – commits are basically snapshots where you said, OK, I want to, I want to save this code as it is right now – you can look through your commits and try to find the one before you broke the project. Or you can use tools that are built into the version control systems.

Like Git has one called bisect that lets you sort of divide and conquer your commits. Basically, you tell it the last point that you know the project was working and then it'll put you halfway between that and where you are now and say, "OK, is the project working here?" You say yes or no, and then it will bisect sick either side of that.

So if you say," no, it's not working here," it'll go halfway between that point and the point where you know it was working and try it there. If you say, "yes, it was working here," it'll go halfway between that point and your current point and ask if it was working there. Eventually you're going to get down to a single commit where the project was broken, so then you just have to look through that commit and see what was changed to try to figure out what the problem was. You can even just switch back to that commit if you need to get something back up and working quickly, which is often the case.

GitHub used to have a really nice interactive Git course, but that has somehow disappeared. But we do fortunately have some really good resources. The best one I found is called Git It, and it is an application you run on your computer that walks you through a Git tutorial that you're actually doing in real time on your computer. The application that you download, checks your progress and make sure you're doing what you're supposed to do. So it gives you the instructions, you go over to a terminal window to type in your Git commands, and then you'll come back to the application and say, verify, and it will check that and make sure that you did what you were supposed to do.

It takes you through a whole tutorial. That way, by the time you get to the end of it, you'll have a good working understanding of Git for version control. If you're serious about learning Git, the only way you're going to get it done is by using it on every project. When you're out doing real work, you should be using it for every project, so there's no better time to start that habit than now, and the more you use it, the more comfortable you will get.

What I've found about Git in my experience is that there are hundreds or maybe even thousands of Git commands with command line and on a daily basis I use maybe three of them. So I'm using those all the time. Occasionally I find a need for something else and I'll go out and do a search for it.

So you don't have to try to memorize the entire command line interface for Git. You do though, need to use it over and over so that those critical commands that you're using all the time will just become muscle memory and you'll be able to do them in your sleep.

The last skill I want to touch on is deployment. I've noticed a trend with new web developers and that is that a lot of projects never get shipped. They never get deployed anywhere. Not only does this deprive people of being able to see your work and enjoy it live on the internet, but it deprives you of the experience of deploying a project, which is really important. I think a lot of the times, bootcamps don't do a great job of covering deployment. They sort of gloss over it. Even if you think you're going to go into a job where you're just going to write code and you're going to hand it off to someone else to be deployed and, and set up on a server somewhere – and maybe you will – I think it still shows a great deal of perseverance and commitment to your projects to have the projects you've worked on actually deployed somewhere.

How to learn deployment is a difficult question to answer because there are lots of different places you can deploy a project and each one, the method for deploying there is a little bit different. I can't really recommend a single resource to teach you deployment, but what I will suggest is every project you complete, make sure you deploy it somewhere and ideally deploy each one somewhere different, if that makes sense.

For static HTML and CSS projects or projects that are just running front end JavaScript on top of HTML and CSS, try deploying to Netlify. It's a great static site hosting provider. Try deploying to AWS's S3 service. It's an object store that can also serve up public webpages for static sites. Try deploying a static site to GitHub Pages. That's another option, and there are tons of other places where you can deploy static sites. So just decide where you want to try to deploy your next project and just follow the documentation and get it deployed and make sure it's available so that people can look at it and see that it's a real project and it's done.

If you're building something that's not a static site and maybe you have a Node backend running for a project, you can deploy that on AWS's Elastic Beanstalk. You could deploy it on one of the other cloud providers like Google or Azure. You could try deploying one on Heroku. The keys here to learning this are to stretch yourself – so get outside your comfort zone, deploy to somewhere you've never deployed to before, and just see what it's like – and find the documentation appropriate for your project, for whatever target you're deploying to.

I may have said in the last video that that was the last technical video, but I think this is actually the last technical video in this series, and I'm glad I circled back to it because I do think this stuff is really important. It makes you appear much more robust as a developer, especially if you're going to do freelance work. You may be the only one. You may be the lone wolf who's responsible for doing all this stuff, so there it's critically important, but even if you're going to get a job, I think it shows great initiative that you're able to branch out and do some of these other tasks as well.

Thanks for watching the video. Be sure to subscribe so you don't miss the next step on the 2020 developer roadmap, and I will talk to you then.

---
title: 'jQuery Vs. Javascript in 2021 (Explained Through Analogies)'
date: '2018-09-10'
tags:
  - 'javascript'
  - 'jquery'
coverImage: 'images/tools.jpg'
description: 'If youʼre having trouble with the difference between jQuery and Javascript, this article will help you understand where they overlap and the right use cases of each.'
---

## What Are Javascript and jQuery?

### javascript is like a hammer.

### jQuery is like the soft cushy grip.

![Javascript is the hammer, jQuery is the cushioned grip](images/tools.jpg) **Javascript is a tool** like a hammer. This tool lets you **add interactivity** to your web pages. That’s pretty broad, but it’s intentional because Javascript is pretty broad. Here are just a few examples of things you can do with it:

- Pull in search results and put them on the page the user is looking at **without having to load the new page**
- **Validate inputs in a form** to, for example, make sure the user entered a proper zip code
- **Build a calculator** that takes input from your users and displays the results **live on the page**

**jQuery is an enhancement** to the tool that makes it more comfortable to use, kinda like a comfort grip on a hammer. It specializes in DOM manipulation (i.e. accessing or changing elements on the page). If you want to manipulate the DOM, you’ll first need to grab on to an element. If you wanted to grab all the paragraph elements on your page without jQuery, you might do this:

```javascript
document.querySelectorAll('p');
```

The `p` is just a selector like you would use in CSS. Pass that into `document.querySelectorAll()` as a string to get back all those elements in your Javascript. To do the same thing in jQuery, you’d do this:

```javascript
$('p');
```

If you wanted to hide the paragraphs, you’d do this in vanilla Javascript (that is, Javascript without any libraries like jQuery on top of it):

```javascript
document.querySelectorAll('p').style.display = 'none';
```

In jQuery, it looks like this:

```javascript
$('p').hide();
```

If you hold the grip on the hammer as you hammer in a nail, it feels much nicer than holding a bare handle or trying to hold the hammer by the head to drive the nail. Despite that, **you’re still using the hammer**. In the same way, when you use jQuery, **you’re still using Javascript**. You’re just using jQuery’s _interface_ to make Javascript do stuff instead of doing it with Javascript _directly_.

## Which one should I learn?

### Javascript is like the iPhone Mail app.

### JQUERY IS LIKE SIRI'S INTERFACE TO MAIL.APP.

![Javascript is like Mail.app, jQuery is like using Siri to send email](images/siri-vs-mail.jpg)

Since jQuery is just a nice interface on top of Javascript, trying to pit them against one another doesn’t make a ton of sense. Usually, people trying to compare them **are wondering whether they should learn vanilla Javascript or skip straight to jQuery**. Although jQuery is easy to learn and lets you start quickly manipulating the DOM, Javascript has more advanced capabilities. Here’s another analogy to illustrate.

Siri makes it really easy to send an email without even having to look at or touch your phone. That’s great if you need to quickly get out a simple email, but what if you need to CC (courtesy copy) someone? What if you need to send an attachment? In those cases, **you’ll need to fire up the mail app**.

In the same way, jQuery puts a nice friendly interface on some aspects of Javascript like [selecting DOM elements](https://api.jquery.com/jQuery/), [changing inline styles](https://api.jquery.com/css/), and [binding events](https://learn.jquery.com/events/). If you wanted to do something _outside_ of what jQuery provides an interface to, you’d need to reach for your regular Javascript skills. It’d be a shame if you only knew jQuery because, as much as it can do, **Javascript still has a breadth of capabilities beyond what jQuery touches**. Just to give an example, if you wanted to [use the browser’s storage](https://raddevon.com/articles/using-localstorage-api/) ( i.e. LocalStorage), you can with Javascript, but jQuery doesn’t give us a way to access it.

Since jQuery doesn't do anything Javascript can’t do, **why don’t we just skip jQuery altogether**?

## Should I even learn jQuery in 2021?

### Javascript is like a smartphone camera.

### jQuery is like a point-and-shoot camera.

![Javascript is a smartphone camera, jQuery is a point-and-shoot](images/camera-vs-smartphone.jpg)

Put yourself in the headspace of a person who likes to take photos — not a _pro_ photographer or even an amateur photography _enthusiast_, but **an average person who likes to take photos of family and friends**. Just a few years ago, that sort of person would have bought a point-and-shoot camera for their photos because smartphone cameras weren’t all that great.

Smartphone cameras continued to improve steadily until **it no longer made sense for the casual photographer to buy a separate point-and-shoot camera**. The smartphone camera was close enough that the cost and hassle of the additional gadget was no longer justified.

This is similar to the arc of Javascript and jQuery. When jQuery came along, it provided a nice experience on top of Javascript for some things that were difficult to achieve. For example, Javascript had no native [`map` method](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map). The `map` method allows you to create a new array from an existing one by running the same function on each value in the existing array. The value returned from the function becomes one of the values in the new array `map` creates. jQuery had a method that would do this before Javascript had its array `map` method. You could certainly have created your own array manually in Javascript by looping through an existing array, but it would be nowhere near as convenient as the `map` method.

Javascript later added its own `map` method making the jQuery `map` method less necessary. The jQuery method can do more than the native one: in addition to building new arrays from existing ones, it can also build an array from an object. Even so, the critical functionality of the `map` method is part of Javascript now **making jQuery look just a little bit more like that old point-and-shoot camera**.

As Javascript has been improving, so have the browsers. The core of jQuery — DOM manipulation — used to work slightly differently from one browser to the next. It was a pain to write DOM manipulation code in vanilla Javascript which would work across all browsers. jQuery smoothed out all those inconsistencies so that you could write code that would work on even the most idiosyncratic browsers. This bought web developers in the heyday of Internet Explorer a little more sanity.

These days, modern browsers all support the same API for DOM manipulation, so **this advantage that made jQuery a no-brainer has mostly gone away**. It’s still a little nicer to write jQuery in my opinion, but it’s not nearly as painful as it once was to write the vanilla Javascript equivalent.

On the other hand, as a web developer, you’re not always starting new projects from scratch. jQuery is extremely widespread, so **you’ll probably find yourself working on projects that use it**. If you’re not familiar with it, you’ll have to skill up for that project. Even when you’re starting a new project, you may be working with a team who is stuck in their ways and reaches for jQuery by default. Maybe you can convince them [they don’t need it](http://youmightnotneedjquery.com/) (if, in fact, that’s the case), but other times you’ll have to fall in line.

Back to the analogy: If a stranger hands you a point-and-shoot camera and asks if you'd take a picture for them, you'd want to be able to do that for them even if you know you'll never own a point-and-shoot yourself.

[thrive_leads id='1366']

## The Upshot

- **jQuery is a cool tool built on top of Javascript that makes specific parts of Javascript easier.** It’s not a substitute for Javascript, and it doesn’t incapsulate every feature of the language.
- jQuery was extremely useful as Javascript and browsers’ support of it were maturing, but **many of its advantages have been nullified** by new language features and more standards-compliant browsers (i.e., browsers which behave more predictably; note the irony of introducing that phrase with "[i.e.](https://tedium.co/2015/03/19/the-history-of-internet-explorer-hatred/)" 😆).
- **It’s still useful to know jQuery** because you’ll find many codebases that still use it and occasionally teams that use it for every project.
- jQuery is a great skill to add to your web developer toolkit, but you should **focus the lion’s share of your effort on [learning vanilla Javascript and modern front-end frameworks](/articles/should-i-start-by-learning-vanilla-javascript-or-a-framework/)**.

---
title: 'Am I too old to become a web developer?'
date: '2018-04-19'
tags:
  - 'business'
  - 'career'
description: 'I can demonstrate that itʼs harder to get hired as an older web developer. That doesnʼt mean there arenʼt ways around the prevailing prejudices though. Hereʼs my advice, based on your age.'
coverImage: 'images/skeleton-laptop.jpg'
---

This question pops up quite frequently online: "Am I too old to be a web developer?" It often comes up as newbies begin to realize just how many different technologies they need to understand in order to be an effective web developer.

In my observation, the question masks two different anxieties depending on who is asking it. Let's split those out and dig in.

## For Teens and Twenty-Somethings

Teenagers who ask this question seem to believe that, because a peer began writing code 4 years (or 6 years or 10 years) earlier, they will never catch up. For the sake of argument, lets imagine your peer _does_ have a head start and that you'll never close the gap.

No matter how early this individual started, there's almost always someone who started earlier. Does the industry need only the best, most experienced coder? No. **It needs people at all different skill levels with different backgrounds, worldviews, and niches.**

Having that additional experience doesn't even necessarily mean the person will be better than you. Even if it _does_ mean that in your specific case, you still have a lot to offer. Don't let this anxiety keep you from trying.

## For 30-Somethings and Beyond

As people reach their 30s, the anxiety starts to morph a bit, especially for those trying to start a new career in development. It manifests in two anxieties: that of the teen and twenty-somethings of being behind and a new anxiety about **whether it will be harder to get hired because I'm too old**.

I'd address the experience question in the same way for this group. I embarked on my own web development career at 30, and it was the most important change I've made in my life. I was way behind other developers who started doing professional work in high school, but I was still able to find plenty of work.

According to [StackOverflow's 2018 developer survey](https://insights.stackoverflow.com/survey/2018?utm_source=so-owned&utm_medium=meta&utm_campaign=dev-survey-2018-promotion#developer-profile-age), the average age of a developer in the US is about 29 years-old, with nearly **75% of developers falling between ages 18 and 34**.

Software development skews young and hiring appears to have an element of ageism keeping it that way. According to [a recent study by Vizier](http://markets.businessinsider.com/news/stocks/u-s-study-reveals-systemic-ageism-exists-in-tech-hiring-practices-1002576460), Generation X hiring for tech roles was not proportionate to the pool of available candidates from that generation. Although there are plenty of Gen Xers looking for the jobs, **they're not being hired as easily and frequently as Millennials**. The Baby Boomers have it even worse. How do you keep playing when the deck is stacked against you?

## The Great Equalizer: Results

Most of the preceding info is geared toward full-time employment. Good businesses are generally results oriented and _should_ hire without regard to age. It's harder when you're bringing on a permanent team member because you're not really hiring for a single project. You can't see far enough ahead to imagine all the projects they'll be working on. You need a person who can be effective at any project you throw at them. That's when, as the person making the call on hiring, your biases can seep in and color the hiring process.

If you're hiring a freelancer for a specific project, though, you're much more likely to look toward the end result. If a freelancer can show you that their last 10 $50,000 clients each doubled their money in the first year, suddenly **your age and other personal attributes melt away**.

## How to Move Forward

We know that ageism **does exist** for web developers. What do we do with this? Where do we go from here? For new or aspiring web developers, it does mean getting your job will be tougher, but you're still probably going to be better off than in your current career. Here are some stats on full-time permanent web developer positions [according to the US Department of Labor Bureau of Labor Statistics](https://www.bls.gov/ooh/computer-and-information-technology/web-developers.htm):

- Average salaries for web developers were **$66,000** in 2016 while the overall average for all occupations was only $37,000.
- The number of web developer jobs is expected to grow **15%** by 2026 while the total growth across all occupations is only 7%.
- While many high-paying careers require advanced degrees, the most common requirement for a web development career is **a two-year degree** (and many positions don't even require that).

Besides these benefits, web developer positions usually offer healthcare and a number of other benefits. I've seen **gym memberships, education allowances (for purchasing books and courses), transportation benefits, on-site meals, snacks, and drinks, and generous or even unlimited paid time off**. Many companies offer the opportunity to do some or even all of your work remotely so you don't have to suffer a commute.

If you're looking at doing freelancing work, you'll get even greater freedom with more responsibility. You can **live and work anywhere you want, work any time you want, take as much time off as you want, and there's almost no ceiling to how much you can earn**.

<!-- [thrive_leads id='1378'] -->

How does this compare to _your_ career? For most people, the carrot is much bigger than the stick. The best thing you can do to combat ageism is to develop a freelance practice. (_If you want help getting started, read my post on [getting your first freelance gig](/articles/get-first-freelance-web-development-job/)._) Become such an incredible investment that people seek you out and can't afford _not_ to hire you once they've seen the results you've been able to generate. Once you've done this, you can get a permanent full-time job easily if you want, but you'll be doing well enough you probably won't want to.

Even if you never build a high-powered freelance practice and just want to settle into a 9-5, it's probably worth enduring a few additional "no"s on the road to your new web development career. The plethora of benefits make it all worthwhile.

_Freelancing starts to sound pretty good, right? Read on for [how to get your first freelance gig](/articles/get-first-freelance-web-development-job/)._

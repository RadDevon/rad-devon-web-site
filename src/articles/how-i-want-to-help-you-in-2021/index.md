---
title: 'How I want to help you in 2021'
date: '2021-01-01'
tags:
  - 'meta'
  - 'motivation'
coverImage: 'images/channel-update-2021-01-thumb.jpg'
---

Last year, I decided to try making videos. It's been an awesome experiment, but this year, I'm not going to be doing a video each week. Watch this video to see why, and read on below it to learn more about my plans for Rad Devon in 2021.

https://youtu.be/AN0a_Wsd6KE

This means you'll start seeing different types of content in addition to video. I'm letting the concept I want to teach dictate the medium rather than starting from the medium and trying to teach everything in the same one. This will give me more flexibility to create a better resource for you.

In addition to more types of content this year, you may notice weeks go by without a new piece of content. I'm going to be periodically taking time to flesh out some of the content I've already created. For example, I'd like to flesh out the descriptions of each of my [project ideas](http://articles/10-great-web-development-learning-project-ideas/) since a lot of people write me to ask for more information about them.

I also want to make sure each of my videos has a transcript and that each transcript reads like a text-native article. That way, it's more accessible and people can consume in whichever medium most appeals to them.

Finally, I'd like to look for any gaps in what I've written or videos I've created and try to fill them. Which brings me to a couple of questions for you. **Is there anything you've read or watched here that you'd like me to expand upon? Is there anything you want to know about or understand better that you can't find here?** [Please let me know.](mailto:devon@raddevon.com)

I'm going to be experimenting with some new things throughout the year too. I'm not ready to commit to those yet, but rest assured 2021 won't be boring around these parts.

Thank you for taking the time to read this. I'm looking forward to another year of helping people like you start your new careers and your new lives! 🎆

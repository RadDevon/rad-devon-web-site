---
title: 'Start your MAMP dev server with Grunt'
date: '2013-07-05'
tags:
  - 'development-environment'
  - 'grunt'
  - 'gruntjs'
  - 'mamp'
  - 'php'
  - 'workflow'
coverImage: 'images/boar.jpg'
---

I just started a new project that requires a PHP server, but the built-in PHP server used by [grunt-php](https://github.com/sindresorhus/grunt-php) is not beefy enough. Instead, I need to run the [MAMP](http://www.mamp.info/en/index.html) server in order to test it.

To get around this, I installed the excellent [grunt-exec](https://github.com/jharding/grunt-exec) plugin which allows Grunt to run arbitrary commands. Add these tasks to your Gruntfile to allow you to start and stop the MAMP server:

```javascript
exec: {
      serverup: {
        command: '/Applications/MAMP/bin/start.sh'
      },
      serverdown: {
        command: '/Applications/MAMP/bin/stop.sh'
      }
}
```

Now, if you have a grunt task which runs `watch`, all you need to do is add `serverup` before `watch` and `serverdown` after it. The server will be started before Grunt begins the watch. It will be taken back down after you exit the watch task.

[thrive_leads id='1366']

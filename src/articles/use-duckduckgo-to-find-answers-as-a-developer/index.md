---
title: 'Use DuckDuckGo to find answers as a developer'
date: '2021-05-08'
tags:
  - 'duckduckgo'
  - 'google'
  - 'search'
coverImage: 'images/duckduckgo-thumb.jpg'
---

I pride myself on teaching [the developer skills that not many people focus on](https://raddevon.com/articles/new-web-developer-skills-you-probably-dont-know/) – [dealing with rejection](https://raddevon.com/articles/when-you-get-rejected-as-a-web-developer-its-not-personal/), [understanding business](https://raddevon.com/articles/books-that-made-me-the-personal-mba/), [finding work](https://raddevon.com/articles/get-web-development-clients-while-on-lockdown/)… those kinds of things. Here's a skill that I use every day but hadn't even considered talking about: **searching**.

![](images/duckduckgo-thumb-1024x576.jpg)

It occurred to me when I found an article by Marko Denic on [how to use Google's search syntax](https://markodenic.com/use-google-like-a-pro/). I highly recommend you check it out. It's a quick read, and, even though I'm searching all day every day, I picked up a few new tips.

The one thing I don't care for about the article is that it's very Google-centric. We're well past the "don't be evil" Google of the past and into the "maneuver around the law and common decency in an effort to collect the most possible data on our users" era of the company. It's why I switched over to a privacy-focused search engine called [DuckDuckGo](http://duckduckgo.com).

I'm going to be up-front with you. The results on DDG are not always on par with those on Google, and I do still use Google when I need to. What I find, though, is that for 90% of the searches I need to do, DuckDuckGo works just as well and sometimes better.

Google is much better at finding results in online discussion which is very useful when you're trying to find the answer to a specific question. Google will return multiple results from StackOverflow, Reddit, and support forums. DuckDuckGo has these, but it seems to find fewer of them and it doesn't always surface the relevant ones.

Here's my simple search workflow:

1. Try my search on DuckDuckGo.
2. In the event I don't find what I'm looking for, I add "!g" to my search to search on Google.

This uses a DDG feature called "bangs" that allow you to send your search over to another site. Adding "!g" just sends your same search over to Google. You'll get back the Google results for your search terms (on Google; they're not proxied through DDG, so it's literally identical to searching on Google).

This always feels like giving up (and frankly, it is), but the bangs feature goes beyond just redirecting failed searches over to Google. DDG has thousands of bangs, and many of them are super useful for developers. Here are a few you might find useful:

- !mdn- Mozilla Developer Network
- !sof- StackOverflow
- !csst- CSS-Tricks
- !r- Reddit
- !dev.to

These are just the ones I think are most generally useful to developers, but there are tons of these things. You have language-specific bangs like "!py" for searching the Pyton documentation, front-end framework specific bangs like "!vue" for the Vue documentation, and tool-specific bangs like "!vscode" to search the Visual Studio Code docs. I could go on, but instead you should check out [DDG's bangs page](https://duckduckgo.com/bang?__s=xxxxxxx&utm_source=drip&utm_medium=email&utm_campaign=The+developer+skill+everyone+%28including+me%29+takes+for+granted) and find the bangs that will help you code faster.

You could still use "site:mdn.org" for example to search just on the MDN site, but this works differently from a bang. The bang searches through the site's own search engine. Limiting your DDG search to a site using "site:" still uses DDG's search and not the site's own search. Either approach might be useful depending on the context.

You might be wondering, "Why wouldn't I just go directly to the site I want to search and search there instead of going to DDG and using a bang to redirect my search to the other site?" I'd reply, "That's an astute observation, intelligent reader," and proceed to address it like so: bangs shine when DDG is your browser's primary search engine. If you decide to set DuckDuckGo as your browser's search engine, you can now search directly from the address bar, giving your address bar some cool superpowers.

You can now type your search in the address bar to search DuckDuckGo, or you can type your search and add "!g" to search Google (e.g. "flexbox !g"). Maybe that leads you to a StackOverflow article with a recommendation for a book on CSS. Awesome! Now, you hit command/control-L to focus the address bar again and type "!a flexbox in css" to find the book on Amazon. (Note that your bang can go anywhere in the search. It doesn't have to be at the end.) Then, you order the book and search "!mdn flexbox" to find a quick reference on MDN while you wait for your book to ship. That's the true power of the bang.

In case you're wondering about the search tips in the Marko Denic article, most of them work in DuckDuckGo.

- Quotes work the same way.
- The "AND" and "OR" keywords don't exist in DDG, but search terms are implicitly "OR"ed.
- The minus (-) operator is slightly different in DDG. Where Google eliminates results with the term, DDG gives the term specified by the operator less weight. It's not as useful, but it can still get the job done.
- Wildcards don't seem to work in DDG, best I can tell.
- Limiting search to a site with "site:" or to a file type with "filetype:" works exactly the same.
- Numeric ranges and dates in the search don't work, but you can date limit the search via DDG's UI.

So, we're close to feature parity, but not quite there. It's going to be enough for most searches. If you absolutely need to refine a search by adding a wildcard to it, just do it and also add the "!g" hashbang to send that search over to Google instead.

There are two things I want you to take away from this article:

1. Take some time to improve your searching skills. It's a massive part of what we do as developers and, even though it's not tested in interviews like algorithms and data structures, it's a skill you'll get a lot more milage out of in the long run.
2. You don't have to throw all your data into Google's data lake in order to get great results. At the same time, you don't have to quit Google cold turkey.

Thanks for reading. Hope this will help you find whatever it is you're searching for!

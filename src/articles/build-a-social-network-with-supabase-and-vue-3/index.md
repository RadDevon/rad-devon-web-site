---
title: 'Build a social network with Supabase and Vue 3'
date: '2021-06-30'
tags:
  - 'javascript'
  - 'projects'
  - 'supabase'
  - 'tutorials'
  - 'vue'
  - 'vue3'
coverImage: 'images/timeless-supabase-thumb.jpg'
---

Build a social network with me using [Supabase](https://supabase.io/) and Vue! Supabase is a new-ish competitor to Firebase. They don't offer all the services Firebase offers, but they do currently offer database and authentication (among others) which is enough to build out our social network.

I call this social network Timeless. It's kinda like Twitter except that users may post only a single post a year. Better make it a good one!

I created a starter kit for the Timeless app. We won't be using it extensively in this build, but you may want to grab a copy for a cool logo and some wireframes that explains the project and how it might work. This build is scaled down from that, but the context would still be helpful. Share your email below to get a copy of that.

[thrive_leads id='3587']

This video encompasses the entire build, but we still need to deploy. Be sure to subscribe on YouTube or share your email address with me (through any form on this site, like the Timeless starter kit form above) to be notified when that video goes live.

Here are some notes that will be helpful as you work through the build.

- The [Homebrew](https://brew.sh/) command to install Postgres on macOS is brew install postgresql. In Windows, [download the Postgres installer](https://www.postgresql.org/download/windows/) and run it.
- Check out [my video on SQL](/articles/learn-the-basics-of-sql/) to learn the basics. It's not required, but it will give you a better understanding of the database underlying Supabase.

https://youtu.be/x25a_q0vHUY

Thanks for building along with me. If you have any questions, please let me know [on Twitter](http://twitter.com/raddevon). Otherwise, I'll see you in the next few weeks when we will deploy the app!

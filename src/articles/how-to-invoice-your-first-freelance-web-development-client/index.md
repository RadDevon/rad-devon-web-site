---
title: 'How to Invoice Your First Freelance Web Development Client'
date: '2018-08-04'
tags:
  - 'business'
  - 'clients'
  - 'freelancing'
  - 'invoicing'
  - 'videos'
coverImage: 'images/thumbnail.jpg'
description: 'If you want to get paid for your work, you need to invoice. What does that even mean? How do you do it? Watch this video and learn my easy invoicing method.'
---

Getting some early freelance clients is great to build your confidence, restore your willpower, and drive your learning. If you know HTML and CSS reasonably well, [you're probably ready to start picking up work](/articles/know-im-ready-first-web-development-job/).

So, you have some skills that are valuable to clients. Now, you just need a way to capture that value. We're talking about 💰 getting paid 💰! To do that, you need to send your client an invoice to let them know how much they owe. Here's the cheap and easy method I use to track my time and send invoices. You can get set up in about 20-30 minutes just by watching this video!

https://youtu.be/obHJy72RmP8

## Resources

You'll need two things to follow my method: a [Stripe](https://stripe.com/) account for taking payments and a [Harvest](http://try.hrv.st/7-54807) account for sending out your invoices. They're quick and free to create. I demonstrate in the video how to set up your Harvest account, but you'll want to create your Stripe account on your own beforehand.

[thrive_leads id='1378']

## Video Transcript

Hey everyone. This is Devon. I just got an email recently from one of my readers. He was really excited that he's only been studying web development for a few months and he just picked up his first paying freelance client. His only problem is **he's not sure how to invoice them**. So in this video, I'm going to show you how to get set up to invoice your first freelance client.

Okay, so this is Harvest. This is what I use to do my invoicing and I'm just going to demonstrate with this because it's really easy to get set up and the more time you spend evaluating tools to start your freelancing the less time you're spending doing billable work. So we want to... we want to favor simplicity where we can.

And the nice thing about this is they have a free trial but then they also have a free tier that lets you have up to two active projects. So if you're a new developer and just learning you can probably use this for free until you get more than two simultaneous clients that you're juggling and at that point, you're probably hopefully if you're charging enough you're making enough that the twelve dollars a month that it costs is, is pretty insignificant.

So we're going to start here at "Try Harvest for Free."

Okay, and this is just a demo. Hopefully you'll use a better password than what I've used here.

Okay, and we're going to tell them that we are using Harvest for a freelance business. We have a few settings to start with. Um, if you're in the U.S., you can probably leave all these the way they are.

And we don't care about any of this right now. So I'm going to go straight to invoices because that's what we care about and I'm going to configure. So, this will set what our invoices look like when they go out to our clients. So you'll want a name up here. This is... this could be _your_ name or it could be the name of your business and then an address.

And (the address) is really important because some of your clients may want to pay you with a check and they'll have to know where to send that. So, this will put your address on the invoice. So, down here at "Send Message As," that will allow you to set a reply to address on your invoice.

So the invoices will come from, I think it's notifications@harvestapp.com maybe, but they'll come from a different email address and then the reply-to will be set to whatever you set down here (at "Send Message As"). So, now let's go over to "Default Values."

As far as the tax value goes you'll probably want to talk to an accountant about that. Here in Seattle, I don't have to charge sales tax on consulting, but, where I'm from in East Tennessee, I _did_ have to charge sales tax under most circumstances… or under _many_ circumstances. So, **it's really important to check with your accountant on this** so that you don't accidentally not charge your clients sales tax when you need to be charging them sales tax.

The rounding: I left it at "No Rounding" for a long time and I just charged based on the actual time I was tracking, but now I round up to the nearest 15 minutes and I started doing that because some of my clients needed to account for the time and needed to account for it in quarter hours. They couldn't do individual minutes quite as easily. So now I round up to the nearest 15, but you can set that however you want and, if you're not billing on time, that's fine. This is still useful for you. Harvest is still going to be useful for you. It will let you bill for fixed-price projects.

Then your payment due date: I typically set "Net 30." That means, once the bill or once the invoice is received, the client has 30 days to pay you and this can be changed on a per client, per invoice basis. So, I have one client that's "Net 45." They pay me 45 days after I bill them, and so I just change that on their invoices.

I always leave the invoice summary and invoice notes blank — the default values — because, if I want to set those on a particular invoice, it's probably specific to that invoice. So, the default doesn't really help me a lot. So, I'll save those values and now go to appearance and this is where I'm going to start out by selecting a logo.

So, let's see...

I need one with a background because this will be placed over white. So, if I want the background to appear I need it there. If you are mailing invoices, you can tick this. I suggest that you not unless you have to and then we're just going to leave all these visible for now. And now we're going to skip down to online payment.

You may want to poke through these other tabs to see what options they offer. They're not critical, so I'm just going to skip over them. We _do_ want to set up an online payment processor though because this allows people to click a link from our invoices that we send and immediately pay us with a credit card.

You'll incur some sort of fee when someone does that so you won't make the entire amount. You might prefer that people pay with a check but it's important to have this option because you want... you want to just put **as few barriers as possible** between your clients paying you. So, I use Stripe.

I would steer clear of PayPal. They have a history of freezing funds and locking accounts, and I don't think Stripe is quite as notorious for doing that. So, I like to use Stripe. I'm not going to set up a Stripe account for this demo, but what you would do is just click this ("Connect with Stripe" button). You'll create a Stripe account. Stripe's going to want some bank account information so they can make deposits of your payments and they're also going to want some tax information so that they can report your earnings to the IRS. And once you've done that and created that account, then you can connect it to your Harvest account and those links will automatically go out with your invoices.

So the next thing you need to do to set up Harvest is you need to **create a project**. So you go to the "Projects" tab and click on "New Project."

And now you're going to **add a client**. Once you add this client, you'll be able to select them from a dropdown here. But for now, we'll just be creating a new client. Let's say we're doing some work for Spumco, and we're just going to call this their website. I'm going to say SWS (as the project code) for "Spumco web site." And we're not going to set start on or end on dates, and now we need to decide if this is a **fixed-price project** or if this is an **hourly billed project**. Either one is fine. Harvest supports both. So, I'll just kind of show you what both look like. So, this is a fixed-price project. You just put in the total amount. So if we were charging $5,000, we put that, and then there's a budget field you can fill, and you might want to.

In this case, (the budget field) is going to be helpful for _you_ because, if you quote a project with a fixed price, you may have in your head sort of a number of hours that that needs to take you for it to be profitable. So, you can set the total project hours and say, "OK, I need to get this done in 75 hours for this to make the amount of money I want to make," and, by ticking this box, you can have it remind you once you start getting close to that budget. So, that's a nice way to manage your time and make sure your projects are all profitable.

For this project we're going to bill hourly, and we're going to set a project hourly rate. We can set a person hourly rate, but that's not very useful for a one-person shop and a task hourly rate if you wanted to charge differently for development versus planning versus meetings, you could with a task hourly rate I typically don't do that just because it's too complicated. So, let's say our hourly rate is going to be $75 and we want to… the client wants to spend no more than $5,500.

So, you might think we could just as easily set a number of hours and take $5,500 and divide it by 75 and see how many hours that would be, but project fees could be a little bit different because you might have expenses. Maybe you need to license a plugin to use on this client's site, and that would be bundled up in the total project fees. So, if they know exactly how much they want to spend you can set that as a total project fees budget.

Down here you have the tasks that you're going to be doing as part of this project and over to the right, this is very important, **this checkbox determines whether or not Harvest will bill for those tasks.** You can change it later, so it's not critical that you get it right now, but, generally, I think this is going to be "All" because you want to bill for any work you're doing for the client.

And you can add some others (tasks) too. So, if you're going to have meetings — which I guess could fall under project management, but we'll just leave it in there, and then you select which members of your team are going to be working on this. Again, one-person shop, it's not really meaningful.

So, I'm going to save this project. Okay, cool. Now I'm ready to **start tracking time for this project**. I'd encourage you to track time even if you're billing a fixed rate for the project because that will let you know how profitable you are. It'll also let you know how inaccurate your estimate was. If you thought about this in terms of how many hours it's going to take, maybe you thought it was going to take 50 hours and it actually took a hundred. It's useful to know that so that, next time you're doing an estimate, you can maybe double it so that you'll get a more accurate estimate.

OK. The next thing you're going to do is you're going to go here to [getharvest.com/apps](http://getharvest.com/apps), and that will allow you to download some of their time tracking apps. They've got a mobile app, which is useful if you're away from your computer, but the one I use most is the desktop app, and I do use the Mac app.

Actually, you can see it right up here on my menu bar, and the way it works is you click the play button and then it'll ask you which project you're on and what task you're doing and you can hit start and that will start a timer ticking. It's really nice because, if it detects you're not active on your computer — maybe you forgot to stop the timer — it will give you an opportunity to stop the timer and remove that time or if you're actually working — maybe you went away and made some notes and you're coming back — you can continue timing and keep the time you had. I have added some time to our project. So you can see here on Monday we did an hour and half of project management and some graphic design, Tuesday some programming, and Wednesday some programming, and we're just going to pretend like this project is done. So, **we're ready to invoice for it**. I'll go to invoices and click "New Invoice" and I'm going to choose a client: Spumco, and I want to create an invoice for our tracked time and expenses. So, this is what you would do whether you have a project you're billing hourly for or a fixed rate project.

OK. Now, I'm checking the projects for that client I want to bill for, and in this case, it's the only one. And I'm going to bill for all uninvoiced and billable hours. You could bill for a month at a time if you wanted using this or any arbitrary period of time you wanted.

I'm not sure why you would want to do this (not bill for any time). I guess you just want to bill expenses, maybe. I don't think that's a very common use case.

And then you're going to choose how you want to display the time. "By Person" may not be relevant unless you have other people working with you. "By Project": not going to be relevant in this case since we only have one. We could do by task which would be nice or we could do a "Detailed" which is going to break it down into each entry on our timesheet not just by task.

So, if we worked on... for instance, we worked on programming on Tuesday, and then also on Wednesday. That's going to be two lines on a detailed listing versus one line on by task since it's both... since they're both the same task. I'll just do by task for this one. In general I don't think your client should care when you did the work as long as you did it.

And so that generates this invoice, this is our invoice number one. There's our default due date, net 30, and here are the services we're billing for based on our timesheet. Here's the quantity. So that's the number of hours. The unit price — so, the hourly rate and then the total, and then we get the subtotal down here.

If we had a tax rate applied, then that would be applied here to get the amount due. Since we don't have one, it's the same as the subtotal.

Now we can save this invoice and that's what it's going to look like to the client. So, we're ready to send this off. I'm going to click "Send Invoice."

And this is actually going to be a problem. So, if we look at recipients, the only thing I can do is I can send myself a copy, and that's because we created this new client, Spumco, and **we don't have a contact**. So, let's add a contact. Okay, and these items are all optional.

I'm just going to leave them blank. I will include the PDF version of the invoice. If you have a payment processor set up, you'll also have a check box down here to send a payment link and you'll probably want to leave it checked unless you have a client who you know never pays online, who only pays by check or some other payment method.

OK. So, now the invoice is ready to send. Just for the purposes of this demo, I'm not going to send to that address (of the client contact). I'm going to send to myself.

OK, and let's see what that actually looks like. Here is our first invoice. It has our logo at the top and summary of the invoice, the amount there, the due date. If we do not pay this invoice by the due date, we'll start getting reminders and you can set up how frequently those go out and what they say.

I think that's in the messages tab. Let's take a look, invoices, configure, messages. Yes. So, here are your **reminder messages.** They're turned off by default. So you can turn those on once an invoice is three days late and every 7 days after and then you can customize the message.

And if you had a payment processor configured, that link would be in here as well. The user could click, enter their credit card number, and you'd be paid as soon as the processor releases the funds to you. Let's look at the PDF. That looks pretty much like it did on the preview before.

Now we can look at an overview of our invoices. We've got one open invoice. It's due in 30 days. There's the amount. Now, I'm going to edit this invoice to show you what an overdue invoice looks like. So let's pretend we sent this last month and it was Net 15. That means it would have been due 15 days after we sent it.

So now when we're looking at the invoice. It's marked as late. In the overview, we see it's late. Due 16 days ago.

And now, let's actually record a payment. So, we got a check from this client. So, I'm going to record that payment. I might put the check number here. And, if you have payment reminders turned on and you have clients that mail checks, you want to really make sure that you do this. Otherwise, **they're going to start getting reminders telling them they haven't paid you when they already have**.

OK. So, now this is marked as "Paid," and, if we go back to the overview, it has moved from "Open" to "Total Paid Amount," and it's no longer in this tab listed under "Open Invoices." It's now going to be here, and all invoices and that's it.

That's basically everything you need to know about invoicing in Harvest.

Thanks everyone for watching. I hope you found this video helpful. And if you want more stuff like this go to raddevon.com where I teach people how to leave their 💩 jobs and become web developers.

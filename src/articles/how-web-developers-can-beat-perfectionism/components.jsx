import React, { useState } from 'react';

export const ToggleEditsButton = () => {
  const [showEdits, setShowEdits] = useState(false);
  const toggleEdits = () => {
    setShowEdits(!showEdits);
  };
  return <button style={{marginBottom: '1.5em'}} className={showEdits ? 'edits-on button -outlined' : 'button'} onClick={toggleEdits}>{showEdits ? 'Hide' : 'Show'} Edits</button>;
};
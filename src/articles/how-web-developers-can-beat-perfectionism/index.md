---
title: 'How Web Developers Can Beat Perfectionism'
date: '2019-04-05'
tags:
  - 'motivation'
  - 'perfectionism'
  - 'productivity'
coverImage: 'images/prfct-shirt.jpg'
description: 'We think of it as a good thing, but perfectionism kills productivity. Hereʼs how you can beat it.'
---

import './visible-edit-styles.css';
import {ToggleEditsButton} from './components';

![Man wearing a shirt with the text PRFCT!](images/prfct-shirt.jpg)

Thanks for checking out this article! I have a special interactive treat for you. A bit of a peek behind the curtain, into how imperfect _I_ am (and how that's totally OK). I also want to prove that I practice what I preach.

I want to reinforce the ideas that 1) it's OK to mess up and make things that aren't perfect and 2) you can keep refining even after you ship. As a result, **I've left all my edits of this article intact**. I continue to find and fix problems and refine articles even after they go live. Click this toggle button 👇 to show and hide the edits in the article below.

<ToggleEditsButton />

Perfectionism kills productivity. It leads people to spend their entire lives **not really finishing anything**. I've thought of myself as a perfectionist in the past, and <del>,during <ins>that</ins> those parts times time in my life, I didn't finish much</del> I have very little to show that I completed during those years.

<p><del>Finishing</del>
<ins>Starting</ins> projects is easy. Finishing them is hard. Perfectionism is
yet another obstacle to keep <del>us</del>
<ins>web developers</ins> from reaching the finish line… as if we _needed_ another
obstacle. We already have to deal with <del>buggy,</del> poorly documented libraries,
impossible expectations, <del>hardware</del>
<ins>computer</ins> problems, network outages, sick kids, and cats who insist on
sitting on the keyboard. Now, we have to beat our own hang-ups on top of all
that.</p>

<p><ins>As I look back, <del>perfectionism in my wake</del> <ins>with perfectionism behind me</ins>, I can see it for what it really is. When you're in it, it <ins>doesn't</ins> _feel_<del>s</del> like you're <del>not</del> flaking on projects — you're just going to keep working on them until they're perfect. You're a victim of a universe in which nothing will ever be perfect. <strong>It's the universe's fault your projects never come to life, not yours.</strong></ins></p>

<p><ins>In reality, perfectionism is a tool that allows you to <strong>never finish anything while also avoiding not feeling any responsibility guilt</strong>. The sad truth is this: we're only fooling ourselves.</ins></p>

Facing so many factors we can't control, we need to deal swiftly with those we can. Here's how I kicked perfectionism and started **getting things done**!

<!-- If you need a hand breaking through your barriers *🚪\_\_🔨* to becoming a web developer, I'm here to help. Let's set up a mentoring session and figure out your next steps together!

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

## Step 1: Accept that perfectionism is harmful

As a developer, perfectionism is almost exclusively harmful to your career, whether you freelance or work full-time. The dilemma is not between perfectionism and turning out garbage — **it's between perfectionism and shipping**. Given that, choice, all your clients and employers will choose to ship.

Perfectionism means <del>fewer completed projects</del> <ins>missed deadlines</ins> for clients and employers and fewer projects to show in your portfolio. No one wants to hire you on the back of a bunch of half-finished projects.

## Step 2: Rescope current projects

For any projects you're working on, reduce the scope to **the absolute minimum you need to accomplish your goal**. <ins>Be ruthless in finding what that minimum is.</ins> This way, even if perfectionism <del>intervenes</del> <ins>creeps back in</ins>, you'll have fewer moving parts to perfect!

## Step 3: Set aggressive deadlines

Set deadlines that won't allow time for perfectionism and stick to them. Focus on completing your projects in passes. Your **first pass** should be getting all the **basic functionality** in-place. For <del>your second</del> **<ins>each subsequent</ins> pass**, <del>thing about</del> <ins>complete</ins> **the most impactful refinement** you can make.

## Step 4: Complete projects

Keep making refinement passes until you hit your deadline, and **ship what you have at the end**. You can always return to the project for more refinement passes if you have time later. <ins><strong>Shipping on the web doesn't have to mean you never work on your project again.</strong></ins>

## Step 5: Reflect

Once you've completed a few projects this way, look back on the work you've actually finished. You'll realize it's not as bad as your perfectionism led you to believe it would be. You'll feel a <del>since</del> <ins>sense</ins> of pride around completing projects. <ins>That pride will help you keep the perfectionism monster off your back.</ins>

You've taken one more step to set yourself apart. You've moved from a large group of people who can write code into <strong>a much more <del>elusive</del> exclusive club of people who can use code to get things done</strong>!

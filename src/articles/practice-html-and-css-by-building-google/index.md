---
title: 'Practice HTML and CSS by Building Google'
date: '2020-09-18'
tags:
  - 'css'
  - 'html'
  - 'practice'
  - 'projects'
coverImage: 'images/build-google-thumb.jpg'
---

You've read articles, watched videos, and gone through tutorials. Now, it's time to make it real. You're going to put your HTML and CSS skills to the test by building the Google page. This is a great first project for anyone new to HTML and CSS.

Watch the video to get started. Keep scrolling for some resources that will help you as you build.

https://youtu.be/Z0WCoaxb1s8

## Things You'll Need

You'll want to save any images from the page to use in your build, but whatever you do, **don't view the page source**! You don't want to spoil your build with knowledge of the way Google does it.

If this tutorial goes out of date and you need to grab new or additional SVGs or something like that, view the source, just try to ignore everything except what you're actually trying to pull down. Right-click > Inspect Element is your friend for copy/pasting the code for a single embedded SVGs.

They use inline SVGs for a couple of images, which just means the entire SVG is right there in the HTML. I've copied the two inline SVGs they use (as of September 2020) and provided them below. You can just copy and paste them into your HTML to include them in your build.

### Magnifier SVG

```xml
<svg focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
  <path d="M15.5 14h-.79l-.28-.27A6.471 6.471 0 0 0 16 9.5 6.5 6.5 0 1 0 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
</svg>
```

### Microphone SVG

```xml
<svg class="HPVvwb" focusable="false" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
  <path d="m12 15c1.66 0 3-1.31 3-2.97v-7.02c0-1.66-1.34-3.01-3-3.01s-3 1.34-3 3.01v7.02c0 1.66 1.34 2.97 3 2.97z" fill="#4285f4"></path>
  <path d="m11 18.08h2v3.92h-2z" fill="#34a853"></path>
  <path d="m7.05 16.87c-1.27-1.33-2.05-2.83-2.05-4.87h2c0 1.45 0.56 2.42 1.47 3.38v0.32l-1.15 1.18z" fill="#f4b400"></path>
  <path d="m12 16.93a4.97 5.25 0 0 1 -3.54 -1.55l-1.41 1.49c1.26 1.34 3.02 2.13 4.95 2.13 3.87 0 6.99-2.92 6.99-7h-1.99c0 2.92-2.24 4.93-5 4.93z" fill="#ea4335"></path>
</svg>
```

Finally, if you want to try to include the app menu button (the grid-looking thing in the top right) using FontAwesome to get kinda close, [the "th" icon](https://fontawesome.com/icons/th?style=solid) is the closest one I found. Feel free to use anything else that you prefer, whether that's a different FontAwesome icon or something else entirely.

## Level-Up Challenges

On the video, I've included ideas for some additional challenges to make this a bit tougher. I'm not going to share those here so I don't intimidate anyone who isn't ready, but watch the video if that's something you're up for.

## Extra help

I've included some extra help in the video as well. I won't recount it here to avoid spoiling it for those who want to figure everything out on their own. It's there if you need it, though, so don't feel bad watching all the way through.

<!-- If you enjoy this project, and you want to try something more substantial, download my project starter kit to get started quickly on a project you can really make your own! 👇

[thrive_leads id='3587'] -->

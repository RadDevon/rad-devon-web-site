---
title: 'Learn Git with My free Video Workshop'
date: '2020-05-01'
tags:
  - 'git'
  - 'tutorials'
  - 'version-control'
coverImage: 'images/fcc-april-2020.png'
---

If you want to be a developer of any stripe, Git is required learning. In this video, I'll go through the [Git It](https://github.com/jlord/git-it-electron) interactive tutorial to learn how to create a repo, make commits, push to GitHub, and more!

https://youtu.be/H7m0xVdGZqg

You can't be a web developer without learning the technical skills (like Git), but you also can't be a web developer with _just_ technical skills. You need to take it a step further and **start applying those skills to solve problems** that make people money. That's why I love freelancing as a starting point for new developers. It puts that fact in sharp focus.

If you want to try freelancing, take my free crash course below 👇. It will teach you how to set up your business so that you can start taking on clients and applying what you know to earn a living!

[thrive_leads id='2262']

---
title: 'Web Developer Roadmap Step 5: Relationships Are Make or Break'
date: '2020-02-07'
tags:
  - 'clients'
  - 'freelancing'
  - 'meetups'
  - 'networking'
  - 'vue'
coverImage: 'images/thumbnail.jpg'
description: 'Getting your first developer gig isnʼt about learning the most frameworks or having the slickest résumé. Itʼs all about who you know. In this video, learn the relationships you need to build to find success.'
---

https://youtu.be/x5BpLs_CIdQ

## Video Notes

Not many links to share for this installment of the roadmap on relationships, but I will summarize some of the key points in the video.

- 🤝 **Start early** forming relationships with other developers. Do this at meetups, conferences, and hackathons.
- 💵 Once you've learned the basics, start dividing your relationship-building time between maintaining your existing relationships with devs and **forming new relationships with people who could give you work**.
- 🔍 You can **start the search broad** at your local Chamber of Commerce, BNI groups, Toastmasters, or other groups where people go to talk and do business.
- 🔬 Broad is fine to start, but you want to go narrow. Think about **what kind of people you'd want to work for**.
- 🗣 Once you have an ideal client nailed down, **find where they go to discuss business**. Look for their meetup groups, conferences, and organizations.
- ⌚️ **Show up and be consistent**. You're not selling. You're learning about what they do and making friends. Make sure they know what you do too. (I wrote an article on [how to network to get work](/articles/how-to-get-more-freelance-clients-through-networking/) that might help.)
- 🔨 If there's a problem you can help with, **let them know**.

I used a video from [New Tech Seattle](https://www.newtechnorthwest.com/events/new-tech-seattle/) to show what a meetup group might look like. They're one of the largest groups in Seattle and a great hangout for new developers. Check them out!

Thanks for watching the video! I'd love to hear any questions you have in the comments [on YouTube](https://youtu.be/fn4kJcbk72Q) or directly [on Twitter](https://twitter.com/raddevon). [Subscribe on YouTube](http://www.youtube.com/channel/UCxgZYWJq2Cxk7vIePGiTxFw) so you don't miss the rest of the roadmap!

## Other Videos In this Series

If you like this video, watch the others in the series for a good overview of the philosophy I used to break into web development with no connections or experience.

- [Step 1: Set Your Big Goal](/articles/2020-web-developer-roadmap-step-1-set-your-big-goal)
- [Step 2: Learn HTML and CSS](/articles/2020-web-developer-roadmap-step-2-learn-html-and-css)
- [Step 3: Learn Javascript](/articles/2020-web-developer-roadmap-step-3-learn-javascript)
- [Step 4: Learn Terminal, Git, and Deployment](/articles/2020-web-developer-roadmap-step-4-learn-terminal-git-and-deployment)
- [Step 6: Time to Get Paid!](/articles/2020-web-developer-roadmap-step-6-time-to-get-paid)

## Transcript

In the previous step of the roadmap, we talked about the Unix terminal, Git, and deployment. In this step, we're starting to move over to some soft skills. This step is one that a ton of people skip and they don't understand why everyone around them can get into the industry and they can't and it's a lot of times down to this one step, and that is relationships.

No, seriously. It's really important. Let me show you why.

More than any other step we've talked about in this series, relationships will determine whether you succeed in becoming a web developer. Let's talk about which relationships are important and why they're important.

The first relationships you want to start developing our relationships with other developers. These are going to start becoming important very early as you're starting to learn HTML, CSS, and Javascript. The reason this is important is because it's hard to learn on your own. When you're just building all these mental models for these new concepts, you're going to screw up some of them and you're not going to know how you screwed up, so you're not going to know what to search for or how to fix it.

Having someone else you can talk to, that can save you a ton of time and really can get you somewhere you may never have gotten on your own. You're going to be reciprocating this to developers you have relationships with. So you're going to understand some things they don't and they're going to understand some things you don't and you're going to share back and forth, and both of you are going to be better for it. It's good to have some relationships with people who are basically on the same level, who are learning the same things at the same time because they misunderstand things the same way, and when they figure it out, they can convey that to you.

It's also good to have relationships with more senior developers because they can see two to three steps ahead of where you are, and they can know the the next barriers you're going to run into and let you know about those before you have to spend hours and hours banging your head against them. They can also help when you have a sort of a vague idea of what you want to do, but you don't really understand all the steps to get there. It's great to be able to talk to somebody who has done something similar.

You can meet developers online and you definitely should look for communities around the technologies you're interested in, the industries you want to work in… those sorts of things. But the most fruitful relationships will come from in-person meetings. So find meetup groups that you can attend on a regular basis. Go to developer conferences and make relationships with the people that you meet there and maintain those relationships so that you can be there for those people when they need you and they can be there for you as well.

Meetups are usually free. Conferences usually cost money, and sometimes they are very expensive. You may need to avoid the very expensive ones, but if you can pull together fifty or a hundred or a couple of hundred dollars to go to a conference that's around something you're working in, it's a really valuable experience.

And those relationships, I find – even though they're just sort of a one-and-done – conferences usually just happen… maybe they happen once a year, but they're not regular like a meetup. I still find that sometimes those relationships can be more enduring maybe because everybody has some skin in the game. I'm not really sure, but it's worth doing if it's something that you have the means to try.

Of the two different kinds of relationships that are going to be important to you, the developer relationships are usually the easier ones. Everybody is kind of struggling with the same sorts of problems. You can get together and have pity parties because you've got to support Internet Explorer or you can sing the praises of TypeScript or complain about TypeScript, but you kind of have some common ground and it feels like everybody's in this together. So those relationships are a little bit easier to form. The next type of relationship is maybe even more important.

Once you have some technical chops, it's a good idea to start forming relationships with potential clients or potential employers. If you can start forming relationships with the people who are going to be able to work with you directly, then you have a more direct path to making money off of this skill. Most of the juniors I know who have either come straight out of bootcamp or have been self taught and it looks like they just rolled right into a great job, most of those people knew somebody at the company who was able to refer them and put in a good word and help them get that job. Basically, all of the freelance work I do is built on the back of relationships. I'm never going to people cold just asking for work and getting good work that way. Relationships are the foundational element for making a career in web development.

You can meet people who could become clients or employers at chambers of commerce. You might want to join the chamber of commerce for yourself. By joining you usually get listed in a directory somewhere, but that's not where the value comes from. The value comes from the events. They'll usually have regular events and then they have maybe some special events throughout the year that happen once a year that you can go to. Go to those and meet people and talk to them.

The biggest thing you want to do at any sort of event where you're trying to meet someone like this is you want to come in with curiosity, not coming in with a sales pitch. You're not trying to get them to sign a contract or to get them to buy any services from you. That's not the goal here. The goal here is to form a relationship. If there is an obvious place where you can help – if they've got a problem that you know you can solve, maybe you've even solved it before, that's even better – then you should let them know. But it's not a sales pitch. None of it's a sales pitch. You're trying to build relationships. So you want to go to these events and you want to keep showing up. You want to be seen as a reliable, credible person.

A few more options: BNI groups. That's Business Networking International. This is where a bunch of people of different disciplines – and when I say different disciplines, I mean all across the board; you'll have potentially a web developer in the room, you'll have real estate agents, you'll have people doing carpet cleaning, people selling Mary Kay products… whatever – all these people get in a room together, and they pass referrals to each other. The carpet cleaner knows somebody who needs a website developed – maybe another carpet cleaner in another city that they're friends with. They bring their information to you, and then you reach out and get in touch with them and tell them that you can help. Maybe you have a client at some point that casually mentions they need their carpet cleaned and their old carpet cleaner, closed up shop, and then you bring that information to the carpet cleaner in your BNI group. I don't think this is the best way to get good high paying work, but it is a good way to get started and start getting some work in the door.

I never would have thought this would be a good way to get work until I joined one myself and actually got work. But joining Toastmasters. Toastmasters, by the way, is a club designed around getting better at speaking. One, it's going to help you be better at speaking and presenting your ideas, which will make it easier for you to sell anyway. But two, you're again, building relationships with people, and usually the people in Toastmasters are ambitious people who are doing lots of work and may need your help.

The last place I want to talk to you about where you can find these potential clients or employers – and I think maybe this is the best way. First you figure out what kind of client you would want to work for, and then you go find where they are. So what meetup groups do they go to that are specific to what they're doing? What conferences do they attend that are specific to what they are doing?

It could be kind of broad. It might be nonprofits. It might be startups. A lot of people say small businesses. Don't say small businesses; go more narrow than that. Maybe you do all your work for yoga studios. Maybe you do all your work for bicycle manufacturers.

Go as narrow as you want to go, and just go find where these people are hanging out. If you can establish with them that you not only have an expertise in your domain but also you have an expertise in their domain, then you're really going to look like an obvious choice whenever they need a web application or a website built.

I want to challenge you, introverts, extroverts, and everybody in between. Every couple of weeks, find at least one event you can go to. If you can do more, great, but at least one every couple of weeks, go out and just start to meet people and talk to them. In the beginning when you're first learning, those are going to be other developers like you.

Once you have a decent grasp on some set of technologies you can use to build something, then you want to start also adding in some other sorts of contacts who might be able to pay to do work in the future.

Go out and form those relationships and make sure you maintain them. Keep them up so that they'll be there when you need them, and you can be there for them when they need you.

Thanks again for watching the video. Make sure you [subscribe](https://www.youtube.com/channel/UCxgZYWJq2Cxk7vIePGiTxFw) so you'll catch the next installment in the developer roadmap, and I'll talk to you then.

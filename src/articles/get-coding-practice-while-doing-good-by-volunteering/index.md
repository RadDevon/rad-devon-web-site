---
title: 'Get coding practice while doing good by volunteering'
date: '2020-09-11'
tags:
  - 'career'
  - 'charity'
  - 'tailwind'
  - 'volunteering'
coverImage: 'images/volunteer-opportunities-thumb.jpg'
---

You have several great options to get your coding practice:

🚀 [start a project](https://raddevon.com/articles/10-great-web-development-learning-project-ideas/)  
💰 [pick up freelance work](https://raddevon.com/articles/2020-web-developer-roadmap-step-6-time-to-get-paid/)  
💻 [contribute to open source](https://raddevon.com/articles/how-do-i-contribute-to-open-source-projects/)

But there's one I haven't covered that will let you skill up while also giving you the warm fuzzies: volunteering!

https://youtu.be/FR5BoIQL0_c

Here are 4 sites where you can find a volunteer coding gig that works for you.

❤️ [VolunteerMatch](https://www.volunteermatch.org/virtual-volunteering/Computers%20&%20Technology)  
❤️ [UNV Online Volunteering](https://www.onlinevolunteering.org/en/opportunities?f[]=field_task_id:5)  
❤️ [Idealist](https://www.idealist.org/en/volunteer?actionType=VOLOP&functions=TECHNOlOGY_SUPPORT_WEB_DESIGN&isVirtual=YES&q=&searchMode=true)  
❤️ [/r/volunteer](https://www.reddit.com/r/volunteer/?f=flair_name%3A%22Opportunity%22)

Bonus option: 🗺 [Volunteer locally](https://greatnonprofits.org/)

<!-- If you're looking for ways to practice, another great one is by starting a project. I put together a starter kit to help you get out of your own way and start writing code! 👇

[thrive_leads id='3587'] -->

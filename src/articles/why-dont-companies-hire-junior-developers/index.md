---
title: "Why don't companies hire junior developers?"
date: '2019-05-24'
tags:
  - 'career'
  - 'motivation'
coverImage: 'images/1990-impact.jpg'
description: 'What can early electric cars teach you about your web development career? Read on to find out.'
---

![An early electric car](images/1990-impact.jpg)

_What can early electric cars teach you about your web development career? Read on to find out._

If you've tried looking for work as a new web developer, you've probably gotten frustrated and asked yourself this question: Why don't companies hire junior devs? In order to answer that, we first have to take a step back and understand why companies hire at all.

_Quick note: this won't be representative of every company or every hire. Some companies hire junior developers and some hires are made for reasons other than those I'm discussing here._

## Why do companies hire developers?

When companies are hiring developers, it's usually for one of three reasons:

1. They have **too much work** to do.
2. They need to be able to finish their work **more quickly**.
3. They **lack an important skill** they need to move their business forward.

Hiring is hard and expensive, so, by the time a company has decided to do it, one of these three has probably gone slightly further than it should have. They have a problem and they need it fixed yesterday.

## What They Hear When You Say "Junior"

When you say you're a "junior" developer, that takes some of the pressure off you. It calibrates expectations for a prospective employer. To you, it's an acceptable way to convey your insecurities about starting a new career.

To them, it's something entirely different. To them, it's a merciful advance warning to stay away. Alarms are sounding and lights are flashing as soon as the word is past your lips. Here's exactly what they hear:

- **It's going to take months** before I can contribute in a meaningful way.
- During those months, **I'm going to need to be mentored and trained** by your other developers, decreasing _their_ productivity.
- I have never been a professional web developer, so it's entirely possible you'll spend 6 months trying to get me up to speed **before learning I'm not going to work out**, wasting all that time and money and having to start over with the hiring process.

The only potential benefit to them is that you'll save them money over hiring a more senior developer. That's all well and good, but it doesn't solve their original problem: they need to **go faster**, **increase capacity**, or **gain a skill** they didn't have before. "Cheaper" doesn't help with any of those.

Maybe you'll turn out to be their most productive developer by your second month… but you probably won't. It's a big gamble for the company, and companies are not in the business of gambling.

## How can we fix this?

Sorry to disappoint, but you can't fix it. The reason you can't fix it is that **it isn't broken**.

Some companies have the time, resources, and foresight to hire and train junior developers. Most do not. That means our industry is hard to get into because we have relatively few senior developers with tons of companies who want to hire them, but we have **lots of junior developers with very few openings** that will work for them. That's just how it is… but, even though you can't "fix" it, there is still something you can do.

## What Electric Cars Can Teach You About Becoming a Web Developer

In the 90s, GM produced an all-electric car called the [EV1](https://en.wikipedia.org/wiki/General_Motors_EV1). Some people loved them, but they wouldn't work for most people. The range of 60 miles and 8-hour charge time was incompatible with the lifestyles of people spoiled by the convenience of the range and quick refueling of gas-powered vehicles.

How did we get from there to here? Today, some of the hottest cars are all-electric. How did the car companies "fix" people so they would buy their electric cars?

They didn't. Instead, they sold the early electric cars with little range to people who could use them while improving the technology to make future models useful to more people. **They fixed the product instead of the customer.**

You should take the same approach. You _can't_ get a job that requires an industry veteran, and you won't be able to convince those companies they should hire a junior instead. You _can_ **go find people who can use the skills you have**. Do that to start while you **continue to improve your product** until you have a wider base you can sell to.

## Who wants to hire juniors?

There's no one out there pining to hire an inexperienced developer. There are plenty of people who set a different bar though. If you talk to engineers, they care about engineer stuff. That's exciting since that's the world you want to live in, but it's also a difficult bar to clear at this stage in your career.

Business owners who need small pieces of software — a marketing site, an integration between two of their apps, a customer database — they set the bar much lower, and they don't care about the labels we give ourselves in the engineering world like junior and senior. By [starting your career with freelancing](https://raddevon.com/articles/inverted-career-path/), you can start getting paid while you build your skills to take your next step.

<!-- _If you're tired of shotgunning applications and hoping for the best, freelancing might be the solution. Take my Freelancing Crash Course to learn everything you need to know to start your freelancing business in just a week._ 👇

[thrive_leads id='2262'] -->

---
title: 'The Best Projects for Learning Web Development'
date: '2018-07-09'
tags:
  - 'learning'
  - 'motivation'
  - 'projects'
  - 'willpower'
coverImage: 'images/ddr-pad.jpg'
description: 'When youʼre learning web development, please donʼt try to build another to-do list. Find a project youʼll actually be motivated to finish. Hereʼs my approach.'
---

I've always struggled to keep up an exercise routine. Exercise for the sake of it is just so boring. I have pretty good willpower, so I can keep it up for a while. Problem is, I always seem to fall off after a few months and, once that streak is broken, I need a new novel routine before I'll even start again.

Even while I'm in the streak, it's very perfunctory. I do 20-30 minutes each day and barely break a sweat.

Something happened in my late teens, though. Over the course of several months, **I lost 35 pounds doing about 3 hours of cardio every night** after work. It didn't even _feel_ like working out because that wasn't the point. The secret I discovered was Dance Dance Revolution.

https://www.youtube.com/watch?v=I_jb85TGrdI

## How to Hack Your Brain

How can a person who doesn't enjoy exercise and never keeps up with a routine suddenly start doing 3 hours of cardio every night? The secret was that I _wasn't_ doing 3 hours of cardio every night; I was playing my favorite video game. I loved it so much, I _wanted_ to play for hours a day. The exercise was just a side effect. I didn't even need or want to lose 35 pounds. It just happened.

> The secret was that I wasn't doing 3 hours of cardio every night; I was playing my favorite video game. I loved it so much, I wanted to play for hours a day.

The only time in my life I was successful in keeping up a respectable exercise routine was a complete accident, but what I *learned* from it was extremely valuable in my life going forward. We do the things we *want* to do without prodding, and we do the things we *need* to do until our willpower runs out. This means that, in order to keep up the habits we _need_, we should Trojan Horse them into our lives inside something we *want*.

## The Best Kinds of Projects

When you're trying to figure out which project to tackle as you're learning, first figure out **what is the project that will drive you to work at least an hour every single day?** For me, the most compelling projects to drive learning are my own ideas. These are web applications I've thought of either to fix my own problems or as possible business ideas that might make me a living someday.

**Learning web development is hard.** Find an idea that excites you. You're going to need this to sustain your willpower for a long time. It's not always fun, so find something that will pull you forward through the tough barriers and the frustration.

## How to Find These Project Ideas

The trick to coming up with your own ideas is to **become hyper sensitive to problems** — yours and those of others. When you notice yourself or someone else coming up against some friction in life or work, make a note of it and think about whether or not this can be solved with software. When you talk to people about their work, ask them **what is the most difficult part of their job or business**.

I keep track of my project ideas in an Evernote notebook appropriately named "ideas." Each note in the notebook is titled with the basic concept for the project with additional details in the body of the note. When I want to work on something, I can go back through this notebook and see what excites me most right now. That's going to give my willpower a major boost when I start getting frustrated and wanting to quit.

## Next Steps

Now that you know what kinds of learning projects work best and how to find them, you might wonder what you should do next. My Big Goals course is very short and designed with this idea in mind: that you're more likely to do something difficult when you can hitch it to something you want.

The Big Goals course is all about finding the thing that will keep you awake at night. Your Big Goal will give you the desire to push through all the obstacles on your journey to becoming a web developer.

Share your email below to get started. Can't wait to hear about what drives you!

[thrive_leads id='1360']

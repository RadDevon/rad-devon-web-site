---
title: 'Should a web developer build their portfolio site from scratch?'
date: '2018-01-28'
tags:
  - 'freelancing'
  - 'portfolios'
description: 'Most new developers feel that they lose credibility by using off-the-shelf components and libraries in their portfolios now that they know how to build everything themselves. Is that actually the case?'
coverImage: 'images/construction.jpg'
---

This is a common question from web developers who are just building their first portfolios to start showing off their work. The thinking is, “If I’m a web developer and I use an off-the-shelf CMS or even an off-the-shelf theme, **I’m basically a fraud, right?** I _can_ build this stuff, so I _should_.”

Building a really cool custom portfolio site can give you an edge to stand out amongst other developers… **but it can also be a trap**. Most of your prospective clients/employers are interested in **what problems you have solved and how you approached them**. Your portfolio needs to show this first and foremost. It’s easy to waste a lot of your time building your site instead of figuring out how to highlight [the skills people find most valuable](/articles/the-most-valuable-skill-for-web-developers/). As developers, it’s what we gravitate towards since building things is fun!

While having a really sweet custom portfolio site _can_ show off your technical prowess, that’s not as valuable as being able to translate business problems into software solutions, whatever forms those might take. That’s why I recommend using off-the-shelf solutions when they already exist. The software to display your projects already exists (many of them do, in fact). The theme ecosystems for popular CMSs are flush with great designs. You can find one that’s close to what you want and customize it to suit you.

[thrive_leads id='1354']

Your time is valuable. Make sure you’re spending your time **on work that will return value back**. When you’re trying to get more work, that value comes from showing the value you have provided to others. So, rather than building a custom CMS and carefully theming it so that the style is a reflection of your personality, install WordPress (or Drupal or Craft or anything), get a nice theme, and **start showing your clients and employers the stuff they really want to see**.

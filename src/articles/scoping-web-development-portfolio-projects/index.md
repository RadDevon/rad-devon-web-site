---
title: 'Scoping Web Development Portfolio Projects'
date: '2019-05-10'
tags:
  - 'career'
  - 'portfolios'
  - 'scope'
coverImage: 'images/web-design-planning-notebook.jpg'
description: 'Scoping is the process of determining which features will be included in your finished project. Inexperience can get you into real trouble because youʼll gravitate toward the kitchen sink approach (that is, including everything but the kitchen sink). Hereʼs how to avoid this pitfall.'
---

![A web design and development planning notebook](images/web-design-planning-notebook.jpg)

If you're starting to build your first web development portfolio, you may not have a robust body of work to fill it with. You might decide to build your own projects to seed the portfolio.

If this is you, chances are you haven't scoped projects before. Scoping is the process of determining which features will be included in your finished project. This inexperience can get you into real trouble because you'll gravitate toward the kitchen sink approach (that is, including everything but the kitchen sink).

## The Least You Need to Build Is Probably Less Than You Think

This is the trap new web developers fall into. They want to put their best foot forward. They want their portfolio projects to be comprehensive and full-featured. This is a great way to waste a ton of time.

Instead, borrow a concept from Silicon Valley: the **minimum viable product**. The minimum viable product is the crux of the **lean startup movement** that grew up out of lean manufacturing and now drives the tech startup world. The idea is that, instead of building the most full-featured version of what you're envisioning, you'll build the _least_ you can to get the job done.

## What job are you trying to do?

Despite what you might think, **doing less work requires you to be more thoughtful** than the kitchen sink approach. Instead of planning to build a giant list of features and using all the hottest new frameworks and libraries, you need to be critical about **what you're actually trying to show** with this portfolio and build to that target.

The job you're trying to do with a portfolio project will change depending on who or what you're trying to achieve with the portfolio. **A "portfolio" is not a singular thing that always fits one mold.** It should be tailored to the audience you want to reach with it. Build to what will speak to them.

For example, a portfolio to get you hired in a full-time permanent position needs to show much different things than a portfolio to pick up freelance work. You can further subdivide.

A portfolio to get hired by Microsoft, Amazon, or Apple might show lots of work with algorithms and data structures where a portfolio to present to startups might be more effective showing small, practical pieces of software.

A freelance portfolio for a developer who wants to sell performance audits should show the results of some past audits. A portfolio for someone who builds custom API integrations should show examples of those.

<!-- _Hey there. If your portfolio is the first step you're taking in your freelancing career, my Freelancing Crash Course will help you with the other pieces you need to get started._ 👇

[thrive_leads id='2262'] -->

For the portfolio project you're going to build, answer the question, "what job should this portfolio piece do?" If you extrapolate that question all the way out, the answer for _any_ portfolio piece is that it should build trust that you can do what you're trying to be hired to do, but how should it do that? Take your answer to "how will this portfolio piece build trust," and put your project on the "lazy path."

## Find Success on the "Lazy Path"

Laziness gets a bad rap. Sometimes it can be an incredibly useful tool. Use it heavily when you're scoping your portfolio projects.

You're trying to build trust here, so your projects **still need to be good**. The "lazy" path is not about slapping something together but about doing your best work on building **the smallest feature set** that does the job.

For a more comprehensive view of what your portfolio should do, check out my post on [what you're doing wrong with your portfolio](/articles/what-youre-doing-wrong-with-your-web-development-portfolio/).

## Being lazy is usually easy, but…

You got into web development because you wanted to build things, so the temptation to build too much can be strong. Set your scope to the smallest one possible to get the result you want, and **stick to it**. If you don't, you'll end up with an unfinished portfolio of robustly conceived but incomplete projects and no paying work to show for it.

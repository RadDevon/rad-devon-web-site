---
title: 'How to Get Freelance Web Development Prospects to Hurry Up!'
date: '2019-09-27'
coverImage: 'images/turtle-in-the-desert.jpg'
description: 'Prospects are never on your timeline, but you can implement processes to speed things up. A mindset shift may also be in order.'
---

You talked to a new prospect, and they sounded interested. You've discussed the problem and a way you can solve it by building incredible web software, and it sounded like a great project. You spent a couple of hours pulling together an estimate for them, and sent it over. Want to know what happens next? Click "play" on the video below.

https://www.youtube.com/watch?v=K8E_zMLCRNg

Crickets. You hear nothing from the prospect **for weeks or maybe months**! This _might_ happen after you have an estimate together… or it may happen right after you met and sent them an initial email… or maybe the prospect sent over some project details but you're waiting (and waiting… and waiting) to hear back on some follow-up questions you had.

![Turtle (your prospect) crawling through the desert](images/turtle-in-the-desert.jpg)

Whenever it happens, it feels bad… especially if you need to close that job soon. **How can you get your prospects to hurry up?**

## Truth Is…

You can't and shouldn't try to get a force a prospect to convert on your timeline. If a prospect is taking their time to convert, it reflects one of a few different things. Here are some possibilities:

- The prospect has other priorities
- This project isn't really that important
- The prospect isn't sure your solution is right
- The prospect generally isn't sure about you and your ability to deliver

Remember when you were a kid going on vacation with your parents? You'd sit in the back seat asking, **"Are we there yet?"** every 15 minutes of an 8 hour drive, making everyone miserable in the process. **You never want to be that as a service providers.**

If you want to try to get a client to convert faster, you can **pull on some of the levers these issues reveal**. Do more to show them the project will be valuable to them. Do more to show them you're reliable. Do more to show them this is the best solution to their problem. These are way better than emailing every week saying, "Are you ready to start now? No? How about _now_?"

## But that's not really a solution either.

Your clients are busy. They have a business that asks them to do a lot of different things. The web site or application you're going to build is one of **potentially dozens** of projects they have going on. It's probably the only project _you'll_ have going on with them, so, while it may be top priority for you, it might _not_ be for them.

You can't change that. What you _can_ change is _your_ side of the equation. You never want anything to hinge on getting a single client to sign a contract at a specific time. The way you fix this problems is by **filling the top of your sales funnel**.

## What's a "Sales Funnel?"

The "sales funnel" is a flawed analogy people use to talk about how selling works. If you think about a funnel, you can put a lot more in the top than you can get out of the bottom.

In the sales funnel, that's supposed to reflect the fact that **you'll have more people _interested_ in what you're selling than you'll have people who actually _buy_**. (The difference is that _actual_ funnels usually don't lose matter going from the top to the bottom while sales funnels do, but I digress.)

## How Does This Help Convert Clients Faster?

It doesn’t. All this means you have to put a lot of prospects into the top of your funnel in order to get a few sales out the other end. **If you're finding yourself under pressure to convert a single prospect at a certain time, that probably means your funnel isn't full enough.**

If you're finding that you sometimes have _no_ clients to work on, **you just need more prospects.** If you have a single prospect and you need to fill a gap in your schedule, the chances you'll be able to convert the prospect to a client in time is slim. If you have _20_ prospects, the chances you'll be able to convert 1 of those to fill the gap in your schedule is way better.

<!-- _Psst. As your funnel gets fuller, you need to have a good grasp on how to price and estimate your projects so you can more quickly move those prospects through the funnel. I have just the solution for that._ 👇

[thrive_leads id='2463'] -->

## Change _Your_ Behavior Instead of the Prospect's

It's much easier to change what _you're_ doing than to try to bend each and every prospect to your schedule. With more prospects, you'll be much less likely to have those challenging gaps in your schedule.

If you're worried about what happens when your schedule gets filled, it's not a problem. Any prospect who want you to start while your schedule is full will have to wait. Now, instead of letting _their_ schedule affect _your_ business, **you've taken back control of your freelance well-being**.

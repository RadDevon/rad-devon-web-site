---
title: 'Web Developer Roadmap Step 6: Time to Get Paid!'
date: '2020-02-14'
tags:
  - 'clients'
  - 'freelancing'
  - 'networking'
coverImage: 'images/thumbnail-1.jpg'
description: 'You may find that, even after you know how to build web sites and applications, you canʼt find a company to hire you. In this video, I share my solution to your problem.'
---

https://youtu.be/Aa7Dw2rvnkc

## Video Notes

You may find that, **even after you know how to build web sites and applications, you can't find a company to hire you**. In this video, I share my solution to your problem.

### Why Freelancing

Start with freelancing because it allows you to **gain experience** while also **earning money** and **getting your foot in the door with great companies**.

### How to Fail

Here are some common ways people fail at freelancing as new web developers.

- **🎯 Targeting tech companies**\- This doesn't work for the same reason it's hard to get hired straight after bootcamp or after you've learned to build on your own. The bar is too damn high.
- **🐔 Counting your chickens before they hatch**\- In the video, I called this mistake "putting all your eggs in one basket," but this is actually the better chicken-based cliché to describe this problem. A prospect is close to signing a contract, so you stop pursuing others. Then, that client falls through and you have to start over again. Keep going until the contract is signed and the deposit check has cleared (and even after that)!
- **😖 Getting discouraged**\- You're going to get turned down a lot. It's nothing personal. Just keep going until you find the right fit. In fact, [you should be looking for reasons to disqualify clients](/articles/how-to-get-fewer-freelance-clients/).
- **🤦‍♀️🤦🤦‍♂️ Overpromising**\- Don't lie to sign a client. It's bad business. Balance this against the knowledge that you're never going to feel ready to start freelancing. You'll have to start before you feel ready, but don't swing too far in the other direction and start claiming you can deliver things you can't.

### How to Succeed

Here are my tips for succeeding at freelancing as new web developers.

- **🤝 Build your practice on relationships**\- See [the previous video in this series](/articles/2020-web-developer-roadmap-step-5-getting-work-through-relationships/).
- **⚡️ Short-circuit trust-** You get hired when people trust you, but it can take a long time to earn that trust. Find ways to shortcut that like by building a proof of concept to show you can build what the client wants.
- **🗣 Make sure people know what you do**\- You want clients and you want to get paid. Don't be shy about telling people what you do and when you can help with a problem. Don't be salesy though.
- **💸 Help for free**\- Don't spend 1,000 hours building someone an app for free, but if you have some quick advice that would help someone, tell them! This helps earn trust. The same people you help for free will come back when they need something big.
- **🤑 Keep selling when you have enough work**\- Much easier said than done. If you don't do this, you're going to be in the feast/famine cycle forever.
- 💵 **Charge enough**\- You probably need to charge more than you think. Quick tip: take the yearly salary you'd like and divide by 1,000. That's a reasonable hourly rate for starters.

### Stuff I Used in the Video

- [A chime sound](https://freesound.org/people/Stickinthemud/sounds/44165/) by Stickinthemud on freesound.org
- A cool song by [this Jason Shaw person](https://audionautix.com/creative-commons-music)
- Clips from various TV shows and movies

Thanks for watching the video! I'd love to hear any questions you have in the comments [on YouTube](https://youtu.be/Aa7Dw2rvnkc) or directly [on Twitter](https://twitter.com/raddevon). [Subscribe on YouTube](http://www.youtube.com/channel/UCxgZYWJq2Cxk7vIePGiTxFw) so you don't miss the rest of the roadmap!

## Other Videos In this Series

If you like this video, watch the others in the series for a good overview of the philosophy I used to break into web development with no connections or experience.

- [Step 1: Set Your Big Goal](/articles/2020-web-developer-roadmap-step-1-set-your-big-goal)
- [Step 2: Learn HTML and CSS](/articles/2020-web-developer-roadmap-step-2-learn-html-and-css)
- [Step 3: Learn Javascript](/articles/2020-web-developer-roadmap-step-3-learn-javascript)
- [Step 4: Learn Terminal, Git, and Deployment](/articles/2020-web-developer-roadmap-step-4-learn-terminal-git-and-deployment)
- [Step 5: Relationships Are Make or Break](/articles/2020-web-developer-roadmap-step-5-getting-work-through-relationships)

## Transcript

Last time in the roadmap, we talked about relationships and how important those are to your web development career. In this step, we're going to take the final step towards going pro and learn how to get paid for your new skills.

If you've taken time during your learning to practice by building projects and by shipping projects, you should at this point and be ready to take on a small freelance project. I recommend that **everyone** start with freelancing, even if it scares you to death, and even if you're 100% certain that you're not going to do freelancing as your career. Here's why.

The number one reason is that, especially if you're self-taught or if you're coming out of a bootcamp, probably nobody's going to hire you. I know that might sound discouraging, but really, I'm just trying to encourage you to go down a different path.

Also, I realize this isn't always true. Some people can come straight out of boot camp or finish their last freeCodeCamp certification and immediately roll straight into a great entry level job. That's great. But I've seen enough people flounder after learning what they think are all the skills they need that I think there's a better way to actually break into the industry.

The reason you probably can't get hired is because you're an unknown quantity. You've learned the skills, but no one you're handing your resume to knows what that is actually going to translate to in the workplace. You need to fix that problem and make yourself look less risky. Freelancing is one way you can get there. If you go out and do some freelance projects and have success there, now you can approach a prospective employer as not just somebody coming in off the street who's built some pet projects, but as someone who's actually had success working with someone else and getting paid to build a project that's actually being used.

They can call the people you worked with and ask them what you were like to work with. If you just come in with a portfolio of your own projects, they can't do that. If you do six months of freelancing and do a handful or maybe a dozen projects in that time, you can approach prospective employers as a much safer risk than you were six months earlier, and you'll have some extra money in your pocket too.

Sometimes freelancing can in a much more direct way lead to your getting a full time job. Most of my career has been freelance work, but I've also had a couple of full time jobs. Both of those employers learned about me through working with me as a freelancer.

The companies grew, as we were working together and they needed someone to come on to do the work I was already doing. Well, of course the first thing they're going to do is they're going to ask me. In the case of the two of those offers I've taken, I was also the last person they asked. That means any other bootcamp grads or self-taught developers who might've been looking for a job at those companies at the time, wouldn't have even had a shot at this one. Those jobs were never advertised.

When those companies _did_ end up making those offers to me, there was no behavioral interview. There was no whiteboard coding challenge. There was no take home coding project. Actually, there kind of was a take home coding project. It was all of my freelance work up to that point. The big difference is I got paid to do it all.

The other thing I really love about freelancing – and this has really changed my entire career – is that it empowers you knowing that you can go out on your own and basically flip a switch and create a career out of nothing. It gives you all sorts of power to shape your career in the ways you choose. It doesn't mean that you have to always work alone. It doesn't mean that you can't ever work for someone else, but if you get into a situation where you're working for someone and things change and the work environment becomes toxic, you know that you can go off and do your own thing. You don't have to stay there because you need the paycheck. You can go create the paycheck for yourself.

I want to share a little story with you that happened to me. Once upon a time there was a guy named Devon. Devon was a web developer at a company he liked and he enjoyed his job. He worked hard, and he learned a lot. One day, he got a new boss. They got along well. Things were great… until Devon learned his new boss expected him to know some things he didn't know. How can he fix it?

Devon didn't sign up for these new responsibilities, but he was ready to tackle them if he just had some time to learn. Unfortunately, this wasn't possible. The company needed to move fast.

Devon was miserable. Even though he was doing everything he was hired to do, he wasn't meeting his new expectations. He was letting his company down and he felt bad that he could no longer do the job. Instead of suffering and leaving his company without the skills they need, he resigned.

Since he knew he could flip a switch and have a business doing what he loved, he could move on with confidence.

It's also really great in salary negotiations because, if somebody is coming to you because they want to hire you and you are already making the money you need to make on your own, you can really ask for exactly what you want, and you can just say "no" if you don't get it. You don't have to take whatever they're offering.

Hopefully that does a good job of explaining why I like freelancing as the first step in taking your new skills and turning those into a career.

Before we get into how to succeed in freelancing, I want to talk about some of the ways you might fail.

The first one I see a lot of people doing is they want to target the big tech companies. They want to do freelance work for Google, Amazon, Microsoft, Apple… These are companies that it's really hard to freelance for. They already have massive teams of engineers on staff, so they're looking for someone who is the absolute best or they're looking for someone who has a very narrow area of expertise that they don't have someone on staff for.

As a brand new developer, you probably don't fit into either of those categories, so all you're going to find by courting tech companies and trying to offer them freelance work is a lot of rejection usually. It's like you decided you wanted to try basketball and your first step is to try out for the Lakers. Let's try starting in the rec league instead.

There are tons of businesses out there that are not tech companies but who need tech to do what they do or to do it better. When you approach them and tell them you can help, they're not going to have an engineer looking into your background to make sure you have 10 years of experience in some esoteric tech stack. What they really want to know is, are you credible and can you solve the problem they're having? Those are the kinds of companies, as a new developer, you want to try to work with.

Another mistake I see when people are starting to really court freelance work is putting their eggs in one basket and then another basket, and then another basket. And here's what I mean by that. Lots of conversations around starting a freelance project get to the point where everyone seems interested. New freelancers especially can get the wrong idea that that means the deal is done. It's just an issue of formalities now. But really at this point, there are all sorts of ways that this deal can still fall apart. If you're not still pursuing other clients while you're trying to close this deal that seems like it's imminent, if that deal falls apart, you're left starting from square one again.

Instead, if you have five different conversations going with different people, there's a much better chance that one of those will close and become a project that you'll actually get to work on. Maybe even two of them close and then more of your time is utilized and you're making more money.

What you _don't_ want is to get really excited about every project that's getting close and not worry about keeping that sales pipeline full so that you have backup projects and next projects to work on.

A lot of people who want to become web developers are not in any way used to selling, and that definitely describes me. So it can be really easy to get discouraged when people start turning you down. You're going to get a lot of nos and that's okay. That's part of the process. Just keep moving and find someone else who is a better fit for you.

This is something that's really hard at first if you're not used to it and it gets easier and easier as you go through the process. It's almost impossible to not feel a little bit hurt and take it a little bit personally the first time somebody says they don't want to work with you.

I would even go a step beyond saying not to be discouraged when people say no. I would say to look for reasons to disqualify people. Try to find the reasons that a project is not going to work out, either for you or for your prospect, before you start that project. Make sure you're only working with clients who are going to come away happy with what you can build for them and make sure that you're going to enjoy working with them too.

You may be worried about turning down work, but the thing is, taking on a project that's not the right fit leaves you with less time to take on a better project that comes along in a week or two. I wrote a small article all about [disqualifying clients](/articles/how-to-get-fewer-freelance-clients/) that you should check out.

Another thing I find people doing is they hear all these really sexy stories about people starting new businesses and "fake it till you make it," and they just lied their way to the top until they actually had the skills they were pretending they had. And then they finished the awesome project and made a ton of money and everything worked out, and that was the first step on their path to stardom.

I'm pretty sure there's a survivorship bias in those stories. They get told, so we only hear the good side of those stories. I'm sure there are probably… for every one of those fairy tale outcomes, there are probably 10,000 horror stories about somebody who pretended they could do something and then everything just fell apart.

The goal – for me at least – is not to get a signature on the dotted line at all costs. It's to work on projects that make everyone win. But I will throw in a caveat that most people never feel like they're ready to start doing freelance work, so you have to balance that. You may have to start doing it before you fully feel like you're ready.

Those are a few of the ways you might fail at freelancing. Now let's talk about some of the ways you can succeed.

The first one is to build your practice on a foundation of relationships and showing up and being consistent. And if you want more on that, check out [the previous video in the roadmap](/articles/2020-web-developer-roadmap-step-5-getting-work-through-relationships/). It's all about relationships.

The second way to succeed, especially in the early going, is to find ways to short circuit trust building. Trust takes a long time to build. Showing up, like we talked about in the last way to succeed, can help build trust. I stumbled upon a great way to short circuit trust building in my own freelance career when I was getting started.

I was looking at this gig that I was mostly qualified to do and I thought it would be fun to work on, but one thing they asked for that I didn't have any experience with is… they were using jQuery Mobile. I wasn't sure if I would be able to build anything with jQuery Mobile, so I wanted to prove that to myself and to do so, I built a small proof of concept.

It probably took me a couple of hours to build this just a little application and using the specific jQuery Mobile component they were using in their application that they had asked about. I didn't really have much trouble with it. I got it up and running, and I felt like I was ready to take the job based on that. And here's where the trust-building short circuit happened. I figured that since I had this proof of concept I had already built to prove to myself that I could do it, why not just share that with them so that they can know that I know how to do it as well? Long story short, I shared the proof of concept with them, and I'm like 99% sure that's the reason they picked me to take the job.

Another way to make sure you succeed at freelancing is when you're out doing your relationship building, make sure people know what you do. They're probably going to ask anyway because that's kind of what people do in these sorts of settings, so it's not going to be hard or a stretch to try to slip it into the conversation. It's going to be pretty easy to do. Just make sure you're not shy about it and, going one step further, if someone is talking about a problem you know you can fix for them, let them know. It doesn't have to be a hard sell or, or a pitch of any kind. Just "Oh yeah, I worked on a problem like that for somebody else. Here's what we did." Share with them. You are helping them by showing them a solution to a problem they're having, and as a result, they might come to you to build a similar solution for them.

Another way to be successful as a freelancer is to give your knowledge away for free. Tell people how to do what they need to do to solve their problems or tell people how you would do it. Now, you're not going to go tell them how to build some massive web application, but if it's something short that you think they could do or that it would be easy to tell them about, just share it with them.

Two things could come out of doing this. One, they go do it themselves. Sure they don't hire you, but they feel really good about you because you help them for free, so next time when they have something that needs to be done that they probably can't do, and they think you can do it, they're gonna look for you. And the second thing is that they go back and try to do it themselves, realize they're not going to be able to do it at a high level, and they also don't want to take the time to do it. Then they might come back to you immediately and ask you to do it. It's kind of a paradox, but giving away your knowledge for free usually leads to more money for you in the end.

I touched on this one a little bit earlier in the things you shouldn't do, but make sure you keep selling even when you have work to do. As developers, most of us would much rather be writing code than trying to sell contracts, so it's very attempting when you have a decent workload to just lock yourself into your office and code for 8, 10, maybe 12 hours a day until these projects are done. The problem with doing that is now you have no work and you have nothing in the pipeline.

And the last tip I'll talk about for making your freelancing successful is to charge enough… but not _too_ much. You want to make sure you're earning what you're worth, but if you try to charge more than the project is worth, you're just going to lose that project. That's probably OK. In a lot of cases, you really want to lose projects that are not worth your time.

Most new developers watching this video though, are probably not going to have much trouble charging too much. Their problem is going to be charging too little. A quick shortcut for figuring out what your hourly rate should be, if you want to start by charging an hourly rate, is to take the annual salary you would be comfortable with and divide it by 1,000.

So if you'd like to earn $100,000 annual salary, a decent rate would be $100 an hour. If you'd like to earn $50,000 a year, an hourly rate might be $50 an hour. This shortcut isn't perfect, but it does generally account for things like buying your own health insurance, taking vacation time… those sorts of things, the benefits that your employer might pay for that you're now going to be paying for yourself.

I just put out [a book that goes into a lot more detail on pricing and estimating projects](/freelance-web-development-pricing-rates-estimating-projects-and-turning-a-profit/), so if you want to explore that further, that book would be a great resource for you.

Like I said, at the top of the video, whether you'd like to do freelance work forever and have that sort of freedom or if you just want to go to work for somebody and get a steady paycheck and have, you know, your health benefits paid for and all that good stuff, I think freelancing is a great first step to start building that real world experience while also getting paid to do the work.

Thanks for watching the video. Be sure to [subscribe to this channel](https://www.youtube.com/channel/UCxgZYWJq2Cxk7vIePGiTxFw) for more resources on becoming a web developer, and I will talk to you in the next one.

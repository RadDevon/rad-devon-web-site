---
title: "What You're Doing Wrong with Your Web Development Portfolio"
date: '2019-02-01'
tags:
  - 'career'
  - 'freelancing'
  - 'marketing'
  - 'portfolios'
  - 'sales'
coverImage: 'images/upside-down-jetski.jpg'
description: 'Learn how new web developers usually go wrong with their portfolios so you can avoid the same fate.'
---

In this article, I'll show you how new developers usually go wrong with their portfolio sites and what you should do instead.

When you've recently learned how to build for the web, you're naturally proud of the things you've learned. I've looked at dozens of portfolio sites built by new developers, and the biggest mistake is always the same. They build a portfolio that's all about them and all the things they've learned.

![Upside-down Jetski](images/upside-down-jetski.jpg)

This doesn't make for an effective portfolio site. Your portfolio site should be focused on reducing the risk in hiring you. You'll do this by showcasing work similar to the work you want to be hired to do. That work should solve a problem for the person you want to hire you, and you'll want to highlight how it does this. You can use social proof. Show testimonials or recommendations from people who are like the people you want to pay attention.

Here are some of the things you *shouldn't* include in your portfolio site, and, where it's not obvious, what you'll do instead.

## Long Lists of Skills & Skill Meters

It feels like a long list of skills makes you look more attractive, but it really just **makes you look unfocused**. Meters are a way to show off all your hard work learning this stuff. Unfortunately, they may make *you* feel better, but **they don't tell people viewing your portfolio site much of anything**.

![Example of skill meters](images/skill-meters.png)

Example of skill meters

### What to Do Instead

**If you're going after a full-time position**, it makes sense to list a few key skills. Pick the 4-5 that are most applicable to the work you want to do. If you don't _know_ what kind of work you want to do, just pick something. (You can always change it anytime you want.)

That makes your portfolio glanceable, but make sure you include details about your projects that **show how you can actually apply those skills**.

**If you're a freelancer**, make sure your skills speak to the clients you want to serve. Businesses generally don't care if you're building their solutions with Node or with Python. The skills _they_ care about are those that generate **business results**. Instead of a bulleted list, make sure you lead your portfolio site with the problem you solve. When you display your projects, tell the result of your solution in each case.

<!-- *If you're getting started in your web development career and need some help getting started, I'd love to help. Sign up for a free mentoring session!* 👇

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

## Highlighting “Common Denominator” Skills

When you want to highlight something you're good at, answer this question first: **Would anyone claim they're _not_ good at this skill?** If they wouldn't, you're not differentiating yourself by claiming you _are_ good at it. Can you think of someone who would tell a prospective employer they are not honest? That they are a poor communicator? That they are unambitious or a slow learner? Can you think of a single developer who makes sites that are not "beautiful?" Making these claims doesn't really give you a leg up.

## Projects without Context

**Showing pictures of a web site you built isn't very useful to someone who may want to hire you.** Even if you provide a link, it still isn't enough. For one thing, no one knows what you actually did on the project. For another, it leaves behind important context like the problem you were trying to solve, the solution you built, and the results.

Buying your services based on an image would be like buying a car without driving it or buying a house without walking through it.

## Being Spread Too Thin

I touched on this when I talked about skills, but it's important enough to deserve its own heading. Even if you do everything else right, if your portfolio shows 3-4 different categories of solutions you work on, it's only going to serve to alienate anyone who wants to hire you for any one of those solutions.

Imagine you want some really good ice cream. Are you more likely to go to the all-you-can-eat buffet that also has a soft serve machine, or will you go out of your way to find the place that makes their own ice cream on-site and does nothing else? Easy decision, right?

### What to Do Instead

**You want to be that easy decision.** Focus your portfolio on one or, at most, two kinds of solutions you build, and highlight projects that show you're a home run for anyone who has that problem.

This doesn't mean you have to provide only a single service to a single kind of client. You can have different portfolio sites that each speak to a different kind of client/employer or highlight a different kind of solution.

## Using the Language That's Meaningful to You

You'll start out writing your portfolio page using the language that's meaningful to _you_. You may use technical jargon when you talk about your projects, or you might refer to yourself as a "web developer." Depending on who will be looking at your portfolio, that might be OK. If you want to get hired for a full-time role, or if you want to slot into an existing team as a freelancer, the person making the decision will probably be technical enough for this to work.

In fact, you will *want* to use the language that's meaningful to you **_if_ that's also the language that's meaningful to your audience**. If it's not, though…

### What to Do Instead

**…use the language that means something to the people you want to serve.** You may be a "web developer" in your head, but you don't want to be trying to sell "web development" to a yoga studio, a shipping company, a local restaurant, or a television station. They likely already have some kind of web site. Instead, you need to be selling them on the benefit you can deliver that they desperately need. In your terms, you might improve performance of WordPress sites, but, to your clients, you may help them increase their search engine ranking. You do this by improving performance, but they don't really care about that.

**Find the language that is meaningful to your clients and use that.** If you don't know what this is, it's time to get in front of those people and talk to them.

## What if I don't have any projects to show?

You need to fix that. You can't build a portfolio without projects to show. Figure out who you want to attract with your portfolio, and build a tightly scoped project or two that shows off what you can do for a client or for an employer.

If you're not sure who you want to work for or what kind of work to do, you still need projects to prove you can build something that would be useful to someone. If you're having trouble coming up with ideas, use [my list of project ideas](/articles/10-great-web-development-learning-project-ideas/) for inspiration.

## But, <famous developer/> doesn't follow any of this advice!

Once your reputation precedes you, you too can break all the rules and build a "developer portfolio" that's just descriptions of all your cats. Until then, **you need every advantage you can get** to break through the noise and earn the trust of visitors to your portfolio site.

## If you take away only one thing…

Everything in your portfolio should be centered around the people you want to hire you. Don't talk about you. Talk about **their problems**. Don't build an inventory of your skills. Show **solutions** you've built to their problem.

Keep this principle at the forefront as you build your portfolio, and you'll make a profound impact on people who visit it.

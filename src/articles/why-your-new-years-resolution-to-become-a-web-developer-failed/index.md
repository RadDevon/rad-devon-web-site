---
title: "Why your New Year's resolution to become a web developer failed"
date: '2020-12-27'
tags:
  - 'career'
  - 'motivation'
coverImage: 'images/more-effective-resolutions-thumb.jpg'
---

Remember that familiar pattern with New Year's resolutions? You make them before the year starts. You're doe-eyed, sure everything is going to be different this year.

Maybe you're able to stay on track for a while, but eventually, you run smack into a wall. You say you'll pick back up on it next week… which then becomes next month or next year.

I looked at the most common New Year's resolutions and tried to understand why they fail. I've identified two weaknesses. If you can eliminate these from your resolutions to become a web developer this year, you'll have a solid chance of making it happen.

https://youtu.be/BSGr-N2QZ00

While we're here, I want to leave you with one more tool that can help you stick with it this year. It's my Discover Your Big Goal course. Having a Big Goal is one thing that gave me the strength and motivation to keep pushing about seven years ago when I set out to become a web developer (after many failed attempts). Take this free short email course to develop that tool for yourself and understand how to use it.

[thrive_leads id='1360']

While you're here, you might also check out my video from last year about [why you maybe shouldn't make a resolution at all](/articles/your-new-years-resolution-makes-it-harder-to-become-a-web-developer/). Whether you do or not, everything I said there still holds true this year.

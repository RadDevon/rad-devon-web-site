---
title: '8 Things I Learned to Get From Walmart Cashier to 6-Figure Web Developer'
date: '2018-08-17'
tags:
  - 'career'
  - 'freelancing'
  - 'mindset'
  - 'philosophy'
coverImage: 'images/justification-for-higher-education-1.jpg'
description: 'These 8 lessons took me from low-paying jobs to well-compensated web developer, and they might help you too.'
---

One of my middle school teachers had a poster in her classroom. It was a multi-car garage, doors open showing several exotic cars. The text on the poster: “Justification for higher education.”

![Justification for Higher Education poster](images/justification-for-higher-education-1-1024x683.jpg)

I doubt there was ever a time when most college graduates were going on to own Ferraris and Lamborghinis, but I _do_ believe there was a time when a college degree **guaranteed a comfortable standard of living**. Those days have long since passed.

Now, we need another kind of education. A school of hard knocks cobbled together from **the disappointments and failures of the traditional career path**. We were lead to believe that, if we followed that path, the rest would take care of itself… but what happens when it doesn’t?

I followed that track until I was 30. It took me from cashier jobs at a grocery store and at Walmart to working in a call center for Comcast to a job in IT paying about $36k a year.

The nice thing about low-paying jobs is that they are easier to leave. I spent the next six years figuring out how to make things work on my own. These are **the 8 lessons** I feel were most critical to get me where I am now.

1. [Find **a Big Goal**](/articles/stay-motivated-learning-web-development/) that drives you forward, stops you from quitting, and can refill your willpower reserves.
2. Do tutorials and read books as-needed to get up-to-speed, but **use [personal projects](/articles/the-best-projects-for-learning-web-development/) to drive your learning**. ([I shared some ideas](/articles/10-great-web-development-learning-project-ideas/) if you're short on inspiration.)
3. **Don’t stress** over the tech and software you use to get started. (That's why this site has a single recommendation each for [the best laptop](/articles/the-best-laptop-for-web-development-its-complicated/) and [IDE](/articles/best-ide-beginning-web-developer/) you should be using.)
4. Waiting to be hired **costs time and money**: **time** because you need to find a junior position which is harder to come by and **money** because you’re unproven and are asking your employer to take on that risk. This means you’ll probably get paid less than what you’re worth.
5. **[Start your career with freelancing](/articles/inverted-career-path/)**. This will force you to learn about business which will make you a better web developer. Every web project starts with a business goal. If you can focus on solving that instead of just building a web site, you’ll be [much more valuable](/articles/the-most-valuable-skill-for-web-developers/) as a web developer. Besides that, it's a much easier on-ramp, and you get experience which will help you be paid what you're worth if you seek a permanent position later. (I can help you get [your first freelance gig](/articles/get-first-freelance-web-development-job/) and [learn how to send invoices](/articles/how-to-invoice-your-first-freelance-web-development-client/).)
6. [**Disqualify clients early and often**](/articles/how-to-get-fewer-freelance-clients/). Each client you take on represents an opportunity cost: it’s another, perhaps _better_ client you can’t take on. It’s better for the prospective client too. If you’re not the best fit for their job, they’ll have less success and be less satisfied. You want your freelance jobs to be **a home run** for everyone involved.
7. [**Build relationships with people**](https://raddevon.com/articles/how-to-get-more-freelance-clients-through-networking/), even if you’re an introvert. Friendships and great work opportunities will follow. Go to meet-ups and conferences. Talk to people about what you do, and, more importantly, about what they do.
8. [**Invest in yourself**](/articles/smart-investments-in-your-freelance-web-development-career/). Buy the right tools you need to do a good job. Spend money on learning new things if that leads to a better/quicker outcome than doing it for free. Hire people to do things you’re bad at or don’t want to do, both in your business and your personal life.

Knowing these principles alone won't make you successful. Heck, I _wrote_ them, and I can barely force myself to follow them a lot of the time. For now, just read them, try to understand them, and keep them in your back pocket. If you're like me, it will take a few false starts to muster the discipline to try them.

Once you have that discipline and start living by these rules, you'll see your web development career start to take off. 📈

<!--*If you're wondering where to start, try the first lesson on this list: finding your Big Goal. I teach you how to find yours in my free Big Goal course. Share your email in the form below.* 👇 -->

[thrive_leads id='1360']

---
title: 'Woo Clients By Showing Instead of Telling'
date: '2013-09-26'
tags:
  - 'business'
  - 'clients'
  - 'contracting'
  - 'freelancing'
  - 'sales'
description: 'Telling a prospect how great a web developer you are is way easier than actually being great. Thatʼs why showing what you can do is more powerful. Hereʼs how I used this to get an early client.'
coverImage: 'images/silence.jpg'
---

If people already know you and like you, it's easy for them to decide to pay you to perform a service they need. If that prior relationship _doesn't_ exist, they need something else that feels substantial. Many pitches to clients begin and end with telling the client what you can do. Some go a small step further and say how you will do it.

The problem with this from the client's perspective is that **anyone can tell them anything**. If they don't have trust already established in you, why would your claims be any more credible than those of the others who are pitching them? The best you'll get from the client by telling is the benefit of the doubt. This is where showing over telling can provide a powerful advantage. If you show what you can do, you leave no room for doubt.

I'll give the example of my recent client. Showing them what could be done was the biggest factor that contributed to me beating out the other freelancers pitching their services. I found this client on reddit. He is from New York. He had never met me or heard of me before I pitched him. After his single project posting, he was pitched by about 20 members of reddit, myself included.

He already had mock-ups of the pages he wanted built. He needed the mock-ups converted to an HTML template he could use for his live site. The first item I showed him was the result of a similar mock-up I had taken to HTML. This made it easy for him to see I could do the same for him.

[thrive_leads id='1378']

The next bit was a little trickier. He wanted to know that I could implement a specific technology in his templates (jQuery Mobile, in case you're wondering). I had no experience with this technology in particular, but I was confident I could do it because I have used many similar technologies. I asked him which elements of this, in particular, he was interested in, and he sent me a list.

For my final round of show-instead-of-tell, I decided to quickly build up a page and implement one of these elements just to prove to him I could do it. This took 5-10 minutes, but this set me apart from most of the other freelancers. I had been up-front with him in saying I had no experience with the tech, but, unlike others who may have been _very_ experienced with it, he now had an example of **an actual working implementation**.

It was practically nothing: a jQuery Mobile header on a blank web page that took, as I mentioned before, about five minutes to implement. Despite all that, it's still more concrete, more tangible than any big talk, claims, or promises I could have given. I share it here to show how little it may take to show your client that you are the right person for their project.

If you think about this from the client's perspective, a **live working example** of what you want to build is far more powerful even than claims of years of experience. Those years of experience may still have produced garbage in the end, but it's hard to deny the value of a working example.

I can't prove that this demonstration is what got me the job. All I can say is that, after seeing it, my client wrote back asking for details about my contract. At that point, we were about two more messages away from a signed contract. It seemed that, by showing the client my skills, I was able to overcome many disadvantages I had. It only took a few minutes to instill the confidence needed to convert my _prospective_ client into a real one.

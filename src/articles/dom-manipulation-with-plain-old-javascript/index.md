---
title: 'DOM Manipulation with Plain Old Javascript'
date: '2020-11-06'
tags:
  - 'dom'
  - 'javascript'
coverImage: 'images/dom-manipulation-1-thumb.jpg'
---

One of the most useful things you'll be doing with Javascript is manipulating the DOM. The DOM is the document object model. It's a model of your HTML document in Javascript that allows you to make changes to your web page as users are using your page.

DOM manipulation is what makes it possible to build web sites that work like applications. When you're using a web site that changes in response to your inputs (clicks, keypresses, etc.) without having to reload a new page each time, that's happening through DOM manipulation.

Front-end frameworks exist to help you build web apps that rely heavily on DOM manipulation, but they're not necessary if you just need to do some small changes in response to a couple of different user interactions. If that describes what you're building, do your users a favor and skip the framework. Manipulate the DOM yourself in plain old Javascript.

I've created a short video series to get you started. In this first video, I'll teach you to select the element you want to interact with by querying the DOM, how to change CSS classes on an element, and how to style an element.

https://youtu.be/suWO7Y27_R8

<!-- While you're here, grab my Javascript By Example cheatsheet. Stop Googling every concept you need to recall in order to learn faster. This cheatsheet works as a quick reference for the basics of Javascript!

[thrive_leads id='3815'] -->

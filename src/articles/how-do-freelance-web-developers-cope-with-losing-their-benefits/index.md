---
title: 'How do freelance web developers cope with losing their benefits?'
date: '2019-04-26'
tags:
  - 'freelancing'
  - 'naming'
  - 'rates'
coverImage: 'images/medical-exam.jpg'
description: 'Health benefits are expensive here in the US. As a freelancer, youʼre goig to be buying your own benefits. So, how do you do it? It all comes down to one thing.'
---

![Doctor taking someone's blood pressure](images/medical-exam.jpg)

As an employee, your benefits like health insurance and life insurance are taken care of, so what happens when you go freelance and lose those benefits?

This question has a big fat assumption rolled into it that we need to address right off the bat. It is that, as an employee, you get **free benefits** like health insurance, life insurance, and free money in your 401k. The actual truth is that **none of this is free**.

Your employer is paying for it and, when they look at how much it costs to employ you, these costs get rolled into a number called "**total compensation**." It's the number they care about because it's how much money they have to spend to employ you.

In general, they don't really care how that number is allocated. (I know this because I once negotiated to get my employers contribution to my healthcare costs as cash instead because I didn't like the health plans they offered.) What that means is that, if they weren't paying for those benefits, **they could pay you that compensation in cash instead**. They might refuse to do so, but they _could_.

<!-- _If you like this post, you'll probably enjoy my upcoming book Freelance Web Development Pricing. Check out a free chapter via the button below._

[thrive_2step id='2487'] Get Your Free Chapter📕! [/thrive_2step] -->

All that put together means that you're really paying for your benefits. It may *seem* like you don't because the money never hits your bank account, but you are. This makes the question about how to cope as a freelancer much easier. The simple answer is to…

## Charge More

It all comes down to your rate. Your rate as a freelancer is going to be **much higher than a typical hourly rate for a permanent employee** to account for 3 things:

1. It's almost impossible to bill 40 hours per week of development because **you also have to sell, market, do bookkeeping, and wear many other hats**.
2. **You have to buy your own benefits.** In addition to the health insurance, life insurance, and retirement we've already mentioned, some employers pay for transit passes, gym memberships, books and courses, and even meals. You need to charge enough to replace the benefits you value.
3. **You have to pay self-employment taxes.** Your employer pays a portion of your taxes. When _you're_ the employer, _you_ pay that portion in addition to your other tax obligations.

**Your clients should expect you to be more expensive on an hourly basis than an employee.** If they have employees, they can easily see why you would be since they're not paying your benefits or any portion of your taxes. If they _still_ want to pay you like they would an employee, **don't work with them**. By hiring a freelancer, they get flexibility by not having to pay your salary, benefits, and taxes. If there's no work for you, they don't have to pay where an employee would still collect a salary. **That flexibility is what your premium rate buys them.**

Make sure you're providing a solution valuable enough to **justify what you need to charge**. If you find a solution you can provide, but you can only reasonably charge $40/hour to build it and you need to clear $60, **that isn't a viable solution to sustain your business**. Look for something you can offer that's more valuable to your ideal client.

A good rule of thumb to help you figure out a reasonable (for you but not necessarily for them; see the note on providing a valuable solution) hourly rate is to **take what you would expect to earn yearly in a full-time position and divide by 1,000**. If I expect to earn $100k, my hourly rate would be $100 to maintain a similar standard of living. You can do more work to get a more exact number, but this simple calculation will get you pretty close.

Set a reasonable freelance rate, and you won't feel the pinch of having to pay your own benefits. You'll get the added perk of being able to pick and choose exactly **the benefits that make the most sense for you**!

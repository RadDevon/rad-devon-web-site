---
title: 'The Basics of Binary (and Other Number Systems) for Web Developers'
date: '2018-03-23'
tags:
  - 'binary'
  - 'hexadecimal'
  - 'number-systems'
  - 'tips'
description: 'Learn how understanding binary helped me understand a web development problem and why you might want to understand it too. If I can convince you, Iʼll also teach you how it works!'
coverImage: 'images/binary.jpg'
---

## Why Web Developers Should Care

Most of us know on some level that computers operate in binary, but we generally don't have to care. All of that gets abstracted away by the time you get to the higher level programming languages of web development like Javascript.

There _are_ occasions when an understanding of other numbering systems and how they work is useful. When I wrote about [why you can't pass `parseInt` directly into the array's `map` method](/articles/cant-use-parseint-map-javascript/), we learned that the `parseInt` method takes a second argument: the radix or **base** of the number being parsed. Understanding numbering systems was the key to fully understanding why this doesn't work.

Here's another case where it's useful knowledge for web developers. You have probably expressed numbers as hexadecimal values in your CSS. When you use `#000000` for black, `#ffffff` for white, or `#fb79a4` for pink, you're representing a color as a hexadecimal number. You may think of this as a magical secret code that somehow translates into colors. It's actually much simpler when you understand that hexadecimal numbering is base-16 (and when you understand how base-16 numbering works).

To get started in understanding other number systems, let's first analyze the one we're most familiar with: **decimal numbering**.

## Decimal Number System (Base-10)

Decimal is the number system you’re most familiar with. That’s a base-10 system. (Base-10 because there are 10 possible values in the numbering system, those being 0-9.)

But you can represent bigger values than just 0-9 with decimal numbering, so how does that work? By combining digits together back-to-back and assigning each place a different value, you can represent larger numbers. Here are the place values of a 5-digit decimal number:

<table><tbody><tr><td>10<sup>4</sup></td><td>10<sup>3</sup></td><td>10<sup>2</sup></td><td>10<sup>1</sup></td><td>10<sup>0</sup></td></tr></tbody></table>

or

<table><tbody><tr><td>10,000</td><td>1,000</td><td>100</td><td>10</td><td>1</td></tr></tbody></table>

To get the value represented by a 5-digit number, you multiply the number by its place value. Then add all the results together. If we had the number 53,367, the places break down like this:

| 10,000 | 1,000 | 100 | 10  | 1   |
| ------ | ----- | --- | --- | --- |
| 5      | 3     | 3   | 6   | 7   |

If we multiply those out, it looks like this:

5 × 10,000 3 × 1,000 3 × 100 6 × 10 7 × 1

That gets us right back to 53,367. This exercise is not very useful for decimal numbers since we've already learned to think of numeric values as decimal numbers. Since other number systems use the same rules, knowing how this process works can be useful to convert number systems we _aren't_ accustomed into the decimal numbers we understand.

## Binary (Base-2)

Binary is base-2 because there are two possible values for each digit: 0 or 1. Since this is base-2 numbering, each place value is a power of 2 instead of a power of 10. Here are the place values for a 5-digit binary number:

<table><tbody><tr><td>2<sup>4</sup></td><td>2<sup>3</sup></td><td>2<sup>2</sup></td><td>2<sup>1</sup></td><td>2<sup>0</sup></td></tr></tbody></table>

or

<table><tbody><tr><td>16</td><td>8</td><td>4</td><td>2</td><td>1</td></tr></tbody></table>

Now, let's break down an example binary number: 11011

| 16  | 8   | 4   | 2   | 1   |
| --- | --- | --- | --- | --- |
| 1   | 1   | 0   | 1   | 1   |

1 × 16 1 × 8 0 × 4 1 × 2 1 × 1

Multiply those out and add them up to find the value of the binary number 11011 is 27 in decimal. You'll notice that, the lower the base, the more digits required to represent larger values.

## Hexadecimal (Base-16)

Hexadecimal numbers have one of 16 possible values for each digit. You might wonder how that's possible since we only have 10 numbers to work with. Once we run out of numbers, we simply start using letters to represent numeric values. `0-9` still take their decimal values. If we have a digit with a value of 10, that is represented as an `a`. `b` is 11, `c` is 12 and so on up to `f` which is 15. That gives us 16 possible values in each place: `0-9` and `a-f`. Here are the place values for a 6-digit hexadecimal number (since the example we're going to try will be 6 digits):

<table><tbody><tr><td>16<sup>5</sup></td><td>16<sup>4</sup></td><td>16<sup>3</sup></td><td>16<sup>2</sup></td><td>16<sup>1</sup></td><td>16<sup>0</sup></td></tr></tbody></table>

or

<table><tbody><tr><td>1,048,576</td><td>65,536</td><td>4,096</td><td>256</td><td>16</td><td>1</td></tr></tbody></table>

The place values have really exploded in base-16! Let's try converting the example of our hexadecimal value for pink from earlier. Maybe, while we're at it, we can understand _why_ that value represents pink: fb79a4.

| 1,048,576 | 65,536 | 4,096 | 256 | 16  | 1   |
| --------- | ------ | ----- | --- | --- | --- |
| f         | b      | 7     | 9   | a   | 4   |

15 × 1,048,576 11 × 65,536 7 × 4,096 9 × 256 10 × 16 4 × 1

That gives us a decimal value of 16,480,676. Hmm. That doesn't get us any closer to understanding why this represents pink. However, I know that hexadecimal color values in some way represent the amounts of red, green, and blue in a color. If we divide the hexadecimal number into three parts — one for each color — the answer becomes clearer.

| Red | Green | Blue |
| --- | ----- | ---- |
| fb  | 79    | a4   |

If we convert each of those to decimal, we can see that they represent how much of each color is mixed to make the resulting color.

| Red | Green | Blue |
| --- | ----- | ---- |
| 251 | 121   | 164  |

If we want to tweak the color to make it a bit more purple, we can increase the last two hex digits to increase the amount of blue in it. If we increase the blue portion from `a4` (164 in decimal) to `ff` (255 in decimal), we go from a pink color to a much more vibrant purpley-pink.

## Other Number Systems

Now that you understand number systems, you can represent a value in any number system you want. Just remember that each place's value is a power of the base and you'll be able to easily convert your numbers from any other base to decimal (base-10).

As I said at the top, this isn't absolutely critical for a web developer to know, but it does come up from time-to-time. Someday you may find yourself with a problem that understanding binary helps you solve. Even if you _never_ find yourself in that place, at least you'll know how to quickly tweak your hexadecimal colors. 😜

[thrive_leads id='1366']

_If you're just beginning as a web developer and binary is a new concept, you might also not be familiar with regular expressions. This is a really powerful tool for finding patterns in strings. Check out my post on [validating user input with regular expressions](/articles/validate-user-input-javascript-regular-expressions/) in Javascript to learn how to use them!_

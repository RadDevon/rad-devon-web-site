---
title: "What to Do When a Vim Keymap Doesn't Work"
date: '2015-03-16'
tags:
  - 'debugging'
  - 'editors'
  - 'keymaps'
  - 'troubleshooting'
  - 'vim'
coverImage: 'images/vim-lawrencium-20150319212341.png'
description: 'Vim is highly customizable, but those customizations can be hard to debug. Hereʼs an approach to debugging keymaps that donʼt do what you expect them to.'
---

I just started learning Vim, and I'm maybe a bit too enamored with the level of customization I can do. I installed a great Mercurial plugin called [Lawrencium](https://github.com/ludovicchabant/vim-lawrencium/) (which is excellent, by the way). I loved everything about it… except that I couldn't get Ctrl-S to commit when viewing the repo status.

![Vim Lawrencium](images/vim-lawrencium-20150319212341.png)

The key is [plainly mapped in the plugin](https://github.com/ludovicchabant/vim-lawrencium/blob/ae96cc657e31c1e26230df6fed9e698985576390/plugin/lawrencium.vim#L1034). After I hunted down the mapping and figured out the command it should be running, I found I could run the command manually, but I wasn't sure where to go next to find out why it didn't work. This led me to StackOverflow where I asked [how to debug a faulty keymap](http://stackoverflow.com/questions/29033976/how-do-i-debug-a-non-functioning-keymap-in-vim). Here's what I learned.

### Make Sure the Key Is Mapped to the Proper Command

Using `:map` will show you your current key mappings. Add the key you want to check to see only that mapping. In my case, the command was `:map <C-S>` which produced this output:

```bash
v         @:Hgstatuscommit
n         @:Hgstatuscommit
```

The `v` and `n` tell me that the key is mapped in visual and normal modes respectively. The last bit is the actual command. I'm not entirely clear what the `*@` means, but the rest is the `:Hgstatuscommit` command followed by a carriage return.

If the mapping happened to be to something I _didn't_ expect, I could have gone a step further and used `:verbose` to have Vim tell me where the keys were mapped. That command looks like this: `:verbose map <C-S>`

Its output shows me it was mapped in the Lawrencium plugin.

```bash
v         @:Hgstatuscommit
        Last set from ~/.spf13-vim-3/.vim/bundle/vim-lawrencium/plugin/lawrencium.vim
n         @:Hgstatuscommit
        Last set from ~/.spf13-vim-3/.vim/bundle/vim-lawrencium/plugin/lawrencium.vim
```

### Try to Run the Command Manually

If you can't run the command in the Vim command line (summoned with `:`), then the command itself is likely the problem rather than the keymap (although, if you're really unlikely, you might be having both problems at once). If that's the case, find where it's declared inside the plugin and use the same sorts of techniques you would use to debug any code you write: put in a few `echo`s to see how far into the command you're getting and to be sure you're getting the values you expect.

### Assume the Shortcut Isn't Getting to Vim

This was the final outcome in my case. The shortcut key Control-S was being intercepted by my terminal and was never getting through to Vim. The Vim Wikia had [a helpful solution](http://vim.wikia.com/wiki/Map_Ctrl-S_to_save_current_or_new_files) that fixed the problem. As soon as I added the fix to my .zshrc, the command began working like a charm.

[thrive_leads id='1366']

_Vim is a fun editor to use and fast when you're proficient with it, but it's not for everyone. If you're a new developer, you already have a ton to learn. You don't need to toss an editor with a steep learning curve onto the pile. Check out [Visual Studio Code](/articles/best-ide-beginning-web-developer/) instead!_

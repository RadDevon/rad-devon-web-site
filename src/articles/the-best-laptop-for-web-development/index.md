---
title: 'The best laptop for web development (Updated for 2022)'
date: '2018-05-18'
tags:
  - 'computers'
  - 'laptops'
  - 'macos'
  - 'notebooks'
  - 'productivity'
coverImage: 'images/2021-macbook-pro.jpg'
description: 'Your laptop is the single tool youʼll use most often as a web developer. Might as well make it a good one. Check out my recommendation along with my rationale and some second opinions from readers.'
---

## TL;DR

If you can swing it, buy a 16" MacBook Pro with an M1 Pro or Max. If you need to go cheaper, look for the same thing refurbished or used. If you need to go cheaper still, look at the 13" M1 MacBook Pro, new, refurb, or used. To go even cheaper, look at an older 16" Intel Mac. If you can't stomach the idea of a Mac, just get what will make you excited to work.

## The Details

If you have an unlimited budget for a computer as a web developer in 2022, you should buy [a 16" 2021 MacBook Pro](https://www.apple.com/macbook-pro-14-and-16/). It's fast, quiet, power efficient, and runs pretty much anything you'll need to run to build whatever you, your clients, or your employer can imagine. If you don't have the budget for that system, keep reading. I'll share a few alternatives.

I'm just about a week into ownership of my 2021 MacBook Pro at the time of writing this, but it has reduced friction dramatically. I'm much more able to just think about solving problems in the software I'm _writing_ rather than in the software and hardware I'm _running_. When choosing a computer as a developer, that's the best you can ask for: a computer that gets out of your way and lets you do your work.

![My 2021 MacBook Pro as I'm updating this very article you're reading… also watching an Earthbound let's play in picture-in-picture](images/2021-macbook-pro.jpg)

## Why should you trust me?

I've been a professional web developer for over 8 years now. Before that, I worked in IT managing thousands of computers for a public school district, including Windows and macOS-based machines. I've been primarily a Windows user for most of my life until the last decade or so, and I currently run systems on macOS, Windows, and Linux at home.

## Why a Mac?

All of my past recommendations have been for MacBook Pros for several reasons. First, the macOS operating system is the perfect OS for web developers. It supports most of the proprietary applications web developers often need access to that you can't get on Linux like the Adobe stuff. At the same time, it's built on top of a Unix-based operating system, so you can do any of the Unix stuff developers need to do, and 99.9% of the time, it will just work on your Mac without any tinkering. This is not the case on Windows where even the new [Windows Subsystem for Linux](https://docs.microsoft.com/en-us/windows/wsl/about), which brings the best Linux interoperability Windows users have ever seen, still has its share of quirks. (For example, here's [a 2000-word article on getting Docker working perfectly in WSL](https://nickjanetakis.com/blog/setting-up-docker-for-windows-and-wsl-to-work-flawlessly).)

Since macOS is built on top of Unix, most directions on how to get development tools working in Linux will just work. Copy/paste commands into your terminal straight out of documentation, and you can use the same tools Linux nerds use. In the event you're brand new to all this, it's important because most of the internet runs on Unix, so many of the tools to build the internet are also built on Unix. It's especially a boon when you're learning because you already have so many mountains to climb. The last thing you want is your OS making things harder. Linux is harder because things don't always work out of the box and require tinkering. Windows doesn't work because Unix is bolted on top. macOS is a happy medium.

Besides that, the build quality of Macs is outstanding. They're not perfect – nothing is – but they hold up longer than other computers, and that's reflected in their resale value. To put it in concrete terms, I traded my 2018 MacBook Pro in on this one for $1,280. I probably could have sold it for even more if I wanted to hassle with that. There's virtually nothing in the Windows PC space that will resell for $1,280 after 3.5 years of use.

## Which monitor?

I continue to recommend the largest monitor you can get. On these latest models, that is the **16”** monitor. More room to work with = more productivity. Crank up the resolution to give you the most workspace possible while still being comfortable to look at.

## How much RAM?

The thing I like least about the MacBook Pro is the fact that it's not upgradeable. That led me to go way overboard and outfit this system with 64GB of RAM. I don't think you strictly _need_ this much RAM. I would probably set the baseline at **16GB**, with **32GB** being a nice sweet spot if you can swing it. I waffled quite a bit about whether I wanted to lay out the cash for this upgrade and ended up deciding to at the last possible second.

The architecture of these new systems allows for faster communication between components and the RAM. Then you have faster SSDs which means that, if your RAM is full, data can be written to the SSD and later read back without the same massive performance penalties you'll see with slower SSDs.

## SSD?

The SSD is less critical than the RAM because there are ways to add storage to your computer after the fact. Nothing you can add is going to touch the built-in SSD, but at least you can have extra space if you need it.

I still recommend you get the biggest SSD you can afford, not because web stuff takes up a ton of space, but because **you want to spend time developing web sites and applications and billing clients, not curating the contents of your hard drive**. 512GB is pretty good. 1TB is better. 2TB+ is best, although the upgrade cost once you pass that 2TB threshold gets pretty wacky.

## GPU?

As a web developer, you're not likely to be limited by the GPU. I went with the 24-core GPU because it allows for the upgrade to 64GB of RAM and doubles the memory bandwidth. At the sweet spot, you'd be fine with the **16-core GPU**.

## Answering the Most Common Advice

This sounds great, but even _used_ MacBook Pros are pretty expensive. (See my point at the top about Macs holding their resale value.) That’s what makes the following often-parroted advice really tempting to follow:

### “You don’t need incredible specs for most web development.”

This is true, but incredible specs buy you **the ability to not have to think about resources**. You won’t have to close down applications because your computer has come to a grinding halt. You won’t have to delete old projects or your photos or music to make room for a new project. You won’t have to close Slack so you can rebuild a Vagrant machine, install dependencies for a Node project, or run several Docker containers. **This lets you spend your time and brain power on things that make value for other people which, in turn, makes value for you.**

Telling a web developer "web development doesn't have crazy requirements, so buy a cheap machine" is akin to telling a carpenter that you can knock a nail into some wood with a solid rock, so find one of those instead of buying an expensive hammer. Maybe this turns out OK if you're only ever going to drive a single nail, but, if you want to do this every day and make a living from it, you should invest in the best tools you can afford.

Let's look at a few scenarios that could lead you to make a different choice.

## What if…

### I can’t afford/find a Mac

It's not easy to fork over for the top-of-the-line MacBook Pro. It's my top pick, but there are perfectly suitable alternatives. Here's my ranking of web development workstations if I happened to find myself limited by resources or availability:

1. [Refurbished](https://www.apple.com/shop/refurbished/mac/macbook-pro) M1 Pro/Max MacBook Pro- These are not available at the time of this writing, but you may start seeing them [sometime in January](https://www.refurb.me/blog/when-will-the-new-macbook-pro-be-refurbished). Refurbished Macs get the same warranty as new.
2. Used M1 Pro/Max MacBook Pro- Again, probably going to be tough to find for a while still.
3. Refurbished M1 13" MacBook Pro- These are live on Apple's refurbished store right now for just over $1k. I don't love the smaller screen, but the M1 has been such a boon for me, I think I'd pick that over an Intel Mac with a larger screen.
4. Refurbished 16" Intel MacBook Pro- These had the improved keyboard over the older 15" Intel MacBook Pros (like my previous 2018 model). It's a solid system if you can't get an M1.

I realize these are all still Macs, but they are more affordable options over my recommendations. If you can't get these either, here's my ranking of non-Mac options:

1. A solid Linux laptop- I'd either do some research to find a Windows laptop with hardware that is entirely compatible with a popular Linux distro, or I'd find a quality laptop that ships with Linux installed. Problem with the latter is that it probably won't be cheaper than one of the options above.
2. A **reliable** Windows laptop- Reliable is the key word here. There are tons of cheap Windows laptops out there, and you usually get what you pay for. In many cases, that's not much. I don't want to be worried about my computer breaking down while I'm trying to do good work. That's doubly true if I'm trying to _learn_ to do good work.

I'd love to recommend something more specific in this area, but I just haven't been invested in these ecosystems for a long time and couldn't make a great personal recommendation.

### I’ve never used a Mac

I hadn’t either until about 11 years ago. Actually, that’s not _entirely_ true. I _had_ used them in high school art class but not very much.

It took a couple of weeks to get comfortable enough with the differences of the operating system to be able to work effectively in it. After six months, I had “mastered” using the OS to nearly the same level of proficiency I had on Windows. **It’s an investment, both in time and money, which will pay off with less friction developing and greater productivity.**

If you still can’t stomach the thought, or if you don’t have time to spin up on a new OS right now, go with whatever makes you most comfortable.

## Second Opinions

I polled some of my followers to see what they like for their daily drivers. I got a couple of replies, both recommending Tuxedo Linux-based notebooks. They look nice, but I can't personally vouch for them. These folks can though!

### Julian

The only issue with the new MacBook Pro that's just come out is that the 14 inch version is just a bit too small and the cheapest 16 inch option is 2400 pounds in the UK. One other thing I would say is they have a heavy reliance on China for manufacturing and build and these days I feel like globally we should try to reduce dependence on China.

What I found in my research for a new laptop is this company based in Germany that builds Linux based laptops. I've linked to the model I like below. Granted, the parts are no doubt still manufactured in and around China, but at least the computers are put together and built in Europe. I know that the spec isn't quite as good with a 3k display, but it does cost almost 1000 less. Plus, I'm planning on connecting an external display, which can be 4k.

[Tuxedo Polaris 15 Gen 3](https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/15-16-inch/TUXEDO-Polaris-15-Gen3.tuxedo)

## Nicolas

I'm fan of Linux, and my dream laptop is a [Tuxedo Infinity Book 14](https://www.tuxedocomputers.com/en/Linux-Hardware/Linux-Notebooks/10-14-inch/TUXEDO-InfinityBook-Pro-14-Gen6.tuxedo). I love everything about this machine. The battery life is awesome: 14hours. The weight is also a strong point: less than 1kg. And the best: I can have my own keyboard layout (I'm a dvorak user).

Bonus track, I can have my own logo in the case of the laptop. No more companies' logos! Just my own logo!

For all these things I love this laptop!

## Others

I got short notes from others using a wide variety of different laptops. The takeaway for me is this: The real best laptop for web development is **the one you already have** or **the one you're comfortable with**. That doesn't mean you shouldn't try to do your research and invest in a great laptop if you are ready to buy a new computer, but don't feel like you _have_ to buy a new computer right now just because you want to be a developer.

## Other Questions You Might Have

Here are questions other people have when they're trying to start a career in web development:

- [Which language should I learn to become a web developer?](/articles/which-web-development-language/)
- [How do I know if I'm ready for my first web development job?](/articles/know-im-ready-first-web-development-job/)
- [How do I get my first freelance gig?](/articles/get-first-freelance-web-development-job/)
- [Should I build my web development portfolio from scratch?](/articles/build-portfolio-site-from-scratch/)
- [Am I too old to become a web developer?](/articles/am-i-too-old-to-become-a-web-developer/)
- [I've gone down this road before and ended up quitting. How can I hack my brain to keep going when learning web development gets tough?](/articles/stay-motivated-learning-web-development/)
- [Do I need a degree to become a web developer?](https://raddevon.com/articles/do-i-need-a-degree-to-become-a-web-developer/)
- [How do I get a web development job without experience?](https://raddevon.com/articles/get-a-web-development-job-without-experience/)

Have a question that isn't covered? Share your email in the form below. Not only will you get new articles as I post them, but you'll have a direct line to me to ask about your specific situation any time. Just hit "reply!"

[thrive_leads id='1354']

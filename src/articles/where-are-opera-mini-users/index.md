---
title: 'Where Are Opera Mini Users?'
date: '2014-01-19'
tags:
  - 'browser-support'
  - 'browsers'
  - 'demographics'
  - 'opera-mini'
coverImage: 'images/StatCounter-browser-ww-monthly-201311-201401-map-20140119192000.jpg'
description: 'At the time of writing this article, Opera Mini was a massive force in the mobile browser market. This was a problem because it broke with many web standards. Read up to find where those users were .'
---

_This article is really old and probably won't be relevant to you anymore._

I love icon fonts. You can imagine how I may have felt over the past couple of weeks as stories began hitting the web about the fact that wide swaths of the Internet [would not even see my icon fonts](http://filamentgroup.com/lab/bulletproof_icon_fonts/). Much of this is due to the fact that Opera Mini, a browser which apparently ditches several beloved modern web standards has a massive userbase — 261 million according to Opera themselves. (I used to link to the Opera Mini page where the company made this claim, but that no longer exists!)

This is difficult to believe for most web developers. Have you ever run into _anyone_ who uses Opera Mini? I haven't. This is entirely anecdotal, of course, but statistics like this tend to manifest as at least _one_ person I might come across in my daily life using the popular thing. That's not the case here. Here's why.

The magic of Opera Mini — and the reason it has such a large userbase — is that it uses compression to help squeeze more browsing into restrictive data plans. Opera claims the browser can reduce the size of pages by up to 80%. In this context, it doesn't make sense to download fonts. These are typically only for aesthetics and don't really have a place in Opera's plan to deliver the most web possible with the smallest footprint.

This all breaks when it comes to icon fonts. Since these use non-standard characters, the content of the page changes when they are not loaded. If we end up looking at some text displayed in a system font other than Gotham, we'll still be able to read and understand it. If we look at glyphs from an icon font that never loaded, this is not the case. Instead, we see some character which the browser displays to mean, "I don't know what this thing is." That's helpful to no one.

Let's get back to why every third person you meet on the street doesn't have Opera Mini on their mobile device despite its userbase. It's because this technique is most useful in countries where mobile data is prohibitively expensive. Opera Mini has incredible marketshare in many African countries, for instance, where they offer data plans as low as 10MB per month. Larger data plans are available, but most users are likely unable to afford these plans.

![A map showing the most used browser by country. Opera Mini is on top in most of Africa and parts of Asia.](images/StatCounter-browser-ww-monthly-201311-201401-map-20140119192000.jpg)

As of last year, a 1GB mobile data plan from Vodacom, the largest cellular service provider in Africa, was the equivalent of $28 US per month. In Sudan, where Opera Mini has one of its largest footholds at 56% of the mobile market, the minimum wage is 425 Sudanese Pounds per month or about $75 per month. Likely, these workers are not the ones who will purchase any sort of data plan, but this information can calibrate our expectations of even the relatively well-off in Sudan. A $28 data plan is a luxury few can afford.

[thrive_leads id='1366']

In this context, the browser makes sense despite its faults. It has strong marketshare in Africa, the Middle East, Eastern Europe, India, and Russia, among others. For these users, Opera Mini allows them to experience web sites with varying degrees of their intended functionality through their mobile devices where they otherwise wouldn't be able to do so.

My first reaction when these issues and their workarounds began to circulate online was, "Why are so many people using this crippled browser?" Now, it makes sense, but it also brought to light another consideration: Are the users of this browser in my site's audience? 261 million is a massive number — enough to give any developer pause — but what if it's simply the case we don't need to talk to those users?

The point is not to encourage you to hang on to your favorite development techniques even if it means ignoring reality, but, instead, I advocate that we avoid reflexively changing out workflows simply because an extremely large number is being thrown around. How many sites need to be able to reach every individual in the world with any sort of Internet access? I'd venture a guess very few.

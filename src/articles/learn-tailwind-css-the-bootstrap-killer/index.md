---
title: 'Learn Tailwind CSS, the Bootstrap Killer ☠️'
date: '2020-09-25'
tags:
  - 'css'
  - 'tailwind-css'
coverImage: 'images/tailwind-css-thumb.jpg'
---

If you're interested in front-end web development, this UI framework should be on your radar. The creators of Tailwind CSS call it a "utility first" framework and it's a major departure from the dominant UI frameworks of the last decade. (Think Bootstrap.)

Join me to learn the basics of the framework. We'll use it to build a simple UI component and explore how to extract components so they are easy to reuse. Using this technique, you can easily build your own custom UI framework on top of Tailwind. It's a web developer superpower that you want. Trust me on this.

## Things You'll Need

- [Node](https://nodejs.org/en/)
- A text editor. You can get [Visual Studio code](https://code.visualstudio.com/) for free. I talked about [why I recommend it for new developers](https://raddevon.com/articles/best-ide-beginning-web-developer/).
- I use the terminal on my Mac a fair bit. If you're on Mac or Linux, you're all set. You can use the terminal emulator bundled with your OS. If you're on Windows, you can get by with the Windows command line or Powershell, but I'd recommend Windows 10 users [install the Linux Subsystem](https://docs.microsoft.com/en-us/windows/wsl/install-win10) so you can use a real Unix terminal emulator.

https://youtu.be/yjznFulX1FM

Finally, if you want to try to include the app menu button (the grid-looking thing in the top right) using FontAwesome to get kinda close, [the "th" icon](https://fontawesome.com/icons/th?style=solid) is the closest one I found. Feel free to use anything else that you prefer, whether that's a different FontAwesome icon or something else entirely.

## Resources

- 🔧 [Tailwind](https://tailwindcss.com/)
- 🔧 [iTerm](https://iterm2.com/)
- 🔧 [Emmet for VS Code](https://code.visualstudio.com/docs/editor/emmet) (In the video, I said this was an extension, but it's actually built into the editor now.)
- 🔧 [rem length units](https://www.sitepoint.com/understanding-and-using-rem-units-in-css/)

If you have any questions, please leave them [on the YouTube video](https://youtu.be/yjznFulX1FM), or feel free to send them to me over [on Twitter](http://twitter.com/raddevon).

<!-- Looking for a project to try out Tailwind on? I've got a project starter kit that will get you up and running in no time. Just toss out the included CSS, add Tailwind, and start building your UI! 👇

[thrive_leads id='3587'] -->

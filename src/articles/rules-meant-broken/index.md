---
title: 'Rules Are Meant to Be Broken'
date: '2013-04-08'
tags:
  - 'css'
  - 'horizontal-rules'
  - 'html'
  - 'pseudo-elements'
  - 'styling'
coverImage: 'images/broken-line.jpg'
---

Last month, I enrolled in the [Thinkful](http://www.thinkful.com/) Front-End Web Development course. Our second project was to duplicate the design of [37signals](http://37signals.com/) as best we could after studying up on the fundamentals of HTML and CSS. I went into this perhaps a bit too enthusiastic about a new CSS concept I had recently learned: the [pseudo element](http://docs.webplatform.org/wiki/css/selectors/pseudo-elements).

My first real problem came when I got to the image rule with the three little stars. (Sometimes, I suck at words.) Take a look:

I wanted to use an hr since that seemed to be the most semantically correct element for the job. Dropping an image in there would have been trivial, but I wanted a challenge. I decided the best way to style this was to hide the rule and create a pseudo-element after it with the image as a background. Surprisingly, it worked.

All was well until I found I would need more traditional rules for the grid at the bottom of the page.

At this point, my `hr` CSS selector was no longer specific enough to meet my needs. I would need to devise a better selector for the job. All the rules except those in the grid were direct children of `div.centered`. My new selector was `.centered > hr`.

I still don't quite understand what happened next, but this broke my pseudo-element. I've since learned that `hr` doesn't produce any content so it doesn't make an `:after` element. Why it ever worked to style the `hr` with `:after`, I don't know.

All this trouble left me wishing I had an easy way to discover which elements supported pseudo-elements... so I built it. It's certainly not pretty, but it works. The aptly-named "[Which elements support `:after?`](http://raddevon.github.io/which-elements-support-after/)" ([repo](https://github.com/raddevon/which-elements-support-after)) project was born.

[thrive_leads id='1366']

The implementation is pretty simple. I have an HTML5 page with most of the elements included. I then have a stylesheet that applies `:after` pseudo-elements to every element.

```css
:after {
  content: attr(data-element) ' supports the :after pseudo-element.';
  color: green;
  text-decoration: none;
  display: block;
}
```

I also used jQuery to take the tag name of each element and add it to an attribute (which is then used in the content of the `:after` element) to make it easier to see just by looking at the page which elements have support.

```javascript
$(document).ready(function () {
  $('').each(function () {
    $(this).attr('data-element', $(this).prop('tagName'));
  });
});
```

And with nothing more than that, you have a somewhat ugly list of elements supporting pseudo-elements.

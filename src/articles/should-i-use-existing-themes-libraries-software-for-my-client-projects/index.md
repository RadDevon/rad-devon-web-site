---
title: 'Should I use existing themes, libraries, and software for my client projects?'
date: '2019-10-18'
tags:
  - 'opensource'
coverImage: 'images/new-construction.jpg'
description: 'Now that youʼre a developer, that means you have to build everything from scratch, right? Well, not exactly. Hereʼs how you should think about it.'
---

![A new house under construction](images/new-construction.jpg)

Now that you're a developer, you really need to **start building everything from scratch**. If you use open-source libraries or buy themes or off-the-shelf software to build a client's project, you're introducing all of that software's bugs and security holes into your project.

Besides that, you'll either have to make sacrifices to work around the missing features of the software you integrate or you'll have to modify the software to work exactly the way you want. If the feature you need is trivial, that's not so bad, but, if it goes against the grain of how the software is already designed, **you'll be swimming upstream** to write your changes.

Wait a second… **no, that's not right at all!** Some of these problems are real, but they don't constitute a reason to build every project from scratch with 100% original code. Of course you should integrate other software into your client's projects. Even if you don't feel like a "real developer" unless you build it all from scratch.

## Am I a Real Developer if I Don't Build Everything?

New developers especially have this belief that stems from [imposter syndrome](/articles/5-tools-to-help-web-developers-overcome-imposter-syndrome/). It says that, if you don't build everything from scratch yourself, **you'll eventually be outed as a "fake developer"** who just cobbles together solutions.

One of the most important skills of a developer is to be selective about applying your skill to built it from scratch. Sure, you could spend 100 hours (or 500 or 1,000) building a custom CMS for your blog, but how much better would that really be than reaching for WordPress or an existing static-site generator? **Can you justify all that time** to your client, your employer, or, if you're working on a personal project, yourself? These are the hard questions that a "real developer" asks.

## What's the Harm?

The real issue with rebuilding software that already exists is the **opportunity cost**. What could you be doing with that time if you _weren't_ rebuilding WordPress? You could be building something more unique for your site that gives you an edge rather than building something that won't even reach parity with software that already exists. That's a poor use of your time.

## But mine will be better/more secure/whatever

Maybe you're right, but in a lot of cases, you're not. Every project looks beautiful and pristine before you've started it. I like what [React](https://raddevon.com/articles/why-should-i-care-about-react/) gives me, but I wish it was easier to do two-way data binding. I'll just build my own! I'm sure it will turn out perfect!

This is where you pump the brakes. If you decide to build your own front-end framework, you'll very quickly start coming to inflection points where you need to **make compromises**… just like the developers of React did when they made the decisions _you_ don't like. These things are hard to see before the project starts and can quickly take it off the rails.

Approach these situations with **a healthy dose of humility**. If I'm looking at something like React, what are the chances that I could build something from the ground up in a reasonable amount of time that would end up making better decisions around architecture and security than those of the entire Facebook engineering team plus hundreds of other contributors. Unless you have a large team behind you, the chances are slim. I'm much more likely to solve my problem and create 10 bigger ones to replace it.

## Lean on Whatever Tools You Can Find

Writing everything from scratch is rarely practical. The responsible thing to do — especially if you're a freelancer of working for a small company — is to lean on tools you have access to. These could be libraries like React, software like WordPress, or even themes that will help get you closer to your desired solution without the investment of building from the ground up.

Apply your valuable engineering time judiciously to build the best solutions you can while also being practical. Put together pieces to get you the 80% solution and use your programming skills to close the remaining gap.

<!-- You probably have some other questions as you get started in your web development career. I'm here to help you find the answers. Sign up for a free mentoring session! 👇

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

---
title: "How do I know if I'm ready for my first web development job?"
date: '2018-02-09'
tags:
  - 'career'
  - 'freelancing'
description: 'Unpacking this questions reveals a few different fears that drives it. Iʼll outline those, test the assumptions theyʼre based on, and answer them.'
coverImage: 'images/baby-computer.jpg'
---

This is a common question among aspiring developers. **How much do I need to learn before I'm ready to go get a job?** Bundled up in this seemingly simple question are some fears and an assumption about starting a career in web development. Let's unpack them and address each individually.![Web developer running out of time](images/when-am-i-ready.jpg)

## Fear: I don't want to get caught in a web of continually learning for Learning's Sake.

You are right to be afraid of this. If you decide you want to become a developer, you'll probably start learning **HTML** and **CSS**. Then, you'll move on to Javascript. Once you have a good grasp of vanilla **Javascript**, you should probably learn one of the popular frameworks like **Vue**, **React**, or **Angular**. Then, maybe you should come to grips with a CSS pre-processor like **Sass**. You'll also need to learn a module bundler like **Webpack** so you can package up your code for production.

Now, you're reasonably well-equipped for front-end, but what about back-end development? You'll need to learn **Node.js** and **Express**. You'll want to know **Docker** so you can develop on the same environment you run in production. On the other hand, maybe you just skip all that and learn to build **serverless** back-ends…

This rabbit hole doesn't end. The number of technologies you could learn today is almost limitless, and new ones are being developed all the time. **If you allow yourself, you can be a perpetual student of web development.**

That's not why you're here, though. You didn't set out to learn web development just for the sake of doing it; **you have [a goal](/articles/stay-motivated-learning-web-development/) you want to accomplish**. Take a lean approach and learn as little as you can to accomplish it. Once you're there, let your _new_ goals drive what you learn going forward.

It's easier said than done. You still need to know where to draw the line. Never fear. Read on to find out **exactly what you need to learn** to get started.

## Fear: I don't want to waste time looking for a job when I don't have the right skills.

This is another totally valid concern. **If you throw yourself into a search for a permanent position before you have sufficient skills, you're going to run into a wall of frustration as you contend with rejection after rejection.** Here are my recommendations about what skills you should have before you start looking for a permanent position.

- **HTML**\- This one should go without saying, and it's the easiest to obtain. You should have a firm grasp of HTML. You don't have to have every element memorized, but you should be able to build most pages without much referencing of outside materials. No big deal if you still need to look up attributes or form controls.
- **CSS**\- You should know the basic selectors ([type](https://developer.mozilla.org/en-US/docs/Web/CSS/Type_selectors), [class](https://developer.mozilla.org/en-US/docs/Web/CSS/Class_selectors), [ID](https://developer.mozilla.org/en-US/docs/Web/CSS/ID_selectors), [universal](https://developer.mozilla.org/en-US/docs/Web/CSS/Universal_selectors), and [attribute](https://developer.mozilla.org/en-US/docs/Web/CSS/Attribute_selectors)) and how to use them. You'll want to at least be aware of the combinators ([descendent](https://developer.mozilla.org/en-US/docs/Web/CSS/Descendant_selectors), [child](https://developer.mozilla.org/en-US/docs/Web/CSS/Child_selectors), [general sibling](https://developer.mozilla.org/en-US/docs/Web/CSS/General_sibling_selectors), and [adjacent sibling](https://developer.mozilla.org/en-US/docs/Web/CSS/Adjacent_sibling_selectors)) — enough to know when you need one and to be able to search to see how to use it. Know about [pseudo-classes](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes) and [pseudo-elements](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-elements) and when to use them. Be able to use most comment properties without referencing. Know [the various units of measurement](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Values_and_units) available for setting sizes and which you can use with with properties. Know which properties have [shorthand properties](https://developer.mozilla.org/en-US/docs/Web/CSS/Shorthand_properties). Understand [browser prefixes](https://www.thoughtco.com/css-vendor-prefixes-3466867) and when you need to use them.
- **Javascript**\- The Javascript concepts you should know are so numerous I can't name them all here, but I'll touch on a few. Make sure you're at least familiar with [the available data types](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures) and that you know the most common ones. Know how to interact with the [DOM](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model). Know how to [loop](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration), [manage control flow, and handle errors](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling). Understand how to use [OOP in Javascript](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Objects). Know how to implement third-party libraries and interact with third-party web APIs. Perhaps most importantly if you're hoping to work with a team, **know how to eschew clever code in favor of readable code**.

These recommendations come with several caveats.

- You _can_ find jobs that don't require all of these skills. In general, the more skills you have, the more money you can earn.
- Jobs that require fewer skills pay less and are harder to get because the pool of potential candidates is larger. The more skills you have, the fewer people you'll compete with.
- If you specialize in one area, you may not need to have as much knowledge in the other areas.
- If you look for **contract work instead of permanent positions**, the whole equation changes. This brings me to my next point…

## Assumption: Getting a full-time job is the easiest/best/only way to start a career in web development.

Most people asking this question want to know how to go to work in a full-time permanent position. For most people, this is the _only_ mode of working they know. Freelancing is scary for a multitude of reasons like health insurance (if you're in the US), having to sell all the time, and dealing with droughts.

Despite all that, **it's a great way to get started in your web development career**. Since contract jobs generally have tighter scopes than permanent positions, you immediately lower the bar for yourself and allow yourself to start working for pay sooner. You can also get paid while you learn to work with a team and while you build out your skills. **Instead of trying to guess what to learn next**, you can take contracts that stretch you just a little and let this push you forward to learn. By doing this, you know you're learning skills that are worth money because you're already being paid for them.

Those are some of the benefits, but you still have to overcome the fears and uncertainty around freelancing. For those concerned about health insurance, the solution is to **charge more**. People expect to pay a higher rate for contractors because they _aren't_ paying benefits and employment taxes on them. Make sure you're charging enough to cover whatever health benefits you want to buy yourself and to pay your self-employment taxes.

Having to constantly sell yourself can be difficult and it can feel like starting over after each job. When you first start working this way, **it feels like free-falling**: you don't know what's going to happen next. After a while, though, you get used to it and you learn ways to cope. It's definitely better than applying for permanent position after permanent position and not having any success.

Droughts are similarly scary. When you freelance, you'll find yourself in times when you can't finish all the work you have while other times, you may not have a single job for several weeks. Again, this is difficult only until you get used to it. You'll learn to **start looking for work even when you're flush with it** and to **keep a buffer in your bank account** to make it through any droughts you encounter.

A permanent position isn't the _only_ way to get started in web development. Consider freelancing as a great way to continue learning, get real-world experience, and get paid while you do it. You may find you like the freelance lifestyle and want to stay with it. You may see it as a jumping-off point for a future full-time position. In either case, you're better off getting started than waiting for that perfect job opening.

Freelancing also changes that answer to the original question. If you're receptive to trying it, learn HTML and CSS based on the previous recommendations. **Start getting your feet wet in Javascript while you look for contract work you can do with your HTML and CSS skills.** If you find a job that seems like it's within or only slightly outside your current skill set, go for it. (If you're not sure how to start getting freelance work, read my post on [how to get your first freelance gig](/articles/get-first-freelance-web-development-job/).)

[thrive_leads id='1378']

As you're looking through contract jobs, **note those that are interesting but that you don't have the skills for**. You'll start to see trends that will tell you which technologies you should learn next to take on the work you want. This is much more effective and valuable than blindly learning the framework-of-the-week.

## The Answer

So, what's the _real_ answer to the question, “How do I know when I'm ready for my first web development job?” You're ready as soon as you can find a contract you can complete with the skills you have. Start with a good foundation in HTML and CSS. Try to find a job you can do with just those, and start learning Javascript at the same time. As you continue working and learning, **the pool of contracts you can take will increase** alongside your reputation as you successfully complete work. This will make you much more attractive to either future prospective clients or to prospective full-time employers.

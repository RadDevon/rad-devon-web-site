---
title: 'HackerRank Is Teaching You to Write Terrible Code'
date: '2019-09-13'
coverImage: 'images/garbage-dumpster.jpg'
description: 'You might assume the code you find on a platform like HackerRank would be written with industry best practices in mind. Well, Iʼm here to tell you that it most certainly is not.'
---

![Full dumpster illustrating what your code is like if you let HackerRank model good code for you](images/garbage-dumpster.jpg)

In case you're not familiar with HackerRank, it's a platform for practicing coding, and, increasingly, it has become a platform for companies to test their engineering candidates. This is why it's a problem that they're encouraging people who practice with them or interview using their platform **to write bad code**.

```javascript
function Rectangle(a, b) {
}
```

This is the starting point of a function they ask you to write for their 10 Days of Javascript practice problem set. Creating a rectangle object is [day 4](https://www.hackerrank.com/challenges/js10-objects/problem) of those 10 days. Most coders will take this starting point as it is and fill in the space between the brackets. That's probably what HackerRank _expects_ you to do with their problems: leave the starting point they've given you as-is and fill in what's missing.

If this is all they ever gave you, you could probably work out that `a` is one side of the rectangle and `b` is the other side. If you read the problem description, they'll tell you explicitly that `a` is the length of the rectangle and `b` is the width.

## But Why All the Secrecy?

![Batman](images/batman.jpg)

Batman needs to keep his identity secret. He spends his evenings breaking the law. Even though he's doing it for good, he could still be arrested. In order to stay on the streets delivering vigilante justice, he can't let anyone know that he is actually Bruce Wayne.

But, as far as I can tell, `a` and `b` don't _need_ to keep their true identities (the length and width of the rectangle) hidden. In fact, **all they're ever going to do in this function is be the length and width of the rectangle**. That means they don't even need to ever be `a` and `b`.

The names length and width are not reserved by Javascript. So, why not just call a spade a spade? Why can't the length be `length` and the width be `width`?

## Yeah, but What's the Harm?

In this HackerRank exercise, you're going to write a few lines of code at most. Not to mention the only two logical parameters to create a rectangle are its length and width. You're probably not going to permanently lose track of what `a` and `b` are. Even so, there's still going to be **a split second where you don't know what `a` and `b` are**.

This might even make you feel cool. Like you're part of a fun club with a secret code. Muggles look at `a` and `b` and throw their hands into the air, but you… **you can decipher it in a fraction of a second**. It feels like a superpower. And if HackerRank does it like this, **it must be right right way**… right? 😰

In your career, you'll never write an application that's this short and that does so little. Imagine the split second it takes to remember what `a` and `b` are in the `Rectangle` function. Now put that in a context where the meaning of the parameters are _not_ so clear. Maybe you have a function that displays an alert on your page. It takes three parameters: `a`, `b`, and `c`. Any idea what those parameters are? Yeah, me neither.

Now take this new naming issue and blow it up to an application with 10,000 lines of code. Each function has parameters with cryptic single-letter names. Since you learned this is OK, your variables are probably not thoughtfully named either. **You're going to spend most of the time you work on this application just trying to figure out what it does.** I pity anyone else who has to pick this app up and work on it. I hope they have plenty of aspirin.

You might think I've cherry picked the only example or maybe the worst example of this on HackerRank. **You'd be very very wrong**. Not _every_ exercise uses this opaque variable and parameter naming, but many do and some are even worse than my first example. Here's your starting code for another indecipherable exercise:

```javascript
'use strict';
process.stdin.resume();
process.stdin.setEncoding('utf-8');
let inputString = '';
let currentLine = 0;
process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});
process.stdin.on('end', _ => {
    inputString = inputString.replace(/s*$/, '')
        .split('n')
        .map(str => str.replace(/s*$/, ''));
    main();
});
function readLine() {
    return inputString[currentLine++];
}
// Complete the minimumBribes function below.
function minimumBribes(q) {
}
function main() {
    const t = parseInt(readLine(), 10);
    for (let tItr = 0; tItr < t; tItr++) {
        const n = parseInt(readLine(), 10);
        const q = readLine().split(' ').map(qTemp => parseInt(qTemp, 10));
        minimumBribes(q);
    }
}
```

The variable naming in that main function is seriously making my head spin. `t`? `tItr`? `n`? `q`? `qTemp`? Why are these practice exercises not more readable?

<!-- _Hey there. I help people like you start their web development careers. It starts with identifying a goal. Take this free course to get started!_ 👇

[thrive_leads id='1396'] -->

## Code Is For Humans

You may think that code is for computers. **It's not.** Code is for humans. If it was exclusively for computers, we'd have no need for a high-level language like Javascript or Python.

Take this to heart as you're writing your code. Write it to be read by someone other than you. Variable, parameter, and function names should encapsulate **as much context as possible** to help the reader understand what they're reading. `a` is bad. `length` is good. `lengthInInches` is even better (if, in fact, you're expecting the measurement to be in inches). If your code changes, make sure you update your names to reflect what they now represent.

It's easy to look at HackerRank and assume they're starting you out with good code. They're a huge, well-established, and, if the number of interviews administered through them is any indicator, well-respected site. Unfortunately, **that's not the case**.

If you practice on HackerRank, do yourself a favor and **start your exercise by refactoring their terrible meaningless names to something that conveys the meaning and context needed to read the code**. When you're working on projects outside the platform, make sure their bad habits don't poison your code and give you a reputation as a developer who writes indecipherable code.

By writing your code for **the next developer coming behind you**, you make it easy not only for other developers to work _with_ you but for your clients to re-hire you and recommend you to other companies who need your help.

---
title: 'Working with HTML Form Fields in Javascript'
date: '2020-11-20'
tags:
  - 'dom'
  - 'javascript'
coverImage: 'images/dom-manipulation-3-thumb.jpg'
---

You might want to go back and watch the first two videos in my DOM manipulation series if you haven't already. This one follows from those two.

Forms and their controls have slightly different methods and properties from the other DOM elements. In this video, I show you a couple of those and demonstrate the foundational concepts of how you'd validate a user's input.

https://youtu.be/3sKsoyFAA5A

<!-- Even now after writing Javascript almost daily for quite a few years, I still need to look things up, often just to see examples of them in use. I created this cheatsheet so you can have examples of the Javascript basics at the ready. 👇

[thrive_leads id='3815'] -->

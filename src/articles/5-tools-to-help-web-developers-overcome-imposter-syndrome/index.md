---
title: '5 Tools to Help Web Developers Overcome Imposter Syndrome'
date: '2018-10-05'
tags:
  - 'career'
  - 'freelancing'
  - 'imposter-syndrome'
  - 'motivation'
coverImage: 'images/trick-or-treaters.jpg'
description: 'As a developer with several years of experience, I can say with confidence that imposter syndrome never goes away, but it does get better. Here are the tricks I use to come back from it.'
---

![Trick-or-treaters](images/trick-or-treaters.jpg)

Do you ever feel like **every day is Halloween** and you're going as a web developer?

Imposter syndrome hits new and aspiring web developers fast and hard and **at the most inconvenient times**. It will hit when:

- you just got passed over for an amazing job.
- you lost a contract because your rate was too high or someone else had more experience.
- you discover you pushed a bug to production.
- a colleague or manager berates you for not having a particular skill.
- a client refuses to pay you.

No matter how amazing you are (and, let’s face it, you’re probably _really_ amazing), **imposter syndrome will eventually rear its ugly head**. You need some tools to deal with it, to pull you back from the brink of devastation and set you back on your feet. Here are **the 5 tools that help me recover** when I feel like I’m just not good enough to be a web developer.

1. **Talk to friends and family about it.**Your friends and family want to see you succeed. When you’re feeling like you can’t do it, they’ll be the ones who will remind you how far you’ve come and what you’re capable of. I find it’s good to talk to developer friends since their estimation of your ability carries more weight than people who are outside the field.
2. **Measure your progress, not against other people, but against where you were yesterday, last month, or last year.** The most visible developers are more accomplished than we are. That doesn't mean we have nothing to offer. We can be extremely valuable without being the best or without even being in the top 10%. A middle-of-the-road web developer is still better than 99.9% of the business owners who could find ridiculous value in their services.
3. **Realize that "expert" doesn't have to mean the best.** Related to but distinct from the last one. Someone once told me that, to be an expert, you just have to be one lesson ahead. If we can do the thing someone needs to move their business forward right now, it doesn't matter that we don't know everything around that or even that we just learned to do it last week. I started this site even though I'm not an “expert” in the sense most people think of an “expert," but I have a set of experiences that lets me give you some value where you're standing right now. (I hope that's the case, anyway. 😉) That makes me the "expert” in this context and I'm able to use my "expertise" to help you move forward even though there's plenty I don't know.
4. **Recall and celebrate your past accomplishments.** Sometimes it feels like everything is falling down, and that's usually when imposter syndrome takes hold. Even though your luck may be failing you at this very moment, you've still got plenty of accomplishments to reflect on. Just a few weeks ago, work stopped coming in for me on both of the recent paid projects I've been working on. I got 0 paid work for three weeks. That's really scary, and it caused me to question if I can even do this. One way I helped myself overcome it was to think back on how, just a few years ago, I could never have dreamed of moving across the country to Seattle where I currently call home. I thought about how I got a job and negotiated an incredible salary. I thought about the fact I've had articles published on [Entrepreneur.com](https://www.entrepreneur.com/article/242406) and [CSS-Tricks](https://css-tricks.com/an-intro-to-web-app-testing-with-cypress-io/). I looked at my projects directory on my computer and remembered all the things I've built over the years. Sure, for those three weeks I wasn't doing so hot, but history tells me I will soon be back on top of things again.
5. **Teach people.** Even when you're getting started, there's someone on a lower rung of the ladder than you're on. It's incredibly gratifying to be able to help someone else. It forces you to make sure you really understand what you're teaching. Here are some places you can find people who could really use your expertise:
   - **Forums** like those at [Sitepoint](https://www.sitepoint.com/community/), [FreeCodeCamp](https://www.freecodecamp.org/forum/), [Codecademy](https://discuss.codecademy.com/latest), and [The Odin Project](https://forum.theodinproject.com/)
   - **Subreddits** like [learnprogramming](https://www.reddit.com/r/learnprogramming/), [learnjavascript](https://www.reddit.com/r/learnjavascript/), and [webdev](https://www.reddit.com/r/webdev/)
   - **Developer meetups**, particularly those that say they are newbie friendly

<!-- _Need help getting your web development career off the ground?_

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step] -->

Teaching others is another reminder of how far you've come and that, no matter how many people you see who know more than you, there are still plenty who know less. With a little effort selling, some of those people will become your clients, and **that’s proof you're not an imposter**.

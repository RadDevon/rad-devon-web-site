---
title: "HackerNews' Freelancing Thread: Is it good for new web devs?"
date: '2020-09-04'
tags:
  - 'career'
  - 'freelancing'
coverImage: 'images/hn-freelancers-thread-thumb.jpg'
---

[HackerNews](https://news.ycombinator.com/) is a reddit-like site for developers. Every month, they host a thread called ["Freelancer? Seeking Freelancer?"](https://hn.algolia.com/?dateRange=all&page=0&prefix=false&query=%22Freelancer%3F%20Seeking%20freelancer%3F%22&sort=byDate&type=story) in which users are welcome to post freelance gigs they need to find developers for _or_ their own profiles as freelancers in hopes of getting contracted by someone.

I'm looking at the thread through the lens of a new-ish web developer. Is this something you should be paying attention to? If you _do_ devote time to it, what's the most effective way to use it?

https://youtu.be/iCHM-6J7WhQ

<!-- If you're going to be spending some time on the "Freelancer? Seeking freelancer?" thread, you'll want to grab a copy of the bookmarklet I demonstrated in the video to make it easier to find the gigs in the thread without having to sift through everything else. Share your email below to get your copy! 👇

[thrive_leads id='4024'] -->

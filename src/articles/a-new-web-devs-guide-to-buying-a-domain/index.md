---
title: "A New Web Dev's Guide to Buying a Domain"
date: '2019-11-08'
tags:
  - 'dns'
  - 'domains'
coverImage: 'images/man-crying-about-email.jpg'
---

![Man crying because he still uses a Yahoo email address](images/man-crying-about-email.jpg)

The number one step you can take to legitimize yourself as a web developer is to **build web sites and web applications**. That said, the _easiest_ way you can make yourself look more legit is to register a domain name for your portfolio or for your freelancing business.

It's not hard to do, but it can be intimidating if you've never done it before. Let's work through it together.

## What to Buy

What domain you should buy depends on how you want to brand yourself. Do you want to promote your name or a business name, or would you rather promote yourself starting from the problem you solve?

Imagine you specialize in building sites for attorneys. You could register _your_ name as a domain name (This site is a modified example of that.), or you could register attorneysites.com (or something like it). You might have a slight SEO advantage with the latter, but it probably wouldn't make much difference.

I'll leave that decision to you, but you should keep these rules in mind:

- Try for a dot com if you can. Even today, people are more comfortable trusting them. This depends on the audience you're serving and isn't a hard-and-fast rule.
- Avoid clever spelling — "2" for "to," missing letters, and the like.
- Avoid articles like "the" unless it's an ingrained part of an existing brand. Even Facebook used to be "thefacebook.com" and ended up dropping "the." It's just too hard for people to remember.
- If people could get confused whether a word in your domain is plural or singular, register both variations and point both to your site.
- Avoid dashes in your domain.
- Avoid numbers in your domain — spelled out or otherwise.
- After observing the above rules, make your domain as short and memorable as you can.

One quick test that encapsulate most of the above rules: imagine yourself telling someone your URL. Does it require a long explanation? Think about the difference between telling someone to go to awesomeweb.com versus 2-websites-4-1.com. For the first one, you'd just say it. For the second, it would go something like this: "It's two websites for one dot com with the number two, the number four, and the number one, and with dashes between each of them." It's a nightmare.

Just like most other decisions you'll make, your domain decision isn't set in stone. Domains are cheap, and changing them isn't impossible. Don't let this slow you down. Register something now. If you find you made a mistake, fix it later.

## Where to Buy

The easiest place to go to get your domain is wherever you're hosting your site. In general, I tend to recommend the easiest path with the fewest barriers (because those are the paths that people will walk). Here's a rare instance where I'll break with my convention.

In the past, I've seen unscrupulous hosts use domains registered with them to make it harder for you to change hosts. Most domain registrars make it easy to change DNS settings which allows you to point your domain wherever you want it to go. This particular provider didn't make it easy to change DNS settings which made changing hosts more difficult.

By the time I had witnessed this, I fortunately already had all my domains at a registrar that gave me easy access to all the DNS settings. I _had_ to have this since I didn't have any hosting with them. I'd recommend you make sure to use a registrar that gives you full access to DNS settings so that you always have the flexibility to change hosting whenever you need to. Don't just register your domain with your host because it's easiest.

For the past 7 years or so, I've been registering all my domains with [Namecheap](http://namecheap.pxf.io/akQnN), and they've been great so far. As the name suggests, they're cheap, but they also have a nice interface, making it pretty easy to make changes when you need to. I have also worked with clients who swear by [Hover](https://hover.evyy.net/xPD9x), and my experience with it has also been smooth, although it's a little more expensive.

I try to avoid GoDaddy since they've been caught in bad business practices in the past. On an old HackerNews thread, a user reports that, after doing some research to find the best domain name for a project, they returned to one of their early candidates to find [it had been registered by GoDaddy](https://news.ycombinator.com/item?id=2326790). This practice of registering your user's domain searches before they can is called "front running."

Despite this, I have dealt with GoDaddy's support on several occasions since they're one of the most well-known registrars and clients often come to me with domains already registered on GoDaddy. The support experience has always been good, and their reps are knowledgeable and friendly.

## What to Do With Your Domain

Now that you've selected a domain name and registered it, put it to work to start making you look more credible. My favorite way to get an email address on my domain is using [G Suite](http://goo.gl/8jIe4C). In addition to the email address, it gives me a calendar, documents, and quite a few other tools to keep business stuff organized.

If you absolutely can't spend any money, Namecheap offers [email forwarding for free](https://www.namecheap.com/support/knowledgebase/article.aspx/308/2214/how-to-set-up-free-email-forwarding). You can set up as many virtual email addresses as you want and have them forward to your existing address. This is cheap (free) for a reason: you can't send emails from this address. It's just an email box that forwards messages to your actual email address. If you can squeeze it into your budget, the G Suite option or something similar would be much better.

With email squared away, you'll want to point your domain to your web site. This will vary depending on where your web site is hosted. Quality hosts should have documentation on how to configure a domain to work with their hosting. Namecheap has some general instructions covering [what you'll need to do on their end](https://www.namecheap.com/support/knowledgebase/article.aspx/9837/46/how-to-connect-a-domain-to-a-server-or-hosting).

## Who Enters Your Domain?

Running a business, particularly as a web developer, without a domain is kinda like selling organs out of your trench coat: it doesn't exactly inspire confidence. Fortunately, the cost is extremely low, and the credibility it lends you is well worth that price. It won't make your career, but it's one more tiny step toward building trust with your web development prospects.

If you're looking for other strategies to build trust with your prospects (and you should be; that's why people hire you), check out [Getting Started in Freelance Web Development Without Experience](https://raddevon.com/articles/getting-started-in-freelance-web-development-without-experience/).

<!-- If you're ready to get your freelancing business going, my week-long free course teaches you everything you need to know to get the business up and running! 👇

[thrive_leads id='2262'] -->

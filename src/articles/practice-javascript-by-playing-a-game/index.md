---
title: 'Practice Javascript by Playing a Game'
date: '2020-06-26'
tags:
  - 'tutorials'
coverImage: 'images/fcc-june-2020.png'
---

TwilioQuest is a learn to code role-playing game. I recently played some live on stream. Come for the game and code. Stay for the live viewer questions and answers!

https://youtu.be/r_ck5w1EsBA

You can't be a web developer without learning to code. Thankfully, we have fun ways to do that like this game, but you also can't be a web developer with _just_ technical skills. You need to take it a step further and **start applying those skills to solve problems** that make people money. That's why I love freelancing as a starting point for new developers. It puts that fact in sharp focus.

<!-- If you want to try freelancing, take my free crash course below 👇. It will teach you how to set up your business so that you can start taking on clients and applying what you know to earn a living!

[thrive_leads id='2262'] -->

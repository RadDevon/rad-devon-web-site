---
title: 'How to Estimate a Web Development Project'
date: '2018-10-13'
tags:
  - 'estimates'
  - 'estimation'
  - 'freelancing'
  - 'pricing'
coverImage: 'images/stonehenge.jpg'
description: 'Estimating projects is a pain in the butt. If you get it wrong, you could lose the gig or end up not getting paid a decent rate. Hereʼs a example of how I approach the problem.'
---

Properly estimating how long a web development project will take comes down to two things: being able to **break the project into its bite-size chunks** and **drawing from your experience to understand how long each chunk will take**. In this article, I'll share approaches for doing those two things as well as what to do if you _don't_ have that experience to draw on.

![Stonehenge](images/stonehenge.jpg)

## When You Need to Estimate

Estimating how long a project will take is important if your billing is based on **time**. You may think that, if you're billing by the hour (or week or month), you won't _need_ to estimate the time. This _might_ be true, but most clients will want to understand the total cost of the project.

If you're charging a **flat project price anchored to the time the project will take**, the estimate will be critical to make sure you're making at least your minimum hourly rate and hopefully taking a profit on top of that. If you estimate half the time it actually takes to complete a project, that means **your hourly rate is now half** what you originally calculated. Nothing is more demoralizing than having to work on a flat-rate project that's already well over your original estimate.

If you're pricing projects based on **value**, you still need at least a rough estimate of the effort required. This allows you to do a reality check against your pricing to make sure you're making at least your minimum hourly rate. Value-based pricing _should_ allow you to dramatically increase your effective hourly rate, but, if you try to work on a project with a low value to your client, you could end up working at a very low rate. As you gain experience, you'll gain an innate sense of **which projects you can afford to take on** and which are not valuable enough to your client to be worthwhile.

_For an in-depth discussion of pricing your projects, check out my [Ultimate Guide to Pricing Freelance Web Development](/articles/the-ultimate-guide-to-pricing-freelance-web-development/)._

## Breaking it Down

Most projects as you receive them are **too large to estimate**. Projects are collections of many discrete tasks. You have a much better chance of taking any one of those tasks and accurately estimating how long it will take than trying to estimate the project as a whole. Record those tasks individually so you can look at estimating each of them separately.

**Let's look at an example.**

### Example: Make My WordPress Blog Faster and Cheaper!

A client comes to me with a high-traffic WordPress site. The site is slow and hosting is expensive. The client wants me to help solve these problems. I pitch the idea of adapting the site to [GatsbyJS](https://www.gatsbyjs.org/), a static-site generator built on React. This would allow the client to serve the site as a static single-page making it super-fast and easy to host. The client likes the idea but asks the dreaded question, **"How much will it cost?"** 😱

To answer this, we'll first need to understand how long this will take. For the purposes of this example, we'll assume we're pricing this project **based on the time it will take to complete** since that's the scenario where estimating is most important. Let's break it up into parts.

#### Top-Level Breakdown

- Adapt the WordPress theme to GatsbyJS
- Migrate the content to the GatsbyJS site
- Make it easy for the user to add content, build, and deploy the site

Now, we've taken a single project — move a WordPress site to GatsbyJS — and we've broken it into three pieces. **These pieces are still too large to estimate**, though. How long does it take to "adapt a WordPress theme to GatsbyJS?" I don't know. How long does it take to "make it easy for the user to add content, build, and deploy the site?" Again, I don't know. That means my pieces are not small enough. Let's make another pass.

#### Breakdown of Theme Adaptation

- Adapt the WordPress theme to GatsbyJS
  - Find the fonts used in the WordPress theme
  - Migrate the global typographical styles
  - Adapt the header
  - Adapt the content area
  - Adapt the footer
  - Adapt the sidebar

In the process of thinking about how to break down adaptation of the theme, I realized there are some pages on the site that don't use the global header, content area, footer, and sidebar. These pages are built custom with a builder. I'll need to build those manually.

- Re-create custom WordPress pages

This is a pleasant side-effect of breaking down your projects. **You'll discover pieces of the project you might have otherwise overlooked.**

#### Thinking Through the Content Migration

- Migrate the content to the GatsbyJS site

As I looked at this one, I realized **I had some decisions to make**. Gatsby has plugins that allow me to pull content from many different sources. I need to decide **which source makes sense for this project**.

If I wanted, I could pull it from the existing WordPress database, but that would mean the client would still need to host the database somewhere. It would give my client the ability to create content in the WordPress dashboard which is what they're already accustomed to. I'm not sure how important that is. That's probably a good question to ask the client.

If I wanted to make a clean break with WordPress, I could use a different CMS like [Contentful](https://www.contentful.com/). That still leaves us with a sizable bill for the CMS though. Since my client is concerned about price, I don't want to go that route. In discussing this with my client, I learned that **they are comfortable with [Markdown](https://daringfireball.net/projects/markdown/)**, so I'll go with that for the content.

I did a little research and learned this decision made the current step pretty straightforward. I found [a tool that converts your WordPress posts to Markdown](https://github.com/weiran/wordpress-gatsby-migrater) and creates the appropriate directory structure for Gatsby. I should probably test it to be sure it will work, but, if it doesn't, I could write a command line script that would do the conversion for me. If I want to be totally safe, I can estimate for this instead. If the existing tool works, that's a bonus for me!

In either case, I don't think I need to break this one down any further right now.

#### Automating Builds and Deployment

- Make it easy for the user to add content, build, and deploy the site
  - Generate pages from Markdown sources

Since I've decided on Markdown, I know I need this step to allow my client to add new content. I'm not sure quite what I want to do about building and deployment though. The easiest thing to develop would be a simple command-line script that would build and deploy, but that's not going to work well for every client. Even many developers are not comfortable at the command line, let alone regular human beings. 😉 Some more research reveals [Netlify CMS](https://www.gatsbyjs.org/docs/sourcing-from-netlify-cms/). This is a fancy front-end for editing files in a Git repository. I found [a Gatsby starter](https://github.com/netlify-templates/gatsby-starter-netlify-cms) which includes everything I need to build and deploy the site after new content is added via Netlify CMS. Again, this valuable research has made this piece much more straightforward.

- Work from gatsby-starter-netlify-cms starter

#### The Final Breakdown

- Adapt the WordPress theme to GatsbyJS
  - Find the fonts used in the WordPress theme
  - Migrate the global typographical styles
  - Adapt the header
  - Adapt the content area
  - Adapt the footer
  - Adapt the sidebar
  - Re-create custom WordPress pages
- Migrate the content to the GatsbyJS site with the converter tool
- Make it easy for the user to add content, build, and deploy the site
  - Generate pages from Markdown sources
  - Work from gatsby-starter-netlify-cms starter

Now, I finally have some chunks I could hope to accurately estimate.

## Estimating the Parts

Here's where the stakes are: putting numbers to each of these tasks. You'll be drawing on one of two things to come up with numbers:

1. **Your past experience doing the same thing or something similar.** If all you have is your memory, that's better than nothing… but not much. Ideally, you have been tracking your time, and you can look back through the history to know how long this task actually took in the past.
2. **Your best guess.** It's sad but true, but science and facts will only get you so far in your estimation. Hopefully, you have some kind of basis for this. Even if you haven't done the task you're estimating, maybe you've learned to do something similar in the past and then actually done it. The time it took to do _that_ might be similar to the time it will take to do _this_.

### But I don't have any/enough/the right experience!

When you're new, you'll find yourself leaning far more heavily on guessing than on experience. **That's OK**.

**You are going to mess up.** You're going to dramatically underestimate something and reduce your hourly rate on that project to $8/hour. You may even do this two or three times (or more).

**What you're buying with all that lost income is the experience you'll need to make a better estimate next time.** You're adding a data point that will factor in to your next project estimate. Please make sure you capture it somehow so that your money doesn't go to waste.

## Things You Won't Even Think About

### Client Communication

What are the client's expectation for communication? Do they want to meet with you for an hour every day or once a week? Do they leave that up to you? Regardless, you're going to spend some time communicating with the client. Make sure you consider this time in your estimate.

### Revisions

This problem can be fixed in your contract by requiring that the terms be re-negotiated if there are extensive revisions, but you'll probably have to do at least some minor revisions on almost every project. Make sure you make allowances for this.

### Travel

Most web development jobs don't require you to travel, but some expect you to come into an office to work or just to check-in or attend meetings. This travel takes time that you should be charging for as part of the project.

[thrive_leads id='1378']

## How to Get Started

If you're new to freelancing, here's step-by-step how to get started.

1. **Break the project down as best you can.** You want parts small enough that you can estimate them, but you want to avoid spending 3-4 hours on this step breaking down everything into a fine powder.
2. **Figure out the parts you have the least clue about.** Do a little bit of research on each of those until you have a slightly better idea how long they are going to take. Again, strike a balance here. You could obviously just do the project to eliminate all the guesswork, but that's several steps too far.
3. **Take your best shot with each task.** I know you don't have much experience, but draw on what you have. If you come across a task you have **absolutely no clue** where to start with, this job may not be good for you at this point in your freelancing practice. There's no shame in telling the client the project isn't a great fit and recommending someone else. You can still be the hero by helping the client find a solution.
4. **Double all your estimates.** _At least_ double them. Web developers are extremely optimistic. You'll find your estimates which you thought were based on the "worst-case scenarios" were actually somewhere in the middle of the possible outcomes. You'll find that out by hitting the _actual_ worst-case scenario on a couple of your tasks. Doubling everything will mean you won't lose your shirt on those tasks and the extra buffer you have on the tasks which went well may allow you to still hit your overall estimate for the project.

If you have a single take-away from this article, it should be that **you will screw this up** as both a new developer and as a veteran consultant. Don't waste a ton of time trying to get it perfect.

Take an hour or two to break things down and do some research. Get as close as you can and move on. Record what actually happened and feed that info into your process next time. Eventually, you'll notice underestimating will happen **far less often** than you get close. That's really the best you can ever hope for.

---
title: 'What are my responsibilities after a freelance gig ends?'
date: '2018-10-19'
tags:
  - 'freelancing'
  - 'gifts'
coverImage: 'images/huts-and-mountains.jpg'
description: 'Is the gig done once you hand off or deploy the code, or does your responsibility extend beyond that? Thereʼs no one answer, but read this to learn about some things youʼll want to consider when answering it for your projects.'
---

You just wrapped up a web site for a client. The client signed off and made the final payment. A week later, they came back asking for a revision or a bug fix. What are your responsibilities to that client (a.k.a. when can I finally schedule that long overdue vacation 😜)?

![Huts and Mountains](images/huts-and-mountains.jpg)

## What is "the norm?"

In short, there are no norms. Before you begin any project, you should have **an agreement in writing** over what you will build for the client. Some freelancers include this in their contract document while others use a separate "statement of work." Whatever the document you use, you should be **clearly laying out what work will be done** as part of the project.

The only norm is that you as the freelance developer are expected to do everything you've agreed to and you don't have to do anything that's not there (although there may be times you'll want to; we'll go into that a little later).

## How to Set Expectations

The first step to setting expectations is to discuss them as part of your preliminary project discussions. When you feel you're getting close to closing a project, outline for your prospective client what you will do, and, maybe more importantly, what you _won't_ do.

This discussion may lead to some negotiation between you and your prospect. Maybe you don't want to provide _any_ support and maintenance after the project is delivered. **That's your call.** Having heard this, the client may ask if you would consider providing a year of support and maintenance. It's then your call whether you want to do this and what you want to get in exchange for it. Maybe you want to charge a monthly fee for ongoing support and maintenance. Maybe you want to charge a yearly fee. Maybe you do it yourself or maybe you farm it out to someone else. Maybe you just can't be bothered and decide to refuse. All these are valid responses.

Now, it's your prospect's turn. If you refuse what they've asked, they may decide they don't want to work with you. This is also fine. If you hate doing maintenance and support and you come across a client who demands this, **that's not the right client for you** (and you shouldn't be trying to capture every single client). By filtering them out early by refusing their request, you will have made both your lives better by recognizing a bad fit before it was too late. Congratulations!

## The Contract

If the prospect is still on-board after the discussion, both you and your new client need to sign the dotted line on a document that tells what you will do. This should mirror what was in your prior discussions.

Most people think contracts are only to tell what you _will_ do, but, just like you did in your discussion earlier, it's nice to also outline what you _won't_ do. If something is unsaid in your contract, clients will sometimes make an assumption about it. If you can **anticipate what kinds of things they might expect you to do and spell out in your contract whether you will or won't do those**, you can save everyone some heartache.

Here are some things clients _might_ expect:

- perpetual support
- perpetual maintenance
- updates with new "small" features
- free migration to new hosts or cloud providers

Some of these may sound unreasonable without an explicit declaration that you _will_ provide them, but some clients will assume them anyway. Some people may have slightly tempered expectations that still fall outside the boundaries of what you've agreed to provide. For instance, a client may realize that expecting perpetual support is crazy, but they may expect that you'd support them for "at least a year" just by nature of the fact that you built the software. Still not reasonable, but they may expect it.

You're not going to be able to anticipate everything your client might expect, so do your best and move on. If the client comes to you expecting something that wasn't spelled out in your contract, **politely explain to them that this is out of scope for the project**, but that you'd love to work with them on this if they need help.

If you find yourself explaining to several clients that you don't provide X because it isn't in scope, that might be a good expectation to add to your contracts for future clients, just to head off the problem before it occurs.

## Unless…

When a client comes asking for additional work, it's always up to you whether to provide it. If a client comes to you asking for something that wasn't promised and you deliver it, one of two things happen depending on that client's disposition: you'll either become an easy mark in that's client's eyes and they'll try to see how much free work they can get from you, or they'll realize you did something you didn't have to do for them and spread the story to the far reaches of the globe. Usually, the former happens, so **be very careful** not to become a doormat by giving away your services for free.

Try to judge how the client will perceive what you're doing for them based on how they've behaved with you in the past. Pair that with a guess about how long it will take to do what they're asking. Have they been respectful, grateful, and easy to work with? Are they asking for something that takes 10 minutes or 10 hours? If they're a great client and just need a few minutes of effort from you, go ahead and take care of them. You've lost some billable time, but **you've created a great customer service experience** for a client you might like to have as a repeat client in the future. They're more likely to send other prospects your way in the future too.

## Times You Should Always Do Free Follow-Up Work

I can think of one scenario in which you should **always do free work** even after the contract is up: **you goofed up**. If you delivered the web app or the web site and one of your promised features didn't work (and wasn't discovered before the final bill was paid), you should go fix it, regardless of the time it takes. Don't let the client broaden what constitutes "done," but make sure all the explicitly laid out expectations are in place.

Here's a quick scenario. You just shipped a marketing site for a client. It was supposed to have a button on the front page that popped up a modal with an email opt-in form. You presented the site and the client signed off, but it was later discovered the modal didn't work properly in Safari even though browser support for the latest version of Safari was promised.

In this case, you would go back and fix the modal so that it works in Safari. But what if the client then comes back and says they've found another issue. The emails being collected by the form are not going to their MailChimp account. This is the first you've heard about the client's need for emails to go to MailChimp, and you're simply emailing them to the client when forms are submitted instead. It's probably going to be a few hours of work to write a MailChimp integration, so this is likely where you'd want to draw the line.

Explain to the client that you didn't scope for the integration in the initial project. Let them know you'd love to help and ask if they'd like you to estimate for a new project to add the MailChimp integration. You've now made good on your original agreement, but **you've drawn a clear boundary** for the client.

[thrive_leads id='1378']

## Guidelines for Post-Project Work

### When to Always Do It

- When you didn't deliver everything laid out in the agreement
- When you delivered a feature that doesn't work as promised

### When You Probably Shouldn't Do It

- When the ask was not part of the contract and it will take significant time to execute
- When the ask was not part of the contract and the client is a jerk, regardless of the time it will take
- When something breaks after-the-fact and the client declined a maintenance contract (or you refuse to do maintenance)

### When You Get to Make the Call

- When the problem was due to something outside your control (an API was suddenly deprecated, a critical bug was found in the version of a library you used, the client decides to host with a different provider, etc.)
- When the ask wasn't discussed beforehand but you know it won't take long to do

## Value Should Flow in Both Directions

Let this be your guiding principle when you're asked to do work after a contract has ended. You're effectively being asked to do some work for free. **What are _you_ going to get out of it?**

It may sound selfish… and it _is_. That's OK. Business doesn't happen unless everyone gets something out of it. It's totally reasonable to expect something in return for the work you do. Sometimes, it's money. Sometimes, it's good will. **Value should never be flowing in only one direction.** That means your business isn't working.

Don't do the work begrudgingly. That sets a bad tone for your relationship going forward. If you make sure the value is balanced, everyone will be excited about it and everyone goes home happy. There's no harm in saying "no" to work after the project, but you don't always have to say "no" if a "yes" makes sense for everyone.

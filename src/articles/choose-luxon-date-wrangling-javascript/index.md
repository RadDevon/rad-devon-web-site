---
title: 'Why You Should Choose Luxon for Date Wrangling in Javascript'
date: '2018-01-22'
tags:
  - 'dates'
  - 'javascript'
  - 'luxon'
  - 'moment-js'
  - 'time'
  - 'tips'
description: 'Luxon is a great new option for working with dates in Javascript from one of the makers of Moment.js. Should you make the switch?'
coverImage: 'images/dates.jpg'
---

[Javascript's native Date object](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date) is severely lacking. Once you try to do much more than very basic formatting and manipulation, you'll probably start reaching for a library like the excellent [Moment.js](http://momentjs.com/).

Moment.js is much nicer than native Dates, but there's still room for improvement. Luxon is a modern Javascript date/time library built by one of the Moment.js developers to address some shortcomings in the old standby for date manipulation. It shares a lot in common with Moment.js, but I recommend you **reach for Luxon the next time you need to do heavy lifting on dates and times**. Here's why:

## Luxon objects are immutable.

Immutability means that objects cannot be modified (or mutated, hence im**muta**ble) once created. For a new developer, this might sound like a lack of flexibility. Instead, it actually makes it *easier* and more predictable to do manipulation on dates. Let's look at an example of Moment's mutability directly out of the Luxon documentation that illustrates the problem.

```javascript
var m1 = moment();
var m2 = m1.add(1, 'hours');
m1.valueOf() === m2.valueOf(); //=> true
```

The `add` method **returns the original Moment object** rather than a new object. The addition is accomplished by **changing the original object**. Since Moment objects are mutable (i.e. can be mutated), it's difficult to **create new objects by manipulating old ones**.

<!-- If you need a refresher on the Javascript basics, this is for you. 👇

[thrive_leads id='3815'] -->

Most native Javascript methods _do not_ mutate the original data. Developers come to expect this from Javascript, and it's surprising when libraries break with that convention. Here's an example of the built-in array object's `join` method. This method takes the items in an array and joins them together in a string separated by commas, returning the string.

```javascript
var days = ['Monday', 'Wednesday', 'Thursday'];
var daysString = days.join();
console.log(days); // ['Monday', 'Wednesday', 'Thursday']
console.log(daysString); // "Monday,Wednesday,Thursday"
```

Here we want to get a string of the days so we can display them to the user. We use the array's `join` method to do that. We still want to keep the original array because we might want to work with the days as an array later on in our application.

It would make our lives difficult if the `join` method converted the original array into a comma-separated string. That's exactly what happens in the Moment example above.

**UPDATE:** A reddit user [has pointed out](https://www.reddit.com/r/javascript/comments/7sa0y3/why_you_should_choose_luxon_for_date_wrangling_in/dt3y2jf/) there are plenty of array methods that mutate the array like `[push](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push)` and `[pop](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/pop)`. He's absolutely right. I'd argue, though, that the explicit purpose of these methods is to modify the array and the verbs are more indicative of that. The default mode of Javascript native object methods is still to return a value rather than to mutate.

If we want to have _two_ objects after the manipulation of the Moment object, we have to clone the object to make a second copy before we manipulate it.

```javascript
var m1 = moment();
var m2 = m1.clone().add(1, 'hours');
m1.valueOf() === m2.valueOf(); //=> false
```

It's not hard to do, but it _is_ easy to miss. Stuff like this **can cost you hours of debugging time** if you overlook it. With Luxon, this manipulation looks a lot like our previous array example. Here's the example shown in their documentation:

```javascript
var d1 = DateTime.local();
var d2 = d1.plus({ hours: 1 });
d1.valueOf() === d2.valueOf(); //=> false
```

The Luxon object's `plus` method, rather than changing the value of the original object, returns a new Luxon object with the new value. We no longer have to clone the object first since the Luxon object's methods predictably return new objects. This is consistent with Javascript's built-in data types and their methods, so it's one less thing we have to remember when we decide to use Luxon.

Next time you're working with dates, make your brain less cluttered and your life simpler by trying Luxon. Both it and Moment whip dates into shape like the Javascript Date never could, but Luxon ditches the annoying mutability gotcha. Your applications will handle dates with ease and you can work with dates just the way you'd expect!

---
title: 'I review my old Git commit messages 🤦‍♂️'
date: '2020-10-02'
tags:
  - 'commit-messages'
  - 'git'
  - 'version-control'
coverImage: 'images/git-commit-messages-thumb.jpg'
---

##### Before you start…

If you don't even know Git yet, let alone how to write good commit messages, check out these videos to learn the basics:

- [Learn Git and Github Part 1](/articles/learn-version-control-with-git-and-github-part-1/)
- [Learn Git and Github Part 2](/articles/learn-version-control-with-git-and-github-part-2/)

I dig deep into my GitHub account and evaluate some of my commit messages. **Learn from my mistakes** to start writing better commit messages. Your colleagues will love you, and you'll immediately become the life of every party (once parties are a thing people do again). 🎉

https://youtu.be/HJYY3NtQIus

The criteria I used to evaluate these come from an excellent article by Chris Beam called [_How to Write a Git Commit Message_](https://chris.beams.io/posts/git-commit/).

Here are Chris's 7 tips:

1. **Separate subject from body** with a blank line
2. Limit the subject line to **50 characters**
3. **Capitalize** the subject line
4. **Do not end the subject line with a period**
5. Use the **imperative mood** in the subject line
6. Wrap the body at **72 characters**
7. Use the body to explain **what and why** vs. how

<!-- Now that you know how to write an incredible commit message, you just need some commits to test out your new skills on.  Grab my project starter kit to get up and running on a project. Start making those commits! 👇

[thrive_leads id='3587'] -->

---
title: 'Which meetups should I attend as a web developer?'
date: '2019-03-01'
tags:
  - 'freelancing'
  - 'marketing'
  - 'meetups'
  - 'networking'
  - 'sales'
coverImage: 'images/meetup.jpg'
description: 'There are tons of meetups out there. Read this to learn which ones are the right ones to attend as a new developer. Iʼll share some great examples here in Seattle so youʼll know what to look for in your hometown.'
---

Building your network is an important part of becoming a successful web developer. If you're in a city of any size, you have for more meetup options than you have time. Let's look at the meetups that are most effective for aspiring web developers.

![People at a meetup](images/meetup.jpg)

## Newbie-Focused Meetups

When you're just starting to learn, newbie-focused meetups can help you level up much faster. They put you in a room of other people learning just like you. Some even include veterans willing to offer mentorship. You can come away from these with **people to pair program with or to compare notes with when tackling difficult concepts**.

You can still derive a lot of value from these even once you're more established as a developer. Mentoring others is a great way of giving back, but it also **helps you understand the technologies you're teaching better**. Mentors at every level are extremely valuable. It's easier to relate to people when you're just a step or two ahead of them. Teaching people who are 5-6 steps back can be a challenge.

The [Seattle Free Code Camp](https://www.meetup.com/free-code-camp-sea/) meetup is a great example of a newbie-focused meetup. I recently took over as the organizer of this group, so, if you're in the area, please come out to one of our events!

I want to help you start your web development career. Sign up for a free mentoring session, and we'll work together to figure out your next steps.

[thrive_2step id='1528'] Get a Free Mentoring Session! [/thrive_2step]

## Technology-Specific Meetups

Technology-specific meetups usually target people across skill levels, but it's great to attend them even if you're just getting started. Many of these feature **talks with some very practical takeaways** you can start applying in your projects.

Besides this, you'll form **relationships** with people who can become your mutual support network as you're learning and starting your career. You'll lean on them when you need help, and you'll lend them a hand when they get stuck.

Take inventory of the various technologies you're using. In a small-ish city, you may find meetups only as granular as the languages you're using. Look for Javascript, Python, Ruby, Java, or whatever you're into. In larger cities, you can find framework-specific meetups. Look for React, Angular, Flask, Django, and Rails meetups and check out a few for the tech you're learning.

Staying local with my examples, [Seattle JS](https://www.meetup.com/seattlejs/) is a great example of this kind of meetup.

## Business Meetups

I talk to a lot of aspiring web developers who have started going to developer meetups. This is a great way to enhance your learning and start making some valuable connections, but, if you're going _only_ to developer meetups, **you may be cheating yourself**.

Especially if you're interested in doing freelance work (and, if you're trying to break into the industry, [you probably should be](/articles/inverted-career-path/)), developer meetups won't give you everything you need. Your most valuable currency as a freelancer is trust. You need to **start building that trust by forming relationships with people who might hire you**. As a junior, you won't find many of these people at developer meetups.

Your best bet is to **narrowly define who you want to work for** and **find where those people congregate** to talk business. It's OK to start broader and try your local chamber of commerce, lead swapping groups, and general small business meetups, but you'll want to quickly go narrower.

You'll have an easier time selling if you're intimately familiar with the needs of a smaller market. Find meetups, events, and conferences specific to them and start building your networks there. The [Seattle Ed Tech Meetup](https://www.meetup.com/EdTechSeattle) is a great example for anyone wanting to build web software for clients in education.

## Finding the Right Balance

Your time is limited, and you shouldn't be devoting it all to either tech meetups or business ones. You need to find the proper balance. What that balance is can change depending on your goals and where you are in your career transition.

If you're brand new, you should probably focus more on the tech meetups since that's what you'll be selling. **Don't completely neglect the other side though**. Relationships take time to develop. Wouldn't it be nice to already have friends in the industry by the time you're ready to start taking on work?

Once you can build solutions on your own, **it's time to shift your focus to business meetups**. Start discovering the problems, building trust, and figuring out what you should build. This could be a one-off solution for a single entity, or it could be a generalized solution for the industry as a whole. You could start with a one-off solution and, assuming you retain ownership of the code in your contract, evolve it into a generalized solution you can sell to others.

Re-evaluate your needs and **tweak the balance as you go**. If you find you don't have many people to reach out to when you need help or if you just need to find camaraderie with others who do the same work, shift toward tech meetups. If you need to get more business in the pipeline, hit up the business meetups.

For most developers, the meetups that let you hang out with other developers are the most fun. You have to be constantly vigilant to **be sure you're doing what you _need_ instead of what you _want_ to do**. Do what enables your business to survive.

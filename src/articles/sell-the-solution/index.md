---
title: 'Sell Your Solution, Not Yourself'
date: '2019-06-14'
coverImage: 'images/space-megaforce.jpg'
description: 'Those of us new to selling a service (like web development) want to start by talking about ourselves. Hereʼs why that isnʼt effective and how you can sell more easily by by focusing elsewhere.'
---

One of my readers contacted me recently for feedback on their web development portfolio. It was very nice aesthetically and showed off some cool projects.

The only part that needed some love was the introductory text above the projects. "I am an aspiring web-development student. I work in JavaScript, HTML, and CSS. I love to write beautiful code and learn new skills."

## Why This Isn't Very Effective

Imagine you’re a video game collector. You stumble upon a really rare game that’s priced very low, as if the seller doesn’t really know what they have. You buy it and take it home. Awesome! 🎉

![Space Megaforce game with price stickers](images/space-megaforce.jpg) The problem is it has all these nasty price stickers left over from the store it was originally purchased at right on the front. Now, even though you got a great deal, **you have a problem**: you need to get rid of those stickers without damaging the box to realize the full value of your pick-up. You go to the hardware store and ask if they have products to remove stickers. They do, fortunately, and they show you to an aisle with three options. You decide to read what’s on the bottles to help you decide which to buy. The first bottle has a big logo at the top for “Acme” which is the company that makes the product. Aside from that logo, this is the only text you can find on the bottle:

> We at Acme are passionate about producing great products that are affordable to everyone. Our R&D department is always hard at work to build new products that people will love.

The second bottle is labeled “Adhese-Away.” It says,

> Removes all kinds of light adhesives without leaving a residue.

The third bottle is labeled “Game Cleaner.” It says

> Restore games in your collection to their full glory. Removes unsightly stickers without damaging games or their packaging.

If someone is looking to hire a web developer, that means **they have a problem they need to solve**, just like *you* have a problem with your new game — the stickers. If you give them a pitch that doesn’t say anything about the problem they’re looking to solve, they don’t have any reason to hire you, the same way *you* don’t have a good reason to buy the Acme product to remove your sticker since you're not even clear what it does. If you write something like the label on the second bottle that _does_ address the problem but is very general, you’ll have a much better chance of winning clients. It feels less like a shot in the dark and more like a potential solution.

<!-- Need help pricing your freelance web development services? Check out a sample chapter of my upcoming book! 👇

[thrive_leads id='2463'] -->

If you can get **even more specific** to your customer’s problem like the third bottle, **you’ll look like an absolute home run ⚾️** . Even though there aren’t as many people who have the problem you’re solving, the people who *do* will pay you **several times as much** as they would the generalist because it feels like you understand them and the specific problem they’re dealing with. This will probably require a little work on your part. If you’re like most new developers, you probably haven’t thought much about **who you work for** and **what problem you’re solving for them**. I would urge you to do this work so you’re selling something that your prospective clients can get really excited about. This is what will **separate you from thousands of cheaper options** and allow you to earn a comfortable living as a web developer.

---
title: "When you get rejected as a web developer, it's not personal"
date: '2020-12-18'
tags:
  - 'career'
  - 'motivation'
coverImage: 'images/its-not-personal-thumb.jpg'
---

As a web developer, you're going to get rejected a lot. You'll end up not being able to close a freelance deal. You'll make it to a third-round interview and end up getting passed over for someone else. Maybe it's just a recommendation you make that the rest of your team doesn't go for.

When you're new, it's hard not to take that personally. I did, and it led to a bunch of unnecessary pain and self-doubt. Here's a pep talk that should help keep you from making the same mistake.

https://youtu.be/Ca-1_gxZPMk

While you're here, I want to do more to help you get started as a freelance developer. I've got a free crash course that will tell you the ins and outs of starting your business. Just share your email to get that and more! 👇

[thrive_leads id='2262']

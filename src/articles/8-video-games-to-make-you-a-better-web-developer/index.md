---
title: '8 Video Games to Make You a Better Web Developer'
date: '2019-11-15'
tags:
  - 'games'
  - 'video-games'
coverImage: 'images/thumbnail.jpg'
description: 'These 8 games are a blast to play. Not only that but theyʼll each make you a better web developer while you play. Play for fun and profit!'
---

https://youtu.be/ihdPbuwbQbI

## The Games

1. [**Automachef**](https://www.automachef.com/) lets you build algorithms by piecing together complicated machines that will make delicious food.
2. [**Return of the Obra Dinn**](https://obradinn.com/) feels like debugging unfamiliar code. You get on the ship with no idea what happened and start to slowly unravel the mystery.
3. [**HackNet**](http://hacknet-os.com/) has you virtually hacking using real Unix commands. Learn the Unix terminal, and add a useful tool to your web dev arsenal.
4. [**Wilmot's Warehouse**](http://wilmotswarehouse.com/) could just as well be called Refactoring: The Game. Arrange your warehouse so you can find items when orders come in. Then, re-arrange when you realize you did it wrong.
5. [**Keep Talking and Nobody Explodes**](https://keeptalkinggame.com/) sharpens your communication skills to make you a better team member and improve your documentation.
6. [**Vim Adventures**](https://vim-adventures.com/) just teaches you to use the code editor Vim. I'll leave you to imagine how this might be useful. 😉
7. [**Kind Words**](https://store.steampowered.com/app/1070710/Kind_Words_lo_fi_chill_beats_to_write_to/) teaches you to be kind to yourself and to others. Great if you're dealing with imposter syndrome.
8. [**Hypnospace Outlaw**](http://www.hypnospace.net/) lets you relive (or experience for the first time) the wild west of the early web by taking on the role of a moderator of a community similar to Geocities.

## Transcript

Hey, this is Devon, and today I'm going to tell you about eight video games that will make you a better web developer.

There's this whole sub-genre of games that's popped up in the last few years that's basically around, creating algorithms to do various things. And the one I played most recently is called Automachef. It's a really cool game where you're sort of setting up this automated kitchen in a restaurant.

You start the level and the game gives you a food that you are going to be making and then tells you how you need to make it: the ingredients you need and the different processing you'll have to do on the ingredients to get the final dish. You take these components that each do different things - for example, you have conveyor belts that move food through your machine - and you piece them all together and build something, some sort of contraption that will make the food the game is asking you to make.

These are really just algorithms. It's the same sort of thing you do to solve problems in programming except, whereas in programming the syntax of the language you're using makes up the components of your machine, in these games, you have a more visual representation of that with actual parts that you're piecing together to create the machines you need to make to build whatever it is you're making.

There are a bunch of games like this. Automachef is not the only one or even the best one as far as I know. There is also, Infinifactory, there's Factorio, and if you need something that's hitting on almost every platform, there's one called Human Resource Machine that's a lot of fun. it's on mobile devices. So if you have an iOS or Android device, a tablet or a phone, you can play Human Resource Machine on that.

Automachef is a great game in this genre, and it is available on Steam for $15.

The first thing you're going to notice about Return of the Obra Dinn is that it has a really striking look about it. It's made to emulate really old computer graphics, and you can actually pick which computer system you want to emulate with the graphics, and it'll apply different shaders to the game. But that's not really what the game is about.

The game is about being an insurance adjuster. Fun, right? But it actually is pretty fun because what you're doing as an insurance adjuster is you're sort of investigating this mystery. This ship, the Obra Dinn, it set sail with some trade cargo, never actually arrived at its destination, and now five years later, it's drifted back ashore, completely empty.

So it's up to you as the insurance adjuster to go on the ship and investigate and find out what happened to the people who set sail five years ago. And to do that, you're given this superpowered pocket watch that lets you turn back time and watch the events when you come across someone's remains, and your job is to go through this book and fill in the name of the person that died, how they died, and where applicable, who killed them.

It reminded me a lot of debugging or coming into a legacy code base that you're not familiar with because you don't really have a frame of reference. You don't know what's going on or how, so you just kind of pick a spot that seems relevant and you start investigating from there and trying to understand how the pieces fit together, and, in the case of your code base, when you're debugging, where it's breaking, where it's not doing what it's supposed to be doing. I think that The Return of the Obra Dinn does a great job of capturing that and wraps an interesting story around it and some really cool graphics too.

Return of the Obra Dinn is $20, and you can get it on Steam, Switch, Playstation 4, or Xbox One.

HackNet is a game about hacking, and it's not the first game about hacking, but it is one of the more authentic games about hacking. What's authentic about it is that it uses actual Unix commands to do the hacking.

You're going to be using "ls" to list the contract contents of a directory. You're going to be using "rm" to delete files. You'll use "scp" to copy files across servers. If you're not comfortable with the command line, this is a great low-pressure way to get some comfort using the command line and doing things and understanding sorta how you get a sense of where you are on the computer and what you're able to do when you're using the terminal - when you're using a Unix terminal specifically.

If you're running Linux or Mac , you have access to this by default. It's a really powerful tool for developers. If you're using Windows, newer versions of Windows have the Linux subsystem that lets you access Linux inside of Windows, and so you would have access to a terminal too.  
It gives you a fun story to explore too. There is a well known hacker who's been killed and you're trying to understand what happened, and so you're working through this story and unraveling the mystery as you're actually using real terminal commands.

HackNet is available for $10 on Steam.

In Wilmot's Warehouse, you play as this little square with a face and you're actually managing a warehouse. You'll get deliveries and the deliveries are also little squares with icons on them, and you don't know exactly what the icons mean, but you're tasked with arranging the warehouse and organizing it despite that.

At certain points throughout the game, people will come in and give you orders and you'll have to go fetch the items out of your warehouse and bring them to them. And you have a time limit in which to do that. So hopefully you've organized your warehouse in a manner that's at least efficient enough that you can go and find the stuff.

But without a doubt, you haven't, and you'll discover that as you're trying to fulfill these orders. So the next time you're between orders, you're gonna want to go back and reorganize.

This is basically a game about refactoring. I do this all the time when I'm writing code, I'm, I'm actually trying to maintain code I've written months back that seemed okay at the time. Maybe I wrote something before that didn't quite work exactly the way I wanted it to, and now I want to go back and fix it. I've always got a limited amount of time and I'm left with a decision about whether it's worth it to go back and refactor the code or if I just run with it. Wilmot's Warehouse does a great job at capturing that and creating that same decision for you and giving you lots of opportunities to practice making that decision in the low stakes context of this game that's actually really fun too.

Wilmot's Warehouse is $15 on Steam and on Nintendo Switch.

In Keep Talking and Nobody Explodes, you and another player are tasked with diffusing a bomb. The catch is that one of you can only see the bomb and cannot see the bomb diffusal manual and the other player has the manual but cannot see the bomb.

The game is about communicating back and forth. The player with the bomb is describing what they see, and it's not always very straight forward. It's not always easy to describe. Some of the markings are abstract and it's just difficult to get across what you're looking at. The player with the manual is having to describe… first having to understand what bomb the person is actually looking at so that they know they're looking at the right section of the manual and then describe how to diffuse that piece of the bomb.

As a developer, you're often working on or with a team, and communication is critical. So this is good practice for that. But also even if you're not working with a team live in real time, you're hopefully at least documenting what you're building and that documentation is just an asynchronous form of communication.

What Keep Talking does for you as a web developer is it lets you practice really quickly and figure out what works and what doesn't. You can incorporate some of what you learn there into your real time communication with your team or into your documentation of your code, your software, and your libraries.

Keep Talking and Nobody Explodes is available on just about every platform you can possibly imagine. It's $10 on a mobile platforms, Android and iOS, and then it's $15 on Steam. Switch, Xbox One, and PlayStation 4, and just about any virtual reality platform you could have. If you have a virtual reality headset, it's a really fun way to play this game because it enforces that separation between the player that can see the bomb and the player that can see the manual.

VIM Adventures is the most directly applicable game on this list. It is actually teaching you how to use a code editor called Vim. Vin is a really interesting editor. It's almost game-like itself. I use it on a regular basis and it sort of makes me feel like I'm playing a game when I'm writing code, but it's not just for fun. It's also really useful. It lets you do some things that typical code editors do not. So if you think about Visual Studio Code or Sublime Text or Atom, those texts editors are always in insert mode. That means when you open one of those applications and start typing, whatever keys you press, those letters are going to start coming out in your code.

In Vim, that's not the default mode. You're not inserting by default. So when you launch Vim, each of your keys have some sort of function that affects your code. One of those functions is to change to insert mode. In insert mode, you can start typing just like any code editor.

But when you're not in insert mode, you have some really powerful features you can use. For example, you can replace words or you can do selections. You can navigate through your code in various ways. The best way I've heard it described the, the benefit of coding in this way is that you wouldn't paint a picture by taking the brush and holding it down on the canvas and keeping it down on the canvas the entire time until the painting is finished. Most of the time, you're gonna have the brush off the canvas and you're gonna be doing other things. Maybe it's thinking about where to place the brush or mixing colors together. Something along those lines, and code is really the same way.

Most of your time spent coding is not entering characters into your editor. Well, it shouldn't be at least, but most editors sort of enforce that mode of working on you because they're always in that mode by default. Vim breaks with that and gives you some other options for ways to manipulate your code.

Vim Adventures is a game that teaches you how to use Vim. The biggest downside of Vim is that it's extremely difficult to learn because there's not a good way to discover what each of the keys do in the application. So you sorta just have to memorize what they do. I don't think anybody really memorizes them all, so you just memorize the ones you use.

Vim Adventures does a great job of teaching you that by using Vim's keys to navigate through an adventure game, and it's actually a pretty fun little game.

The way you buy this one is a little bit different from all the others. Most of the games, or actually every other game on this list. You pay for it once and then you own it and you can play at any time. Them adventures. You pay $25 for a six month license and you just access it through your web browser so you don't, as long as you have access to a web browser hooked up to something that has a keyboard and you can play, but the $25 only gives you six months, so you can't, you can't go to it in three or four years and pick up and start playing again.

The good news is that. Six months is probably all you would need to clear all of the content of the game. And by the time you do that, you'll have a pretty good grasp of Vim. The game is not really compelling enough to go back to after you've finished it, and really the goal of it is to learn Vim. Once you've done that, you, you won't really need it anymore. So effectively, $25 gets you this game.

The next game on the list is a little different from most of the others. The game is called Kind Words, and it's something like a forum. It's like an internet forum wrapped up in a game.

When you launch the game, you see a little person in a room from an isometric point of view, and you see that person sitting down at a desk. There are these paper airplanes flying through the screen, and you can click on them and each one has a motivational message from a real person who's also playing the game.

You can also write your own motivational messages, and those get sent to other people's games as little paper airplanes that they can read. The other thing you do in this game is you can make and fulfill requests. You'll type a problem you're having, and other people can receive that – it's anonymous – but they see your problem and they can respond directly to your problem.

It's a more direct way to get help than the generic motivational messages that are flying through as paper airplanes. You can also look at _other_ people's requests and send messages to help them. This game is really something different and I think it's a great way, one to get your head out of code for a little bit, which is really important. You don't want to get burned out, and this is a way to sort of break out of that and also gives you some strength that you may need, especially if you're suffering maybe with imposter syndrome. That's a good opportunity for you to get in and make a request and while you're waiting for people to answer your request, jump in and look at some other people's requests and try to see what you can do to help other people feel better, to help other people move through their days and get over the problems they're having.

Kind Words is $5 and it is available on Steam.

If you've been around the internet long enough, you probably remember Geocities. It was a place where anyone could go and build a website and they would host it. They had these, I think they called them neighborhoods, and they were just basically sections of Geocities around different themes. So if you had a sports website, you would be in a particular neighborhood with other sports websites, video game websites, all shared a neighborhood, movie, websites, music websites…

It was really the first place that _I'm_ aware of that democratized access to publishing on the internet. It closed and a few years back. Some people kept archives of it, but it's just not what it was before.

If you want to get a little taste of that, there's a game I discovered called Hypnospace Outlaw that really captures the feel of Geocities. In Hypnospace Outlaw, you are essentially a moderator on the internet, over a community, very much like Geocities, and you're given these tasks and you have to go through and moderate the pretend Geocities community.

It's got a really fun, quirky sense of humor that's fun to read the pages. They all have music blaring in the background and they're just tacky and pretty gross, but it's fun to look at and they'll remind you of that time if you experienced it. If you _didn't_ experience it, it'll give you a taste of what the web was like in the late 90s and maybe into the early 2000s. All of it's wrapped in this interesting game with a fun story and lots of cool things to discover.

Hypnospace Outlaw is available on Steam and it is $20.

Those are eight games to help you become a better web developer. You probably have your own that I missed. Please [tweet them at me](https://twitter.com/raddevon)!

Thanks for watching, and I'll see you in the next video.

---
title: "Don't fall for these tricks in your Slack developer chats 😈"
date: '2020-10-23'
tags:
  - 'career'
  - 'slack'
coverImage: 'images/slack-tricks-thumb.jpg'
---

I'm not as active in my developer Slacks as I once was. I find most of the direct messages I'm getting these days are not people I know or people who found me online somehow and are curious to learn more about me. It's people who want to quickly become my "friend" so they can extract some resource I have.

I'll show you a couple of the scams I've seen lately. Well, one may not qualify as a scam, but it's still suspect. If you get contacted by someone with one of these asks, you should probably just avoid them.

https://youtu.be/7OOW-548I6E

Thanks to Tequask for [the great thumbnail image](https://commons.wikimedia.org/wiki/File:TP%27ing_In_California-02.jpg)!

When I recorded this video, I thought it would go live after Halloween. You'll hear me reference that fact a couple of times in the video. It ended up going live early, so the light Halloween theme is just in time!

[thrive_leads id='1378']

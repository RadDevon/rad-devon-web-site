---
title: "Times I've Failed in My Web Development Career"
date: '2019-05-31'
coverImage: 'images/ice-cream-drop.jpg'
description: 'Even when you have some success, itʼs not all gravy. Here are some times Iʼve failed in my web development career.'
---

I've told you many times that becoming a web developer changed my life, but it hasn't been without its rough spots. Here are the mistakes I've made and how you can avoid them.

![Ice cream dropped on the ground](images/ice-cream-drop.jpg)

## Working as a Sub-contractor

In general, sub-contracting has worked really well for me. Someone else finds the work — the part of freelancing I'm least excited about — and I write the code. It's a great arrangement… _if_ you're working through someone who is reliable.

If you're working through someone who _isn't_, the job may be held up for reasons outside your control (contracts or delays with other resources they're working with on the job). You may be interfacing with the client, so **your reputation is on the line** even though you have no way to compel the others to come through on time.

I was in a situation where I was having pre-engagement meetings with a client. They were ready to get started. I communicated that to the studio who was handling the contracts to get the gig rolling. They just didn't do that. The client was expecting me to start, but we had no contract. Effectively, there was no job yet. No communication from the studio on why or when.

It stressed me out and messed with my reputation in the mind of the client even though I never even got paid anything. It was the last time I attempted to work through this particular studio.

## Losing Focus on Relationships

In my experience, opportunity as a web developer is directly correlated to [the number and quality of your relationships](/articles/how-to-get-more-freelance-clients-through-networking/). Even though I know this, **my inner introvert sometimes wins out**, and I end up not getting out of the house for long stretches of time.

It's especially tempting when you already have the work you need. Maybe you have more freelance work than you can do or you have a full-time gig and don't need to look for other work.

These are the times you get complacent. When you let these relationships and the skills to build them atrophy, **you don't have them when you need them**.

I left a full-time gig last year. I had continued doing contract work on the side, but I wasn't actively pursuing new work or trying to make new relationships. When that full-time engagement ended, I was starting from scratch.

That's not entirely true. I had some relationships from my previous time freelancing, and a few of them happened to need help around that time. It wasn't enough though, and my network was weak. I had to start building again from the ground up… and that stuff takes time.

Now, I'm pretty much back where I was before, but I wish I had been here already when I needed the work.

## Falling in Love with a Single Path to Success

This failure comes from outside my web development career, but what I learned from it has been instrumental in getting me to where I am today.

About 10 years ago, I decided I wanted to write about video games. I took a course in pitching articles and got to work putting that into practice.

I had a pitch accepted with a very reputable publication. It was a thrilling win. I was going to be published alongside other writers I admired. I worked really hard to write the best article I could, and submitted it well ahead of the deadline.

The publication notified me a few days later they were killing my article. I tried to get some feedback, but they weren't interested in giving me any. It was a crushing blow, and I retreated to a much smaller web site I already had a relationship with. They were super nice, and I loved working with them. This gig didn't pay anything.

I wrote for them for a few months before the drain on my free time became too much. I ended up dropping the whole idea of becoming a writer.

I did at least two things wrong here. First, **I asked permission** to be a writer instead of just doing it. Second, I decided that asking permission was **the only way** to build a career. When that didn't work, I gave up.

I went into web development thinking I needed to get hired, but, when I applied for a bunch of jobs and never heard back, instead of retreating back to the security of my IT job, I just went out and started doing it. That's the key difference between my least successful career as a writer and my most successful one as a web developer.

<!-- _If you're done asking for permission, try freelancing. The Freelance Crash Course will teach you what you need to get started._ 👇

[thrive_leads id='2262'] -->

## Not Finishing a Project

Most of my projects have gone pretty well. There is at least one, though, that went off the rails.

I started working with a startup to build their MVP. We got that up and running, and they were able to use that alongside their long-term vision for the company to raise a little bit of money. Everything was great!

Around this time, another client came to me with a great job offer which I accepted. I wasn't able to devote as much time to the startup client, so I took on a slightly different role, sorta like a contract CTO without the title.

I started sourcing contractors and managing them on the projects. That went well for a while, and we were able to continue building out the app. Then, we ended up hiring some less experienced developers. I'm still not 100% sure exactly how this went wrong, but I think it was that I didn't manage them as closely as they needed, and I didn't have the time to clean up the mess when we were supposed to be done.

I did thousands of dollars of work, and it ultimately became clear we weren't going to cross the finish line with these particular developers. The company parted ways with me.

I continued sending them bills and reminders for the time they owed payment for, but the payment never came. We talked, and the company was frustrated the project wasn't completed. I could understand this, but, in my mind, I was billing them for time not finished work.

That fact worked out in their favor in many cases when they got valuable projects and features completed for little money. It didn't seem right that I would get paid a lower rate for successful projects and nothing for unsuccessful ones.

After some discussion, we compromised. They paid half the amount owed, and I wrote off the rest. Neither party got what they wanted out of the deal, but I think it was a reasonable resolution for both of us.

Although this was a painful experience, **it left me better able to spot these situations before they happen** and to communicate the risks in a project.

## One Takeaway

Each of these mistakes conveys its own individual lesson, but the overarching lesson is not to fear mistakes. **You will make mistakes**, and it's probably going to be OK. The established developers you look up to have made mistakes. The people you're negotiating with across the table have too. Don't let that fear keep you from trying.

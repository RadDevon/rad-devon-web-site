---
title: 'Learn Version Control with Git and GitHub: Part 2'
date: '2020-08-14'
tags:
  - 'git'
  - 'version-control'
coverImage: 'images/learn-git-part-2-thumb.jpg'
---

We're back to wrap up the Git-It workshop, and it's going to be awesome!

## What You'll Learn in this Video 🎓

- ⚡️ How to connect a Git remote
- ⚡️ How to push changes to a remote
- ⚡️ How to fork a project
- ⚡️ How branches work
- ⚡️ How add collaborators on your GitHub project
- ⚡️ How to make a pull request (This is how you contribute to open source projects.)
- ⚡️ How to merge and clean up old branches

https://youtu.be/QpyExoMNu7Y

## Resources 🧰

- 🔨 [The Git It workshop](https://github.com/jlord/git-it-electron)
- 🔧 [GitHub Desktop](https://desktop.github.com/)

Ask any questions you have [in the YouTube comments](https://youtu.be/wi8vQsaBqy0) or to me directly [on Twitter](http://twitter.com/raddevon). Now, get out there and start protecting your code!

Git lets you turn back the clock in your codebase to a time when things were better. A Big Goal lets you do the same thing with your aspirations to transition into a web development career.

Having a Big Goal allows you to get back to **why you're doing this**. You can revert your enthusiasm, your drive, and your willpower back to nearly what it was when you first started. Take my free email course to figure out your Big Goal. It will keep you from losing all your progress half way through.

[thrive_leads id='1360']

---
title: 'How much of a new freelance project should I already know?'
date: '2018-10-26'
tags:
  - 'freelancing'
  - 'learning'
coverImage: 'images/gromit-lays-down-track.gif'
description: 'Youʼll probably never take a freelance gig that has zero technical unkowns. The question is, how many unknowns is too many. This article gives some examples showing where I personally would draw the line so you can find your own line.'
---

![Gromit lays down track just in time as he rides over it](images/gromit-lays-down-track.gif)

It can be scary to take on work you're not sure how to do. You don't want to mislead anyone, but, at the same time, you don't want to pass on an awesome gig you could have knocked out of the park just because you haven't done **exactly that thing**. If you once built a React app to book appointments for a lawyer, does it matter that you've never built the same thing for an accountant? Does the answer change if the accountant wants a mobile app and you'll have to figure out React Native? Where do you draw the line?

Like most questions about freelancing, the answer is that there _are_ no hard-and-fast answers. You'll figure it out as you go and learn the boundaries as you make mistakes along the way. **This is OK**.

You'd want to start the way you would to [estimate a project](https://raddevon.com/articles/how-to-estimate-a-web-development-project/): break the project down and think about the individual parts. You won't take as much care as you would for an estimate, but you want to understand the **skills you'll need** to complete the project. If you're comfortable with those, you can be comfortable taking the job.

## Sample Projects

Let's look at a few sample scenarios, and I'll share the call *I* would make so you can start to understand where to draw your own boundaries.

### Callfire API automation

The client uses the [Callfire](https://www.callfire.com/) API to automate communication with its customers. They want to build an automation that will send a particular response based on the message received from their customer.

I was unaware of the Callfire service and their API until I read this posting. I've written API integrations before, so I don't think this one would be a problem. Before I make a decision, I'm going to look at the unknowns.

The biggest question mark is the API. I've worked with APIs, but each one is a little different. They can be more or less onerous depending on the design.

[Callfire's API documentation](https://developers.callfire.com/docs.html) is excellent, and everything appears to be fairly straightforward. I can't foresee any problems writing software for this API. Now that I've eliminated my biggest unknown, I'm confident I could take on this job.

### Performance Audit

This client believes their web site is too slow to load. I know a fair bit about how to optimize performance on the web, but it's not my specialty. I'm hesitant to take this as a result.

If this were part of a larger project, I think I could definitely help at least a little with performance, but, with performance as the _only_ goal of the project, it doesn't seem like a great fit for my skillset. I'll pass on this and take my skills where I'm more certain I could deliver a "win" for the client.

### Crowdfunding Site

The client is looking to build a crowdfunding site, and they'd like to have someone who has built one before. I've never built a crowdfunding site, but who cares? When you're building a house, you don't go looking for a contractor who has built **your exact house** before. You go to one who has built great houses and give them your blueprint.

As a newbie, you might be scared away by this posting, particularly since it says they want someone who has experience building a crowdfunding site. (Spoiler: they're probably not going to get that. How many people have actually built a crowdfunding site? Not very many.)

I can imagine how this site would fit together, and I know there's relatively little here I haven't done before; I just haven't combined it all in this particular configuration. I need authentication, payment processing, content management (for creating projects)… and that's kinda it. I'm confident I can do this one. The only challenge is to convince the client they don't need someone who has done this exact thing before. Waiting for their other proposals to come in may be good enough to achieve that.

### Blackjack Game on the Blockchain

This is a great example of a "hard pass" for me. I've never done a game, and I've never done blockchain development.

If I really wanted to try something like this, though, reading the posting might lead me to go **build a small prototype**. If I can build a prototype that gets me through many of the unknowns, I might go to the client with the prototype and say something like, "I've never done this before, but I found your project really interesting. I built this prototype. Is this the sort of thing you're looking for?"

That might be enough to win me the job. (I've won jobs this way in the past.) This is great, win or lose, because I've been totally forthcoming about my skill level, I've shown the client I _can_ build their project, and I've gotten some great experience in a new area.

This is the method I used to get my very first freelance job. I also used it very recently on a job where the client wanted an augmented reality mobile app — something I'd never tried before. It turned out to be much easier than I thought. That job evaporated, but I now know I could take on simple AR apps in the future.

[thrive_leads id='1378']

## Guidelines for Reaching on Projects

Reaching on projects can be really scary, especially for new freelancers. It's an important part of growing as a freelancer, so you should reach as far as you're comfortable and as long as you can still build what you're promising. Here are some guidelines to keep you from [flying too close to the sun](https://en.wikipedia.org/wiki/Icarus).

1. **Be transparent and forthcoming.** The point of reaching on projects is _not_ to sell every single client and figure out how to build it later. Instead, it's supposed to give you some room to grow, give your client a better rate than they might get with a developer with 10 years of experience, and let everyone enter into that arrangement with eyes open.
2. **Do some research into the unknowns.** Don't let your confidence get the best of you. Sure, maybe you've done a few rodeos and you're certain you can get the job done. Just take a step back and look up the parts you don't know. Maybe there's something in there that makes this job impossible or super expensive. You'd build a lot more trust with your client telling them now than after you've taken the contract.
3. **You will mess up.** That doesn't mean everyone should just go on as if nothing has happened. You still have to make good when you do overreach. Maybe that means hiring someone with more experience to clean up for you or partially refunding someone's fees. Whatever the case, do what needs to be done to make it right and move on. You'll do a better job estimating your own abilities next time.
4. **It's OK to be a little conservative on your early jobs.** You won't be comfortable reaching very far with projects until you know what it feels like. Take it easy on your first few gigs. If people want to work with you, they're likely to come back to you (or stay with you) on future projects. Those projects may be a reach for you. This is a great time to gently start reaching and getting used to the feeling. Just like freelancing, it feels like free-falling at first, but you'll eventually get used to it and come to enjoy it!

module.exports = {
  siteMetadata: {
    title: `Rad Devon`,
    description: `I help you leave your 💩 job to become a web developer`,
    siteUrl: `https://www.raddevon.com/`,
    twitterUsername: `@raddevon`,
    image: `/favicon-dark-large-512.png`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-mdx-source-name`,
    `gatsby-plugin-image`,
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `articles`,
        path: `${__dirname}/src/articles/`,
      },
    },
    {
      resolve: 'gatsby-plugin-html-attributes',
      options: {
        lang: 'en-us',
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: ['.mdx', '.md'],
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1200,
            },
          },
          {
            resolve: `gatsby-remark-static-images`,
            options: {
              imageName: (node) => `${node.name}.${node.extension}`,
            },
          },
          {
            resolve: `@trevorblades/gatsby-remark-embedder`,
          },
          {
            resolve: require.resolve(
              './plugins/gatsby-remark-wp-style-shortcodes'
            ),
            options: {
              replacers: {
                '*': (attributes, contentNodes) => {
                  // Do some stuff to generate new output
                  // Return the output
                  return '';
                },
              },
            },
          },
          {
            resolve: `gatsby-remark-smartypants`,
          },
          {
            resolve: `gatsby-remark-highlight-code`,
            options: {
              terminal: 'carbon',
              theme: 'dracula',
            },
          },
        ],
      },
    },
    {
      resolve: 'gatsby-plugin-sitemap',
      options: {
        query: `
        {
          site {
            siteMetadata {
              siteUrl
            }
          }
          allSitePage {
            nodes {
              path
            }
          }
          allFile(filter: {extension: {eq: "md"}}) {
            nodes {
              modifiedTime
              relativeDirectory
            }
          }
        }
        `,
        resolvePages: ({
          allSitePage: { nodes: allPages },
          allFile: { nodes: allMarkdownFiles },
        }) => {
          const filePageMap = allMarkdownFiles.reduce((acc, file, index) => {
            const articlePath = `/articles/${file.relativeDirectory}/`;
            acc[articlePath] = {
              modifiedDate: file.modifiedTime.split('T')[0],
            };
            if (index === 0) {
              console.log(acc[articlePath]);
            }

            return acc;
          }, {});

          return allPages.map((page) => {
            return { ...page, ...filePageMap[page.path] };
          });
        },
        serialize: ({ path, modifiedDate }) => {
          return {
            url: path,
            lastmod: modifiedDate,
          };
        },
      },
    },
  ],
};

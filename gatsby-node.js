const path = require('path');
const { createFilePath } = require('gatsby-source-filesystem');

const createPostArchivePages = ({
  posts,
  postsPerPage = 10,
  createPage,
  stub,
  archiveComponentPath,
}) => {
  const numPages = Math.ceil(posts.length / postsPerPage);
  Array.from({ length: numPages }).forEach((_, i) => {
    createPage({
      path: i === 0 ? `/${stub}` : `/${stub}/${i + 1}`,
      component: path.resolve(archiveComponentPath),
      context: {
        limit: postsPerPage,
        skip: i * postsPerPage,
        numPages,
        currentPage: i + 1,
      },
    });
  });
};

exports.createPages = async ({ graphql, actions, reporter }) => {
  const { createPage } = actions;
  const result = await graphql(
    `
      query Articles {
        allArticles: allMdx(
          sort: {
            fields: [frontmatter___date, frontmatter___title]
            order: [DESC, ASC]
          }
        ) {
          edges {
            node {
              excerpt
              slug
              frontmatter {
                coverImage {
                  childImageSharp {
                    gatsbyImageData(layout: FULL_WIDTH)
                  }
                  publicURL
                }
                title
                tags
                date
                description
              }
            }
          }
        }

        freelancingArticles: allMdx(
          filter: { frontmatter: { tags: { eq: "freelancing" } } }
          sort: {
            fields: [frontmatter___date, frontmatter___title]
            order: [DESC, ASC]
          }
        ) {
          edges {
            node {
              excerpt
              slug
              frontmatter {
                coverImage {
                  childImageSharp {
                    gatsbyImageData(layout: FULL_WIDTH)
                  }
                  publicURL
                }
                title
                tags
                date
                description
              }
            }
          }
        }
      }
    `
  );

  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`);
    return;
  }

  const allArticles = result.data.allArticles.edges;
  createPostArchivePages({
    posts: allArticles,
    createPage,
    stub: 'articles',
    archiveComponentPath: './src/components/all-articles-archive.js',
  });

  const freelancingArticles = result.data.freelancingArticles.edges;
  createPostArchivePages({
    posts: freelancingArticles,
    createPage,
    stub: 'freelancing',
    archiveComponentPath: './src/components/freelancing-articles-archive.js',
  });
};

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions;
  if (node.internal.type === `Mdx`) {
    const value = createFilePath({ node, getNode });
    createNodeField({
      name: `slug`,
      node,
      value,
    });
  }
};
